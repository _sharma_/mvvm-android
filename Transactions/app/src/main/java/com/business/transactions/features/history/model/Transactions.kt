package com.business.transactions.features.history.model

import android.os.Parcel
import android.os.Parcelable

data class Transactions(
	val amount: Double,
	val id: String?,
	val isPending: Boolean,
	val description: String?,
	val category: String?,
	val effectiveDate: String?
) : Parcelable {
    constructor(parcel: Parcel) : this(
		parcel.readDouble(),
		parcel.readString(),
		parcel.readByte() != 0.toByte(),
		parcel.readString(),
		parcel.readString(),
		parcel.readString()
	)

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeDouble(amount)
        dest.writeString(id)
        dest.writeByte(if (isPending) 1 else 0)
        dest.writeString(description)
        dest.writeString(category)
        dest.writeString(effectiveDate)
    }

    companion object CREATOR : Parcelable.Creator<Transactions> {
        override fun createFromParcel(parcel: Parcel): Transactions {
            return Transactions(parcel)
        }

        override fun newArray(size: Int): Array<Transactions?> {
            return arrayOfNulls(size)
        }
    }
}