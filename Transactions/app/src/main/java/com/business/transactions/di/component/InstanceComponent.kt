package com.business.transactions.di.component

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.business.transactions.MainActivity
import com.business.transactions.di.module.ViewModelModule
import com.business.transactions.di.scope.PerInstance
import com.business.transactions.features.detail.DetailFragment
import com.business.transactions.features.history.view.TransactionsFragment
import dagger.BindsInstance
import dagger.Component

@PerInstance
@Component(dependencies = [ApplicationComponent::class], modules = [ViewModelModule::class])
interface InstanceComponent {
    fun inject(fragment: TransactionsFragment)
    fun inject(fragment: DetailFragment)
    fun inject(activity: MainActivity)

    @Component.Builder
    interface Builder {
        fun applicationComponent(applicationComponent: ApplicationComponent): Builder

        fun viewModelModule(viewModelModule: ViewModelModule): Builder

        @BindsInstance
        fun host(host: ViewModelHost): Builder

        fun build(): InstanceComponent
    }
}

sealed class ViewModelHost {
    data class ActivityHost(val activity: FragmentActivity) : ViewModelHost()
    data class FragmentHost(val fragment: Fragment) : ViewModelHost()
}