package com.business.transactions.base

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.business.transactions.TransactionsApplication
import com.business.transactions.di.component.DaggerInstanceComponent
import com.business.transactions.di.component.InstanceComponent
import com.business.transactions.di.component.ViewModelHost
import com.business.transactions.di.module.ViewModelModule


abstract class BaseFragment : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(false)

        val injector = DaggerInstanceComponent.builder().apply {
            applicationComponent((requireContext().applicationContext as TransactionsApplication).applicationComponent)
            viewModelModule(ViewModelModule)
            host(ViewModelHost.FragmentHost(this@BaseFragment))
        }.build()
        inject(injector)
    }

    abstract fun inject(injector: InstanceComponent)

}