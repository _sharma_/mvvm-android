package com.business.transactions.di.component

import android.content.Context
import com.business.transactions.TransactionsApplication
import com.business.transactions.api.TransactionsApi
import com.business.transactions.di.module.NetworkModule
import com.business.transactions.di.module.UtilModule

import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [NetworkModule::class, UtilModule::class])
interface ApplicationComponent {
    fun inject(application: TransactionsApplication)
    fun provideTransactionsApi(): TransactionsApi

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun context(context: Context): Builder
        fun build(): ApplicationComponent
    }
}