package com.business.transactions.features.history.data

import com.business.transactions.R
import com.business.transactions.features.history.model.Transactions
import com.business.transactions.features.history.model.TransactionsResponse
import com.business.transactions.features.history.view.DataItem
import com.business.transactions.utils.ResourceProvider
import javax.inject.Inject

class TransactionsService @Inject constructor(
    private val repository: TransactionsRepository
) {
    internal suspend fun getTransactions(): TransactionsResponse {
        return repository.getTransactions()
    }

    internal fun getMappedTransactionList(transactions: List<Transactions>): MutableList<DataItem> {
        // map date against transactions
        val dateTransactionMap = transactions.groupBy { it.effectiveDate }
        var adapterList = mutableListOf<DataItem>()
        for (entry in dateTransactionMap) {
            val transactionsForADay = mutableListOf<DataItem>()
            for (item in entry.value) {
                transactionsForADay.add(DataItem.TransactionRow(item))
            }
            adapterList = (adapterList + listOf(entry.key?.let { DataItem.HeaderRow(it) }) + transactionsForADay) as MutableList<DataItem>
        }
        return adapterList
    }

    fun getFormattedDollarValue(resourceProvider: ResourceProvider, amount:Double): String {
        return resourceProvider.context.getString(R.string.dollar_value,resourceProvider.context.getString(R.string.dollar_Symbol), amount)
    }
}