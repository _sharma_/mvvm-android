package com.business.transactions.features.history.data

import androidx.lifecycle.viewModelScope
import com.business.transactions.R
import com.business.transactions.base.BaseViewModel
import com.business.transactions.features.history.model.Transactions
import com.business.transactions.features.history.model.TransactionsResponse
import com.business.transactions.features.history.view.DataItem
import com.business.transactions.utils.ResourceProvider
import com.business.transactions.utils.SingleLiveEvent
import kotlinx.coroutines.launch

sealed class TransactionsViewState {
    object Loading : TransactionsViewState()
    data class Success(val body: TransactionsResponse? = null) : TransactionsViewState()
    data class Error(val error: String?) : TransactionsViewState()
}

class TransactionsViewModel(
    private val service: TransactionsService,
    private val resourceProvider: ResourceProvider
) : BaseViewModel() {
    var viewState = SingleLiveEvent<TransactionsViewState>()
    var apiResponse: TransactionsResponse? = null

    init {
        viewState.value = TransactionsViewState.Loading
    }

    internal fun getTransactions() {
        viewModelScope.launch {
            runCatching {
                viewState.postValue(TransactionsViewState.Loading)
                service.getTransactions()
            }.onSuccess {
                viewState.postValue(TransactionsViewState.Success(it))
                apiResponse = it
            }.onFailure {
                viewState.postValue(TransactionsViewState.Error(it.message))
            }
        }
    }
    internal fun getMappedTransactionList(transactions: List<Transactions>): MutableList<DataItem> {
       return service.getMappedTransactionList(transactions)
    }

    fun getFormattedDollarValue (amount:Double): String {
        return service.getFormattedDollarValue(resourceProvider,amount)
    }
}
