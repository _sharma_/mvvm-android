package com.business.transactions.features.detail

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.business.transactions.MainActivity
import com.business.transactions.R
import com.business.transactions.base.BaseFragment
import com.business.transactions.databinding.FragmentDetailBinding
import com.business.transactions.di.component.InstanceComponent

class DetailFragment : BaseFragment() {

    private lateinit var binding: FragmentDetailBinding

    override fun inject(injector: InstanceComponent) {
        injector.inject(this)
    }

    override fun onResume() {
        super.onResume()
        (activity as MainActivity?)?.supportActionBar?.title = getString(R.string.transaction_details)
    }

    @SuppressLint("SetTextI18n")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_detail, container, false)

        if (arguments != null) {
            val transaction = DetailFragmentArgs.fromBundle(requireArguments()).transaction
            binding.title.text = "Category : " + transaction.category
            binding.description.text = "Description : " + transaction.description
            binding.status.text = "Dated : " + transaction.effectiveDate
            binding.amount.text = "Amount : $" + transaction.amount
        }
        return binding.root
    }
}