package com.business.transactions.utils

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat

class ResourceProvider( val context: Context) {
    fun getString(@StringRes resId: Int): String = context.getString(resId)

    @ColorInt
    fun getColor(@ColorRes resId: Int): Int = ContextCompat.getColor(context, resId)

    fun getDrawable(@DrawableRes resId: Int): Drawable? = ContextCompat.getDrawable(context, resId)
}