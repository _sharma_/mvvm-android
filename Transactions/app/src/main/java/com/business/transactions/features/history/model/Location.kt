package com.business.transactions.features.history.model


data class Location (

	val lat : Double,
	val lon : Double
)