package com.business.transactions.di.module


import androidx.lifecycle.ViewModelProvider
import com.business.transactions.di.component.ViewModelHost
import com.business.transactions.utils.ResourceProvider
import com.business.transactions.di.scope.PerInstance
import com.business.transactions.features.history.data.TransactionsViewModel
import dagger.Module
import dagger.Provides

@Module
object ViewModelModule {
    @Provides
    @PerInstance
    @JvmStatic
    internal fun provideViewModelProvider(host: ViewModelHost, factory: ViewModelFactory) =
        when (host) {
            is ViewModelHost.ActivityHost -> ViewModelProvider(host.activity, factory)
            is ViewModelHost.FragmentHost -> ViewModelProvider(host.fragment, factory)
        }

    @Provides
    @PerInstance
    @JvmStatic
    internal fun provideResourceProvider(host: ViewModelHost) = when (host) {
        is ViewModelHost.ActivityHost -> ResourceProvider(host.activity)
        is ViewModelHost.FragmentHost -> ResourceProvider(host.fragment.requireContext())
    }

    @Provides
    @PerInstance
    @JvmStatic
    internal fun provideTransactionsViewModel(viewModelProvider: ViewModelProvider) =
        viewModelProvider.get(TransactionsViewModel::class.java)
}