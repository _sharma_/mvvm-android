package com.business.transactions.features.history.model

data class Atms (

	val id : Int,
	val name : String,
	val location : Location,
	val address : String
)