package com.business.transactions.features.history.model

data class TransactionsResponse (
	val account : Account?,
	val transactions : List<Transactions>?,
	val atms : List<Atms>?
)