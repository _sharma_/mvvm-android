package com.business.transactions.features.history.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.business.transactions.MainActivity
import com.business.transactions.R
import com.business.transactions.TransactionsApplication
import com.business.transactions.base.BaseCoordinator
import com.business.transactions.base.BaseFragment
import com.business.transactions.databinding.FragmentTransactionsBinding
import com.business.transactions.di.component.InstanceComponent
import com.business.transactions.features.history.data.TransactionsViewModel
import com.business.transactions.features.history.data.TransactionsViewState
import com.business.transactions.features.history.model.TransactionsResponse
import kotlinx.coroutines.launch
import javax.inject.Inject


class TransactionsFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: TransactionsViewModel
    private lateinit var binding: FragmentTransactionsBinding

    override fun inject(injector: InstanceComponent) {
        injector.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_transactions, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        viewModel.coordinator =
            BaseCoordinator(
                requireActivity().application as TransactionsApplication,
                findNavController()
            )

        lifecycleScope.launch {
            if (viewModel.apiResponse == null) {
                viewModel.getTransactions()
            } else {
                populateData(viewModel.apiResponse!!)
            }
        }

        viewModel.viewState.observe(viewLifecycleOwner) {
            it?.let(::updateUI)
        }

        binding.pullToRefresh.setOnRefreshListener {
            binding.mainLayout.visibility = View.GONE
            viewModel.getTransactions()
            Toast.makeText(
                requireContext(),
                getString(R.string.fetching_data_msg),
                Toast.LENGTH_SHORT
            ).show()
        }

        binding.pullToRefresh.setColorSchemeResources(
            android.R.color.holo_blue_bright,
            android.R.color.holo_green_light,
            android.R.color.holo_orange_light,
            android.R.color.holo_red_light
        )

        return binding.root
    }

    private fun populateData(response: TransactionsResponse) {
        binding.progressBar.visibility = View.GONE
        binding.mainLayout.visibility = View.VISIBLE
        val adapter = TransactionsAdapter {
            val bundle = Bundle()
            bundle.putParcelable("transaction", it)
            viewModel.coordinator.navigateTo(
                R.id.detailFragment,
                bundle
            )
        }
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerView.adapter = adapter
        val adapterList = response.transactions.let {
            it?.let { it1 -> viewModel.getMappedTransactionList(it1) }
        }
        adapterList?.let { adapter.addHeaderAndSubmitList(it) }

        val account = response.account
        binding.availableAmount.text = account?.available?.let { viewModel.getFormattedDollarValue(it) }
        binding.balanceAmount.text = account?.balance?.let {viewModel.getFormattedDollarValue(it) }
        binding.pendingAmount.text = "$137.66" //not sure where this comes from

        binding.bsbValue.text = account?.bsb
        binding.accountValue.text = account?.accountNumber
        (activity as MainActivity).supportActionBar?.title = account?.accountName
        binding.pullToRefresh.isRefreshing = false
    }

    private fun updateUI(state: TransactionsViewState) {
        when (state) {
            is TransactionsViewState.Success -> {
                state.body?.let { populateData(it) }
            }
            is TransactionsViewState.Error -> {
                binding.pullToRefresh.isRefreshing = false
                binding.progressBar.visibility = View.GONE
                Toast.makeText(
                    requireContext(),
                    getString(R.string.error_try_again),
                    Toast.LENGTH_LONG
                ).show()
            }

            is TransactionsViewState.Loading -> {
                binding.pullToRefresh.isRefreshing = false
                binding.progressBar.visibility = View.VISIBLE
            }
        }
    }
}