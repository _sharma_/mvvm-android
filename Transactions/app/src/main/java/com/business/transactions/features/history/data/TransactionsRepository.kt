package com.business.transactions.features.history.data

import com.business.transactions.api.TransactionsApi
import com.business.transactions.features.history.model.TransactionsResponse
import javax.inject.Inject

class TransactionsRepository @Inject constructor(private var api: TransactionsApi) {
    suspend fun getTransactions(): TransactionsResponse {
        return api.getTransactions(1)
    }
}








    "icon": null,
    "subCategories": [{
    "id": 146,
    "categoryId": 22,
    "subCategoryName": "Bakers",
    "icon": null,
    "clues": "Bakers,bakers bakery,bakers bread loaves,bakers tip top,bakers retail,bakers wholesale,bakers cakes,bakers flat bread,bakers"
},
    {
        "id": 147,
        "categoryId": 22,
        "subCategoryName": "Bakers' & Pastrycooks' Supplies",
        "icon": null,
        "clues": "Bakers' & Pastrycooks' Supplies,bakers' & pastrycooks' supplies"
    },
    {
        "id": 148,
        "categoryId": 22,
        "subCategoryName": "Bakery Equipment",
        "icon": null,
        "clues": "bakery equipment suppliers,Bakery Equipment,bakery equipment pizza ovens,bakery,bakery supplies,used bakery equipment,bakery equipment,used secondhand bakery equipment sales,second hand bakery equipment"
    },
    {
        "id": 165,
        "categoryId": 22,
        "subCategoryName": "Bars",
        "icon": null,
        "clues": "wine bars,bars & pubs,bars & clubs & pubs,bars & clubs,Bars"
    },
    {
        "id": 166,
        "categoryId": 22,
        "subCategoryName": "Bars--Domestic & Commercial",
        "icon": null,
        "clues": "Bars--Domestic & Commercial,bars--domestic & commercial,bars--domestic"
    },
    {
        "id": 188,
        "categoryId": 22,
        "subCategoryName": "Beer & Wine Makers' Clubs",
        "icon": null,
        "clues": "beer taps,beer equipment,beer kegs hire,beer garden,beer making,clubs--beer,beer kegs,beer supplies,beer distributors,beer wine & spirits retail groups,beer gas,beer systems,beer delivery,beer glasses,clubs--beer & wine makers',beer brewing,beer importers,beer shop,Beer & Wine Makers' Clubs"
    },
    {
        "id": 242,
        "categoryId": 22,
        "subCategoryName": "Bottle Shop & Liquor Store",
        "icon": null,
        "clues": "bottle liquor shop,bottle shop,bottle shop delivery,24 hour bottle shop,hotels-accommodation bottle shop,motels bottle shop,bottle shop liquor,bottle shop 24 hour,Bottle Shop & Liquor Store,bottle shop alcohol"
    },
    {
        "id": 258,
        "categoryId": 22,
        "subCategoryName": "Breweries",
        "icon": null,
        "clues": "micro brewery,Breweries"
    },
    {
        "id": 316,
        "categoryId": 22,
        "subCategoryName": "Cafes",
        "icon": null,
        "clues": "cafes play area,cafes coffee,cafes breakfast,cafes italian,coffee cafes,cafes cakes,Cafes,cafes tea rooms,cafes pizzas,cafes take away"
    },
    {
        "id": 317,
        "categoryId": 22,
        "subCategoryName": "Cake & Pastry Shops",
        "icon": null,
        "clues": "cake & pastry shops pies,cake & pastry shops cupcakes,cake & pastry shops cheesecakes,cake & pastry shops cafes,cake & pastry shop,cake shop,cake & pastry shops patisserie,cake & pastry shops pastries,cake & pastry shops,cake & pastry shops cakes,cake & pastry shops weddings,Cake & Pastry Shops,cake & pastry shops christenings"
    },
    {
        "id": 318,
        "categoryId": 22,
        "subCategoryName": "Cake Decorating Supplies",
        "icon": null,
        "clues": "cake decoration supplies & equipment,Cake Decorating Supplies,cake decorating equipment,cake decorating supply,cake decorating accessories,cake decorating supplies,cake decorating shop,cake decorating products,cake decorating wholesalers"
    },
    {
        "id": 319,
        "categoryId": 22,
        "subCategoryName": "Cake Decorators & Decorating Classes",
        "icon": null,
        "clues": "cake decorating school,cake decorators &/or decorating schools,cake decorating classes,Cake Decorators & Decorating Classes,cake decorators or decorating school,cake decorators & decorating school,cake decorating courses"
    },
    {
        "id": 389,
        "categoryId": 22,
        "subCategoryName": "Catering",
        "icon": null,
        "clues": "catering--industrial & commercial,catering,spit roast catering,food catering,mobile catering,Catering,party catering"
    },
    {
        "id": 390,
        "categoryId": 22,
        "subCategoryName": "Catering & Food Consultants",
        "icon": null,
        "clues": "Catering & Food Consultants,catering--industrial & commercial,catering & food consultants,spit roast catering,catering consultants,food catering,mobile catering,party catering"
    },
    {
        "id": 391,
        "categoryId": 22,
        "subCategoryName": "Catering Equipment Hire",
        "icon": null,
        "clues": "catering equipment for hire refrigerators,hire--party equipment catering equipment,catering equipment for hire mobile kitchens,food catering,catering equipment,catering equipment & linen hire,catering equipment for hire mobile equipment,party catering,catering equipment hire,catering--industrial & commercial,catering hire equipment,catering equipment for hire cool rooms,spit roast catering,food catering equipment,mobile catering,Catering Equipment Hire,hire catering equipment,catering equipment for hire"
    },
    {
        "id": 392,
        "categoryId": 22,
        "subCategoryName": "Catering Supplies",
        "icon": null,
        "clues": "food catering,party catering,catering supplies wholesale,catering--industrial & commercial,catering equipment supplies,catering supplies food,spit roast catering,mobile catering,catering equipment,supplies &/or service commercial kitchens,Catering Supplies,catering supplies,catering equipment,supplies &/or service knives"
    },
    {
        "id": 411,
        "categoryId": 22,
        "subCategoryName": "Cheese & Cheese Products",
        "icon": null,
        "clues": "cheese shop,cheese making,cheese products,cheese distributors,cheese & cheese products,cheese wholesalers,cheese suppliers,cheese platters,cheese manufacturers,cheese,cheese cake,cheese wholesale,Cheese & Cheese Products,cheese producers,cheese importers,cheese factory"
    },
    {
        "id": 439,
        "categoryId": 22,
        "subCategoryName": "Chocolate & Cocoa",
        "icon": null,
        "clues": "chocolate gifts,chocolate manufacturers,chocolate fountains,chocolate moulds,Chocolate & Cocoa,chocolate making,chocolate wholesalers & manufacturers,chocolate bouquets,chocolate & cocoa,chocolate factory,chocolate supplies,chocolate confectionary,chocolate,chocolate shop,chocolate restaurant"
    },
    {
        "id": 476,
        "categoryId": 22,
        "subCategoryName": "Coffee Machines & Supplies",
        "icon": null,
        "clues": "commercial coffee machines,coffee machine rentals,saeco coffee machines,coffee beans,coffee equipment & supplies,coffee machines repair,coffee vending machines,coffee roasters,coffee machines,coffee supplies wholesale,coffee supplies retail,coffee cup supplies,coffee shop,Coffee Machines & Supplies,coffee cafes,coffee machines & supplies cafe-bar,coffee machines & supplies,coffee brewing equipment & supplies,coffee machine repair,coffee machines servicing,coffee machines rentals,mobile coffee"
    },
    {
        "id": 477,
        "categoryId": 22,
        "subCategoryName": "Coffee Retailers",
        "icon": null,
        "clues": "coffee supplies wholesale,coffee supplies retail,coffee shop,coffee cafes,coffee machine rentals,coffee retailers,coffee machine repair,coffee beans,Coffee Retailers,coffee roasters,coffee machines,mobile coffee"
    },
    {
        "id": 493,
        "categoryId": 22,
        "subCategoryName": "Commercial Catering",
        "icon": null,
        "clues": "catering--industrial & commercial,commercial catering equipment repair,commercial catering equipment services,commercial catering equipment,commercial catering supplies,catering commercial,commercial catering equipment supplies,catering equipment,supplies &/or service commercial kitchens,commercial catering,Commercial Catering"
    },
    {
        "id": 541,
        "categoryId": 22,
        "subCategoryName": "Confectionery & Lolly Shop",
        "icon": null,
        "clues": "Confectionery & Lolly Shop"
    },
    {
        "id": 568,
        "categoryId": 22,
        "subCategoryName": "Cordials & Syrups--Mfrs & Distributors",
        "icon": null,
        "clues": "cordials & syrups manufacturers & distributors,Cordials & Syrups--Mfrs & Distributors,cordials & syrups--mfrs & distributors"
    },
    {
        "id": 625,
        "categoryId": 22,
        "subCategoryName": "Delicatessens",
        "icon": null,
        "clues": "italian delicatessens,spanish delicatessens,delicatessens,Delicatessens,continental delicatessens,gourmet delicatessens,german delicatessens,polish delicatessens,delicatessens retail,food delicatessens,delicatessens market,retail delicatessens,greek delicatessens"
    },
    {
        "id": 646,
        "categoryId": 22,
        "subCategoryName": "Diary Wholesalers & Manufacturers",
        "icon": null,
        "clues": "diary,Diary Wholesalers & Manufacturers,diary manufacturers"
    },
    {
        "id": 687,
        "categoryId": 22,
        "subCategoryName": "Doughnuts, Equipment &/or Supplies",
        "icon": null,
        "clues": "doughnuts shop,Doughnuts,Equipment &/or Supplies,doughnuts,equipment &/or supplies,doughnuts"
    },
    {
        "id": 697,
        "categoryId": 22,
        "subCategoryName": "Drinking Water Supplies & Accessories",
        "icon": null,
        "clues": "drinking water supplies & accessories,drinking water filter,filters drinking water,water filters--drinking,drinking glasses,drinking water tanks,water drinking,drinking fountains,Drinking Water Supplies & Accessories,drinking water delivery,drinking water,drinking water supplies,drinking straws,eating drinking"
    },
    {
        "id": 715,
        "categoryId": 22,
        "subCategoryName": "Edible Nuts",
        "icon": null,
        "clues": "edible nuts,nuts--edible &/or products,nuts edible,Edible Nuts,edible nuts wholesalers"
    },
    {
        "id": 721,
        "categoryId": 22,
        "subCategoryName": "Egg Merchants",
        "icon": null,
        "clues": "egg wholesalers,egg merchants,Egg Merchants,egg cartons,egg suppliers,egg producers,egg distributors,egg farm"
    },
    {
        "id": 797,
        "categoryId": 22,
        "subCategoryName": "Essences, Flavours & Ingredients",
        "icon": null,
        "clues": "essences,bakery ingredients,flower essences,food ingredients,essences flavours,asian ingredients,cosmetic ingredients,gourmet ingredients retail,japanese ingredients,essences,flavours & ingredients,ingredients,cake ingredients,Essences,Flavours & Ingredients,natural ingredients"
    },
    {
        "id": 878,
        "categoryId": 22,
        "subCategoryName": "Fish & Seafood",
        "icon": null,
        "clues": "fish & seafood,Fish & Seafood,fish & seafood--retail fish & chips,seafood fish,fish & seafood--retail,fish & seafood - wholesale,fish & seafood--retail fresh"
    },
    {
        "id": 879,
        "categoryId": 22,
        "subCategoryName": "Fish & Seafood Production &/or Processing",
        "icon": null,
        "clues": "fish shop,seafood fish,fish & chip shops,Fish & Seafood Production &/or Processing,fish market,fish & seafood--retail fish & chips,fish & chips,fish n chips,take away fish & chips,fish & seafood production &/or processing,fishing,fish & seafood - wholesale,fish tanks,fishing charter,fish & seafood--retail,fish & seafood--retail fresh"
    },
    {
        "id": 909,
        "categoryId": 22,
        "subCategoryName": "Food &/or General Store Supplies",
        "icon": null,
        "clues": "party food supplies,Food &/or General Store Supplies,food &/or general store supplies"
    },
    {
        "id": 910,
        "categoryId": 22,
        "subCategoryName": "Food &/or General Stores",
        "icon": null,
        "clues": "food &/or general stores,health food stores retail,Food &/or General Stores,health food stores re"
    },
    {
        "id": 911,
        "categoryId": 22,
        "subCategoryName": "Food Brokers & Agents",
        "icon": null,
        "clues": "food agents,Food Brokers & Agents,food brokers"
    },
    {
        "id": 912,
        "categoryId": 22,
        "subCategoryName": "Food Delicacies",
        "icon": null,
        "clues": "food delicacies,Food Delicacies"
    },
    {
        "id": 913,
        "categoryId": 22,
        "subCategoryName": "Food Processing & Packing Machinery",
        "icon": null,
        "clues": "food processing equipment suppliers,food machinery importers,food processing,canning & packing machinery,food processing & packaging,food machinery,Food Processing & Packing Machinery,food packing,food packaging machinery,food processing equipment,food processing canning,food processing plants,food processing systems,food industry processing,food processing,food processing machinery,dog food processing"
    },
    {
        "id": 914,
        "categoryId": 22,
        "subCategoryName": "Food Product Processors & Manufacturers",
        "icon": null,
        "clues": "organic food processors,food manufacturers & processors,paper products--disposable food packaging,food products - manufacturers & processors asian finger food,Food Product Processors & Manufacturers,food products - manufacturers & processors asian gourmet food,baby food manufacturers,food & beverage manufacturers,food processors & packers,food products - manufacturers & processors italian food products,food manufacturers,manufacturers food,food products,food products - manufacturers,health food manufacturers,snack food manufacturers,food product manufacturers,importers &/or imported products food,frozen food processors,packaging materials food product packaging,frozen food manufacturers,food products manufacturers & processors,pet food processors,food manufacturers equipment,gourmet food manufacturers"
    },
    {
        "id": 915,
        "categoryId": 22,
        "subCategoryName": "Food Safety Auditors",
        "icon": null,
        "clues": "food auditors,food safety auditors,Food Safety Auditors"
    },
    {
        "id": 916,
        "categoryId": 22,
        "subCategoryName": "Food Technology Consultants",
        "icon": null,
        "clues": "Food Technology Consultants,food technology consultants,food technology"
    },
    {
        "id": 917,
        "categoryId": 22,
        "subCategoryName": "Foot & Hand Carers",
        "icon": null,
        "clues": "Foot & Hand Carers,foot & hand carers,hand & foot carers"
    },
    {
        "id": 937,
        "categoryId": 22,
        "subCategoryName": "Frozen Food Wholesalers & Manufacturers",
        "icon": null,
        "clues": "frozen foods - wholesalers & manufacturers,frozen food retail,frozen food suppliers,frozen food transport,frozen food retailers,Frozen Food Wholesalers & Manufacturers,frozen food courier service,frozen foods delivery,frozen food products,frozen foods,frozen food distributors,frozen food warehouse,frozen food wholesale,frozen food processors,frozen food manufacturers,frozen foods - wholesalers,frozen food outlets,frozen food wholesalers,frozen wholesalers"
    },
    {
        "id": 938,
        "categoryId": 22,
        "subCategoryName": "Frozen Foods--Retail",
        "icon": null,
        "clues": "frozen foods--retail,Frozen Foods--Retail"
    },
    {
        "id": 939,
        "categoryId": 22,
        "subCategoryName": "Fruit & Veg Wholesaler",
        "icon": null,
        "clues": "fruit & vegetable wholesalers,Fruit & Veg Wholesaler"
    },
    {
        "id": 940,
        "categoryId": 22,
        "subCategoryName": "Fruit & Vegetable Packing &/or Packs",
        "icon": null,
        "clues": "fruit & vegetable packing &/or packs,Fruit & Vegetable Packing &/or Packs,fruit & vegetable packing & packs"
    },
    {
        "id": 941,
        "categoryId": 22,
        "subCategoryName": "Fruit &/or Berry Growers",
        "icon": null,
        "clues": "kiwi fruit growers,Fruit &/or Berry Growers,stone fruit growers,fruit &/or berry growers,fruit growers,fruit & berry growers,citrus fruit growers,fruit & vegetable growers"
    },
    {
        "id": 942,
        "categoryId": 22,
        "subCategoryName": "Fruit Juice Merchants & Processors",
        "icon": null,
        "clues": "fruit juice processors,fruit juice merchants,fruit juice merchants & processors,Fruit Juice Merchants & Processors"
    },
    {
        "id": 943,
        "categoryId": 22,
        "subCategoryName": "Fruit Shops & Greengrocers",
        "icon": null,
        "clues": "greengrocers & fruit sellers,Fruit Shops & Greengrocers"
    },
    {
        "id": 944,
        "categoryId": 22,
        "subCategoryName": "Fruit, Vegetable & Grain Exporters",
        "icon": null,
        "clues": "export auto spares used,export of products,export packaging,export pallets,export consultants,Fruit,Vegetable & Grain Exporters,meat exporters,export shipping,export auto spares,export used car batteries,export freight,export international,fruit,vegetable & grain exporters,export frozen chicken wings"
    },
    {
        "id": 945,
        "categoryId": 22,
        "subCategoryName": "Fruits, Nuts & Berries (Nurseries-Retail)",
        "icon": null,
        "clues": "Fruits,Nuts & Berries (Nurseries-Retail),nuts--edible &/or products,frozen berries"
    },
    {
        "id": 1067,
        "categoryId": 22,
        "subCategoryName": "Halal Products",
        "icon": null,
        "clues": "halal restaurant,halal pizza,halal chicken,halal products,Halal Products,halal butcher,halal catering,halal meat,halal food"
    },
    {
        "id": 1087,
        "categoryId": 22,
        "subCategoryName": "Health Food Stores",
        "icon": null,
        "clues": "protein shakes from health food stores,health food retail outlets,Health Food Stores,health food stores retail,health food stores re"
    },
    {
        "id": 1088,
        "categoryId": 22,
        "subCategoryName": "Health Foods & Products Wholesalers & Manufacturers",
        "icon": null,
        "clues": "health food wholesalers,health food & products manufacturers,health foods & products - wholesalers & manufacturers retailers,wholesale health foods,health supplements manufacturers,health foods products wholesale manufacturers,natural health care product manufacturers,health food manufacturers,health food store suppliers wholesale & manufacturers,Health Foods & Products Wholesalers & Manufacturers,health foods & products - wholesalers & manufacturers"
    },
    {
        "id": 1136,
        "categoryId": 22,
        "subCategoryName": "Honey Merchants",
        "icon": null,
        "clues": "Honey Merchants,honey,honey merchants,honey producers"
    },
    {
        "id": 1169,
        "categoryId": 22,
        "subCategoryName": "Ice Cream & Ice Products--Equipment &/or Supplies",
        "icon": null,
        "clues": "ice cream & ice products--equipment &/or supplies,ice cream,ice supplies,ice cream parlour,Ice Cream & Ice Products--Equipment &/or Supplies,ice cream cakes,ice cream vans"
    },
    {
        "id": 1170,
        "categoryId": 22,
        "subCategoryName": "Ice Cream Wholesalers & Manufacturers",
        "icon": null,
        "clues": "ice manufacturers,ice cream cakes,ice cream - wholesalers,ice cream - wholesalers & manufacturers,ice cream - wholesalers & manufacturers gelati,soft ice cream manufacturers,ice cream,Ice Cream Wholesalers & Manufacturers,ice cream manufacturers & wholesalers,ice cream parlour,ice cream manufacturers,ice cream vans"
    },
    {
        "id": 1171,
        "categoryId": 22,
        "subCategoryName": "Ice Cream--Retail",
        "icon": null,
        "clues": "ice cream--retail,refrigeration--commercial & industrial--retail & service ice cream freezer,Ice Cream--Retail"
    },
    {
        "id": 1172,
        "categoryId": 22,
        "subCategoryName": "Ice Making Machine",
        "icon": null,
        "clues": "ice making equipment,Ice Making Machine,ice making machines,ice machine hire,flake ice machine,ice making equipment &/or machines,ice cream machines,ice machine repair,ice cream machine hire,flakers ice machine,ice machines"
    },
    {
        "id": 1243,
        "categoryId": 22,
        "subCategoryName": "Juice Bars",
        "icon": null,
        "clues": "Juice Bars,juice,juice bars"
    },
    {
        "id": 1261,
        "categoryId": 22,
        "subCategoryName": "Kosher Products",
        "icon": null,
        "clues": "kosher restaurant,kosher products,Kosher Products,kosher catering,kosher butcher,kosher bakery,kosher,kosher deli,kosher food"
    },
    {
        "id": 1398,
        "categoryId": 22,
        "subCategoryName": "Meat Exporting & Packing",
        "icon": null,
        "clues": "meat exporting & packing,wholesale meat,meat wholesale,meat packers,meat works,meat shop,Meat Exporting & Packing,meat suppliers,meat exporting and or packing,meat exporters,meat factory,meat market,meat,meat processing,meat slicers,meat trader,meat wholesalers"
    },
    {
        "id": 1443,
        "categoryId": 22,
        "subCategoryName": "Milk Vendors",
        "icon": null,
        "clues": "Milk Vendors,milk bar,milk vendors,milk suppliers,milk distributors,milk,milk powder,milk producers,milk delivery,milk crates,milk supply,milk processing,milk wholesalers,milk man,milk manufacturers"
    },
    {
        "id": 1558,
        "categoryId": 22,
        "subCategoryName": "Oils--Edible",
        "icon": null,
        "clues": "edible oils,oils--edible,Oils--Edible"
    },
    {
        "id": 1560,
        "categoryId": 22,
        "subCategoryName": "Olives & Olive Oil",
        "icon": null,
        "clues": "olives & olive oil,olives,Olives & Olive Oil"
    },
    {
        "id": 1586,
        "categoryId": 22,
        "subCategoryName": "Oyster Suppliers & Farmers",
        "icon": null,
        "clues": "oyster farms,oyster suppliers,oyster suppliers & farmers,Oyster Suppliers & Farmers"
    },
    {
        "id": 1630,
        "categoryId": 22,
        "subCategoryName": "Pasta Products & Equipment",
        "icon": null,
        "clues": "Pasta Products & Equipment,pasta products & equipment,pasta products"
    },
    {
        "id": 1689,
        "categoryId": 22,
        "subCategoryName": "Pie Makers, Pasties & Sausage Rolls Wholesalers & Manufacturers",
        "icon": null,
        "clues": "pies pasties,pies,Pie Makers,Pasties & Sausage Rolls Wholesalers & Manufacturers,pies,pasties & sausage rolls - wholesalers & manufacturers"
    },
    {
        "id": 1702,
        "categoryId": 22,
        "subCategoryName": "Pizza Restaurants",
        "icon": null,
        "clues": "pizza take away,Pizza Restaurants,pizza,pizzas,take away food pizzas,pizza restaurant,pizza shop,restaurants pizza,woodfired pizza restaurant,take away pizza,pizza & pasta,pizza delivery"
    },
    {
        "id": 1746,
        "categoryId": 22,
        "subCategoryName": "Potato Chips & Crisps",
        "icon": null,
        "clues": "potato chips & crisps,potato chips,Potato Chips & Crisps"
    },
    {
        "id": 1749,
        "categoryId": 22,
        "subCategoryName": "Pottery Equipment & Supplies",
        "icon": null,
        "clues": "pottery supplies,Teachers--Ceramics,Porcelain & Pottery,pottery,pottery classes,pottery equipment & supplies,pottery equipment,Pottery Equipment & Supplies"
    },
    {
        "id": 1750,
        "categoryId": 22,
        "subCategoryName": "Poultry Equipment & Supplies",
        "icon": null,
        "clues": "poultry equipment,Poultry Equipment & Supplies,poultry equipment & supplies,poultry processing equipment,poultry processing & supplies,poultry weighing equipment"
    },
    {
        "id": 1751,
        "categoryId": 22,
        "subCategoryName": "Poultry Farmers & Dealers",
        "icon": null,
        "clues": "poultry suppliers & dealers,Poultry Farmers & Dealers,poultry farmers & dealers,poultry dealers"
    },
    {
        "id": 1752,
        "categoryId": 22,
        "subCategoryName": "Poultry Supplies",
        "icon": null,
        "clues": ""
    },
    {
        "id": 1753,
        "categoryId": 22,
        "subCategoryName": "Poultry--Retail",
        "icon": null,
        "clues": "Poultry--Retail,butchers--retail poultry,poultry--retail"
    },
    {
        "id": 1813,
        "categoryId": 22,
        "subCategoryName": "Pubs",
        "icon": null,
        "clues": "pubs bars,pubs,hotels-pubs,pubs hotels clubs,pubs hotels,pubs restaurant,bars & pubs,pubs clubs bars,pubs clubs hotels,pubs taverns & bars,Pubs,pubs & clubs,bars & clubs & pubs,pubs taverns & hotels,pubs & taverns"
    },
    {
        "id": 1903,
        "categoryId": 22,
        "subCategoryName": "Roadhouses",
        "icon": null,
        "clues": "Roadhouses,roadhouses"
    },
    {
        "id": 2095,
        "categoryId": 22,
        "subCategoryName": "Spices",
        "icon": null,
        "clues": "Spices,indian spices,spices,spices shop"
    },
    {
        "id": 2427,
        "categoryId": 22,
        "subCategoryName": "Wedding Cakes",
        "icon": null,
        "clues": "wedding cakes,Wedding Cakes,wedding arrangement & planning services cakes"
    },
    {
        "id": 2451,
        "categoryId": 22,
        "subCategoryName": "Wholesale Butchers",
        "icon": null,
        "clues": "Wholesale Butchers,butchers - wholesale"
    },
    {
        "id": 2452,
        "categoryId": 22,
        "subCategoryName": "Wholesale Cakes & Pastries",
        "icon": null,
        "clues": "cakes & pastries - wholesale,wholesale cakes,wholesale cakes & pastries,cakes wholesale,Wholesale Cakes & Pastries,wholesale pastries"
    },
    {
        "id": 2453,
        "categoryId": 22,
        "subCategoryName": "Wholesale Coffee",
        "icon": null,
        "clues": "wholesale coffee products,coffee-wholesale lebanese,coffee supplies wholesale,Wholesale Coffee,coffee beans wholesale,wholesale coffee"
    },
    {
        "id": 2457,
        "categoryId": 22,
        "subCategoryName": "Wholesale Seafood & Fish",
        "icon": null,
        "clues": "fish tanks wholesale,wholesale tropical fish,Wholesale Seafood & Fish,seafood wholesale retail,wholesale fishing tackle,wholesale fish supplies,wholesale seafood,fish & seafood - wholesale,seafood wholesale,wholesale seafood suppliers,wholesale fisheries,fish tank wholesale,wholesale fish,fish wholesale,fishing tackle wholesale,wholesale aquarium fish,fish wholesale retail,frozen seafood products wholesale"
    },
    {
        "id": 2472,
        "categoryId": 22,
        "subCategoryName": "Wine Education & Consulting",
        "icon": null,
        "clues": "wine consulting,wine education & consulting,Wine Education & Consulting,wine education"
    },
    {
        "id": 2473,
        "categoryId": 22,
        "subCategoryName": "Wine Makers' Equipment & Supplies",
        "icon": null,
        "clues": "wine making supplies,wine makers' equipment & supplies,wine makers' equipment,home wine making equipment,wine making equipment,wine supplies,wine makers,wine equipment,Wine Makers' Equipment & Supplies"
    },
    {
        "id": 2474,
        "categoryId": 22,
        "subCategoryName": "Wine Merchants & Spirit Dealers",
        "icon": null,
        "clues": "liquor wine spirits,wine cabinet dealers,Wine Merchants & Spirit Dealers,wine & spirits,wine & spirit merchants,beer wine & spirits retail groups,wine merchants"
    },
    {
        "id": 2475,
        "categoryId": 22,
        "subCategoryName": "Wine Producers & Spirit Distillers",
        "icon": null,
        "clues": "wine producers & spirit distillers,Wine Producers & Spirit Distillers,liquor wine spirits,wine producers,wine & spirits,wine & spirit merchants,beer wine & spirits retail groups"
    },
    {
        "id": 2507,
        "categoryId": 22,
        "subCategoryName": "Yeast W'salers & Mfrs",
        "icon": null,
        "clues": "yeast w'salers & mfrs,Yeast W'salers & Mfrs,yeast"
    }
    ]
},
{
    "id": 23,
    "icon": null,
    "subCategories": [{
    "id": 17,
    "categoryId": 23,
    "subCategoryName": "Adoption Information Services",
    "icon": null,
    "clues": "animal adoption,adoption lawyers,adoption,adoption agencies,adoption information services,pet adoption,Adoption Information Services,adoption services"
},
    {
        "id": 448,
        "categoryId": 23,
        "subCategoryName": "Civic Organisations",
        "icon": null,
        "clues": "organisations--civic,civic organisations,Civic Organisations"
    },
    {
        "id": 502,
        "categoryId": 23,
        "subCategoryName": "Community Centres",
        "icon": null,
        "clues": "community centre,Community Centres,community centres"
    },
    {
        "id": 732,
        "categoryId": 23,
        "subCategoryName": "Electric Power Lines",
        "icon": null,
        "clues": "electric lighting & power advisory services,electric powerline & maintenance,electric power line,electric power line construction & fittings,electric power line construction,Electric Power Lines,electric power tool"
    },
    {
        "id": 762,
        "categoryId": 23,
        "subCategoryName": "Embassies & High Commissions (ACT only)",
        "icon": null,
        "clues": "embassies & high commissions (act only),Embassies & High Commissions (ACT only)"
    },
    {
        "id": 870,
        "categoryId": 23,
        "subCategoryName": "Fire Brigades",
        "icon": null,
        "clues": "Fire Brigades,fire brigades"
    },
    {
        "id": 1334,
        "categoryId": 23,
        "subCategoryName": "Local Government & Planning Law",
        "icon": null,
        "clues": "organisations--local government,local government & planning law,local government,Local Government & Planning Law"
    },
    {
        "id": 1335,
        "categoryId": 23,
        "subCategoryName": "Local Government Organisations",
        "icon": null,
        "clues": "organisations--local government,local government,Local Government Organisations"
    },
    {
        "id": 1734,
        "categoryId": 23,
        "subCategoryName": "Political Organisations",
        "icon": null,
        "clues": "organisations--political,political consultants,political organisations,political cartoonist,political party,Political Organisations,organisers political"
    },
    {
        "id": 1744,
        "categoryId": 23,
        "subCategoryName": "Postal Services",
        "icon": null,
        "clues": "postal scales,postal tubes,postal delivery,international postage,Postal Services,postage supplies,postage scales,postal agencies,postal services,postal outlets,postage stamps"
    },
    {
        "id": 2274,
        "categoryId": 23,
        "subCategoryName": "Town & Regional Planning",
        "icon": null,
        "clues": "town council,town,town planning advisor,town planning appeals,environmental town planning,town car,Town & Regional Planning,town planning agencies,town planners,town & regional planning planning applications,town & regional planners,town planning consultants,town hall,town planning firms,town & regional planning,town & regional"
    },
    {
        "id": 2284,
        "categoryId": 23,
        "subCategoryName": "Traffic Law",
        "icon": null,
        "clues": "lawyers traffic,traffic law,traffic lawyers,Traffic Law"
    }
    ]
},
{
    "id": 24,
    "icon": null,
    "subCategories": [{
    "id": 111,
    "categoryId": 24,
    "subCategoryName": "Artificial Nails",
    "icon": null,
    "clues": "nails,Artificial Nails,beauty salon equipment & supplies artificial nails,beauty salons artificial nails,beauty salons gel nails,mobile artificial nails,beauty salons acrylic nails,artificial nails,acrylic nails,artificial nails training,nails & beauty,artificial nails wholesale,gel nails,manicure & artificial nails"
},
    {
        "id": 179,
        "categoryId": 24,
        "subCategoryName": "Beauty Courses",
        "icon": null,
        "clues": "beauty training,beauty schools,Beauty Courses"
    },
    {
        "id": 180,
        "categoryId": 24,
        "subCategoryName": "Beauty Salon Supplies & Equipment",
        "icon": null,
        "clues": "beauty therapy equipment,beauty salon supplies,beauty salon equipment & supplies,beauty supplies,beauty therapy supplies,beauty equipment,beauty supplies & equipment,beauty salon equipment & supplies artificial nails,beauty salon solarium equipment & supplies,wholesale beauty supplies,Beauty Salon Supplies & Equipment,hair & beauty equipment,beauty equipment & supplies,hair & beauty supplies,beauty salon spray tan supplies,wholesale beauty equipment,beauty salon equipment,nail & beauty supplies"
    },
    {
        "id": 181,
        "categoryId": 24,
        "subCategoryName": "Beauty Salons",
        "icon": null,
        "clues": "Beauty Salons,beauty salons brazilian waxing,beauty salons day spa,beauty therapy,beauty salons with spray tanning,beauty salons,beauty salon spa,beauty salons ear piercing,beauty salons body wraps,beauty salons gel nails,beauty salons acrylic nails,beauty salon manicure,beauty salon for men,beauty salons hair removal,beauty salons eyelash extensions,beauty salons nail technicians,beauty salons bridal make-up,beauty salons pedicures,beauty therapist,beauty salons hair cuts,beauty services,beauty salon waxing,beauty salons facials,beauty salons microdermabrasion,beauty salons with tanning,beauty salons eyebrow shaping"
    },
    {
        "id": 224,
        "categoryId": 24,
        "subCategoryName": "Body & Ear Piercing",
        "icon": null,
        "clues": "body piercing belly button,body piercing tongue,body & ear piercing,body piercing training,body piercing supplies,body piercing eyebrows,body piercing,body piercing jewellery,Body & Ear Piercing,body piercing studios,body piercing courses,body piercing lips"
    },
    {
        "id": 571,
        "categoryId": 24,
        "subCategoryName": "Cosmetic Containers",
        "icon": null,
        "clues": "Cosmetic Containers,cosmetic containers"
    },
    {
        "id": 572,
        "categoryId": 24,
        "subCategoryName": "Cosmetic Store",
        "icon": null,
        "clues": "retail cosmetic store,Cosmetic Store"
    },
    {
        "id": 574,
        "categoryId": 24,
        "subCategoryName": "Cosmetics Wholesalers & Manufacturers",
        "icon": null,
        "clues": "cosmetics distributors,cosmetics - wholesalers,cosmetics manufacturers & wholesalers,cosmetics - wholesalers & manufacturers,cosmetics contract manufacturers,Cosmetics Wholesalers & Manufacturers,cosmetics wholesale,wholesalers cosmetics,cosmetics manufacturers organic,cosmetics-retail,cosmetics manufacturers industries"
    },
    {
        "id": 814,
        "categoryId": 24,
        "subCategoryName": "Eyelets & Eyeletting Equipment",
        "icon": null,
        "clues": "eyelets,Eyelets & Eyeletting Equipment,eyelets & eyeletting equipment,eyelets supply company"
    },
    {
        "id": 1061,
        "categoryId": 24,
        "subCategoryName": "Hair Care Products",
        "icon": null,
        "clues": "hair products,hair care supplies,hair care,hair care manufacturers,hair care products & wholesale,wholesale hair care products,natural hair products,hair & beauty products,Hair Care Products,hair styling products,african hair products,hair care international,hair care products"
    },
    {
        "id": 1062,
        "categoryId": 24,
        "subCategoryName": "Hair Removal",
        "icon": null,
        "clues": "hair removal laser hair removal,hair removal for men,hair removal male,Hair Removal,hair removal electrolysis,laser hair removals,hair removal clinic,hair removal,cosmetic surgery &/or procedures laser hair removal,hair removal men,hair removal brazilian wax,hair removal laser,hair removal ipl,hair removal waxing,hair removal centre,hair removal training,beauty salons hair removal"
    },
    {
        "id": 1063,
        "categoryId": 24,
        "subCategoryName": "Hair Treatment & Replacement",
        "icon": null,
        "clues": "hair treatment,Hair Treatment & Replacement,hair loss treatment,hair treatment & replacement,hair replacement & or services,hair treatment & replacement services,hair replacement,laser hair treatment"
    },
    {
        "id": 1064,
        "categoryId": 24,
        "subCategoryName": "Hairdressers",
        "icon": null,
        "clues": "hairdressers' supplies hair extensions,hairdressers hair straightening,hairdressers mobile hairdresser,Hairdressers,hairdressers ghd,hairdressers braiding,hairdressers,hairdressers organic hair products,hairdressers' supplies,hairdressers colouring,hairdressers bridal hairstyling"
    },
    {
        "id": 1356,
        "categoryId": 24,
        "subCategoryName": "Make-Up Artists & Supplies",
        "icon": null,
        "clues": "make-up artists college,prom night make-up artists,make-up salon,make-up artists & supplies,make-up artists school,wedding make-up artists,hair & make-up,make-up artists & supplies mobile services,spring racing make-up artists,bridal make-up artists,wedding make-up,make-up consultants,make-up artists mobile,hair & make-up artists,make-up supplies,Make-Up Artists & Supplies,make-up studios"
    },
    {
        "id": 1364,
        "categoryId": 24,
        "subCategoryName": "Manicurists",
        "icon": null,
        "clues": "Manicurists,manicurists"
    },
    {
        "id": 1510,
        "categoryId": 24,
        "subCategoryName": "Nails & Nailing Equipment",
        "icon": null,
        "clues": "Nails & Nailing Equipment,beauty salons gel nails,nails,beauty salons acrylic nails,acrylic nails,nails & beauty,nails & nailing equipment,gel nails,beauty salon equipment & supplies artificial nails"
    },
    {
        "id": 1646,
        "categoryId": 24,
        "subCategoryName": "Perfume & Toiletries Shops",
        "icon": null,
        "clues": "perfumes & toiletries,Perfume & Toiletries Shops,perfumes & deodorants,perfume shop,perfumes & toiletries-retail,perfume"
    },
    {
        "id": 1647,
        "categoryId": 24,
        "subCategoryName": "Perfumes & Toiletries Wholesalers & Manufacturers",
        "icon": null,
        "clues": "perfumes & toiletries - wholesalers & manufacturers,perfumes & toiletries,Perfumes & Toiletries Wholesalers & Manufacturers,perfumes & deodorants,perfumes & toiletries-retail,manufacturers of perfumes"
    },
    {
        "id": 2431,
        "categoryId": 24,
        "subCategoryName": "Wedding Hair & Beauty Services",
        "icon": null,
        "clues": "wedding make-up & hair,wedding video services,wedding hair & beauty services mobile services,wedding hair & make-up,wedding hairdresser,wedding hair stylist,wedding services,Wedding Hair & Beauty Services,wedding hair & beauty services,wedding arrangement & planning services,wedding arrangement & planning services cakes,wedding arrangement services,wedding arrangement & planning services wedding hair,wedding hair & beauty,wedding hair,wedding arrangement & planning services decorations,wedding arrangement services & supplies,wedding photographers & video services,wedding reception services,mobile wedding hair"
    },
    {
        "id": 2460,
        "categoryId": 24,
        "subCategoryName": "Wigs & Hairpieces",
        "icon": null,
        "clues": "wigs & hairpieces,Wigs & Hairpieces,wigs,wigs shop,wigs & hairpieces fancy dress,wigs hire,wigs makers"
    }
    ]
},
{
    "id": 25,
    "icon": null,
    "subCategories": [{
    "id": 2,
    "categoryId": 25,
    "subCategoryName": "Abattoir Machinery & Equipment",
    "icon": null,
    "clues": "Abattoir Machinery & Equipment,abattoir machinery,abattoir,abattoir machinery & equipment,abattoir equipment"
},
    {
        "id": 3,
        "categoryId": 25,
        "subCategoryName": "Abattoirs",
        "icon": null,
        "clues": "Abattoirs,abattoirs rendering,abattoirs"
    },
    {
        "id": 6,
        "categoryId": 25,
        "subCategoryName": "Abrasive Blasting",
        "icon": null,
        "clues": "abrasive blasting bead blasting,Abrasive Blasting,abrasive blasting soda blasting,abrasive blasting,abrasive blasting contractors,abrasive blasting mobile blasting,abrasive blasting sand blasting"
    },
    {
        "id": 7,
        "categoryId": 25,
        "subCategoryName": "Abrasive Blasting Equipment",
        "icon": null,
        "clues": "Abrasive Blasting Equipment,abrasive blasting bead blasting,abrasive blasting soda blasting,abrasive blasting contractors,abrasive blasting mobile blasting,abrasive blasting equipment,abrasive blasting sand blasting"
    },
    {
        "id": 8,
        "categoryId": 25,
        "subCategoryName": "Abrasives",
        "icon": null,
        "clues": "Abrasives,abrasives"
    },
    {
        "id": 12,
        "categoryId": 25,
        "subCategoryName": "Acid Proofing",
        "icon": null,
        "clues": "acid,acid cleaning,acid dip,acid proofing,acid etching,acid stripping,Acid Proofing,acid wash,acid baths"
    },
    {
        "id": 34,
        "categoryId": 25,
        "subCategoryName": "Aerial Agricultural Services",
        "icon": null,
        "clues": "aerial services,aerial photos,aerial,aerial installation,aerial spraying,aerial agricultural,Advertising - Aerial,aerial surveys,aerial agriculture,aerial repair,Aerial Agricultural Services,aerial advertising,tv aerial,aerial agricultural services"
    },
    {
        "id": 37,
        "categoryId": 25,
        "subCategoryName": "Aerosol Containers, Valves &/or Filling",
        "icon": null,
        "clues": "aerosol paint,aerosol container,aerosol containers,valves &/or filling,aerosol fillers,aerosol container valves,Aerosol Containers,Valves &/or Filling,aerosol"
    },
    {
        "id": 42,
        "categoryId": 25,
        "subCategoryName": "Air & Gas Cylinders",
        "icon": null,
        "clues": "air conditioner gas,Air Tools Accessories,Supply & Service,air conditioner re-gas,Air & Gas Cylinders,re-gas air conditioning,car air conditioner re-gas,car air conditioning re-gas,air & gas cylinders,air conditioning re-gas"
    },
    {
        "id": 47,
        "categoryId": 25,
        "subCategoryName": "Air Conditioning Wholesalers & Manufacturers",
        "icon": null,
        "clues": "air conditioning,air conditioning repair,Air Conditioning Wholesalers & Manufacturers,air conditioning--installation & service repairs,air conditioning wholesalers & manufacturers,air conditioning services,air conditioning - wholesales & manufacturers,air compressors manufacturers,air conditioning--commercial & industrial,air conditioning-automotive,air conditioning duct manufacturers,air conditioning manufacturers,auto air conditioning,air conditioning-home,air conditioning--installation & service,air conditioning wholesalers,air conditioning--parts,automotive air conditioning,air conditioning commercial,air conditioning wholesalers parts,air conditioning--installation & service maintenance"
    },
    {
        "id": 71,
        "categoryId": 25,
        "subCategoryName": "Aluminium Fabricators",
        "icon": null,
        "clues": "windows-aluminium,aluminium fabricators,aluminium fencing contractors,aluminium welding,aluminium fabrication,aluminium extrusions,aluminium door,Aluminium Fabricators,aluminium fencing"
    },
    {
        "id": 72,
        "categoryId": 25,
        "subCategoryName": "Aluminium Manufacturers & Suppliers",
        "icon": null,
        "clues": "aluminium manufacturers,aluminium welding,aluminium fabrication & manufacturers,aluminium extrusions suppliers,aluminium extrusions manufacturers,aluminium wholesalers & manufacturers extrusions,aluminium fabrication,aluminium extrusions,aluminium window manufacturers,aluminium boat manufacturers,aluminium fencing,windows-aluminium,aluminium fencing contractors,aluminium wholesalers & manufacturers pool fencing,Aluminium Manufacturers & Suppliers,aluminium window suppliers,aluminium wholesalers & manufacturers,steel & stainless steel & aluminium manufacturers,aluminium door"
    },
    {
        "id": 73,
        "categoryId": 25,
        "subCategoryName": "Aluminium Suppliers",
        "icon": null,
        "clues": "aluminium suppliers,aluminium extrusions suppliers,aluminium window suppliers,Aluminium Suppliers"
    },
    {
        "id": 74,
        "categoryId": 25,
        "subCategoryName": "Aluminium Windows",
        "icon": null,
        "clues": "windows-aluminium,aluminium windows & door,commercial aluminium windows,aluminium windows,aluminium windows manufacturing,aluminium windows commercial,aluminium windows & doors,windows-aluminium louvre,aluminium doors & windows,Aluminium Windows"
    },
    {
        "id": 84,
        "categoryId": 25,
        "subCategoryName": "Anodising",
        "icon": null,
        "clues": "Anodising,anodising,hard anodising,anodising aluminium,aluminium anodising"
    },
    {
        "id": 89,
        "categoryId": 25,
        "subCategoryName": "Apron Wholesalers & Manufacturers",
        "icon": null,
        "clues": "Apron Wholesalers & Manufacturers,disposable apron,apron,apron wholesalers & manufacturers"
    },
    {
        "id": 90,
        "categoryId": 25,
        "subCategoryName": "Aquaculture",
        "icon": null,
        "clues": "aquaculture farm,aquaculture fish farm hatchery,aquaculture,aquaculture tanks,aquaculture supplies,aquaculture hatchery,aquaculture feed,Aquaculture"
    },
    {
        "id": 91,
        "categoryId": 25,
        "subCategoryName": "Aquaponics",
        "icon": null,
        "clues": "aquaponics shop,aquaponics products,Aquaponics,aquaponics"
    },
    {
        "id": 109,
        "categoryId": 25,
        "subCategoryName": "Artificial Breeding Services",
        "icon": null,
        "clues": "Artificial Breeding Services,artificial breeding services"
    },
    {
        "id": 143,
        "categoryId": 25,
        "subCategoryName": "Badges, Badge Making Machine & Equipment",
        "icon": null,
        "clues": "badges making machines,badges embroidery,badges making supplies,badges making equipment,badges making,badges making wholesalers,badges makers,badges machine hire,badges suppliers,badges supplies,badges,badges machine,badges printing,Badges,Badge Making Machine & Equipment,badges & badge making equipment & supplies,badges fitting,name badges,badges hire,badges & badge making equipment & supplies name tags"
    },
    {
        "id": 149,
        "categoryId": 25,
        "subCategoryName": "Balancing Equipment & Services",
        "icon": null,
        "clues": "Balancing Equipment & Services,balancing,balancing equipment & services,balancing equipment,industrial balancing repair & services"
    },
    {
        "id": 152,
        "categoryId": 25,
        "subCategoryName": "Balsa Wood",
        "icon": null,
        "clues": "balsa wood supplies,balsa wood,Balsa Wood,balsa"
    },
    {
        "id": 156,
        "categoryId": 25,
        "subCategoryName": "Banana Growers",
        "icon": null,
        "clues": "Banana Growers,banana growers,banana farm"
    },
    {
        "id": 174,
        "categoryId": 25,
        "subCategoryName": "Bathroom Equipment & Accessories Wholesalers & Manufacturers",
        "icon": null,
        "clues": "bathroom wholesalers,bathroom equipment and accessories - wholesalers and manufacturers,Bathroom Equipment & Accessories Wholesalers & Manufacturers"
    },
    {
        "id": 178,
        "categoryId": 25,
        "subCategoryName": "Bearings & Bushings",
        "icon": null,
        "clues": "bearings supplies,bearings & bushings bosch,bearings & bushings,Bearings & Bushings,bearings & bushings pulleys,bearings & bushings bearings,bearings & bushings seals,bearings & bushings ball"
    },
    {
        "id": 184,
        "categoryId": 25,
        "subCategoryName": "Beds & Bedding Wholesalers & Manufacturers",
        "icon": null,
        "clues": "beds & bedding - wholesalers & manufacturers mattresses,beds and bedding - wholesalers & manufacturers,beds and bedding - wholesalers and manufacturers,Beds & Bedding Wholesalers & Manufacturers"
    },
    {
        "id": 190,
        "categoryId": 25,
        "subCategoryName": "Belts Apparel Wholesalers & Manufacturers",
        "icon": null,
        "clues": "Belts Apparel Wholesalers & Manufacturers,belts - apparel - wholesalers,belts - apparel -  wholesalers & manufacturers"
    },
    {
        "id": 191,
        "categoryId": 25,
        "subCategoryName": "Belts Manufacturers & Suppliers",
        "icon": null,
        "clues": "Belts Manufacturers & Suppliers"
    },
    {
        "id": 207,
        "categoryId": 25,
        "subCategoryName": "Blankets Wholesalers & Manufacturers",
        "icon": null,
        "clues": "Blankets Wholesalers & Manufacturers,blankets--w'salers & mfrs,wool blankets,solar blankets,electric blankets,blankets cleaning,blankets,fire blankets,pool blankets,packing blankets,blankets - wholesalers,blankets box,blankets manufacturers"
    },
    {
        "id": 240,
        "categoryId": 25,
        "subCategoryName": "Bottle & Jar Wholesalers & Manufacturers",
        "icon": null,
        "clues": "Bottle & Jar Wholesalers & Manufacturers,bottle manufacturers,plastic bottle manufacturers,glass bottle manufacturers"
    },
    {
        "id": 255,
        "categoryId": 25,
        "subCategoryName": "Brass Plates",
        "icon": null,
        "clues": "brass beds,brass fittings,brass polishing,brass name plates,Brass Plates,brass plates,brass instruments"
    },
    {
        "id": 256,
        "categoryId": 25,
        "subCategoryName": "Brassware Wholesalers & Manufacturers",
        "icon": null,
        "clues": "brassware wholesalers,brassware,Brassware Wholesalers & Manufacturers,Copper & Brass Sheet,Rod,Section Wholesalers & Manufacturers,hardware--retail brassware,brassware wholesalers & manufacturers"
    },
    {
        "id": 262,
        "categoryId": 25,
        "subCategoryName": "Brick Production Equipment & Supplies",
        "icon": null,
        "clues": "brick suppliers,Brick Production Equipment & Supplies,brick production equipment & supplies,brick paving supplies"
    },
    {
        "id": 308,
        "categoryId": 25,
        "subCategoryName": "Buttons & Buckles Wholesalers & Manufacturers",
        "icon": null,
        "clues": "buttons wholesale,buttons & buckles - wholesalers & manufacturers,Buttons & Buckles Wholesalers & Manufacturers"
    },
    {
        "id": 332,
        "categoryId": 25,
        "subCategoryName": "Can Manufacturers",
        "icon": null,
        "clues": "Can Manufacturers,can manufacturers"
    },
    {
        "id": 365,
        "categoryId": 25,
        "subCategoryName": "Carbon Products Wholesalers & Manufacturers",
        "icon": null,
        "clues": "Carbon Products Wholesalers & Manufacturers,carbon products--w'salers & mfrs,carbon products"
    },
    {
        "id": 373,
        "categoryId": 25,
        "subCategoryName": "Carpet & Carpet Tiles Wholesalers & Manufacturers",
        "icon": null,
        "clues": "Carpet & Carpet Tiles Wholesalers & Manufacturers"
    },
    {
        "id": 393,
        "categoryId": 25,
        "subCategoryName": "Cathodic Protection",
        "icon": null,
        "clues": "Cathodic Protection,cathodic protection"
    },
    {
        "id": 399,
        "categoryId": 25,
        "subCategoryName": "Cement Supplies",
        "icon": null,
        "clues": "cement paving,cement products,cement rendering,cement manufacturers,cement supplies construction,cement supplies bagged cement,blended cement supplies,cement trucks,cement rendering supplies,cement sheet,cement supplies dry mix,cement supplies,general purpose cement supplies,cement driveways,sand & cement supplies,cement supplies sands,cement supplies delivery,cement supplies lime,Cement Supplies,cement slabs,cement,cement supplies bulk cement,cement mixers,cement supplies plasters,render cement supplies"
    },
    {
        "id": 407,
        "categoryId": 25,
        "subCategoryName": "Chair Manufacturers",
        "icon": null,
        "clues": "office chairs,chairs covers,Chair Manufacturers,chairs,chair manufacturers,office chair manufacturers,chairs hire"
    },
    {
        "id": 412,
        "categoryId": 25,
        "subCategoryName": "Chemical & Oil Spill Recovery",
        "icon": null,
        "clues": "oil & chemical spill recovery or dispersal,Chemical & Oil Spill Recovery,oil & chemical spill recovery"
    },
    {
        "id": 413,
        "categoryId": 25,
        "subCategoryName": "Chemical Engineers",
        "icon": null,
        "clues": "employment services for chemical engineers,Chemical Engineers,engineers consulting chemical,chemical engineers"
    },
    {
        "id": 414,
        "categoryId": 25,
        "subCategoryName": "Chemical Processing Equipment",
        "icon": null,
        "clues": "chemical processing equipment,Chemical Processing Equipment,cleaning equipment--steam pressure chemical,oil & chemical spill equipment"
    },
    {
        "id": 415,
        "categoryId": 25,
        "subCategoryName": "Chemical Suppliers",
        "icon": null,
        "clues": "Chemical Suppliers,disinfectants chemical suppliers,industrial chemical suppliers,chemical suppliers"
    },
    {
        "id": 416,
        "categoryId": 25,
        "subCategoryName": "Chemicals--Agricultural",
        "icon": null,
        "clues": "chemicals--agricultural,agricultural chemicals,Chemicals--Agricultural"
    },
    {
        "id": 419,
        "categoryId": 25,
        "subCategoryName": "Chemists--Consulting & Industrial",
        "icon": null,
        "clues": "Chemists--Consulting & Industrial,chemists--consulting,consulting chemists,chemists--consulting & industrial"
    },
    {
        "id": 524,
        "categoryId": 25,
        "subCategoryName": "Concrete Additives",
        "icon": null,
        "clues": "Concrete Additives,concrete additives"
    },
    {
        "id": 525,
        "categoryId": 25,
        "subCategoryName": "Concrete Aggregates",
        "icon": null,
        "clues": "concrete exposed aggregates,concrete aggregates,exposed concrete aggregates,exposed aggregates concrete,Concrete Aggregates"
    },
    {
        "id": 526,
        "categoryId": 25,
        "subCategoryName": "Concrete Block Making Equipment",
        "icon": null,
        "clues": "concrete block suppliers,concrete mixing equipment &/or vibrators,tanks & tank equipment concrete,concrete equipment,concrete tanks equipment,concrete equipment suppliers,concrete block manufacturers,concrete building blocks,concrete block making equipment,concrete equipment finishing,concrete products blocks,concrete blocks,concrete pumps & equipment concrete batching plants,concrete mixing equipment,Concrete Block Making Equipment,concrete equipment hire"
    },
    {
        "id": 528,
        "categoryId": 25,
        "subCategoryName": "Concrete Cutting, Grinding & Drilling",
        "icon": null,
        "clues": "concrete sawing drilling grinding,concrete drilling,concrete sawing & drilling,Concrete Cutting,Grinding & Drilling,concrete sawing,drilling,grinding & breaking,concrete floor grinding,concrete grinding equipment,concrete cutting & drilling,concrete sawing,drilling,grinding & breaking polishing,concrete grinding & polishing,concrete drilling equipment,concrete grinding,concrete sawing,drilling,grinding & breaking core,concrete core drilling,concrete cutting drilling grinding breaking,concrete cutting,concrete cutting equipment,concrete sawing drilling breaking,concrete cutting & sawing,concrete cutting services,grinding concrete,concrete core drilling contractors"
    },
    {
        "id": 529,
        "categoryId": 25,
        "subCategoryName": "Concrete Formwork, Form Ties & Accessories",
        "icon": null,
        "clues": "concrete contractors formwork,concrete formwork excavating,concrete formwork suppliers,concrete columns formwork,concrete forming,concrete formwork contractors,concrete forming products,concrete accessories,formwork concrete,concrete formwork,Concrete Formwork,Form Ties & Accessories,concrete formwork hire,concrete formwork,form ties & accessories,concrete formwork form ties,concrete formwork,form ties & accessories steel fixing"
    },
    {
        "id": 535,
        "categoryId": 25,
        "subCategoryName": "Concrete Pumping",
        "icon": null,
        "clues": "concrete pumping contractors,concrete pumping,Concrete Pumping,concrete pumping services spraying,concrete pumping services,concrete pumping supply,concrete pumping & spraying"
    },
    {
        "id": 542,
        "categoryId": 25,
        "subCategoryName": "Confectionery Mfrs' Equipment & Supplies",
        "icon": null,
        "clues": "confectionery - wholesalers and manufacturers,Confectionery Mfrs' Equipment & Supplies,confectionery mfrs' equipment & supplies"
    },
    {
        "id": 543,
        "categoryId": 25,
        "subCategoryName": "Confectionery Wholesalers & Manufacturers",
        "icon": null,
        "clues": "confectionery - wholesalers and manufacturers,Confectionery Wholesalers & Manufacturers"
    },
    {
        "id": 561,
        "categoryId": 25,
        "subCategoryName": "Cool Room Builders & Construction",
        "icon": null,
        "clues": "cool room builders & designers,cool room builders &/or designers,cool room refrigeration,cool room installers,Cool Room Builders & Construction,cool room insulation,cool room construction"
    },
    {
        "id": 564,
        "categoryId": 25,
        "subCategoryName": "Copper & Brass Products",
        "icon": null,
        "clues": "Copper & Brass Products,copper & brass products,brass & copper rod,section & sheet wholesalers & manufacturers,copper & brass,Copper & Brass Sheet,Rod,Section Wholesalers & Manufacturers,brass & copper"
    },
    {
        "id": 565,
        "categoryId": 25,
        "subCategoryName": "Copper & Brass Sheet, Rod, Section Wholesalers & Manufacturers",
        "icon": null,
        "clues": "copper rod,brass & copper rod,section & sheet wholesalers & manufacturers,copper sheet,Copper & Brass Sheet,Rod,Section Wholesalers & Manufacturers,copper sheet metal"
    },
    {
        "id": 577,
        "categoryId": 25,
        "subCategoryName": "Cotton",
        "icon": null,
        "clues": "cotton,Cotton"
    },
    {
        "id": 586,
        "categoryId": 25,
        "subCategoryName": "Crane Manufacturers & Distribution",
        "icon": null,
        "clues": "crane manufacturers & distributors servicing,Crane Manufacturers & Distribution,crane mfrs & distributors,crane manufacturers,crane manufacturers & distributors"
    },
    {
        "id": 604,
        "categoryId": 25,
        "subCategoryName": "Cutting Services--Fabric, Leather & Plastic",
        "icon": null,
        "clues": "plastic laser cutting,laser cutting plastics,Cutting Services--Fabric,Leather & Plastic,plastic cutting,cutting services--fabric,leather & plastic,cutting services--fabric leather,fabric cutting services"
    },
    {
        "id": 605,
        "categoryId": 25,
        "subCategoryName": "Dairies",
        "icon": null,
        "clues": "dairies,Dairies"
    },
    {
        "id": 606,
        "categoryId": 25,
        "subCategoryName": "Dairy Equipment & Supplies",
        "icon": null,
        "clues": "dairy equipment & supplies,rotary dairy equipment,dairy equipment,Dairy Equipment & Supplies"
    },
    {
        "id": 607,
        "categoryId": 25,
        "subCategoryName": "Dairy Products Wholesalers & Manufacturers",
        "icon": null,
        "clues": "dairy wholesalers,dairy products--w'salers & mfrs,Dairy Products Wholesalers & Manufacturers,dairy products - wholesalers,dairy products,dairy manufacturers"
    },
    {
        "id": 611,
        "categoryId": 25,
        "subCategoryName": "Dangerous Goods Storage",
        "icon": null,
        "clues": "dangerous goods storage facility,dangerous goods storage,Dangerous Goods Storage,dangerous goods warehouse,dangerous goods warehousing"
    },
    {
        "id": 692,
        "categoryId": 25,
        "subCategoryName": "Dredging Contractors & Equipment Manufacturers",
        "icon": null,
        "clues": "dredging contractors,dredging contractors & equipment manufacturers power stations,Dredging Contractors & Equipment Manufacturers,dredging contractors & equipment manufacturers,dredging & reclamation,dredging"
    },
    {
        "id": 701,
        "categoryId": 25,
        "subCategoryName": "Drums Manufacturers, Reconditioners & Dealers",
        "icon": null,
        "clues": "steel drums,oil drums,cable drums,drum manufacturers,reconditioners &/or dealers,Drums Manufacturers,Reconditioners & Dealers,copiers drums,drums,drums & barrels,plastic drums"
    },
    {
        "id": 710,
        "categoryId": 25,
        "subCategoryName": "DVD Mfrs & Distributors",
        "icon": null,
        "clues": "DVD Mfrs & Distributors,dvd distributors,dvd manufacturers & distributors,dvd mfrs & distributors,cd & dvd wholesalers & manufacturers printing,cd & dvd wholesalers & manufacturers duplicating,cd & dvd wholesalers & manufacturers"
    },
    {
        "id": 713,
        "categoryId": 25,
        "subCategoryName": "Dynamometers",
        "icon": null,
        "clues": "dynamometers,Dynamometers"
    },
    {
        "id": 719,
        "categoryId": 25,
        "subCategoryName": "Effluent Treatment Equipment & Services",
        "icon": null,
        "clues": "effluent treatment equipment & services,effluent treatment equipment,Effluent Treatment Equipment & Services,effluent treatment"
    },
    {
        "id": 735,
        "categoryId": 25,
        "subCategoryName": "Electrical Accessories Wholesalers & Manufacturers",
        "icon": null,
        "clues": "electrical accessories - wholesalers & manufacturers,Electrical Accessories Wholesalers & Manufacturers"
    },
    {
        "id": 736,
        "categoryId": 25,
        "subCategoryName": "Electrical Appliance Wholesalers & Manufacturers",
        "icon": null,
        "clues": "electrical appliances - wholesalers & manufacturers,Electrical Appliance Wholesalers & Manufacturers,electrical appliances - wholesalers and manufacturers,electrical light manufacturers"
    },
    {
        "id": 740,
        "categoryId": 25,
        "subCategoryName": "Electrical Switchboard Manufacturers",
        "icon": null,
        "clues": "Electrical Switchboard Manufacturers,electrical switchboard,electrical switchboard manufacturers,electrical switchboard builders,electrical contractors switchboards,electrical switchboards-wholesalers & manufacturers"
    },
    {
        "id": 745,
        "categoryId": 25,
        "subCategoryName": "Electrical Wire Harness Wholesalers & Manufacturers",
        "icon": null,
        "clues": "electrical wire harness wholesalers & manufacturers,electrical wire & cable import,Electrical Wire Harness Wholesalers & Manufacturers,electrical cable & wire suppliers"
    },
    {
        "id": 752,
        "categoryId": 25,
        "subCategoryName": "Electronic Equipment Wholesalers & Manufacturers",
        "icon": null,
        "clues": "electronic security equipment,electronic equipment manufacturers,electronic test equipment,electronic product manufacturers,electronic equipment,electronic components manufacturers companies,calibration & electronic testing equipment,electronic manufacturers,electronic equipment repair,electronic equipment - wholesalers & manufacturers,electronic equipment hire,electronic manufacturers surface mounting,Electronic Equipment Wholesalers & Manufacturers"
    },
    {
        "id": 755,
        "categoryId": 25,
        "subCategoryName": "Electronic Parts Wholesalers & Manufacturers",
        "icon": null,
        "clues": "electronic parts - wholesalers & manufacturers,Electronic Parts Wholesalers & Manufacturers,electronic parts - wholesalers"
    },
    {
        "id": 757,
        "categoryId": 25,
        "subCategoryName": "Electronics Manufacturing Equipment & Supplies",
        "icon": null,
        "clues": "Electronics Manufacturing Equipment & Supplies,electronics,electronics manufacturing equipment & supplies,electronics manufacturers equipment,electronics wholesale"
    },
    {
        "id": 758,
        "categoryId": 25,
        "subCategoryName": "Electroplating",
        "icon": null,
        "clues": "electroplating,electroplating chrome,Electroplating"
    },
    {
        "id": 759,
        "categoryId": 25,
        "subCategoryName": "Electroplating Supplies",
        "icon": null,
        "clues": "Electroplating Supplies,electroplating supplies,electroplating chrome"
    },
    {
        "id": 760,
        "categoryId": 25,
        "subCategoryName": "Elevated Work Platform (EWP) Manufacturers & Distributors",
        "icon": null,
        "clues": "training & development elevated work platform,Elevated Work Platform (EWP) Manufacturers & Distributors"
    },
    {
        "id": 770,
        "categoryId": 25,
        "subCategoryName": "Emu & Ostrich Farmers & Products",
        "icon": null,
        "clues": "emu oil,emu & ostrich farmers & products,Emu & Ostrich Farmers & Products,emu oils,emu & ostrich farmers & products emu oils"
    },
    {
        "id": 771,
        "categoryId": 25,
        "subCategoryName": "Enamelling Services",
        "icon": null,
        "clues": "Enamelling Services,enamelling services,panel beaters & painters baked enamelling"
    },
    {
        "id": 778,
        "categoryId": 25,
        "subCategoryName": "Engineering & Mining Surveyors",
        "icon": null,
        "clues": "Engineering & Mining Surveyors,engineering surveyors,engineering & mining surveyors,surveyors--engineering & mining,mining & engineering surveyors,surveyors--engineering,surveyors--land civil engineering"
    },
    {
        "id": 780,
        "categoryId": 25,
        "subCategoryName": "Engineering Machine Tools",
        "icon": null,
        "clues": "Engineering Machine Tools,machine engineering,engineering tools,engineering machinery,engineering machine shop"
    },
    {
        "id": 808,
        "categoryId": 25,
        "subCategoryName": "Expanded Metals",
        "icon": null,
        "clues": "expanded mesh,expanded metal,Expanded Metals,expanded metal products,metals--expanded,expanded metal mesh"
    },
    {
        "id": 810,
        "categoryId": 25,
        "subCategoryName": "Explosives",
        "icon": null,
        "clues": "Explosives,explosives"
    },
    {
        "id": 815,
        "categoryId": 25,
        "subCategoryName": "Fabric Bonding & Coating",
        "icon": null,
        "clues": "fabric bonding & coating,Fabric Bonding & Coating"
    },
    {
        "id": 816,
        "categoryId": 25,
        "subCategoryName": "Fabrics--Industrial",
        "icon": null,
        "clues": "Fabrics--Industrial,industrial fabrics,fabrics--industrial"
    },
    {
        "id": 820,
        "categoryId": 25,
        "subCategoryName": "Factory & Workshop Equipment Hire",
        "icon": null,
        "clues": "factory equipment hire,hire--factory & workshop equipment,Factory & Workshop Equipment Hire,factory equipment"
    },
    {
        "id": 827,
        "categoryId": 25,
        "subCategoryName": "Farm & Agricultural Advisory Services",
        "icon": null,
        "clues": "Farm & Agricultural Advisory Services,farm & agricultural advisor services,farm & agricultural advisory services"
    },
    {
        "id": 828,
        "categoryId": 25,
        "subCategoryName": "Farm & Agricultural Machinery",
        "icon": null,
        "clues": "used farm machinery,second hand farm machinery,farm machinery repair,farm machinery hire,farm machinery,farm machinery dealers,farm machinery equipment,farm machinery sales,Farm & Agricultural Machinery"
    },
    {
        "id": 829,
        "categoryId": 25,
        "subCategoryName": "Farm Contracting Services",
        "icon": null,
        "clues": "Farm Contracting Services,farm contracting services hay baling,farm contracting services"
    },
    {
        "id": 830,
        "categoryId": 25,
        "subCategoryName": "Farm Supplies & Farming Equipment",
        "icon": null,
        "clues": "farm equipment & supplies,farm equipment & supplies fencing supplies,Farm Supplies & Farming Equipment,farm equipment hire,farm supplies"
    },
    {
        "id": 831,
        "categoryId": 25,
        "subCategoryName": "Farmers",
        "icon": null,
        "clues": "Farmers,farmers"
    },
    {
        "id": 842,
        "categoryId": 25,
        "subCategoryName": "Felt Wholesalers & Manufacturers",
        "icon": null,
        "clues": "Felt Wholesalers & Manufacturers,felt w'salers & mfrs,wool felt,felt,felt suppliers,pool table felt,felt bikes"
    },
    {
        "id": 848,
        "categoryId": 25,
        "subCategoryName": "Fertiliser & Insecticide",
        "icon": null,
        "clues": "Fertiliser & Insecticide,transport - heavy fertiliser"
    },
    {
        "id": 849,
        "categoryId": 25,
        "subCategoryName": "Fertilisers",
        "icon": null,
        "clues": "Fertilisers"
    },
    {
        "id": 853,
        "categoryId": 25,
        "subCategoryName": "Fibreglass Fabricators",
        "icon": null,
        "clues": "Fibreglass Fabricators,fibreglass fabricators"
    },
    {
        "id": 854,
        "categoryId": 25,
        "subCategoryName": "Fibreglass Materials",
        "icon": null,
        "clues": "fibreglass materials,Fibreglass Materials"
    },
    {
        "id": 855,
        "categoryId": 25,
        "subCategoryName": "Fibreglass Repairs & Products",
        "icon": null,
        "clues": "panel beaters & painters fibreglass repairs,fibreglass products,fibreglass repair,fibreglass products & repairs,fibreglass products & repair,Fibreglass Repairs & Products,fibreglass repair products"
    },
    {
        "id": 869,
        "categoryId": 25,
        "subCategoryName": "Fire Brick Manufacturers & Repairers",
        "icon": null,
        "clues": "fire brick manufacturers & repair,Fire Brick Manufacturers & Repairers"
    },
    {
        "id": 899,
        "categoryId": 25,
        "subCategoryName": "Flour Wholesalers & Manufacturers",
        "icon": null,
        "clues": "flour,flour w'salers & mfrs,flour wholesalers,flour suppliers,Flour Wholesalers & Manufacturers,flour mills"
    },
    {
        "id": 905,
        "categoryId": 25,
        "subCategoryName": "Foam Fabricators & Processors",
        "icon": null,
        "clues": "Foam Fabricators & Processors,foam fabricators & processors"
    },
    {
        "id": 908,
        "categoryId": 25,
        "subCategoryName": "Folding Machines",
        "icon": null,
        "clues": "Folding Machines,paper folding machines,folding machines"
    },
    {
        "id": 918,
        "categoryId": 25,
        "subCategoryName": "Footwear Manufacturers Supplies",
        "icon": null,
        "clues": "Footwear Manufacturers Supplies,dancewear,accessories & supplies footwear,footwear manufacturer supplies,footwear mfrs' supplies"
    },
    {
        "id": 919,
        "categoryId": 25,
        "subCategoryName": "Footwear Wholesalers & Manufacturers",
        "icon": null,
        "clues": "rockport footwear,safety footwear,footwear--w'salers & mfrs,footwear manufacturers,footwear - upper manufacturers,Footwear Wholesalers & Manufacturers,footwear--orthopaedic or custom made,footwear wholesalers,footwear wholesale,footwear-retail,footwear importers & wholesalers"
    },
    {
        "id": 923,
        "categoryId": 25,
        "subCategoryName": "Forgings",
        "icon": null,
        "clues": "Forgings,forgings"
    },
    {
        "id": 927,
        "categoryId": 25,
        "subCategoryName": "Foundry & Non-Ferrous Metals",
        "icon": null,
        "clues": "foundry,Foundry & Non-Ferrous Metals"
    },
    {
        "id": 928,
        "categoryId": 25,
        "subCategoryName": "Foundry Equipment & Supplies",
        "icon": null,
        "clues": "foundry equipment & supplies,foundry supplies,foundry equipment,Foundry Equipment & Supplies"
    },
    {
        "id": 968,
        "categoryId": 25,
        "subCategoryName": "Furriers",
        "icon": null,
        "clues": "Furriers,furriers"
    },
    {
        "id": 971,
        "categoryId": 25,
        "subCategoryName": "Game Farmers & Dealers",
        "icon": null,
        "clues": "games shop,computer games,Game Farmers & Dealers,games,game farmers & dealers,video games"
    },
    {
        "id": 995,
        "categoryId": 25,
        "subCategoryName": "Gear Manufacturers & Wholesalers",
        "icon": null,
        "clues": "gear manufacturers,Gear Manufacturers & Wholesalers"
    },
    {
        "id": 1016,
        "categoryId": 25,
        "subCategoryName": "Glass Bricks & Accessories",
        "icon": null,
        "clues": "glass bricks & accessories,glass bricks,Glass Bricks & Accessories"
    },
    {
        "id": 1017,
        "categoryId": 25,
        "subCategoryName": "Glass Etching",
        "icon": null,
        "clues": "glass etching,Glass Etching"
    },
    {
        "id": 1018,
        "categoryId": 25,
        "subCategoryName": "Glass Manufacturing & Processing",
        "icon": null,
        "clues": "Glass Manufacturing & Processing,glass processing machinery,glass processing,glass processing & manufacturers,glass processing & mfrg"
    },
    {
        "id": 1030,
        "categoryId": 25,
        "subCategoryName": "Gold Prospecting Equipment",
        "icon": null,
        "clues": "Gold Prospecting Equipment,gold prospecting,gold prospecting equipment"
    },
    {
        "id": 1039,
        "categoryId": 25,
        "subCategoryName": "Grain & Produce",
        "icon": null,
        "clues": "barley grain & produce wholesale,barley grain & produce retail,Grain & Produce,grain & produce - wholesale,grain & produce,grain & produce--retail"
    },
    {
        "id": 1049,
        "categoryId": 25,
        "subCategoryName": "Grinding & Pulverising Machinery",
        "icon": null,
        "clues": "Concrete Cutting,Grinding & Drilling,grinding & pulverising machinery,grinding burrs,Grinding & Pulverising Machinery,grinding concrete,grinding machine"
    },
    {
        "id": 1050,
        "categoryId": 25,
        "subCategoryName": "Grinding--Cylinder & Crankshaft",
        "icon": null,
        "clues": "Grinding--Cylinder & Crankshaft,grinding--cylinder & crankshaft,grinding--cylinder"
    },
    {
        "id": 1079,
        "categoryId": 25,
        "subCategoryName": "Hardware Manufacturers",
        "icon": null,
        "clues": "hardware manufacturers,furniture hardware manufacturers,Hardware Manufacturers"
    },
    {
        "id": 1146,
        "categoryId": 25,
        "subCategoryName": "Hosiery Wholesalers & Manufacturers",
        "icon": null,
        "clues": "hosiery wholesalers,lingerie,sleepwear & hosiery--retail,lingerie,sleepwear & hosiery--retail bras,hosiery w'salers & mfrs,hosiery wholesale,Lingerie,Sleepwear & Hosiery,hosiery,hosiery manufacturers,Hosiery Wholesalers & Manufacturers"
    },
    {
        "id": 1165,
        "categoryId": 25,
        "subCategoryName": "Hydroponic Producers",
        "icon": null,
        "clues": "hydroponic wholesale,Hydroponic Producers,hydroponic equipment,hydroponic store,hydroponic lights,hydroponic,hydroponic supplies,hydroponic producers"
    },
    {
        "id": 1166,
        "categoryId": 25,
        "subCategoryName": "Hydroponics & Hydroponic Equipment",
        "icon": null,
        "clues": "hydroponics--equipment,hydroponics--equipment & supplies,Hydroponics & Hydroponic Equipment"
    },
    {
        "id": 1188,
        "categoryId": 25,
        "subCategoryName": "Industrial Capacitors",
        "icon": null,
        "clues": "Industrial Capacitors,capacitors--industrial"
    },
    {
        "id": 1189,
        "categoryId": 25,
        "subCategoryName": "Industrial Chemicals",
        "icon": null,
        "clues": "chemicals--industrial,industrial chemicals suppliers,industrial chemicals,Industrial Chemicals"
    },
    {
        "id": 1201,
        "categoryId": 25,
        "subCategoryName": "Insect Control Equipment",
        "icon": null,
        "clues": "Insect Control Equipment,insect screens,insect screens doors,insect fumigation,Insecticides,Herbicides & Fungicides,insect screens repairs,retractable insect screens,insect control,insect,insect screen door,insect control equipment,insect spray"
    },
    {
        "id": 1202,
        "categoryId": 25,
        "subCategoryName": "Insecticides, Herbicides & Fungicides",
        "icon": null,
        "clues": "Insecticides,Herbicides & Fungicides,insecticides,herbicides & fungicides"
    },
    {
        "id": 1209,
        "categoryId": 25,
        "subCategoryName": "Insulation Material Manufacturers & Wholesalers",
        "icon": null,
        "clues": "Insulation Material Manufacturers & Wholesalers,insulation manufacturers,insulation materials,insulation wholesalers,insulation materials--retail batts,insulation materials--retail insulation solutions,insulation materials - wholesalers,insulation materials - wholesalers & manufacturers,polycarbonate insulation materials,insulation materials--retail,insulation materials--retail csr"
    },
    {
        "id": 1229,
        "categoryId": 25,
        "subCategoryName": "Irrigation & Reticulation Systems",
        "icon": null,
        "clues": "irrigation & reticulation systems repairs,irrigation systems,irrigation,irrigation & reticulation,Irrigation & Reticulation Systems,irrigation & reticulation systems pumps,irrigation pumps,irrigation & reticulation systems boring,irrigation & reticulation systems,irrigation supplies"
    },
    {
        "id": 1249,
        "categoryId": 25,
        "subCategoryName": "Kilns--Drying & Equipment",
        "icon": null,
        "clues": "kilns--drying & equipment,Kilns--Drying & Equipment"
    },
    {
        "id": 1295,
        "categoryId": 25,
        "subCategoryName": "Lead Supplies & Products",
        "icon": null,
        "clues": "lead suppliers,lead supplies & products,Lead Supplies & Products"
    },
    {
        "id": 1299,
        "categoryId": 25,
        "subCategoryName": "Leather Clothing Wholesalers & Manufacturers",
        "icon": null,
        "clues": "leather clothing,clothing alterations & mending leather,Leather Clothing Wholesalers & Manufacturers,leather furniture manufacturers,leather manufacturers,leather lounge manufacturers,leather wholesalers,leather goods - wholesalers & manufacturers,leather clothing - wholesalers & manufacturers,leather goods manufacturers,leather clothing repair,leather jackets,leather goods - wholesalers,leather gloves manufacturers,leather clothing alterations"
    },
    {
        "id": 1332,
        "categoryId": 25,
        "subCategoryName": "Livestock Buyers",
        "icon": null,
        "clues": "Livestock Buyers,livestock buyers"
    },
    {
        "id": 1333,
        "categoryId": 25,
        "subCategoryName": "Livestock Transport Services",
        "icon": null,
        "clues": "transport livestock,livestock transport,livestock transport services,Livestock Transport Services,livestock transportation"
    },
    {
        "id": 1344,
        "categoryId": 25,
        "subCategoryName": "Lubrication Equipment Wholesalers & Manufacturers",
        "icon": null,
        "clues": "lubrication redline,lubrication equipment wholesalers & manufacturers,Lubrication Equipment Wholesalers & Manufacturers,lubrication equipment,lubrication oil,lubrication equipment wholesalers,lubrication"
    },
    {
        "id": 1346,
        "categoryId": 25,
        "subCategoryName": "Machinery Reconditioners",
        "icon": null,
        "clues": "Machinery Reconditioners,machinery reconditioners"
    },
    {
        "id": 1347,
        "categoryId": 25,
        "subCategoryName": "Machinery--Specially Designed & Manufactured",
        "icon": null,
        "clues": "machinery--specially designed & manufactured,machinery--specially designed,Machinery--Specially Designed & Manufactured"
    },
    {
        "id": 1349,
        "categoryId": 25,
        "subCategoryName": "Magnetic Equipment Wholesalers & Manufacturers",
        "icon": null,
        "clues": "magnetic equipment,magnetic signs,magnetic,magnetic equipment wholesalers,Magnetic Equipment Wholesalers & Manufacturers,magnetic equipment wholesalers & manufacturers"
    },
    {
        "id": 1363,
        "categoryId": 25,
        "subCategoryName": "Manchester Wholesalers & Manufacturers",
        "icon": null,
        "clues": "manchester wholesalers,manchester manufacturers,Manchester Wholesalers & Manufacturers,manchester--retail"
    },
    {
        "id": 1365,
        "categoryId": 25,
        "subCategoryName": "Manufacturers' Agents",
        "icon": null,
        "clues": "Manufacturers' Agents,manufacturers agents,manufacturers' agents"
    },
    {
        "id": 1401,
        "categoryId": 25,
        "subCategoryName": "Medals & Medal Mounting",
        "icon": null,
        "clues": "Medals & Medal Mounting,medals,medals & medal mounting,replica medals,military medals,mounting medals,war medals,medals mounting,medals & trophies"
    },
    {
        "id": 1422,
        "categoryId": 25,
        "subCategoryName": "Metal Cutting Equipment",
        "icon": null,
        "clues": "metal cutting fabrication,steel metal cutting,plasma metal cutting,metal oxy cutting,Metal Cutting Equipment,metal cutting cnc,metal cutting bandsaw,metal cutting,metal cutting equipment,metal cutting services feature cutting,metal cutting services laser,metal cutting laser,metal laser cutting,metal cutting services water-jet,router cutting metal cutting services,metal cutting tools,metal cutting machines second hand"
    },
    {
        "id": 1423,
        "categoryId": 25,
        "subCategoryName": "Metal Cutting Services",
        "icon": null,
        "clues": "metal cutting fabrication,cutting services metal,steel metal cutting,plasma metal cutting,metal oxy cutting,metal cutting services,metal cutting cnc,metal cutting bandsaw,metal cutting,metal cutting services feature cutting,metal cutting services laser,metal cutting laser,metal laser cutting,cutting services metal laser,metal cutting services water-jet,router cutting metal cutting services,Metal Cutting Services,metal cutting tools,metal cutting machines second hand"
    },
    {
        "id": 1424,
        "categoryId": 25,
        "subCategoryName": "Metal Finishers' Equipment & Supplies",
        "icon": null,
        "clues": "building supplies metal,sheet metal supplies,metal finishers' equipment & supplies,chrome metal finishers,metal roofing supplies,Metal Finishers' Equipment & Supplies,building supplies sheet metal"
    },
    {
        "id": 1425,
        "categoryId": 25,
        "subCategoryName": "Metal Forming & Rolling",
        "icon": null,
        "clues": "metal rolling,Metal Forming & Rolling,metal rolling & forming,sheet metal rolling,metal forming"
    },
    {
        "id": 1426,
        "categoryId": 25,
        "subCategoryName": "Metal Pipes & Fittings",
        "icon": null,
        "clues": "metal pipes & fittings,pipes & fittings--metal,Metal Pipes & Fittings"
    },
    {
        "id": 1427,
        "categoryId": 25,
        "subCategoryName": "Metal Polishing",
        "icon": null,
        "clues": "metal polishing equipment,metal polishing,polishing metal,Metal Polishing,mobile metal polishing,polishing metal tube"
    },
    {
        "id": 1428,
        "categoryId": 25,
        "subCategoryName": "Metal Powder Fabricators",
        "icon": null,
        "clues": "Metal Powder Fabricators,metal powder fabricators,powder coated metal tubing,metal powder coating services,metal powder coating,steel fabricators & manufacturers sheet metal"
    },
    {
        "id": 1429,
        "categoryId": 25,
        "subCategoryName": "Metal Recyclers & Merchants",
        "icon": null,
        "clues": "steel merchants sheet metal,scrap metal merchants batteries,metal recyclers,scrap metal merchants steel,non ferrous metal merchants,scrap metal merchants recycling,scrap metal merchants lead,metal recyclers services,metal merchants,metal scrap merchants,steel merchants perforated metal,scrap metal recyclers,scrap metal merchants pick-up,recyclers metal,metal suppliers & merchants,Metal Recyclers & Merchants,sheet metal merchants,scrap metal merchants aluminium"
    },
    {
        "id": 1430,
        "categoryId": 25,
        "subCategoryName": "Metal Spinning & Spinners",
        "icon": null,
        "clues": "metal spinners,Metal Spinning & Spinners"
    },
    {
        "id": 1431,
        "categoryId": 25,
        "subCategoryName": "Metal Sprayers",
        "icon": null,
        "clues": "Metal Sprayers,metal sprayers"
    },
    {
        "id": 1432,
        "categoryId": 25,
        "subCategoryName": "Metal Spraying Equipment & Supplies",
        "icon": null,
        "clues": "metal spraying equipment & supplies,building supplies metal,sheet metal supplies,metal roofing supplies,Metal Spraying Equipment & Supplies,building supplies sheet metal"
    },
    {
        "id": 1433,
        "categoryId": 25,
        "subCategoryName": "Metal Stamping & Pressing",
        "icon": null,
        "clues": "metal pressing,metal stamping & pressing,sheet metal pressing,precision metal stamping,Metal Stamping & Pressing,metal stamping"
    },
    {
        "id": 1434,
        "categoryId": 25,
        "subCategoryName": "Metal Workers",
        "icon": null,
        "clues": "sheet metal workers ductwork,sheet metal workers tool boxes,Metal Workers,sheet metal workers roofing materials,metal workers,sheet metal workers' machinery,sheet metal workers,sheet metal workers manufacturing"
    },
    {
        "id": 1436,
        "categoryId": 25,
        "subCategoryName": "Metals--Non-Ferrous",
        "icon": null,
        "clues": ""
    },
    {
        "id": 1446,
        "categoryId": 25,
        "subCategoryName": "Mineral Exploration",
        "icon": null,
        "clues": "mineral exploration,mineral exploration drilling,Mineral Exploration,boring contractors mineral exploration"
    },
    {
        "id": 1447,
        "categoryId": 25,
        "subCategoryName": "Minerals & Mineral Earths",
        "icon": null,
        "clues": "minerals,minerals & mineral earths,Minerals & Mineral Earths"
    },
    {
        "id": 1448,
        "categoryId": 25,
        "subCategoryName": "Mining Companies",
        "icon": null,
        "clues": "Mining Companies,mining companies"
    },
    {
        "id": 1449,
        "categoryId": 25,
        "subCategoryName": "Mining Contractors",
        "icon": null,
        "clues": "mining contractors,employment--labour hire contractors mining,excavating & earth moving contractors mining,Mining Contractors,mining earth moving contractors,electrical mining contractors,coal mining contractors,underground mining contractors,mining contractors earthmoving"
    },
    {
        "id": 1450,
        "categoryId": 25,
        "subCategoryName": "Mining Engineers",
        "icon": null,
        "clues": "heavy engineering mining engineers,consulting design engineers mining,consulting mechanical mining engineers,mining consulting engineers,Mining Engineers,consulting engineers mining,mining engineers"
    },
    {
        "id": 1451,
        "categoryId": 25,
        "subCategoryName": "Mining, Quarrying Equipment & Supplies",
        "icon": null,
        "clues": "quarrying,mining & quarrying supplies,Mining,Quarrying Equipment & Supplies,mining & quarrying equipment & supplies,mining & quarrying equipment,mining & quarrying,mining equipment & quarrying & supplies"
    },
    {
        "id": 1454,
        "categoryId": 25,
        "subCategoryName": "Mixing Equipment--Industrial",
        "icon": null,
        "clues": "paint mixing machines,concrete mixing equipment &/or vibrators,chemical mixing,mixing equipment,mixing equipment--industrial,Mixing Equipment--Industrial,concrete mixing equipment,thermostatic mixing valves,paint mixing,asphalt mixing equipment,concrete mixing machine"
    },
    {
        "id": 1516,
        "categoryId": 25,
        "subCategoryName": "Natural Stone Products & Supplies",
        "icon": null,
        "clues": "natural beauty products,natural health products,natural hair products,natural stone,natural products,natural stone importers,natural health supplies,Natural Stone Products & Supplies,natural stone paving,natural stone construction,natural stone suppliers,natural stone tiles,natural body products"
    },
    {
        "id": 1529,
        "categoryId": 25,
        "subCategoryName": "Nickel &/or Alloys",
        "icon": null,
        "clues": "nickel &/or alloys,nickel,Nickel &/or Alloys,nickel mining,nickel plating"
    },
    {
        "id": 1553,
        "categoryId": 25,
        "subCategoryName": "Oil Drilling Equipment & Supplies",
        "icon": null,
        "clues": "oil drilling equipment & supplies,Oil Drilling Equipment & Supplies,oil & chemical spill equipment,oil drilling equipment,oil burners & equipment,oil drilling"
    },
    {
        "id": 1554,
        "categoryId": 25,
        "subCategoryName": "Oil Exploration",
        "icon": null,
        "clues": "oil & gas exploration,oil exploration,Oil Exploration,oil drilling & exploration"
    },
    {
        "id": 1555,
        "categoryId": 25,
        "subCategoryName": "Oil Merchants & Refiners",
        "icon": null,
        "clues": "oil merchants,oil merchants &/or refiners,Oil Merchants & Refiners,oil merchants &/or refiners waste removal"
    },
    {
        "id": 1599,
        "categoryId": 25,
        "subCategoryName": "Paint Manufacturers & Wholesalers",
        "icon": null,
        "clues": "auto paint manufacturers,paint wholesalers & manufacturers,antifouling paint manufacturers,Paint Manufacturers & Wholesalers,paint manufacturers,specialist effects paint manufacturers,paint wholesalers"
    },
    {
        "id": 1615,
        "categoryId": 25,
        "subCategoryName": "Paper Mill Agents",
        "icon": null,
        "clues": "Paper Mill Agents,paper mill agents,paper mill,news agents paper shop"
    },
    {
        "id": 1617,
        "categoryId": 25,
        "subCategoryName": "Paper Wholesalers & Manufacturers",
        "icon": null,
        "clues": "toilet paper manufacturers,paper wholesalers,paper bag wholesalers,wrapping paper wholesalers,paper bag manufacturers,toilet paper wholesalers,Paper Wholesalers & Manufacturers,paper manufacturers"
    },
    {
        "id": 1695,
        "categoryId": 25,
        "subCategoryName": "Pipe Fabricators",
        "icon": null,
        "clues": "pipe fabricators,Pipe Fabricators"
    },
    {
        "id": 1698,
        "categoryId": 25,
        "subCategoryName": "Pipe Organ Builders, Tuners & Restorers",
        "icon": null,
        "clues": "pipe organ builders,tuners & restorers,Pipe Organ Builders,Tuners & Restorers,hi-fi equipment tuners,piano tuners guild"
    },
    {
        "id": 1712,
        "categoryId": 25,
        "subCategoryName": "Plastic Extrusion",
        "icon": null,
        "clues": "Plastic Extrusion,plastic extrusions manufacturers,plastic extrusions"
    },
    {
        "id": 1713,
        "categoryId": 25,
        "subCategoryName": "Plastic Fabrication",
        "icon": null,
        "clues": "plastic pipe fabrication,Plastic Fabrication,plastic fabrication,plastic fabrication suppliers"
    },
    {
        "id": 1714,
        "categoryId": 25,
        "subCategoryName": "Plastic Moulding",
        "icon": null,
        "clues": "plastic moulding machines,mouldings--plastic & wood,mouldings--plastic,plastic moulding,Plastic Moulding,plastic product injection moulding,injection moulding plastic industry,plastic injection moulding,plastic & medical moulding"
    },
    {
        "id": 1717,
        "categoryId": 25,
        "subCategoryName": "Plastic Raw Materials",
        "icon": null,
        "clues": "plastic raw materials,packaging materials plastic,Plastic Raw Materials,plastic materials"
    },
    {
        "id": 1720,
        "categoryId": 25,
        "subCategoryName": "Plastics Machinery & Equipment",
        "icon": null,
        "clues": "plastics--machinery & equipment,Plastics Machinery & Equipment"
    },
    {
        "id": 1721,
        "categoryId": 25,
        "subCategoryName": "Plastics Products Manufacturers & Wholesalers",
        "icon": null,
        "clues": "plastics - products - wholesalers,plastics products wholesalers & manufacturers sheets,manufacturers plastics,plastics -- products -- retail perspex,plastics products wholesalers & manufacturers,plastics -- products -- retail,manufacturers plastics factory,plastics products wholesalers & manufacturers perspex,plastics products manufacturers,plastics products wholesalers & manufacturers packaging,plastics products wholesalers & manufacturers acrylic,Plastics Products Manufacturers & Wholesalers"
    },
    {
        "id": 1722,
        "categoryId": 25,
        "subCategoryName": "Plastics--Coaters",
        "icon": null,
        "clues": "plastics--coaters,Plastics--Coaters"
    },
    {
        "id": 1723,
        "categoryId": 25,
        "subCategoryName": "Plastics--Toolmakers & Engineers",
        "icon": null,
        "clues": "Plastics--Toolmakers & Engineers,plastics--toolmakers & engineers"
    },
    {
        "id": 1736,
        "categoryId": 25,
        "subCategoryName": "Polystyrene Products",
        "icon": null,
        "clues": "polystyrene suppliers,polystyrene cladding,polystyrene manufacturers,polystyrene,polystyrene boxes,polystyrene panels,polystyrene mouldings,polystyrene sheets,polystyrene packaging,polystyrene foam,polystyrene insulation,polystyrene cutting,Polystyrene Products,polystyrene products,polystyrene recyclers"
    },
    {
        "id": 1737,
        "categoryId": 25,
        "subCategoryName": "Polyurethane Products",
        "icon": null,
        "clues": "polyurethane foam,polyurethane coatings,polyurethane insulation,polyurethane lining,polyurethane sheet,polyurethane casting,polyurethane products,polyurethane door,Polyurethane Products,polyurethane spray,polyurethane screens,polyurethane floor finishes,polyurethane spray painting,polyurethane,polyurethane painters"
    },
    {
        "id": 1747,
        "categoryId": 25,
        "subCategoryName": "Potato Growers",
        "icon": null,
        "clues": "potato farmers,potato farms,Potato Growers,potato growers"
    },
    {
        "id": 1754,
        "categoryId": 25,
        "subCategoryName": "Powder Coating",
        "icon": null,
        "clues": "sandblasting powder coating,powder coating services,Powder Coating,aluminium powder coating,laser powder coating services,powder coated,metal powder coating services,industrial powder coating,metal powder coating"
    },
    {
        "id": 1755,
        "categoryId": 25,
        "subCategoryName": "Powder Coating Supplies & Equipment",
        "icon": null,
        "clues": "sandblasting powder coating,Powder Coating Supplies & Equipment,powder coating equipment,powder coating supplies & equipment,powder coating suppliers,industrial powder coating,powder coating supplies & services,powder coating services,aluminium powder coating,laser powder coating services,powder coated,metal powder coating services,metal powder coating"
    },
    {
        "id": 1756,
        "categoryId": 25,
        "subCategoryName": "Power Steering",
        "icon": null,
        "clues": "power steering exchange,power steering services,power steering pump,power steering racks,power steering,power steering pumps,power steering repair,Power Steering,power steering specialist,power steering reconditioning,power steering hoses,reconditioning power steering"
    },
    {
        "id": 1771,
        "categoryId": 25,
        "subCategoryName": "Primary Production & Rural Organisations",
        "icon": null,
        "clues": "organisations--primary production & rural,Primary Production & Rural Organisations,organisations--primary production"
    },
    {
        "id": 1775,
        "categoryId": 25,
        "subCategoryName": "Printers--Glass",
        "icon": null,
        "clues": "Printers--Glass,printers--glass"
    },
    {
        "id": 1815,
        "categoryId": 25,
        "subCategoryName": "Pump Manufacturers, Sales & Service",
        "icon": null,
        "clues": "pumps - manufacturers & merchants water supply,water pump manufacturers,pump services,pump manufacturers,pump sales,Pump Manufacturers,Sales & Service,pump manufacturers suppliers,water pump sales"
    },
    {
        "id": 1822,
        "categoryId": 25,
        "subCategoryName": "Quarries",
        "icon": null,
        "clues": "Quarries,sandstone quarries,quarries stone products,quarries,sand quarries,limestone quarries"
    },
    {
        "id": 1862,
        "categoryId": 25,
        "subCategoryName": "Refractories",
        "icon": null,
        "clues": "refractories,Refractories"
    },
    {
        "id": 1884,
        "categoryId": 25,
        "subCategoryName": "Resins & Resin Products",
        "icon": null,
        "clues": "resins & resin products,Resins & Resin Products"
    },
    {
        "id": 1922,
        "categoryId": 25,
        "subCategoryName": "Rubber Products Manufacturers & Wholesalers",
        "icon": null,
        "clues": "rubber manufacturers,rubber manufacturers & wholesalers,Rollers--Wooden,Metal,Rubber Etc.,Rubber Products Manufacturers & Wholesalers,rubber wholesalers,rubber flooring,rubber products - wholesalers,rubber matting,rubber stamps,foam rubber,rubber stamp manufacturers,rubber manufacturers international,rubber products--retail,rubber,rubber products--w'salers & mfrs,rubber products manufacturers"
    },
    {
        "id": 1941,
        "categoryId": 25,
        "subCategoryName": "Salt Wholesalers & Manufacturers",
        "icon": null,
        "clues": "Salt Wholesalers & Manufacturers"
    },
    {
        "id": 1950,
        "categoryId": 25,
        "subCategoryName": "Sawmilling & Logging Equipment",
        "icon": null,
        "clues": "sawmilling equipment,sawmilling & logging equipment,Sawmilling & Logging Equipment"
    },
    {
        "id": 1951,
        "categoryId": 25,
        "subCategoryName": "Sawmills",
        "icon": null,
        "clues": "Sawmills"
    },
    {
        "id": 1967,
        "categoryId": 25,
        "subCategoryName": "Scrap Metal Merchants",
        "icon": null,
        "clues": "scrap metal merchants batteries,Scrap Metal Merchants,scrap metal merchants steel,scrap metal removals,scrap metal merchants recycling,scrap metal,scrap metal merchants lead,scrap metal merchants free car removals,scrap metal pickup,scrap merchants,scrap metal yards,metal scrap merchants,scrap metal buyers,scrap metal recyclers,scrap metal dealers,scrap metal merchants pick-up,scrap batteries merchants,scrap metal merchants,scrap metal merchants aluminium,scrap steel merchants"
    },
    {
        "id": 1973,
        "categoryId": 25,
        "subCategoryName": "Screw Wholesalers & Manufacturers",
        "icon": null,
        "clues": "screw conveyors,screw wholesalers,Screw Wholesalers & Manufacturers,screws,screw piles,screw piers"
    },
    {
        "id": 1995,
        "categoryId": 25,
        "subCategoryName": "Seed Suppliers",
        "icon": null,
        "clues": "Seed Suppliers,bird seed suppliers,grass seed suppliers,barley seed suppliers,lawn seed suppliers,seed suppliers"
    },
    {
        "id": 2009,
        "categoryId": 25,
        "subCategoryName": "Sewing Contractors--Industrial",
        "icon": null,
        "clues": "sewing contractors--industrial,Sewing Contractors--Industrial"
    },
    {
        "id": 2020,
        "categoryId": 25,
        "subCategoryName": "Sheep Shearing Contractors",
        "icon": null,
        "clues": "Sheep Shearing Contractors,sheep shearing contractors"
    },
    {
        "id": 2021,
        "categoryId": 25,
        "subCategoryName": "Sheep Shearing Equipment & Supplies",
        "icon": null,
        "clues": "Sheep Shearing Equipment & Supplies,sheep shearing equipment & supplies,sheep shearing equipment"
    },
    {
        "id": 2023,
        "categoryId": 25,
        "subCategoryName": "Sheet Metal Fabricators",
        "icon": null,
        "clues": "sheet metal,sheet metal fabrication,sheet metal workers ductwork,sheet metal manufacturers,roof construction sheet metal,sheet metal supplies,sheet metal workers' machinery,steel fabricators & manufacturers sheet metal,sheet metal workers,sheet metal workers manufacturing,Sheet Metal Fabricators"
    },
    {
        "id": 2024,
        "categoryId": 25,
        "subCategoryName": "Sheet Metal Machinery",
        "icon": null,
        "clues": "sheet metal,sheet metal fabrication,sheet metal workers ductwork,sheet metal manufacturers,roof construction sheet metal,sheet metal supplies,Sheet Metal Machinery,sheet metal workers' machinery,sheet metal workers,sheet metal workers manufacturing"
    },
    {
        "id": 2027,
        "categoryId": 25,
        "subCategoryName": "Sheltered Workshops",
        "icon": null,
        "clues": "sheltered workshops,Sheltered Workshops"
    },
    {
        "id": 2048,
        "categoryId": 25,
        "subCategoryName": "Silicone Supplies & Products",
        "icon": null,
        "clues": "silicone,promotional products silicone bracelets,silicone hose,silicone supplies & products,silicone tubing,Silicone Supplies & Products,silicone oil,silicone sealing,silicone supplies,silicone bracelets,silicone sealant,silicone bakeware,silicone rubber,silicone carbide,silicone wristbands,silicone moulds,silicone coating"
    },
    {
        "id": 2050,
        "categoryId": 25,
        "subCategoryName": "Silos",
        "icon": null,
        "clues": "grain silos,grain silos manufacturers,Silos,silos - wholesalers & manufacturers,silos"
    },
    {
        "id": 2070,
        "categoryId": 25,
        "subCategoryName": "Smallgoods Wholesalers & Manufacturers",
        "icon": null,
        "clues": "smallgoods manufacturers' supplies,smallgoods wholesalers,Smallgoods Wholesalers & Manufacturers,smallgoods manufacturers,smallgoods wholesalers & manufacturers"
    },
    {
        "id": 2078,
        "categoryId": 25,
        "subCategoryName": "Soft Drink Manufacturers, Distributors & Supplies",
        "icon": null,
        "clues": "soft drink,soft drinks manufacturers' supplies,equipment,soft drink wholesale,soft drink factory,soft drink manufacturers' supplies & machinery,Soft Drink Manufacturers,Distributors & Supplies,soft drink wholesalers,soft ice cream manufacturers,soft drink production,soft drink suppliers,soft drink mfrs,distributors & supplies,soft drink distributors,soft drink manufacturers & distributors,soft drink manufacturers,soft drinks distributors,soft drink manufacturers,distributors & suppliers"
    },
    {
        "id": 2080,
        "categoryId": 25,
        "subCategoryName": "Soil Conditioners & Treatments",
        "icon": null,
        "clues": "Soil Conditioners & Treatments,soil conditioners & treatments"
    },
    {
        "id": 2085,
        "categoryId": 25,
        "subCategoryName": "Solvents",
        "icon": null,
        "clues": "Solvents,solvents suppliers,solvents,solvents recyclers"
    },
    {
        "id": 2104,
        "categoryId": 25,
        "subCategoryName": "Sports Gear Wholesalers & Manufacturers",
        "icon": null,
        "clues": "sports gear,sports nets - wholesalers & manufacturers,sports goods wholesalers.distributers,sports clothing manufacturers,sports lighting manufacturers,Sports Gear Wholesalers & Manufacturers,motor sports gear,water sports gear,manufacturers of sports goods"
    },
    {
        "id": 2107,
        "categoryId": 25,
        "subCategoryName": "Sports Nets Wholesalers & Manufacturers",
        "icon": null,
        "clues": "sports nets,Sports Nets Wholesalers & Manufacturers,sports nets - wholesalers & manufacturers,sports goods wholesalers.distributers,sports clothing manufacturers,sports lighting manufacturers,manufacturers of sports goods"
    },
    {
        "id": 2117,
        "categoryId": 25,
        "subCategoryName": "Spring Manufacturers & Wholesalers",
        "icon": null,
        "clues": "spring wholesalers & manufacturing,Spring Manufacturers & Wholesalers,spring wholesalers & manufacturers trailers,spring wholesalers & manufacturers,spring manufacturers,spring wholesalers"
    },
    {
        "id": 2130,
        "categoryId": 25,
        "subCategoryName": "Stationery--W'sale & Mfrg",
        "icon": null,
        "clues": "stationery wholesalers,wholesale stationery,wholesale stationery suppliers,wedding stationery wholesale,stationery - wholesale,wholesale wedding stationery,wedding stationery wholesalers"
    },
    {
        "id": 2131,
        "categoryId": 25,
        "subCategoryName": "Steel & Iron Foundry",
        "icon": null,
        "clues": "Steel Rolling Mills,steel,stainless steel,steel foundry,Steel & Iron Foundry,steel & iron,steel merchants,steel fabrication & manufacturers,foundries--iron & steel,steel fabrication,steel supplies,iron & steel,stainless steel fabrication,stainless steel foundry"
    },
    {
        "id": 2132,
        "categoryId": 25,
        "subCategoryName": "Steel Fabrication & Supplies",
        "icon": null,
        "clues": "steel supplies bega,steel pipe fabrication,steel fabricators & manufacturers fabrication,fabrication steel,steel fabrication window bars,steel fabrication beams,stainless steel supplies,steel fabrication & manufacturers,steel fabrication,steel fabricators & manufacturers repetitive engineering fabrication,steel supplies,stainless steel fabrication,steel & stainless steel fabrication,steel supplies sheet metal,building supplies steel,Steel Fabrication & Supplies,structural steel fabrication"
    },
    {
        "id": 2133,
        "categoryId": 25,
        "subCategoryName": "Steel Supplies & Merchants",
        "icon": null,
        "clues": "steel supplies bega,steel merchants metaland,stainless steel supplies,steel merchants,steel beam merchants,steel merchants bluescope steel,steel merchants metalcorp,scrap steel merchants,steel merchants sheet metal,stainless steel merchants,scrap metal merchants steel,steel merchants steel cut to length,steel plate merchants,steel merchants reinforcing products,steel merchants onesteel,steel merchants perforated metal,steel supplies,steel merchants building materials,steel pipe from steel merchants,steel supplies sheet metal,building supplies steel,steel merchants pipes,Steel Supplies & Merchants,steel merchants stainless steel,steel merchants structural steel"
    },
    {
        "id": 2134,
        "categoryId": 25,
        "subCategoryName": "Steel Wool Wholesalers & Manufacturers",
        "icon": null,
        "clues": "steel fabricators & manufacturers stainless steel,manufacturers steel,tubular steel furniture manufacturers,steel fabricators & manufacturers fabrication,steel manufacturers,steel wholesalers,stainless steel wholesalers,steel fabrication & manufacturers,stainless steel manufacturers,steel fabricators & manufacturers repetitive engineering fabrication,steel fabricators & manufacturers,steel & stainless steel & aluminium manufacturers,steel fabricators & manufacturers stairs,Steel Wool Wholesalers & Manufacturers,steel wool w'salers & mfrs,steel fabricators & manufacturers sheet metal,steel fabricators & manufacturers structural steel,steel fabricators & manufacturers railing"
    },
    {
        "id": 2139,
        "categoryId": 25,
        "subCategoryName": "Stock Feeds, Fodder & Supplements",
        "icon": null,
        "clues": "stock feeds & supplements pets,stock feeds & supplements hay and chaff,stock feeds manufacturers,stock feeds & supplements wholesale,grain & fodder,stock agents & supplements,Stock Feeds,Fodder & Supplements,bags & sacks stock feeds,stock feed & supplements,stock feeds & supplements,grain & fodder merchants,grain & fodder crop growers"
    },
    {
        "id": 2142,
        "categoryId": 25,
        "subCategoryName": "Stockyards & Equipment",
        "icon": null,
        "clues": "Stockyards & Equipment,stockyards,stockyards & equipment"
    },
    {
        "id": 2152,
        "categoryId": 25,
        "subCategoryName": "Stud Breeders--Cattle or Sheep",
        "icon": null,
        "clues": "Stud Breeders--Cattle or Sheep,stud breeders--cattle or sheep,stud breeders--cattle,stud breeders--cattle & sheep"
    },
    {
        "id": 2178,
        "categoryId": 25,
        "subCategoryName": "Swimwear Manufacturers & Wholesalers",
        "icon": null,
        "clues": "swimwear manufacturers,Swimwear Manufacturers & Wholesalers,swimwear wholesalers & manufacturers,swimwear wholesalers"
    },
    {
        "id": 2190,
        "categoryId": 25,
        "subCategoryName": "Tanneries",
        "icon": null,
        "clues": "Tanneries,tanneries"
    },
    {
        "id": 2225,
        "categoryId": 25,
        "subCategoryName": "Televisions & Parts - W'salers & Mfrs",
        "icon": null,
        "clues": "televisions & radios,Televisions & Parts - W'salers & Mfrs,televisions & parts - wholesalers  & manufacturers lcd tvs,seconds televisions,electrical appliances--retail televisions,televisions & parts - wholesalers  & manufacturers,televisions & parts - wholesalers  & manufacturers sony,electrical contractors televisions,loewe televisions,lcd televisions & parts wholesalers & manufacturers,used televisions"
    },
    {
        "id": 2236,
        "categoryId": 25,
        "subCategoryName": "Textile Machinery",
        "icon": null,
        "clues": "used textile machinery,textile machinery,Textile Machinery"
    },
    {
        "id": 2249,
        "categoryId": 25,
        "subCategoryName": "Timber Logging",
        "icon": null,
        "clues": "timber logging,Timber Logging"
    },
    {
        "id": 2270,
        "categoryId": 25,
        "subCategoryName": "Towel W'salers & Mfrs",
        "icon": null,
        "clues": "towel hire,towel services,towel rails,towel bathrobes,towel wholesale,towel manufacturers,towel racks,towel embroidery,paper towel dispensers,towel warmers,towel importers,towel dispensers,towels,Towel W'salers & Mfrs,towel wholesalers,towel rental,towel w'salers & mfrs"
    },
    {
        "id": 2271,
        "categoryId": 25,
        "subCategoryName": "Towers",
        "icon": null,
        "clues": "Towers,towers"
    },
    {
        "id": 2275,
        "categoryId": 25,
        "subCategoryName": "Toy Manufacturers",
        "icon": null,
        "clues": "toy manufacturers,Toy Manufacturers"
    },
    {
        "id": 2305,
        "categoryId": 25,
        "subCategoryName": "Travel Goods Wholesalers & Manufacturers",
        "icon": null,
        "clues": "travel goods-retail & repairs,travel goods repair,insurance travel,travel agencies,travel goods - wholesalers & manufacturers,Travel Goods Wholesalers & Manufacturers,travel agents & consultants,travel wholesalers,travel doctor,travel goods,travel,travel goods--retail,travel goods-retail & repairs computer bags"
    },
    {
        "id": 2317,
        "categoryId": 25,
        "subCategoryName": "Tube & Pipe Bending Machinery",
        "icon": null,
        "clues": "aluminium tube & pipe,Tubes--Collapsible,tube stock,Tube & Pipe Bending Machinery,Electric Lamps,Globes & Tubes,tube,tube & pipe bending machinery,tube rolling,tube supplies,pipe & tube,tube fittings"
    },
    {
        "id": 2318,
        "categoryId": 25,
        "subCategoryName": "Tube Bending",
        "icon": null,
        "clues": "Tubes--Collapsible,tube stock,Tube Bending,tube bending,Electric Lamps,Globes & Tubes,tube,tube rolling,tube supplies,tube fittings"
    },
    {
        "id": 2325,
        "categoryId": 25,
        "subCategoryName": "Tungsten Carbide Products & Services",
        "icon": null,
        "clues": "tungsten carbide products,tungsten carbide products & services,tungsten,tungsten ring,tungsten electrode,tungsten carbide coatings,tungsten carbide,Tungsten Carbide Products & Services"
    },
    {
        "id": 2326,
        "categoryId": 25,
        "subCategoryName": "Turbines",
        "icon": null,
        "clues": "turbines,steam turbines,wind turbines,Turbines"
    },
    {
        "id": 2346,
        "categoryId": 25,
        "subCategoryName": "Underground Penetrating Radar & Locators",
        "icon": null,
        "clues": "leak detection underground services,underground service locators leak detectors,underground mining,underground services locator,underground service locators cable locating,underground boring,underground services,underground service locators,underground water tanks,Underground Penetrating Radar & Locators"
    },
    {
        "id": 2365,
        "categoryId": 25,
        "subCategoryName": "Vegetable Growers",
        "icon": null,
        "clues": "Vegetable Growers,vegetable growers,fruit & vegetable growers"
    },
    {
        "id": 2377,
        "categoryId": 25,
        "subCategoryName": "Vibratory Feeding Equipment",
        "icon": null,
        "clues": "vibratory feeding equipment,Vibratory Feeding Equipment,vibratory equipment"
    },
    {
        "id": 2385,
        "categoryId": 25,
        "subCategoryName": "Vinyl Welding & Repairs",
        "icon": null,
        "clues": "Vinyl Welding & Repairs,vinyl floor,vinyl layers,vinyl signs,vinyl repair,vinyl floor coverings,vinyl records,vinyl welding & repairs,vinyl welding,vinyl flooring,vinyl"
    },
    {
        "id": 2455,
        "categoryId": 25,
        "subCategoryName": "Wholesale Grain & Produce",
        "icon": null,
        "clues": "barley grain & produce wholesale,Wholesale Grain & Produce,grain & produce - wholesale,wholesale produce"
    },
    {
        "id": 2463,
        "categoryId": 25,
        "subCategoryName": "Windmills",
        "icon": null,
        "clues": "windmills,Windmills"
    },
    {
        "id": 2477,
        "categoryId": 25,
        "subCategoryName": "Wineries & Vineyards",
        "icon": null,
        "clues": "Wineries & Vineyards,wineries & vineyards"
    },
    {
        "id": 2485,
        "categoryId": 25,
        "subCategoryName": "Wood Benders & Carvers",
        "icon": null,
        "clues": "wood benders & carvers wood carving,wood benders & carvers,wood benders & carvers australian,wood benders & carvers wood artist,wood benders & carvers special projects,Wood Benders & Carvers,wood benders & carvers woodcraft,wood benders & carvers colonial,wood carvers,wood carvers & benders,wood carvers supplies,wood benders & carvers statues,wood benders & carvers corbels,wood benders & carvers interiors,wood benders & carvers furniture,wood carvers tools,wood benders & carvers design"
    },
    {
        "id": 2487,
        "categoryId": 25,
        "subCategoryName": "Wood Machinists",
        "icon": null,
        "clues": "wood machinists,Wood Machinists"
    },
    {
        "id": 2488,
        "categoryId": 25,
        "subCategoryName": "Wood Turners",
        "icon": null,
        "clues": "Wood Turners,wood turners"
    },
    {
        "id": 2492,
        "categoryId": 25,
        "subCategoryName": "Wool Brokers",
        "icon": null,
        "clues": "wool,Wool Brokers,wool shop,wool brokers,knitting wool"
    },
    {
        "id": 2493,
        "categoryId": 25,
        "subCategoryName": "Wool Buyers & Merchants",
        "icon": null,
        "clues": "Wool Buyers & Merchants,wool,wool buyers & merchants,wool shop,knitting wool,wool merchants,wool buyers"
    },
    {
        "id": 2494,
        "categoryId": 25,
        "subCategoryName": "Wool Classers",
        "icon": null,
        "clues": "wool,Wool Classers,wool shop,wool classers,knitting wool"
    },
    {
        "id": 2495,
        "categoryId": 25,
        "subCategoryName": "Wool Processing Equipment & Services",
        "icon": null,
        "clues": "wool,Wool Processing Equipment & Services,wool shop,wool processing equipment & services,knitting wool,wool processing"
    },
    {
        "id": 2496,
        "categoryId": 25,
        "subCategoryName": "Wool Stores",
        "icon": null,
        "clues": "wool stores,Wool Stores,wool,wool shop,knitting wool"
    },
    {
        "id": 2500,
        "categoryId": 25,
        "subCategoryName": "Worm Farms",
        "icon": null,
        "clues": "worm bins,worms,Worm Farms,worm farms,worm suppliers"
    },
    {
        "id": 2503,
        "categoryId": 25,
        "subCategoryName": "Wrought Iron",
        "icon": null,
        "clues": "wrought iron security door,wrought,wrought iron beds,wrought iron supplies,wrought iron furniture,wrought iron,Wrought Iron,wrought iron fencing,wrought iron gates,wrought iron doors,wrought iron products,wrought iron balustrades"
    },
    {
        "id": 2506,
        "categoryId": 25,
        "subCategoryName": "Yarns--W'salers & Mfrs",
        "icon": null,
        "clues": ""
    }
    ]
},
{
    "id": 26,
    "icon": null,
    "subCategories": [{
    "id": 24,
    "categoryId": 26,
    "subCategoryName": "Advertising",
    "icon": null,
    "clues": "advertising agencies,advertising-outdoor posters,advertising-general,advertising,Advertising - Aerial,balloons--advertising & novelty,advertising agents"
},
    {
        "id": 25,
        "categoryId": 26,
        "subCategoryName": "Advertising - Cinema Screen",
        "icon": null,
        "clues": "cinema advertising,Advertising - Cinema Screen,advertising-cinema screen"
    },
    {
        "id": 26,
        "categoryId": 26,
        "subCategoryName": "Advertising - Direct Mail",
        "icon": null,
        "clues": "advertising-direct mail,advertising art director,Advertising - Direct Mail"
    },
    {
        "id": 27,
        "categoryId": 26,
        "subCategoryName": "Advertising - General",
        "icon": null,
        "clues": "advertising-general,Advertising - General"
    },
    {
        "id": 28,
        "categoryId": 26,
        "subCategoryName": "Advertising - Outdoor Posters",
        "icon": null,
        "clues": "Advertising - Outdoor Posters,advertising agencies,advertising-outdoor posters,advertising-general,outdoor advertising,Advertising - Aerial,balloons--advertising & novelty,advertising posters,advertising agents,advertising outdoor,outdoor advertising agencies"
    },
    {
        "id": 29,
        "categoryId": 26,
        "subCategoryName": "Advertising - Transit",
        "icon": null,
        "clues": "Advertising - Transit,advertising-transit"
    },
    {
        "id": 30,
        "categoryId": 26,
        "subCategoryName": "Advertising Agency",
        "icon": null,
        "clues": "Advertising Agency"
    },
    {
        "id": 31,
        "categoryId": 26,
        "subCategoryName": "Advertising Distributors",
        "icon": null,
        "clues": "Advertising Distributors,advertising distributors pamphlets,advertising distributors coffee cups,advertising distributors railway stations,advertising distributors"
    },
    {
        "id": 32,
        "categoryId": 26,
        "subCategoryName": "Advertising Media Representatives",
        "icon": null,
        "clues": "advertising agencies,advertising-outdoor posters,Advertising Media Representatives,advertising-general,advertising media representatives,multi media advertising agencies,advertising media agencies,Advertising - Aerial,balloons--advertising & novelty,advertising media,advertising agencies media placement,advertising agents,media tv & advertising,media advertising"
    },
    {
        "id": 124,
        "categoryId": 26,
        "subCategoryName": "Audio Engineering & Acoustic Consultants",
        "icon": null,
        "clues": "Audio Engineering & Acoustic Consultants,audio engineering,Acoustic Consultants,audio visual consultants"
    },
    {
        "id": 132,
        "categoryId": 26,
        "subCategoryName": "AV Equipment & Production",
        "icon": null,
        "clues": "AV Equipment & Production"
    },
    {
        "id": 573,
        "categoryId": 26,
        "subCategoryName": "Cosmetic Surgery",
        "icon": null,
        "clues": "cosmetic surgery,cosmetic surgery &/or procedures penile surgery,cosmetic surgery perspiration treatments,cosmetic surgery &/or procedures,Cosmetic Surgery,cosmetic surgery & procedures,cosmetic surgery clinic,cosmetic surgery &/or procedures plastic surgery,cosmetic surgery &/or procedures dermatology,cosmetic surgery &/or procedures liposuction,cosmetic surgery &/or procedures laser hair removal,cosmetic surgery &/or procedures eye surgery"
    },
    {
        "id": 669,
        "categoryId": 26,
        "subCategoryName": "Distributors--Newspapers, Magazines &/or Directories",
        "icon": null,
        "clues": "distributors--newspapers,magazines &/or directories yellow pages,business directories,Distributors--Newspapers,Magazines &/or Directories,funeral directories,magazines from newsagents,distributors--newspapers,magazines &/or directories"
    },
    {
        "id": 709,
        "categoryId": 26,
        "subCategoryName": "DVD & CD Wholesalers & Manufacturers",
        "icon": null,
        "clues": "DVD Mfrs,cd dvd wholesalers,dvd movie wholesalers,dvd manufacturers & distributors,cd & dvd wholesalers & manufacturers printing,DVD & CD Wholesalers & Manufacturers,cd & dvd wholesalers & manufacturers duplicating,cd & dvd wholesalers & manufacturers,dvd wholesalers"
    },
    {
        "id": 856,
        "categoryId": 26,
        "subCategoryName": "Film & Video Distributors",
        "icon": null,
        "clues": "film distributors,Film & Video Distributors"
    },
    {
        "id": 857,
        "categoryId": 26,
        "subCategoryName": "Film Equipment & Repairs",
        "icon": null,
        "clues": "film equipment & repairs,Film Equipment & Repairs"
    },
    {
        "id": 858,
        "categoryId": 26,
        "subCategoryName": "Film Production Facilities & Equipment",
        "icon": null,
        "clues": "Film Production Facilities & Equipment,film production facilities,film production houses,film production hire,film & tv production & post production,film & video production,film post production,film production,film production facilities & equipment,film tv production,television film production,film & television production,production film movie,film television video production,film sound production"
    },
    {
        "id": 859,
        "categoryId": 26,
        "subCategoryName": "Film Production Services",
        "icon": null,
        "clues": "film editing services,Film Production Services,film production services,film services"
    },
    {
        "id": 1221,
        "categoryId": 26,
        "subCategoryName": "International Telecommunications Services",
        "icon": null,
        "clues": "international telecommunications services,International Telecommunications Services"
    },
    {
        "id": 1348,
        "categoryId": 26,
        "subCategoryName": "Magazines & Periodicals",
        "icon": null,
        "clues": "distributors--newspapers,magazines &/or directories yellow pages,Distributors--Newspapers,Magazines &/or Directories,magazines & periodicals,magazines from newsagents,Magazines & Periodicals"
    },
    {
        "id": 1402,
        "categoryId": 26,
        "subCategoryName": "Media Information & Services",
        "icon": null,
        "clues": "media agencies,media training,media services,media buying,media buying services,media consultants,media information & services,media production,Media Information & Services,media information"
    },
    {
        "id": 1463,
        "categoryId": 26,
        "subCategoryName": "Modelling Agencies",
        "icon": null,
        "clues": "baby modelling agencies,promotional modelling agencies,Modelling Agencies,children's modelling agencies,modelling agencies,child modelling agencies"
    },
    {
        "id": 1494,
        "categoryId": 26,
        "subCategoryName": "Multimedia Services &/or Equipment",
        "icon": null,
        "clues": "multimedia,multimedia services,multimedia services &/or equipment,multimedia services & equipment dvd duplication,multimedia services &/or equipment cd duplication,multimedia services & equipment,Multimedia Services &/or Equipment,multimedia services & equipment website design,multimedia services &/or equipment animation"
    },
    {
        "id": 1596,
        "categoryId": 26,
        "subCategoryName": "Paging Systems--Radio",
        "icon": null,
        "clues": "Paging Systems--Radio,paging services,paging systems,paging systems--radio"
    },
    {
        "id": 1640,
        "categoryId": 26,
        "subCategoryName": "Pay Television",
        "icon": null,
        "clues": "Pay Television,pay television,television antenna services pay tv equipment,pay tv installers,pay tv,pay advance,pay television services,pay phones,pay tv services"
    },
    {
        "id": 1745,
        "categoryId": 26,
        "subCategoryName": "Posters--Advertising",
        "icon": null,
        "clues": "advertising-outdoor posters,advertising posters,posters--advertising,Posters--Advertising"
    },
    {
        "id": 1764,
        "categoryId": 26,
        "subCategoryName": "Pre-Press Production Services",
        "icon": null,
        "clues": "Pre-Press Production Services,prepress,prepress printers,pre-press production services"
    },
    {
        "id": 1809,
        "categoryId": 26,
        "subCategoryName": "Publishers--Book",
        "icon": null,
        "clues": "Publishers--Book,book publishers,educational book publishers,publishers--book,children's book publishers"
    },
    {
        "id": 1810,
        "categoryId": 26,
        "subCategoryName": "Publishers--Magazines & Periodicals",
        "icon": null,
        "clues": "Publishers--Magazines & Periodicals,publishers--magazines & periodicals"
    },
    {
        "id": 1811,
        "categoryId": 26,
        "subCategoryName": "Publishers--Music",
        "icon": null,
        "clues": "music publishers,publishers--music,Publishers--Music"
    },
    {
        "id": 1812,
        "categoryId": 26,
        "subCategoryName": "Publishing Support Services",
        "icon": null,
        "clues": "publishing house,custom publishing,publishing books,self publishing,publishing support services,publishing recruitment,Publishing Support Services,publishing magazine,printing publishing,newspapers publishing,publishing services,publishing design,publishing agents,desktop publishing"
    },
    {
        "id": 1829,
        "categoryId": 26,
        "subCategoryName": "Radio & Television Schools",
        "icon": null,
        "clues": "radio & television schools,Radio & Television Schools"
    },
    {
        "id": 1830,
        "categoryId": 26,
        "subCategoryName": "Radio & Television Station Equipment",
        "icon": null,
        "clues": "radio equipment,radio communication equipment,radio communication equipment & service,radio & television station equipment,Radio & Television Station Equipment,radio communication equipment & service uhf,radio communication equipment & service two way radios"
    },
    {
        "id": 1831,
        "categoryId": 26,
        "subCategoryName": "Radio Communications Equipment & Service",
        "icon": null,
        "clues": "two way radio communications,radio distributors,parts & service,radio communication equipment & service,radio communications,radio communication equipment & service uhf,communications radio,Radio Communications Equipment & Service,radio communication equipment & service two way radios"
    },
    {
        "id": 1832,
        "categoryId": 26,
        "subCategoryName": "Radio Stations",
        "icon": null,
        "clues": "radio stations,fm radio stations,Radio Stations,local radio stations,community radio stations"
    },
    {
        "id": 1852,
        "categoryId": 26,
        "subCategoryName": "Recording Studios & Services",
        "icon": null,
        "clues": "sound recording studios,recording studios,Recording Studios & Services,recording services rehearsal studios,rehearsal recording studios,recording sound studios,studios recording,recording studios design,audio recording studios,recording services,music recording studios,recording services recording studio"
    },
    {
        "id": 2210,
        "categoryId": 26,
        "subCategoryName": "Technical Publications",
        "icon": null,
        "clues": "technical publications,Technical Publications"
    },
    {
        "id": 2211,
        "categoryId": 26,
        "subCategoryName": "Telecommunications Consultants",
        "icon": null,
        "clues": "telecommunications consultants,Telecommunications Consultants,telecommunications"
    },
    {
        "id": 2212,
        "categoryId": 26,
        "subCategoryName": "Teleconferencing",
        "icon": null,
        "clues": "Teleconferencing,teleconferencing"
    },
    {
        "id": 2221,
        "categoryId": 26,
        "subCategoryName": "Television Programme Producers & Distributors",
        "icon": null,
        "clues": "television programme producers & distributors,Television Programme Producers & Distributors,television producers,television programme producers--services,television programme producers"
    },
    {
        "id": 2222,
        "categoryId": 26,
        "subCategoryName": "Television Programme Producers--Services & Supplies",
        "icon": null,
        "clues": "Television Programme Producers--Services & Supplies,television programme producers--services & supplies,television programme producers--services,television programme producers"
    },
    {
        "id": 2224,
        "categoryId": 26,
        "subCategoryName": "Television Stations",
        "icon": null,
        "clues": "Television Stations,television stations"
    },
    {
        "id": 2378,
        "categoryId": 26,
        "subCategoryName": "Video & DVD Equipment Wholesalers & Manufacturers",
        "icon": null,
        "clues": "video production equipment,video & dvd equipment,video games wholesalers,video camera equipment,video,video & dvd equipment--retail video cameras,video equipment store,video conferencing equipment,video production,video dvd rental,video to dvd conversion,video wholesalers,video equipment rental,automotive video & audio equipment,video equipment hire,video cassette manufacturers,video & dvd production,video store,video cassette manufacturers & distributors,video rental store,photographic equipment & supplies--retail & repairs video cameras,Video & DVD Equipment Wholesalers & Manufacturers,video & dvd equipment--repairs & service camcorders,video dvd,video equipment"
    },
    {
        "id": 2379,
        "categoryId": 26,
        "subCategoryName": "Video & DVD Equipment--Hire",
        "icon": null,
        "clues": "Video & DVD Equipment--Hire,video & dvd equipment,video,video & dvd equipment--retail video cameras,video production,video dvd rental,video to dvd conversion,video equipment hire,video & dvd production,video store,video rental store,video & dvd equipment--hire,video & dvd equipment--repairs & service camcorders,video dvd"
    },
    {
        "id": 2380,
        "categoryId": 26,
        "subCategoryName": "Video & DVD Equipment--Repairs & Service",
        "icon": null,
        "clues": "video & dvd equipment,video,video & dvd equipment--retail video cameras,video services,video & dvd equipment--repairs & service vcrs,video production,video dvd rental,video to dvd conversion,video & dvd equipment--repairs & service,Video & DVD Equipment--Repairs & Service,video & dvd production,video store,video rental store,photographic equipment & supplies--retail & repairs video cameras,video & dvd equipment--repairs & service camcorders,video dvd"
    },
    {
        "id": 2381,
        "categoryId": 26,
        "subCategoryName": "Video & DVD Equipment--Retail",
        "icon": null,
        "clues": "video & dvd equipment,video,video & dvd equipment--retail video cameras,video & dvd equipment--retail,video production,Video & DVD Equipment--Retail,video dvd rental,video to dvd conversion,video & dvd production,video store,video rental store,photographic equipment & supplies--retail & repairs video cameras,video & dvd equipment--repairs & service camcorders,video dvd"
    },
    {
        "id": 2382,
        "categoryId": 26,
        "subCategoryName": "Video & DVD Libraries",
        "icon": null,
        "clues": "video & dvd equipment,video,video & dvd equipment--retail video cameras,Video & DVD Libraries,video production,video dvd rental,video to dvd conversion,video & dvd libraries,video & dvd production,video store,video rental store,video & dvd equipment--repairs & service camcorders,video dvd"
    },
    {
        "id": 2383,
        "categoryId": 26,
        "subCategoryName": "Video & DVD Movie Sales",
        "icon": null,
        "clues": "video & dvd equipment,video movie hire,video,video & dvd equipment--retail video cameras,video movies,video camera sales,video production,video dvd rental,video to dvd conversion,Video & DVD Movie Sales,video & dvd production,video store,video & dvd movie sales,video sales,video rental store,video & dvd equipment--repairs & service camcorders,video dvd"
    },
    {
        "id": 2384,
        "categoryId": 26,
        "subCategoryName": "Video & DVD Production & Duplicating Services",
        "icon": null,
        "clues": "video production equipment,wedding video services,video camera services,video & dvd equipment,video production sport,video,video & dvd equipment--retail video cameras,video services,Video & DVD Production & Duplicating Services,video production,video dvd rental,video to dvd conversion,wedding photographers & video services,video & dvd production & duplicating services dubbing,video & dvd production & duplicating services 8mm to dvd conversion,video & dvd production & duplicating services,video duplicating services,video & dvd production,video subtitling video production,video & dvd production & duplicating services cds,video store,video production duplication,video rental store,corporate video production,video & dvd duplicating services,video production & duplicating services,film & video production,video & dvd equipment--repairs & service camcorders,video dvd,video production services,video production house"
    },
    {
        "id": 2438,
        "categoryId": 26,
        "subCategoryName": "Wedding Videos",
        "icon": null,
        "clues": "wedding videos,Wedding Videos"
    }
    ]
},
{
    "id": 27,
    "icon": null,
    "subCategories": [{
    "id": 15,
    "categoryId": 27,
    "subCategoryName": "Acupuncture",
    "icon": null,
    "clues": "acupuncture,acupuncture chinese herbal prescriptions,acupuncture herbal remedies,alternative & natural therapies acupuncture,Acupuncture"
},
    {
        "id": 61,
        "categoryId": 27,
        "subCategoryName": "Alcohol & Drug Counselling",
        "icon": null,
        "clues": "alcohol,alcohol & drug services,alcohol rehabilitation,alcohol & drugs,drug & alcohol counselling detoxification,drug & alcohol,drug & alcohol rehabilitation,alcohol & drug rehabilitation,drug & alcohol services,alcohol counselling,alcohol & drug rehabilitation facilities,alcohol shop,drug & alcohol counselling,Alcohol & Drug Counselling"
    },
    {
        "id": 63,
        "categoryId": 27,
        "subCategoryName": "Alexander Technique",
        "icon": null,
        "clues": "alexander technique,Alexander Technique"
    },
    {
        "id": 64,
        "categoryId": 27,
        "subCategoryName": "Allergy & Clinical Immunology",
        "icon": null,
        "clues": "Allergy & Clinical Immunology,Clinical Immunology (including Allergy),allergy,allergy specialist,allergy testing,allergy clinic,allergy & clinical immunology,clinical immunology (including allergy)"
    },
    {
        "id": 65,
        "categoryId": 27,
        "subCategoryName": "Allergy Aid Products & Services",
        "icon": null,
        "clues": "allergy aid products,allergy aid products & services,Allergy Aid Products & Services,Clinical Immunology (including Allergy),allergy,allergy specialist,allergy testing,allergy clinic,allergy products"
    },
    {
        "id": 68,
        "categoryId": 27,
        "subCategoryName": "Alternative & Natural Therapies",
        "icon": null,
        "clues": "alternative & natural therapies remedial therapy,alternative & natural therapies ayurveda,alternative & natural therapies energy healing,alternative & natural therapies crystal healing,alternative & natural therapies stress,alternative therapy,alternative & natural therapies reflexology,alternative & natural therapies acupuncture,alternative medicine,alternative & natural therapies naturopathy,alternative & natural therapies migraine,alternative health practitioners,alternative & natural therapies holism,personal development alternative therapies,alternative & natural therapies iridology,alternative health therapies,alternative & natural therapies,alternative therapies neuro linguistic programming,Alternative & Natural Therapies,alternative & natural therapies chinese methods,alternative therapies,alternative health,alternative & natural therapies spiritual counselling,alternative & natural therapies tantra"
    },
    {
        "id": 69,
        "categoryId": 27,
        "subCategoryName": "Alternative Health Equipment & Supplies",
        "icon": null,
        "clues": "alternative health practitioners,alternative health equipment & supplies,alternative therapy,alternative health,alternative & natural therapies acupuncture,alternative medicine,alternative health products,Alternative Health Equipment & Supplies,alternative health equipment,alternative health services"
    },
    {
        "id": 70,
        "categoryId": 27,
        "subCategoryName": "Alternative Health Therapies Courses",
        "icon": null,
        "clues": "alternative & natural therapies ayurveda,alternative & natural therapies stress,alternative therapy,alternative & natural therapies reflexology,alternative & natural therapies acupuncture,alternative medicine,alternative & natural therapies naturopathy,alternative health equipment,Alternative Health Therapies Courses,alternative & natural therapies migraine,alternative health therapies courses,alternative health practitioners,personal development alternative therapies,alternative & natural therapies holism,alternative health therapies,alternative & natural therapies iridology,alternative therapies neuro linguistic programming,alternative therapies,alternative health,alternative & natural therapies spiritual counselling,alternative & natural therapies tantra,alternative health products,alternative health services"
    },
    {
        "id": 75,
        "categoryId": 27,
        "subCategoryName": "Ambulances",
        "icon": null,
        "clues": "Ambulances,ambulances"
    },
    {
        "id": 79,
        "categoryId": 27,
        "subCategoryName": "Anaesthesia",
        "icon": null,
        "clues": "Anaesthesia,cosmetic surgery &/or procedures local anaesthesia,veterinary surgeons anaesthesia,anaesthesia"
    },
    {
        "id": 102,
        "categoryId": 27,
        "subCategoryName": "Aromatherapy",
        "icon": null,
        "clues": "aromatherapy,Aromatherapy"
    },
    {
        "id": 125,
        "categoryId": 27,
        "subCategoryName": "Audiologist & Hearing Test",
        "icon": null,
        "clues": "Audiologist & Hearing Test,audiologist clinic,audiologist centre,hearing test audiologists,audiologist,audiologists,educational audiologist,paediatrician audiologist"
    },
    {
        "id": 161,
        "categoryId": 27,
        "subCategoryName": "Bariatric Surgery",
        "icon": null,
        "clues": "Bariatric Surgery,bariatric surgeons,bariatric surgery,bariatric"
    },
    {
        "id": 197,
        "categoryId": 27,
        "subCategoryName": "Biological Supplies",
        "icon": null,
        "clues": "biological supplies,Biological Supplies"
    },
    {
        "id": 198,
        "categoryId": 27,
        "subCategoryName": "Bio-Medical Engineers",
        "icon": null,
        "clues": "bio-medical engineers,biomedical engineering,Bio-Medical Engineers,biomedical"
    },
    {
        "id": 212,
        "categoryId": 27,
        "subCategoryName": "Blood Bank & Donor Services",
        "icon": null,
        "clues": "Blood Bank & Donor Services,blood services,blood bank,blood bank & donor services,blood donor,blood test,blood"
    },
    {
        "id": 213,
        "categoryId": 27,
        "subCategoryName": "Bloodstock Agents",
        "icon": null,
        "clues": "bloodstock agents,Bloodstock Agents,bloodstock"
    },
    {
        "id": 244,
        "categoryId": 27,
        "subCategoryName": "Bowen Therapy",
        "icon": null,
        "clues": "Bowen Therapy,bowen therapy,bowen,bowen therapist"
    },
    {
        "id": 368,
        "categoryId": 27,
        "subCategoryName": "Cardiology",
        "icon": null,
        "clues": "doctors-medical practitioners cardiology,cardiology specialist,cardiology doctor,Paediatric Cardiology,cardiology clinic,Cardiology,cardiology,cardiology services"
    },
    {
        "id": 369,
        "categoryId": 27,
        "subCategoryName": "Cardio-thoracic Surgery",
        "icon": null,
        "clues": "Cardio-thoracic Surgery,cardiothoracic surgeons"
    },
    {
        "id": 417,
        "categoryId": 27,
        "subCategoryName": "Chemist & Pharmacy Stores",
        "icon": null,
        "clues": "chemists,Chemist & Pharmacy Stores,midnight chemist pharmacy,late night chemist pharmacy,day night chemist & pharmacy,pharmacy chemist 24 hour,24 hr chemist pharmacy,24 hour chemists,chemist 24 hour,chemist & pharmacy,24 hour chemist or pharmacy"
    },
    {
        "id": 418,
        "categoryId": 27,
        "subCategoryName": "Chemists' Supplies",
        "icon": null,
        "clues": "chemists--consulting,Chemists' Supplies,chemists' supplies"
    },
    {
        "id": 428,
        "categoryId": 27,
        "subCategoryName": "Childbirth Services",
        "icon": null,
        "clues": "Childbirth Services,childbirth classes,childbirth services,childbirth,doulas (childbirth services)"
    },
    {
        "id": 436,
        "categoryId": 27,
        "subCategoryName": "Chinese Medicine",
        "icon": null,
        "clues": "chinese medicine clinic,Chinese Medicine,chinese medicine treatment,chinese medicine,chinese medicine shop,chinese medicine doctor,chinese herbalist"
    },
    {
        "id": 437,
        "categoryId": 27,
        "subCategoryName": "Chiropractor",
        "icon": null,
        "clues": "chiropractor equine,chiropractor supplies,chiropractor clinic,chiropractor paediatrician,Chiropractor,chiropractor chinese,chiropractor chiropractor,chiropractor,chiropractor services,chiropractors"
    },
    {
        "id": 456,
        "categoryId": 27,
        "subCategoryName": "Clinical Genetics",
        "icon": null,
        "clues": "clinical genetics,Clinical Genetics"
    },
    {
        "id": 457,
        "categoryId": 27,
        "subCategoryName": "Clinical Haematology",
        "icon": null,
        "clues": "clinical haematology,Clinical Haematology"
    },
    {
        "id": 483,
        "categoryId": 27,
        "subCategoryName": "Colonic Irrigation",
        "icon": null,
        "clues": "Colonic Irrigation,naturopaths colonic irrigation,colonic,alternative health services colonic irrigation,colonic hydrotherapy,colonic irrigation,colonic flushing,colonic treatment"
    },
    {
        "id": 484,
        "categoryId": 27,
        "subCategoryName": "Colorectal Surgery",
        "icon": null,
        "clues": "Colorectal Surgery,colorectal,colorectal surgery,colorectal surgeons"
    },
    {
        "id": 503,
        "categoryId": 27,
        "subCategoryName": "Community Health Centres & Services",
        "icon": null,
        "clues": "community centre,Community Health Centres & Services,community health nurses,community health,community health centre,community health care centre,community health services,community health clinic,community health centres & services"
    },
    {
        "id": 627,
        "categoryId": 27,
        "subCategoryName": "Dental Equipment--Repairs",
        "icon": null,
        "clues": "dental equipment--repairs,Dental Equipment--Repairs"
    },
    {
        "id": 628,
        "categoryId": 27,
        "subCategoryName": "Dental Health Aids",
        "icon": null,
        "clues": "dental health,dental health services,dental health aids,Dental Health Aids"
    },
    {
        "id": 629,
        "categoryId": 27,
        "subCategoryName": "Dental Hospital & Emergency Dental",
        "icon": null,
        "clues": "dental hospitals,emergency dental hospital,emergency dental clinic,24 hour dental hospital,dental emergency services,emergency dental,dental emergency hospital,dental emergency,public dental hospital,hospital dental,dental emergency clinic,Dental Hospital & Emergency Dental"
    },
    {
        "id": 630,
        "categoryId": 27,
        "subCategoryName": "Dental Laboratories",
        "icon": null,
        "clues": "Dental Laboratories,dental laboratories"
    },
    {
        "id": 631,
        "categoryId": 27,
        "subCategoryName": "Dental Prosthetist",
        "icon": null,
        "clues": "Dental Prosthetist,dental prosthetists,dental prosthetists denture repairs,dental prosthetists relines"
    },
    {
        "id": 632,
        "categoryId": 27,
        "subCategoryName": "Dental Supplies & Equipment",
        "icon": null,
        "clues": "Dental Supplies & Equipment,dental supplies,dental supplies & equipment,dental equipment,dental equipment suppliers,dental equipment selling,dental laboratory supplies"
    },
    {
        "id": 633,
        "categoryId": 27,
        "subCategoryName": "Dental Technicians",
        "icon": null,
        "clues": "dental technicians emergency,dental technicians laboratory,Dental Technicians,dental technicians,dental laboratory technicians,advanced dental technicians"
    },
    {
        "id": 634,
        "categoryId": 27,
        "subCategoryName": "Dentist",
        "icon": null,
        "clues": "dentists,cosmetic dentist,dentists cosmetic,Dentist,children's dentist,dentists whitening,dentists orthodontics,emergency dentist"
    },
    {
        "id": 635,
        "categoryId": 27,
        "subCategoryName": "Dentofacial Radiology Specialists",
        "icon": null,
        "clues": "Dentofacial Radiology Specialists,dentofacial radiology specialists"
    },
    {
        "id": 637,
        "categoryId": 27,
        "subCategoryName": "Dermatology",
        "icon": null,
        "clues": "dermatology,doctors-medical practitioners dermatology,Dermatology,cosmetic surgery &/or procedures dermatology"
    },
    {
        "id": 642,
        "categoryId": 27,
        "subCategoryName": "Diagnostic Radiology",
        "icon": null,
        "clues": "Diagnostic Radiology,bone density testing diagnostic radiology,opg diagnostic radiology,diagnostic radiology,diagnostic radiology opg,diagnostic radiology bone density testing"
    },
    {
        "id": 643,
        "categoryId": 27,
        "subCategoryName": "Diagnostic Ultrasound",
        "icon": null,
        "clues": "diagnostic ultrasound bone density testing,bone density testing diagnostic ultrasound,dexa diagnostic ultrasound,body composition diagnostic ultrasound,diagnostic ultrasound opg,diagnostic ultrasound imaging (business),Diagnostic Ultrasound,opg diagnostic ultrasound,diagnostic ultrasound"
    },
    {
        "id": 657,
        "categoryId": 27,
        "subCategoryName": "Disability Services & Care Organisations",
        "icon": null,
        "clues": "disability support organisations,disability services & support organisations employment services,disability care,disability organisations,disability services & support,disability carers,aged & disability care,disability services,disability services & support organisations disabled access,disability care services,disability support services,disability & home care,disability services & support organisations,disability services providers,Disability Services & Care Organisations"
    },
    {
        "id": 658,
        "categoryId": 27,
        "subCategoryName": "Disability Services & Mobility Equipment",
        "icon": null,
        "clues": "Disability Services & Mobility Equipment,disability aids,disability medical equipment suppliers,disability equipment,disability equipment hire"
    },
    {
        "id": 672,
        "categoryId": 27,
        "subCategoryName": "Doctors-Medical Practitioners",
        "icon": null,
        "clues": "doctors-medical practitioners cardiology,doctors-medical practitioners orthopaedics,doctors-medical practitioners imaging,doctors-medical practitioners dermatology,doctors-medical practitioners family care,doctors-medical practitioners paediatrics,doctors-medical practitioners cancer screening,doctors-medical practitioners,doctors-medical practitioners psychiatry,doctors-medical practitioners neurology,Doctors-Medical Practitioners,doctors-medical practitioners ultrasound,doctors-medical practitioners obstetrics,doctors-medical practitioners gynaecology,doctors-medical practitioners skin cancer,doctors-medical practitioners radiology,doctors-medical practitioners ophthalmology,doctors-medical practitioners women's health,doctors-medical practitioners general practice,doctors-medical practitioners bulk billing,doctors-medical practitioners skin checks"
    },
    {
        "id": 767,
        "categoryId": 27,
        "subCategoryName": "Emergency Medicine",
        "icon": null,
        "clues": "Emergency Medicine,emergency medicine"
    },
    {
        "id": 772,
        "categoryId": 27,
        "subCategoryName": "Endocrinology",
        "icon": null,
        "clues": "paediatrician endocrinology,endocrinology medical,reproductive endocrinology,endocrinology specialist,doctors-medical practitioners endocrinology,Endocrinology,endocrinology doctor,endocrinology"
    },
    {
        "id": 773,
        "categoryId": 27,
        "subCategoryName": "Endodontists",
        "icon": null,
        "clues": "endodontists,Endodontists"
    },
    {
        "id": 803,
        "categoryId": 27,
        "subCategoryName": "Exercise Physiologists",
        "icon": null,
        "clues": "exercise physiologist,Exercise Physiologists,exercise physiologists,exercise physiologists medicare"
    },
    {
        "id": 824,
        "categoryId": 27,
        "subCategoryName": "Family Planning",
        "icon": null,
        "clues": "family planning centre,family planning,family planning doctor,Family Planning,family planning clinic,family planning practice,family planning services"
    },
    {
        "id": 841,
        "categoryId": 27,
        "subCategoryName": "Feldenkrais",
        "icon": null,
        "clues": "feldenkrais,Feldenkrais"
    },
    {
        "id": 877,
        "categoryId": 27,
        "subCategoryName": "First Aid Kits, Training & Supplies",
        "icon": null,
        "clues": "mining first aid training,first aid supplies or instruction,first aid supplies,first aid courses,training first aid,first aid equipment,First Aid Kits,Training & Supplies,first aide supplies,first aid training,first aid kits,training & development first aid,first aid certificates,first aid training providers,first aid training courses,senior first aid training,medical supplies first aid supplies,first aid instruction"
    },
    {
        "id": 902,
        "categoryId": 27,
        "subCategoryName": "Flying Doctor Service",
        "icon": null,
        "clues": "Flying Doctor Service,flying doctor service"
    },
    {
        "id": 991,
        "categoryId": 27,
        "subCategoryName": "Gastroenterology",
        "icon": null,
        "clues": "gastroenterology entomology,gastroenterology specialist,Gastroenterology,gastroenterology surgeons,gastroenterology centre,doctors-medical practitioners gastroenterology,gastroenterology"
    },
    {
        "id": 1003,
        "categoryId": 27,
        "subCategoryName": "General Medicine",
        "icon": null,
        "clues": "General Medicine,general practice medicine,general medicine"
    },
    {
        "id": 1058,
        "categoryId": 27,
        "subCategoryName": "Gynaecological Oncology",
        "icon": null,
        "clues": "Gynaecological Oncology,doctors-medical practitioners gynaecological surgery,gynaecological oncology"
    },
    {
        "id": 1060,
        "categoryId": 27,
        "subCategoryName": "Haematology",
        "icon": null,
        "clues": "Haematology,haematology"
    },
    {
        "id": 1070,
        "categoryId": 27,
        "subCategoryName": "Hand Surgery",
        "icon": null,
        "clues": "Hand Surgery,hand surgery"
    },
    {
        "id": 1091,
        "categoryId": 27,
        "subCategoryName": "Health Support Organisations",
        "icon": null,
        "clues": "health support,Health Support Organisations,health support organisations,health organisations"
    },
    {
        "id": 1093,
        "categoryId": 27,
        "subCategoryName": "Hearing Aids, Equipment & Services",
        "icon": null,
        "clues": "hearing equipment,hearing aids,equipment & services,hearing aids,equipment & services audiological assessments,hearing aid services,hearing aids,hearing aids equipment,hearing aids,equipment & services loop audio systems,Hearing Aids,Equipment & Services"
    },
    {
        "id": 1114,
        "categoryId": 27,
        "subCategoryName": "Hire--Medical Equipment",
        "icon": null,
        "clues": "Hire--Medical Equipment,hire medical,medical bed hire,medical equipment hire,medical supplies hire,hire--medical equipment,medical hire equipment,medical aids hire,hire--medical & nursery equipment,hire medical aids,medical hire"
    },
    {
        "id": 1132,
        "categoryId": 27,
        "subCategoryName": "Homeopathy",
        "icon": null,
        "clues": "Homeopathy"
    },
    {
        "id": 1147,
        "categoryId": 27,
        "subCategoryName": "Hospital Equipment & Supplies",
        "icon": null,
        "clues": "hospital equipment,hospital equipment hire,Hospital Equipment & Supplies,hospital supplies,hospital equipment & supplies,hire hospital equipment,hospital equipment & services"
    },
    {
        "id": 1149,
        "categoryId": 27,
        "subCategoryName": "Hospitals--Public",
        "icon": null,
        "clues": "public hospitals,hospitals-public,public hospitals suppliers,nsw public hospitals,Hospitals--Public"
    },
    {
        "id": 1167,
        "categoryId": 27,
        "subCategoryName": "Hyperbaric Medicine",
        "icon": null,
        "clues": "hyperbaric medicine,hyperbaric,hyperbaric chamber,hyperbaric oxygen,Hyperbaric Medicine"
    },
    {
        "id": 1180,
        "categoryId": 27,
        "subCategoryName": "Immunology",
        "icon": null,
        "clues": "Immunology,doctors-medical practitioners immunology,Clinical Immunology (including Allergy),clinical immunology (including allergy),immunology"
    },
    {
        "id": 1197,
        "categoryId": 27,
        "subCategoryName": "Infectious Diseases",
        "icon": null,
        "clues": "infectious diseases,Infectious Diseases"
    },
    {
        "id": 1198,
        "categoryId": 27,
        "subCategoryName": "Infertility Services (Including IVF)",
        "icon": null,
        "clues": "infertility clinic,Infertility Services (Including IVF),infertility specialist vasectomy reversal,infertility services (including ivf),infertility services,infertility,infertility doctor,infertility specialist"
    },
    {
        "id": 1252,
        "categoryId": 27,
        "subCategoryName": "Kinesiology",
        "icon": null,
        "clues": "kinesiology,Kinesiology"
    },
    {
        "id": 1406,
        "categoryId": 27,
        "subCategoryName": "Medical Agents",
        "icon": null,
        "clues": "medical agents,medical equipment agents,Medical Agents,medical agents general practice"
    },
    {
        "id": 1407,
        "categoryId": 27,
        "subCategoryName": "Medical Centres",
        "icon": null,
        "clues": "medical centres,medical centres mental health,medical centres family medicine,medical centres physiotherapy,electrical contractors medical centres,medical centres open sundays,medical centres women's health,medical centres general practice,medical centres sports medicine,medical centres sexual health,Medical Centres,medical centres skin cancer checks,medical centres travel medicine,medical centres skin conditions"
    },
    {
        "id": 1408,
        "categoryId": 27,
        "subCategoryName": "Medical Equipment & Repairs",
        "icon": null,
        "clues": "medical equipment sales,medical equipment agents,medical equipment,Medical Equipment & Repairs,medical equipment services,medical equipment hire,medical equipment rental,medical equipment & repair,medical equipment & repairs,medical equipment manufacturers,medical equipment suppliers"
    },
    {
        "id": 1409,
        "categoryId": 27,
        "subCategoryName": "Medical Oncology",
        "icon": null,
        "clues": "medical oncology,Medical Oncology,doctors-medical practitioners oncology"
    },
    {
        "id": 1410,
        "categoryId": 27,
        "subCategoryName": "Medical Research",
        "icon": null,
        "clues": "medical research,Medical Research"
    },
    {
        "id": 1411,
        "categoryId": 27,
        "subCategoryName": "Medical Supplies",
        "icon": null,
        "clues": "medical supplies,medical supplies hire,medical supplies & services,Medical Supplies,medical supplies first aid supplies,medical supplies wholesale,medical supplies cpap devices"
    },
    {
        "id": 1496,
        "categoryId": 27,
        "subCategoryName": "Musculoskeletal Medicine",
        "icon": null,
        "clues": "musculoskeletal physician,doctors-medical practitioners musculoskeletal,musculoskeletal medicine,musculoskeletal physiotherapist,Musculoskeletal Medicine,musculoskeletal therapist,musculoskeletal"
    },
    {
        "id": 1509,
        "categoryId": 27,
        "subCategoryName": "Myotherapy",
        "icon": null,
        "clues": "myotherapy & acupuncture,Myotherapy,myotherapy,myotherapy clinic"
    },
    {
        "id": 1517,
        "categoryId": 27,
        "subCategoryName": "Naturopaths",
        "icon": null,
        "clues": "naturopaths colonic irrigation,naturopaths massage therapy,naturopaths,naturopaths natural remedies,naturopaths nutrition,naturopaths remedial therapy,naturopaths bowen therapy,Naturopaths,naturopaths iridology,naturopaths allergy testing"
    },
    {
        "id": 1519,
        "categoryId": 27,
        "subCategoryName": "NDIS Service Providers",
        "icon": null,
        "clues": "NDIS Service Providers"
    },
    {
        "id": 1523,
        "categoryId": 27,
        "subCategoryName": "Neurology",
        "icon": null,
        "clues": "Neurology,neurology,doctors-medical practitioners neurology"
    },
    {
        "id": 1524,
        "categoryId": 27,
        "subCategoryName": "Neurosurgery",
        "icon": null,
        "clues": "Neurosurgery,neurosurgery"
    },
    {
        "id": 1534,
        "categoryId": 27,
        "subCategoryName": "Nuclear Medicine",
        "icon": null,
        "clues": "nuclear medicine bone density testing,nuclear physicists,nuclear medicine,nuclear medicine specialist,Nuclear Medicine,nuclear imaging,nuclear,bone density testing nuclear medicine"
    },
    {
        "id": 1541,
        "categoryId": 27,
        "subCategoryName": "Obstetrics & Gynaecology",
        "icon": null,
        "clues": "obstetrics imaging,doctors-medical practitioners obstetrics,Obstetrics & Gynaecology,obstetrics ultrasound,obstetrics gynaecologist,obstetrics gyms,obstetrics,obstetrics & gynaecology,obstetrics doctor"
    },
    {
        "id": 1542,
        "categoryId": 27,
        "subCategoryName": "Occupational & Environmental Medicine",
        "icon": null,
        "clues": "occupational & environmental medicine,doctors-medical practitioners occupational medicine,occupational medicine,Occupational & Environmental Medicine"
    },
    {
        "id": 1543,
        "categoryId": 27,
        "subCategoryName": "Occupational Therapist",
        "icon": null,
        "clues": "occupational rehabilitation,occupational therapists paediatric,pediatrician occupational therapist,occupational therapist,private occupational therapist,paediatrician occupational therapist,occupational therapists,occupational health,occupational therapy,Occupational Therapist"
    },
    {
        "id": 1550,
        "categoryId": 27,
        "subCategoryName": "OHS - Occupational Health & Safety",
        "icon": null,
        "clues": "oh&s building inspections,oh&s site auditors,oh&s recruitment,OHS - Occupational Health & Safety,oh&s courses"
    },
    {
        "id": 1562,
        "categoryId": 27,
        "subCategoryName": "Ophthalmology",
        "icon": null,
        "clues": "ophthalmology,Ophthalmology,doctors-medical practitioners ophthalmology"
    },
    {
        "id": 1563,
        "categoryId": 27,
        "subCategoryName": "Optical Prescription Dispensers",
        "icon": null,
        "clues": "optical supplies telescopes,optical prescription,optical prescription dispensers contact lenses,Optical Prescription Dispensers,optical dispensers,optical prescription dispensers,optometrists & optical dispensers"
    },
    {
        "id": 1564,
        "categoryId": 27,
        "subCategoryName": "Optical Supplies",
        "icon": null,
        "clues": "optical supplies binoculars,optical supplies telescopes,Optical Supplies,optical supplies"
    },
    {
        "id": 1565,
        "categoryId": 27,
        "subCategoryName": "Optometrist",
        "icon": null,
        "clues": "optometrist,behavioural optometrists,optometrists eye examinations,optometrists behavioural optometry,optometrists repairs,Optometrist,optometrists sunglasses,optometrists"
    },
    {
        "id": 1566,
        "categoryId": 27,
        "subCategoryName": "Oral & Maxillofacial Surgeons (Dental)",
        "icon": null,
        "clues": "Oral & Maxillofacial Surgeons (Dental),oral & maxillofacial surgeons (dental),oral & maxillofacial surgeons (dental) opg x-ray,oral & maxillofacial surgery,oral health,tmj problems oral & maxillofacial surgeons (dentists"
    },
    {
        "id": 1567,
        "categoryId": 27,
        "subCategoryName": "Oral & Maxillofacial Surgery (Medical)",
        "icon": null,
        "clues": "oral & maxillofacial surgeons (dental) opg x-ray,oral & maxillofacial surgery,Oral & Maxillofacial Surgery (Medical),oral health,oral & maxillofacial surgery (medical)"
    },
    {
        "id": 1568,
        "categoryId": 27,
        "subCategoryName": "Oral Pathology & Oral Medicine Specialists",
        "icon": null,
        "clues": "oral pathology & oral medicine specialists,oral medicine,Oral Pathology & Oral Medicine Specialists"
    },
    {
        "id": 1569,
        "categoryId": 27,
        "subCategoryName": "Oral Surgeons",
        "icon": null,
        "clues": "oral & maxillofacial surgeons (dental) opg x-ray,Oral Surgeons,oral surgeons,tmj problems oral & maxillofacial surgeons (dentists"
    },
    {
        "id": 1575,
        "categoryId": 27,
        "subCategoryName": "Orthodontist",
        "icon": null,
        "clues": "orthodontists,orthodontist,orthodontist surgery,Orthodontist"
    },
    {
        "id": 1576,
        "categoryId": 27,
        "subCategoryName": "Orthodontists (Specialty)",
        "icon": null,
        "clues": "orthodontists,Orthodontists (Specialty),orthodontists opg x-ray,tmj problems orthodontists"
    },
    {
        "id": 1577,
        "categoryId": 27,
        "subCategoryName": "Orthopaedic Footwear & Custom Shoes",
        "icon": null,
        "clues": "doctors-medical practitioners orthopaedics,orthopaedic,footwear orthopaedic,orthopaedic specialist,orthopaedic footwear retail,orthopaedic doctor,footwear--orthopaedic or custom made,Orthopaedic Footwear & Custom Shoes,orthopaedic footwear,orthopaedic surgeons"
    },
    {
        "id": 1578,
        "categoryId": 27,
        "subCategoryName": "Orthopaedic Surgery",
        "icon": null,
        "clues": "doctors-medical practitioners orthopaedics,orthopaedic,orthopaedic surgery,orthopaedic specialist,Orthopaedic Surgery,doctors-medical practitioners orthopaedic surgery,orthopaedic doctor,orthopaedic surgeons"
    },
    {
        "id": 1579,
        "categoryId": 27,
        "subCategoryName": "Orthoptists",
        "icon": null,
        "clues": "orthoptists,Orthoptists"
    },
    {
        "id": 1580,
        "categoryId": 27,
        "subCategoryName": "Osteopath",
        "icon": null,
        "clues": "osteopath new,osteopaths,osteopath clinic,osteopathic,Osteopath,osteopath"
    },
    {
        "id": 1581,
        "categoryId": 27,
        "subCategoryName": "Otolaryngology (Ear, Nose & Throat)",
        "icon": null,
        "clues": "nose,nose specialist,otolaryngology,ear nose & throat specialist,nose doctor,nose piercing,ear nose throat,otolaryngology (ear,nose & throat),nose surgeons,doctors-medical practitioners otolaryngology,doctors-medical practitioners ear nose throat,ear nose throat surgeons,Otolaryngology (Ear,Nose & Throat)"
    },
    {
        "id": 1592,
        "categoryId": 27,
        "subCategoryName": "Paediatric Dentists",
        "icon": null,
        "clues": "paediatrician,Paediatric Dentists,doctors-medical practitioners paediatrics,Paediatric Cardiology,paediatric dentists"
    },
    {
        "id": 1593,
        "categoryId": 27,
        "subCategoryName": "Paediatric Medicine",
        "icon": null,
        "clues": "paediatrician,doctors-medical practitioners paediatrics,Paediatric Cardiology,Paediatric Medicine,paediatric medicine"
    },
    {
        "id": 1594,
        "categoryId": 27,
        "subCategoryName": "Paediatric Neurology",
        "icon": null,
        "clues": "paediatric neurology,paediatrician,Paediatric Neurology,doctors-medical practitioners paediatrics,Paediatric Cardiology"
    },
    {
        "id": 1595,
        "categoryId": 27,
        "subCategoryName": "Paediatric Surgery",
        "icon": null,
        "clues": "paediatrician,doctors-medical practitioners paediatrics,Paediatric Cardiology,paediatric surgery,Paediatric Surgery"
    },
    {
        "id": 1597,
        "categoryId": 27,
        "subCategoryName": "Pain Management",
        "icon": null,
        "clues": "pain management doctor,doctors-medical practitioners pain management,dentists pain management,Pain Management,pain management specialist,pain management,pain management centre,pain management clinic"
    },
    {
        "id": 1605,
        "categoryId": 27,
        "subCategoryName": "Palliative Care",
        "icon": null,
        "clues": "palliative care services,palliative,palliative care nursing,Palliative Care,palliative care hospitals,palliative care,pain palliative care"
    },
    {
        "id": 1633,
        "categoryId": 27,
        "subCategoryName": "Pathology Laboratories",
        "icon": null,
        "clues": "pathology laboratories,Pathology Laboratories,pathology laboratory,pathology laboratories pathology testing"
    },
    {
        "id": 1634,
        "categoryId": 27,
        "subCategoryName": "Pathology-General",
        "icon": null,
        "clues": "pathology-general,Pathology-General"
    },
    {
        "id": 1648,
        "categoryId": 27,
        "subCategoryName": "Periodontists",
        "icon": null,
        "clues": "Periodontists,periodontists"
    },
    {
        "id": 1651,
        "categoryId": 27,
        "subCategoryName": "Personal Injury",
        "icon": null,
        "clues": "personal injury asbestos claims,Personal Injury,personal injury asbestos exposure,personal injury lawyers,personal injury,personal injury solicitors,solicitors personal injury,solicitors personal injury (excludes nsw),law firms on personal injury"
    },
    {
        "id": 1663,
        "categoryId": 27,
        "subCategoryName": "Pharmaceutical Products Wholesalers & Manufacturers",
        "icon": null,
        "clues": "pharmaceutical products,pharmaceutical suppliers,pharmaceutical,pharmaceutical products manufacturers,Pharmaceutical Products Wholesalers & Manufacturers,pharmaceutical products - wholesalers & manufacturers,pharmaceutical wholesalers,pharmaceutical products wholesalers,pharmaceutical manufacturers,pharmaceutical distributors,pharmaceutical products--w'salers & mfrs"
    },
    {
        "id": 1664,
        "categoryId": 27,
        "subCategoryName": "Pharmacists--Consultant",
        "icon": null,
        "clues": "Pharmacists--Consultant,pharmacists--consultant"
    },
    {
        "id": 1681,
        "categoryId": 27,
        "subCategoryName": "Physiotherapist",
        "icon": null,
        "clues": "physiotherapist pilates,physiotherapists remedial massage,sports physiotherapist,physiotherapist massage,physiotherapist supplies,physiotherapist dance,physiotherapists,physiotherapist therapist,physiotherapists sports physiotherapy,Physiotherapist,physiotherapist products,physiotherapist paediatrician,physiotherapist clinic,physiotherapist sports,physiotherapist pregnancy,physiotherapist practice"
    },
    {
        "id": 1709,
        "categoryId": 27,
        "subCategoryName": "Plastic & Reconstructive Surgery",
        "icon": null,
        "clues": "plastic & reconstructive surgery,cosmetic surgery &/or procedures plastic surgery,cosmetic plastic surgery,plastic & reconstructive surgeons,cosmetic plastic & reconstructive surgeons,society of plastic & reconstructive surgery,plastic surgery clinic,doctors-medical practitioners plastic & reconstructive surgery,Plastic & Reconstructive Surgery,plastic & reconstructive surgery skin cancer,plastic surgery,plastic surgery & procedures,skin cancer plastic & reconstructive surgery"
    },
    {
        "id": 1730,
        "categoryId": 27,
        "subCategoryName": "Podiatrist",
        "icon": null,
        "clues": "podiatrist,mobile podiatrist,podiatrists orthotics,podiatrists,sports podiatrist,podiatrists treadmill analysis,podiatrists veterans,Podiatrist,podiatrist children"
    },
    {
        "id": 1762,
        "categoryId": 27,
        "subCategoryName": "Pregnancy Counselling & Related Services",
        "icon": null,
        "clues": "pregnancy,pregnancy massage,pregnancy counselling & related services,pregnancy services,family planning practice pregnancy termination services,pregnancy counselling,Pregnancy Counselling & Related Services,pregnancy termination"
    },
    {
        "id": 1763,
        "categoryId": 27,
        "subCategoryName": "Pregnancy Termination Services",
        "icon": null,
        "clues": "Pregnancy Termination Services,pregnancy termination clinic,pregnancy,pregnancy massage,pregnancy termination services,family planning clinic pregnancy termination,family planning practice pregnancy termination services,pregnancy services,termination of pregnancy,pregnancy termination"
    },
    {
        "id": 1782,
        "categoryId": 27,
        "subCategoryName": "Private Hospitals",
        "icon": null,
        "clues": "hospitals--private day surgery,Private Hospitals,hospitals--private,private maternity hospitals,hospitals--private psychiatry,private hospitals renal dialysis,hospitals--private gynaecology,private psychiatric hospitals"
    },
    {
        "id": 1794,
        "categoryId": 27,
        "subCategoryName": "Prosthetics & Orthotics",
        "icon": null,
        "clues": "dentists prosthetics,prosthetics,prosthetics limbs,dental prosthetics,orthotics & prosthetics,Prosthetics & Orthotics"
    },
    {
        "id": 1795,
        "categoryId": 27,
        "subCategoryName": "Prosthetists & Orthotists",
        "icon": null,
        "clues": "prosthetists & orthotists,Prosthetists & Orthotists,dental prosthetists,dental prosthetists denture repairs,dental prosthetists relines"
    },
    {
        "id": 1796,
        "categoryId": 27,
        "subCategoryName": "Prosthodontists",
        "icon": null,
        "clues": "Prosthodontists,tmj problems prosthodontists,prosthodontists"
    },
    {
        "id": 1798,
        "categoryId": 27,
        "subCategoryName": "Psychiatry",
        "icon": null,
        "clues": "psychiatry,Psychiatry,hospitals--private psychiatry,psychiatry counselling,doctors-medical practitioners psychiatry"
    },
    {
        "id": 1800,
        "categoryId": 27,
        "subCategoryName": "Psychologist",
        "icon": null,
        "clues": "child psychologist,clinical psychologist,Psychologist,psychologist,psychologists"
    },
    {
        "id": 1801,
        "categoryId": 27,
        "subCategoryName": "Psychotherapists",
        "icon": null,
        "clues": "Psychotherapists,psychotherapists"
    },
    {
        "id": 1804,
        "categoryId": 27,
        "subCategoryName": "Public Health Dentistry Specialists",
        "icon": null,
        "clues": "Public Health Dentistry Specialists,public health,public health dentistry specialists"
    },
    {
        "id": 1805,
        "categoryId": 27,
        "subCategoryName": "Public Health Medicine",
        "icon": null,
        "clues": "Public Health Medicine,public health medicine,public health"
    },
    {
        "id": 1821,
        "categoryId": 27,
        "subCategoryName": "Quarantine",
        "icon": null,
        "clues": "quarantine services,Quarantine,animal quarantine,quarantine,quarantine kennels"
    },
    {
        "id": 1828,
        "categoryId": 27,
        "subCategoryName": "Radiation Oncology",
        "icon": null,
        "clues": "radiation oncology opg,radiation oncology,Radiation Oncology,radiation oncologist,opg radiation oncology"
    },
    {
        "id": 1833,
        "categoryId": 27,
        "subCategoryName": "Radiographers",
        "icon": null,
        "clues": "radiographers,Radiographers"
    },
    {
        "id": 1861,
        "categoryId": 27,
        "subCategoryName": "Reflexology",
        "icon": null,
        "clues": "reflexology,reflexology massage,reflexology courses,Reflexology"
    },
    {
        "id": 1871,
        "categoryId": 27,
        "subCategoryName": "Rehabilitation Medicine",
        "icon": null,
        "clues": "Rehabilitation Medicine,rehabilitation medicine"
    },
    {
        "id": 1872,
        "categoryId": 27,
        "subCategoryName": "Rehabilitation Services",
        "icon": null,
        "clues": "rehabilitation hospital,safety audits rehabilitation services,drug rehabilitation services,Rehabilitation Services,rehabilitation,rehabilitation services physiotherapy,alcohol rehabilitation services,rehabilitation services,rehabilitation centre,rehabilitation providers,rehabilitation equipment"
    },
    {
        "id": 1877,
        "categoryId": 27,
        "subCategoryName": "Renal Medicine",
        "icon": null,
        "clues": "renal,renal doctor,doctors-medical practitioners renal medicine,renal medicine,renal disease specialist,Renal Medicine,car renal,renal physician,hospital renal dialysis unit,renal clinic,renal specialist"
    },
    {
        "id": 1894,
        "categoryId": 27,
        "subCategoryName": "Rheumatology",
        "icon": null,
        "clues": "doctors-medical practitioners rheumatology,Rheumatology,rheumatology"
    },
    {
        "id": 2013,
        "categoryId": 27,
        "subCategoryName": "Sexually Transmitted Diseases",
        "icon": null,
        "clues": "sexually transmitted diseases,Sexually Transmitted Diseases,sexually transmitted,sexually transmitted infection"
    },
    {
        "id": 2059,
        "categoryId": 27,
        "subCategoryName": "Skin Cancer Clinic",
        "icon": null,
        "clues": "skin cancer,skin cancer clinic,skin screen clinic,clinic skin,skin cancer screening,skin cancer check,skin cancer cosmetic surgery,skin clinics,Skin Cancer Clinic,skin cancer clinics,skin clinic,skin care clinic,skin cancer specialist,cancer skin clinic,skin check clinic,doctors-medical practitioners skin cancer,skin cancer scan,skin cancer treatment,skin & mole clinic,laser skin clinic"
    },
    {
        "id": 2060,
        "categoryId": 27,
        "subCategoryName": "Skin Treatments & Skin Clinics",
        "icon": null,
        "clues": "Skin Treatments & Skin Clinics,skin clinics,skin cancer clinics"
    },
    {
        "id": 2066,
        "categoryId": 27,
        "subCategoryName": "Sleep Disorders",
        "icon": null,
        "clues": "sleep disorders units,sleep disorders clinics,sleep disorders,sleep apnoea,sleep specialist,Sleep Disorders,doctors-medical practitioners sleep disorders,sleep clinic,sleep disorders clinic,sleep school,sleep doctor,sleep physician,sleep disorders centre,sleep centre"
    },
    {
        "id": 2091,
        "categoryId": 27,
        "subCategoryName": "Special Needs Dentists",
        "icon": null,
        "clues": "pre-schools for special needs,Special Needs Dentists,kindergartens & pre-schools special needs,special needs,special needs school,special needs dentists"
    },
    {
        "id": 2092,
        "categoryId": 27,
        "subCategoryName": "Speech Pathologists",
        "icon": null,
        "clues": "speech pathologists,Speech Pathologists,croatian speech pathologists,paediatrician speech pathologists,private speech pathologists,dyslexia speech pathologists,speech language pathologists"
    },
    {
        "id": 2106,
        "categoryId": 27,
        "subCategoryName": "Sports Medicine",
        "icon": null,
        "clues": "Sports Medicine,sports medicine doctor,sports medicine,sports medicine supplies,sports medicine physiotherapist,sports medicine practitioners,sports medicine clinic"
    },
    {
        "id": 2161,
        "categoryId": 27,
        "subCategoryName": "Surgery-General",
        "icon": null,
        "clues": "Surgery-General,surgery-general"
    },
    {
        "id": 2162,
        "categoryId": 27,
        "subCategoryName": "Surgical Oncology",
        "icon": null,
        "clues": "surgical equipment,surgical,surgical instruments,Surgical Oncology,surgical oncology"
    },
    {
        "id": 2163,
        "categoryId": 27,
        "subCategoryName": "Surgical Supplies",
        "icon": null,
        "clues": "surgical equipment,Surgical Supplies,medical & surgical supplies,surgical supplies,surgical,surgical instruments"
    },
    {
        "id": 2245,
        "categoryId": 27,
        "subCategoryName": "Thoracic Medicine",
        "icon": null,
        "clues": "thoracic doctor,thoracic specialist,thoracic medicine,thoracic surgeons,Thoracic Medicine,thoracic,doctors-medical practitioners thoracic medicine,thoracic physician"
    },
    {
        "id": 2306,
        "categoryId": 27,
        "subCategoryName": "Travel Health",
        "icon": null,
        "clues": "insurance travel,travel agencies,travel health clinic,travel health,travel agents & consultants,travel doctor,Travel Health,travel"
    },
    {
        "id": 2338,
        "categoryId": 27,
        "subCategoryName": "Ultrasonic Equipment & Supplies",
        "icon": null,
        "clues": "ultrasonic equipment,ultrasonic equipment & supplies,Ultrasonic Equipment & Supplies"
    },
    {
        "id": 2339,
        "categoryId": 27,
        "subCategoryName": "Ultraviolet Equipment & Services",
        "icon": null,
        "clues": "ultraviolet water purifiers,ultraviolet equipment & services,ultraviolet,ultraviolet ink,ultraviolet lamps,ultraviolet nail suppliers,Ultraviolet Equipment & Services,ultraviolet water treatment,ultraviolet gel"
    },
    {
        "id": 2354,
        "categoryId": 27,
        "subCategoryName": "Urology",
        "icon": null,
        "clues": "urology,doctors-medical practitioners urology,Urology"
    },
    {
        "id": 2363,
        "categoryId": 27,
        "subCategoryName": "Vascular Medicine",
        "icon": null,
        "clues": "vascular medicine,Vascular Medicine,vascular specialist,doctors-medical practitioners vascular medicine,vascular surgeons,vascular,vascular clinic"
    },
    {
        "id": 2364,
        "categoryId": 27,
        "subCategoryName": "Vascular Surgery",
        "icon": null,
        "clues": "vascular specialist,Vascular Surgery,vascular surgeons,vascular,vascular surgery,vascular clinic"
    },
    {
        "id": 2504,
        "categoryId": 27,
        "subCategoryName": "X-Ray Equipment & Supplies",
        "icon": null,
        "clues": "x-ray equipment & supplies,xray supplies,xray centre,xray,X-Ray Equipment & Supplies,xray imaging,x-ray equipment"
    }
    ]
},
{
    "id": 28,
    "icon": null,
    "subCategories": [{
    "id": 66,
    "categoryId": 28,
    "subCategoryName": "Alpaca & Llama Breeders",
    "icon": null,
    "clues": "alpaca breeders,alpaca wool,alpaca & llama breeders,alpaca,Alpaca & Llama Breeders,alpaca farm,alpaca stud,alpaca transport"
},
    {
        "id": 82,
        "categoryId": 28,
        "subCategoryName": "Animal & Pet Enclosures",
        "icon": null,
        "clues": "Animal & Pet Enclosures,animal & pet enclosures reptiles,better pet from animal shelters,animal enclosures,small animal pet shop,animal & pet enclosures"
    },
    {
        "id": 83,
        "categoryId": 28,
        "subCategoryName": "Animal Welfare",
        "icon": null,
        "clues": "Animal Welfare,animal welfare,animal pound,animal shelter,animal welfare organisations,animal welfare organisations rescues,animal hospital,animal welfare centre,animal clinic,animal farm,animal clinics & hospitals,animal rescue,animal welfare shelter"
    },
    {
        "id": 92,
        "categoryId": 28,
        "subCategoryName": "Aquariums & Aquarium Supplies",
        "icon": null,
        "clues": "aquariums & supplies,pet shops aquariums,aquariums & supplies tropical,Aquariums & Aquarium Supplies,aquariums & supplies reptiles,aquariums & supplies koi,aquariums & supplies salt water"
    },
    {
        "id": 199,
        "categoryId": 28,
        "subCategoryName": "Bird & Poultry Clubs",
        "icon": null,
        "clues": "bird aviary,bird vet,Bird & Poultry Clubs,clubs-bird & poultry,bird shop,bird cages"
    },
    {
        "id": 200,
        "categoryId": 28,
        "subCategoryName": "Bird Breeders & Dealers",
        "icon": null,
        "clues": "bird breeders & dealers,bird breeders,bird dealers,Bird Breeders & Dealers"
    },
    {
        "id": 201,
        "categoryId": 28,
        "subCategoryName": "Birdwatching Clubs",
        "icon": null,
        "clues": "Birdwatching Clubs,birdwatching clubs,clubs--birdwatching"
    },
    {
        "id": 388,
        "categoryId": 28,
        "subCategoryName": "Cat Breeders",
        "icon": null,
        "clues": "siamese cat breeders,ragdoll cat breeders,Cat Breeders,russian blue cat breeders,oriental cat breeders,tonkinese cat breeders,cat breeders,persian cat breeders,burmese cat breeders"
    },
    {
        "id": 394,
        "categoryId": 28,
        "subCategoryName": "Cattery",
        "icon": null,
        "clues": "boarding kennels & cattery,cattery boarding,kennels & cattery,cattery for cat,cattery,boarding cattery,Cattery,cattery & kennels"
    },
    {
        "id": 581,
        "categoryId": 28,
        "subCategoryName": "Coursing & Kennel Clubs",
        "icon": null,
        "clues": "clubs-coursing & kennel,Coursing & Kennel Clubs"
    },
    {
        "id": 675,
        "categoryId": 28,
        "subCategoryName": "Dog & Cat Clipping & Grooming",
        "icon": null,
        "clues": "dog & cat clipping & grooming washing,dog & cat clipping & grooming puppies,mobile dog wash,dog grooming,dog & cat clipping & grooming mobile services,mobile dog clipping,dog nail clipping,cat & dog clipping,dog grooming mobile,dog obedience,dog clipping,dog & cat clipping,dog & cat clipping & grooming hydrobaths,dog clipping mobile,dog grooming supplies,dog clipping & grooming,mobile dog grooming,dog grooming & clipping,dog grooming clipping services,Dog & Cat Clipping & Grooming,dog grooming salon,dog & cat clipping & grooming,dog & cat clipping & grooming mobile,dog & cat grooming"
    },
    {
        "id": 676,
        "categoryId": 28,
        "subCategoryName": "Dog & Cat Minding - In Home",
        "icon": null,
        "clues": "dog minding,dog door installation specialists,Dog & Cat Minding - In Home,dog & cat minding--in home,dog & cat minding,dog minding services"
    },
    {
        "id": 677,
        "categoryId": 28,
        "subCategoryName": "Dog Boarding Kennels",
        "icon": null,
        "clues": "dog boarding kennels day care,dog boarding kennels,dog boarding houses,dog kennel boarding,dog kennels accommodation,dog training & boarding,garden sheds dog kennels,Dog Boarding Kennels,dog boarding,dog kennels,dog kennels for sales,dog & cat kennels"
    },
    {
        "id": 678,
        "categoryId": 28,
        "subCategoryName": "Dog Breeders",
        "icon": null,
        "clues": "small dog breeders,australian cattle dog breeders,beagle dog breeders,designer dog breeders,alaskan malamute dog breeders,staffordshire dog breeders,cattle dog breeders,american eskimo dog breeders,Dog Breeders,doberman dog breeders,dog breeders"
    },
    {
        "id": 679,
        "categoryId": 28,
        "subCategoryName": "Dog Clubs",
        "icon": null,
        "clues": "clubs--dog,dog training clubs,Dog Clubs"
    },
    {
        "id": 680,
        "categoryId": 28,
        "subCategoryName": "Dog Training",
        "icon": null,
        "clues": "Dog Training,dog training agility,dog training clubs,dog training school,dog training courses,dog training,dog training & boarding,dog training puppies,dog training puppy school,dog training difficult dogs,dog training kennels"
    },
    {
        "id": 681,
        "categoryId": 28,
        "subCategoryName": "Dogs' Supplies",
        "icon": null,
        "clues": "Dogs' Supplies,dogs' supplies"
    },
    {
        "id": 1137,
        "categoryId": 28,
        "subCategoryName": "Horse Breakers",
        "icon": null,
        "clues": "Horse Breakers,horse breakers"
    },
    {
        "id": 1138,
        "categoryId": 28,
        "subCategoryName": "Horse Dentists",
        "icon": null,
        "clues": "Horse Dentists,horse dentists"
    },
    {
        "id": 1139,
        "categoryId": 28,
        "subCategoryName": "Horse Drawn Vehicles",
        "icon": null,
        "clues": "horse drawn vehicles,horse drawn carriage hire,horse drawn carts,Horse Drawn Vehicles,horse drawn carriage"
    },
    {
        "id": 1140,
        "categoryId": 28,
        "subCategoryName": "Horse Riding",
        "icon": null,
        "clues": "horse riding lessons,horse riding stables,horse riding helmets,Horse Riding,horse riding boots,horse riding club,horse riding camps,horse riding clothing,horse riding agistment,horse riding instructors,horse riding farm stays,horse riding centre,horse riding,horse riding saddlery,horse riding school,horse riding supplies,horse riding dressage"
    },
    {
        "id": 1141,
        "categoryId": 28,
        "subCategoryName": "Horse Stud Breeders &/or Dealers",
        "icon": null,
        "clues": "horse stud breeders,horse stud,horse breeders & dealers,miniature horse breeders,horse dealers,Horse Stud Breeders &/or Dealers,quarter horse stud,horse trainer studs &/or breeders,horse breeders"
    },
    {
        "id": 1142,
        "categoryId": 28,
        "subCategoryName": "Horse Trainers",
        "icon": null,
        "clues": "horse trainers,Horse Trainers"
    },
    {
        "id": 1143,
        "categoryId": 28,
        "subCategoryName": "Horse Transport",
        "icon": null,
        "clues": "Horse Transport,interstate horse transport,horse transport services,horse  transport"
    },
    {
        "id": 1654,
        "categoryId": 28,
        "subCategoryName": "Pet Care",
        "icon": null,
        "clues": "pet care services hydrobath,pet care services day care,pet care services walking,pet care services pet sitting,Pet Care,pet care services dogs,pet care services grooming,pet care services,pet care"
    },
    {
        "id": 1655,
        "categoryId": 28,
        "subCategoryName": "Pet Cemetery & Crematorium",
        "icon": null,
        "clues": "pet cemetery,pet cemeteries,crematoriums & supplies,pet crematorium services,pet cemetery crematorium,pet crematorium,Pet Cemetery & Crematorium"
    },
    {
        "id": 1656,
        "categoryId": 28,
        "subCategoryName": "Pet Clubs",
        "icon": null,
        "clues": "Pet Clubs,pet clubs,clubs-pet"
    },
    {
        "id": 1657,
        "categoryId": 28,
        "subCategoryName": "Pet Shops' Suppliers",
        "icon": null,
        "clues": "pet shop suppliers,pet shops reptiles,pet shops birds,pet shops,pet shops' suppliers kennels,pet shops' suppliers,pet shampoo trade suppliers,turtle pet shops,pet shops aquariums,pet shops enclosures,pet shops goldfish,pet shops cats,Pet Shops' Suppliers,pet door suppliers,pet shops rabbits,pet shops guinea pigs,pet shops dog accessories,pet shops' suppliers pet doors,pet shops puppies,budgie pet shops,pet shops pet food,pet accessories suppliers,pet product suppliers,pet shops dogs"
    },
    {
        "id": 1658,
        "categoryId": 28,
        "subCategoryName": "Pet Supplies & Pet Food",
        "icon": null,
        "clues": "pet food,wholesale pet supplies,pet supplies warehouse,pet food supply,pet food wholesalers,pet supplies retail,pet supplies shop,pet supplies,pet food store,pet food manufacturers,pet foods & supplies,pet food wholesale,pet food processors,Pet Supplies & Pet Food,pet food factory,pet supplies wholesale,pet supplies wholesalers"
    },
    {
        "id": 1659,
        "categoryId": 28,
        "subCategoryName": "Pet Transport Services",
        "icon": null,
        "clues": "pet transport services,pet transport,pet transport services singapore airlines,international pet transport,pet transport services qantas,pet transport international,Pet Transport Services,pet transportation,pet transport services united airlines"
    },
    {
        "id": 1660,
        "categoryId": 28,
        "subCategoryName": "Pet Warehouse - Shops & Stores",
        "icon": null,
        "clues": "pet shops reptiles,pet shops birds,pet shops,pet shops' suppliers kennels,turtle pet shops,pet shops aquariums,pet shop warehouse,Pet Warehouse - Shops & Stores,pet stores online,pet shops enclosures,pet shops goldfish,pet supplies warehouse,pet shops cats,pet food warehouse,pet shops rabbits,pet shops guinea pigs,pet shops dog accessories,pet shops' suppliers pet doors,pet shops puppies,budgie pet shops,pet stores e-mail address,pet shops pet food,pet shops dogs"
    },
    {
        "id": 1690,
        "categoryId": 28,
        "subCategoryName": "Pig Breeders & Dealers",
        "icon": null,
        "clues": "pig breeders,Pig Breeders & Dealers,pig breeders & dealers,guinea pig breeders,pig"
    },
    {
        "id": 2371,
        "categoryId": 28,
        "subCategoryName": "Veterinary Animal Clinics & Hospitals",
        "icon": null,
        "clues": "vet veterinary animal,veterinary surgeons animal hospitals,veterinary surgeons animal clinics,Veterinary Animal Clinics & Hospitals"
    },
    {
        "id": 2372,
        "categoryId": 28,
        "subCategoryName": "Veterinary Laboratories",
        "icon": null,
        "clues": "veterinary laboratories,Veterinary Laboratories"
    },
    {
        "id": 2373,
        "categoryId": 28,
        "subCategoryName": "Veterinary Supplies & Instruments",
        "icon": null,
        "clues": "veterinary supplies,veterinary supplies & instruments,Veterinary Supplies & Instruments"
    },
    {
        "id": 2374,
        "categoryId": 28,
        "subCategoryName": "Vets & Veterinary Surgeons",
        "icon": null,
        "clues": "24 hour vets,Vets & Veterinary Surgeons"
    }
    ]
},
{
    "id": 29,
    "icon": null,
    "subCategories": [{
    "id": 4,
    "categoryId": 29,
    "subCategoryName": "Aboriginal & Torres Strait Islander Associations & Organisations",
    "icon": null,
    "clues": "aboriginal organisations,aboriginal art gallery,aboriginal,aboriginal medical centre,aboriginal & torres strait islander organisations,Aboriginal & Torres Strait Islander Associations & Organisations,aboriginal hostel,aboriginal corporation,aboriginal & torres strait islander associations & organisations,aboriginal services,aboriginal art,aboriginal health"
},
    {
        "id": 10,
        "categoryId": 29,
        "subCategoryName": "Accountants & Auditors",
        "icon": null,
        "clues": "accountants & auditors financial planning advice,accountants & auditors tax services,accountants & auditors commercial,accountants & auditors self-managed superannuation funds,accountants & auditors tax assessments,accountants & auditors audits,chartered accountants,Accountants & Auditors,accountants & auditors forensic accounting,accountants & auditors,accountants & auditors real estate,accountants & auditors inner west,accountants & auditors small business,accountants & auditors annual returns,accountants & auditors business advice,accountants & auditors sole traders,accountants & auditors rental property advice,tax accountants"
    },
    {
        "id": 11,
        "categoryId": 29,
        "subCategoryName": "Accounting/Financial (Computer Software)",
        "icon": null,
        "clues": "Accounting/Financial (Computer Software),accounting/financial (computer software & packages),accounting/financial (computer software)"
    },
    {
        "id": 14,
        "categoryId": 29,
        "subCategoryName": "Actuaries",
        "icon": null,
        "clues": "Actuaries,actuaries"
    },
    {
        "id": 33,
        "categoryId": 29,
        "subCategoryName": "Advocates",
        "icon": null,
        "clues": "advocates,Advocates"
    },
    {
        "id": 35,
        "categoryId": 29,
        "subCategoryName": "Aerial Photography",
        "icon": null,
        "clues": "aerial photography,aerial photos,aerial,aerial installation,aerial spraying,Advertising - Aerial,aerial surveys,aerial agriculture,aerial repair,Aerial Photography,aerial advertising,tv aerial"
    },
    {
        "id": 36,
        "categoryId": 29,
        "subCategoryName": "Aeronautical Engineers",
        "icon": null,
        "clues": "Aeronautical Engineers,aeronautical maps & or mapping,aeronautical engineering,aeronautical,aeronautical engineers"
    },
    {
        "id": 40,
        "categoryId": 29,
        "subCategoryName": "Agents - General",
        "icon": null,
        "clues": "Agents - General,agents-general,Theatrical Managers,Producers & Agents,medical agents general practice"
    },
    {
        "id": 41,
        "categoryId": 29,
        "subCategoryName": "Agistment",
        "icon": null,
        "clues": "horse riding agistment,Agistment,agistment,agistment centre,horse agistment,agistment services"
    },
    {
        "id": 59,
        "categoryId": 29,
        "subCategoryName": "Alarm Systems & Burglar Alarms",
        "icon": null,
        "clues": "security alarms,burglar alarm installation,alarm & burglar systems,alarms systems,burglar alarms,burglar alarm repairs,Alarm Systems & Burglar Alarms"
    },
    {
        "id": 60,
        "categoryId": 29,
        "subCategoryName": "Alarm Systems--Machinery Protection",
        "icon": null,
        "clues": "security alarms,alarm & burglar systems,alarms systems,alarm systems--machinery protection,burglar alarms,Alarm Systems--Machinery Protection"
    },
    {
        "id": 62,
        "categoryId": 29,
        "subCategoryName": "Alerting Systems & Services",
        "icon": null,
        "clues": "alerting systems,alerting systems & services,Alerting Systems & Services"
    },
    {
        "id": 80,
        "categoryId": 29,
        "subCategoryName": "Analysts",
        "icon": null,
        "clues": "water analysts,analysts,systems analysts,business analysts,computer analysts,analysts laboratory,analysts food testing,handwriting analysts,Analysts,chemical analysts,financial analysts"
    },
    {
        "id": 94,
        "categoryId": 29,
        "subCategoryName": "Arbitration",
        "icon": null,
        "clues": "Arbitration,arbitration"
    },
    {
        "id": 95,
        "categoryId": 29,
        "subCategoryName": "Arbitrators--Commercial",
        "icon": null,
        "clues": "arbitrators--commercial,Arbitrators--Commercial"
    },
    {
        "id": 97,
        "categoryId": 29,
        "subCategoryName": "Archaeology",
        "icon": null,
        "clues": "Archaeology,archaeology,heritage & history consultants archaeology,archaeology consultants"
    },
    {
        "id": 99,
        "categoryId": 29,
        "subCategoryName": "Architects",
        "icon": null,
        "clues": "architects interior architects,Architects,architects,architects landscaping,architects heritage,architects design"
    },
    {
        "id": 100,
        "categoryId": 29,
        "subCategoryName": "Archival & Preservation Products & Services",
        "icon": null,
        "clues": "archival storage,Archival & Preservation Products & Services,archival products,art supplies archival,archival & preservation products & services,archival supplies"
    },
    {
        "id": 104,
        "categoryId": 29,
        "subCategoryName": "Art Dealers",
        "icon": null,
        "clues": "art dealers & valuers,Art Dealers,art dealers"
    },
    {
        "id": 113,
        "categoryId": 29,
        "subCategoryName": "Artists--Commercial",
        "icon": null,
        "clues": "artists--commercial,commercial artists,Artists--Commercial"
    },
    {
        "id": 118,
        "categoryId": 29,
        "subCategoryName": "Assayers",
        "icon": null,
        "clues": "assayers laboratory,assayers,Assayers"
    },
    {
        "id": 122,
        "categoryId": 29,
        "subCategoryName": "Auction Rooms",
        "icon": null,
        "clues": "auctions house,furniture auction rooms,Auction Rooms,furniture auctions,auctions,auction rooms"
    },
    {
        "id": 123,
        "categoryId": 29,
        "subCategoryName": "Auctioneers",
        "icon": null,
        "clues": "Auctioneers,auctioneers,auctioneers estates,auctioneers office equipment,auctioneers & valuers,antiques auctioneers"
    },
    {
        "id": 158,
        "categoryId": 29,
        "subCategoryName": "Banks & Financial Institutions",
        "icon": null,
        "clues": "Banks & Financial Institutions"
    },
    {
        "id": 162,
        "categoryId": 29,
        "subCategoryName": "Barristers & Solicitors (TAS only)",
        "icon": null,
        "clues": "Barristers,barristers & solicitors,barristers,barristers local court matters,barristers,barristers chambers,Barristers,Barristers & Solicitors,barristers family law"
    },
    {
        "id": 163,
        "categoryId": 29,
        "subCategoryName": "Barristers (All states except TAS)",
        "icon": null,
        "clues": "Barristers,barristers & solicitors,barristers,barristers local court matters,barristers,barristers chambers,Barristers & Solicitors,barristers family law"
    },
    {
        "id": 164,
        "categoryId": 29,
        "subCategoryName": "Barristers' Clerks",
        "icon": null,
        "clues": "Barristers,barristers & solicitors,barristers,barristers' clerks,barristers local court matters,barristers,barristers chambers,Barristers' Clerks,Barristers & Solicitors,barristers family law"
    },
    {
        "id": 167,
        "categoryId": 29,
        "subCategoryName": "Barter Exchange",
        "icon": null,
        "clues": "Barter Exchange"
    },
    {
        "id": 228,
        "categoryId": 29,
        "subCategoryName": "Bomboniere",
        "icon": null,
        "clues": "bomboniere boxes,bomboniere,Bomboniere,bomboniere supplies"
    },
    {
        "id": 234,
        "categoryId": 29,
        "subCategoryName": "Bookkeeping Services",
        "icon": null,
        "clues": "bookkeeping courses,bookkeeping,bookkeeping businesses,bookkeeping services myob,bookkeeping myob,bookkeeping services MYOB,Bookkeeping Services,bookkeeping services,bookkeeping training,bookkeeping business"
    },
    {
        "id": 270,
        "categoryId": 29,
        "subCategoryName": "Brokers--General",
        "icon": null,
        "clues": "brokers--general,Brokers--General"
    },
    {
        "id": 292,
        "categoryId": 29,
        "subCategoryName": "Bus & Truck Repairs",
        "icon": null,
        "clues": "truck & bus repair,Bus & Truck Repairs,truck & bus repairs"
    },
    {
        "id": 293,
        "categoryId": 29,
        "subCategoryName": "Bus Builders",
        "icon": null,
        "clues": "bus builders,bus & coach builders,bus body builders,Bus Builders"
    },
    {
        "id": 296,
        "categoryId": 29,
        "subCategoryName": "Business & Professional Organisations",
        "icon": null,
        "clues": "Business & Professional Organisations,organisations--business & professional,business & professional organisations"
    },
    {
        "id": 297,
        "categoryId": 29,
        "subCategoryName": "Business (Training & Development)",
        "icon": null,
        "clues": "business training courses,business writing training,business (training & development),Business (Training & Development),small business training,business training,diploma of business training"
    },
    {
        "id": 298,
        "categoryId": 29,
        "subCategoryName": "Business Brokers",
        "icon": null,
        "clues": "business brokers,small business brokers,Business Brokers,accounting business brokers"
    },
    {
        "id": 301,
        "categoryId": 29,
        "subCategoryName": "Business Consultants",
        "icon": null,
        "clues": "business consultants business coaching,small business consultants,computer business consultants,business services & consultants,business strategy consultants,business consultants human resources,business management consultants,Business Consultants,business consultants"
    },
    {
        "id": 302,
        "categoryId": 29,
        "subCategoryName": "Business Law",
        "icon": null,
        "clues": "Business Law,family law mediation (business),business law"
    },
    {
        "id": 303,
        "categoryId": 29,
        "subCategoryName": "Business Records Management",
        "icon": null,
        "clues": "Business Records Management,business records destruction,business records storage,business management,business records,business records management,business records management & storage"
    },
    {
        "id": 304,
        "categoryId": 29,
        "subCategoryName": "Business Systems Consultants",
        "icon": null,
        "clues": "business consultants business coaching,business systems,business consultants human resources,business phone systems,small business consultants,telephone business systems,business telephone systems,computer business consultants,business services & consultants,business strategy consultants,business systems consultants,business management consultants,Business Systems Consultants"
    },
    {
        "id": 305,
        "categoryId": 29,
        "subCategoryName": "Business/Commercial (Computer Software)",
        "icon": null,
        "clues": "Business/Commercial (Computer Software),business/commercial (computer software),business/commercial (computer software & packages)"
    },
    {
        "id": 310,
        "categoryId": 29,
        "subCategoryName": "Buyers' Agent",
        "icon": null,
        "clues": "Buyers' Agent,real estate buyers' agents,buyers agents"
    },
    {
        "id": 323,
        "categoryId": 29,
        "subCategoryName": "Call Centre Services",
        "icon": null,
        "clues": "call centre,call centre operator,Call Centre Services,telemarketing outbound call centre,marketing call centre services,call centre recruitment,call centre outsourcing companies,call centre services,outbound call centre,call centre services human resources,call centre training,call centre services telstra,inbound call centre"
    },
    {
        "id": 371,
        "categoryId": 29,
        "subCategoryName": "Career Counselling",
        "icon": null,
        "clues": "career counselling job search strategies,dyslexia career counselling,career counselling human resources,personal development career counselling,nlp career counselling,neuro linguistic programming career counselling,career advice & counselling,Career Counselling,career counselling"
    },
    {
        "id": 382,
        "categoryId": 29,
        "subCategoryName": "Cartoonists & Caricaturists",
        "icon": null,
        "clues": "Cartoonists & Caricaturists,cartoonists & caricaturists,artists cartoonists"
    },
    {
        "id": 385,
        "categoryId": 29,
        "subCategoryName": "Casting Agencies & Consultants",
        "icon": null,
        "clues": "casting agencies,casting agencies & consultants,Casting Agencies & Consultants,casting agents,casting director,animal casting agencies,casting consultants"
    },
    {
        "id": 386,
        "categoryId": 29,
        "subCategoryName": "Castings",
        "icon": null,
        "clues": "castings,castings importers,steel castings,Castings"
    },
    {
        "id": 400,
        "categoryId": 29,
        "subCategoryName": "Cemetery",
        "icon": null,
        "clues": "pet cemetery,lawn cemetery,cemetery memorials,pet cemetery crematorium,Cemetery,pets cemetery,cemetery,cemetery monument"
    },
    {
        "id": 409,
        "categoryId": 29,
        "subCategoryName": "Charities & Charitable Organisations",
        "icon": null,
        "clues": "charities & charitable organisations,charities clothing,charities,charities clothing store,charities clothing bins,charities op shop,charities organisers,charities & charitable organisations volunteer opportunities,charities shop,charities donation,charities bins,charities collection,charities furniture,Charities & Charitable Organisations,charities regulator,charities organisations,charities & charitable organisations shelter,charities foundations"
    },
    {
        "id": 449,
        "categoryId": 29,
        "subCategoryName": "Civil & Road Contractors",
        "icon": null,
        "clues": "civil contractors excavating,contractors civil,civil engineering contractors,road contractors civil contractors,civil contractors,Civil & Road Contractors"
    },
    {
        "id": 450,
        "categoryId": 29,
        "subCategoryName": "Civil Engineers",
        "icon": null,
        "clues": "civil engineers construction,structural & civil engineers,engineers civil,civil design engineers,civil engineers consultants,consulting civil engineers,Civil Engineers,engineers consulting civil,civil engineers,civil engineers--general,civil engineers residential,civil & structural engineers"
    },
    {
        "id": 451,
        "categoryId": 29,
        "subCategoryName": "Civil Litigation",
        "icon": null,
        "clues": "civil litigation,civil litigation asbestos exposure,civil litigation asbestos claims,solicitors civil litigation,Civil Litigation"
    },
    {
        "id": 452,
        "categoryId": 29,
        "subCategoryName": "Civil Rights Organisations",
        "icon": null,
        "clues": "organisations--civil rights,Civil Rights Organisations"
    },
    {
        "id": 469,
        "categoryId": 29,
        "subCategoryName": "Cloud Computing",
        "icon": null,
        "clues": "Cloud Computing,cloud computing vendors,cloud computing,silver cloud,cloud computing service,cloud computing provider"
    },
    {
        "id": 489,
        "categoryId": 29,
        "subCategoryName": "Commercial & Industrial Photographers",
        "icon": null,
        "clues": "Commercial & Industrial Photographers"
    },
    {
        "id": 497,
        "categoryId": 29,
        "subCategoryName": "Commercial Law",
        "icon": null,
        "clues": "Commercial Law,solicitors commercial law,commercial law"
    },
    {
        "id": 498,
        "categoryId": 29,
        "subCategoryName": "Commodity Brokers",
        "icon": null,
        "clues": "commodity,commodity brokers,Commodity Brokers"
    },
    {
        "id": 501,
        "categoryId": 29,
        "subCategoryName": "Community Advisory Services",
        "icon": null,
        "clues": "community advisory services,Community Advisory Services"
    },
    {
        "id": 516,
        "categoryId": 29,
        "subCategoryName": "Computer Networking & Installation",
        "icon": null,
        "clues": "computer networking lan,computer networking upgrades,computer networking certified,computer equipment--installation,computer equipment--installation & networking,computer equipment--installation & networking installation,computer networking voip,computer networking consulting,computer equipment--installation & networking cabling,computer installation,computer installation & networking,computer networking cables,computer equipment--installation & networking technical support,computer equipment--installation & networking cables,computer equipment--installation & networking ups,Computer Networking & Installation,computer networking fibre optics,computer networking servers,computer equipment--installation & networking fibre optics,computer networking integration,computer networking ups,computer equipment--installation & networking communications,computer networking project management,computer networking licensed,computer networking,computer cable installation,computer networking development,computer software installation,computer networking technical support,computer networking design,computer equipment--installation & networking data cabling,computer networking installation,computer hardware installation"
    },
    {
        "id": 520,
        "categoryId": 29,
        "subCategoryName": "Computer Software",
        "icon": null,
        "clues": "computer software & packages esri australia,computer software training,computer software & packages programming,computer software & packages database developer,Computer Software,computer software,computer software suppliers,computer software & packages,computer software retail"
    },
    {
        "id": 521,
        "categoryId": 29,
        "subCategoryName": "Computer Tech & IT Support",
        "icon": null,
        "clues": "computer support,computer support services,Computer Tech & IT Support,computer technical assistance,computer it support,computer equipment--hardware technical support,home computer support,computer network support,computer technical support,computer tech support"
    },
    {
        "id": 546,
        "categoryId": 29,
        "subCategoryName": "Conservation & Environmental Organisations",
        "icon": null,
        "clues": "Conservation & Environmental Organisations,environmental & conservation,organisations--conservation & environmental,organisations--conservation,conservation & environmental organisations"
    },
    {
        "id": 548,
        "categoryId": 29,
        "subCategoryName": "Construction law",
        "icon": null,
        "clues": "law construction,construction law,Construction law,construction law firm"
    },
    {
        "id": 550,
        "categoryId": 29,
        "subCategoryName": "Construction/Engineering (Computer Software)",
        "icon": null,
        "clues": "construction/engineering (computer software & packages),Construction/Engineering (Computer Software),construction/engineering (computer software)"
    },
    {
        "id": 551,
        "categoryId": 29,
        "subCategoryName": "Consulates & Legations",
        "icon": null,
        "clues": "consulates & legations,Consulates & Legations"
    },
    {
        "id": 553,
        "categoryId": 29,
        "subCategoryName": "Contractors--General",
        "icon": null,
        "clues": "contractors--general,Contractors--General,general contractors"
    },
    {
        "id": 557,
        "categoryId": 29,
        "subCategoryName": "Conveyancer & Conveyancing Services",
        "icon": null,
        "clues": "conveyancers,Conveyancer & Conveyancing Services"
    },
    {
        "id": 558,
        "categoryId": 29,
        "subCategoryName": "Conveyancing & Property Law",
        "icon": null,
        "clues": "conveyancing services,conveyancing lawyers,conveyancing solicitors,Conveyancing & Property Law,conveyancing,conveyancing & property law"
    },
    {
        "id": 567,
        "categoryId": 29,
        "subCategoryName": "Copyright",
        "icon": null,
        "clues": "Copyright,copyright lawyers,copyright,copyright solicitors,copyright barrister"
    },
    {
        "id": 575,
        "categoryId": 29,
        "subCategoryName": "Cost Estimators",
        "icon": null,
        "clues": "Cost Estimators,cost estimators,cost assessors"
    },
    {
        "id": 582,
        "categoryId": 29,
        "subCategoryName": "Court Reporting Services",
        "icon": null,
        "clues": "health & fitness centres & services squash court,court reporting,Court Reporting Services,court reporting services"
    },
    {
        "id": 590,
        "categoryId": 29,
        "subCategoryName": "Cremation",
        "icon": null,
        "clues": "cremation,funeral directors cremation,cremation urns,Cremation"
    },
    {
        "id": 593,
        "categoryId": 29,
        "subCategoryName": "Criminal Law",
        "icon": null,
        "clues": "solicitors criminal law,criminal law,Criminal Law,criminal law solicitors"
    },
    {
        "id": 613,
        "categoryId": 29,
        "subCategoryName": "Data Communications",
        "icon": null,
        "clues": "Data Communications,data communications,communications & data"
    },
    {
        "id": 614,
        "categoryId": 29,
        "subCategoryName": "Data Preparation & Processing",
        "icon": null,
        "clues": "data preparation & processing,data processing,data preparation & processing services,Data Preparation & Processing,data preparation,data contractors,data preparation & processing data entry"
    },
    {
        "id": 615,
        "categoryId": 29,
        "subCategoryName": "Data Recovery Services",
        "icon": null,
        "clues": "data recovery,data recovery services,Data Recovery Services,computer data recovery,hard drive data recovery,data recovery specialist"
    },
    {
        "id": 618,
        "categoryId": 29,
        "subCategoryName": "Debt Collection",
        "icon": null,
        "clues": "debt collection,debt collection agents,Debt Collection,debt recovery,debt collection agencies,debt collection agencies bailiffs,debt collection services"
    },
    {
        "id": 619,
        "categoryId": 29,
        "subCategoryName": "Debt Consolidation",
        "icon": null,
        "clues": "debt consolidation bankruptcy trustees,debt consolidation,debt consolidation finance brokers,debt consolidation finance mortgage loans,debt consolidation liquidation & insolvency services,debt consolidation loans,debt consolidation financial planning,debt consolidation mortgage brokers,Debt Consolidation"
    },
    {
        "id": 638,
        "categoryId": 29,
        "subCategoryName": "Designing Engineers",
        "icon": null,
        "clues": "Designing Engineers,heavy engineering designing engineers,designing engineers"
    },
    {
        "id": 639,
        "categoryId": 29,
        "subCategoryName": "Desktop Publishing Services",
        "icon": null,
        "clues": "desktop publishing equipment,desktop publishing services,Desktop Publishing Services,desktop publishing equipment & supplies,desktop publishing,desktop publishing software"
    },
    {
        "id": 641,
        "categoryId": 29,
        "subCategoryName": "Developers (Computer Software)",
        "icon": null,
        "clues": "database developers,Developers (Computer Software),developers (computer software),developers (computer software & packages)"
    },
    {
        "id": 652,
        "categoryId": 29,
        "subCategoryName": "Dietitians",
        "icon": null,
        "clues": "dietitians nutrition counselling,dietitians weight management,weight reducing treatments dietitians,Dietitians,dietitians"
    },
    {
        "id": 659,
        "categoryId": 29,
        "subCategoryName": "Disadvantaged Groups Aid Organisations",
        "icon": null,
        "clues": "organisations--disadvantaged groups aid,Disadvantaged Groups Aid Organisations"
    },
    {
        "id": 665,
        "categoryId": 29,
        "subCategoryName": "Display Homes",
        "icon": null,
        "clues": "display homes,Display Homes,new homes display centre,display homes builders,house & land display homes"
    },
    {
        "id": 688,
        "categoryId": 29,
        "subCategoryName": "Draftsman & Drafting Services",
        "icon": null,
        "clues": "Draftsman & Drafting Services"
    },
    {
        "id": 714,
        "categoryId": 29,
        "subCategoryName": "Economic Consultants",
        "icon": null,
        "clues": "economic consultants,economics,Economic Consultants,economic development"
    },
    {
        "id": 716,
        "categoryId": 29,
        "subCategoryName": "Editors & Editing Services",
        "icon": null,
        "clues": "editors & editing services,Editors & Editing Services"
    },
    {
        "id": 722,
        "categoryId": 29,
        "subCategoryName": "Elder Law",
        "icon": null,
        "clues": "elder law,solicitors elder law,Elder Law,elderly,elder law specialist,elder law solicitors,elder abuse"
    },
    {
        "id": 738,
        "categoryId": 29,
        "subCategoryName": "Electrical Engineers",
        "icon": null,
        "clues": "electrical engineers outsourcing companies,electrical contractors engineers,Electrical Engineers,mechanical electrical engineers,engineers consulting electrical,electrical engineers consulting,engineers electrical,electrical engineers air conditioning,electrical consultants engineers,consulting electrical engineers,motor engineers & repairers electrical repairs,electrical engineers,electrical design engineers,electrical consulting engineers"
    },
    {
        "id": 742,
        "categoryId": 29,
        "subCategoryName": "Electrical Test & Tag",
        "icon": null,
        "clues": "electrical tagging equipment,test & tag electrical testing,test & tag electrical,electrical tagging,Electrical Test & Tag,electrical testing & tagging,electrical tagging & testing"
    },
    {
        "id": 751,
        "categoryId": 29,
        "subCategoryName": "Electronic Engineers",
        "icon": null,
        "clues": "engineers electronic,electronic engineers,Electronic Engineers"
    },
    {
        "id": 768,
        "categoryId": 29,
        "subCategoryName": "Employment & Industrial Relations",
        "icon": null,
        "clues": "Employment & Industrial Relations,employment & industrial relations,employment relations,public relations employment"
    },
    {
        "id": 769,
        "categoryId": 29,
        "subCategoryName": "Employment & Recruitment Agencies",
        "icon": null,
        "clues": "employment services,employment agencies disabilities,mining employment agencies,employment agencies hospitality,employment agencies it,employment agencies western australia,recruitment employment,employment services permanent,employment--labour hire contractors,employment recruitment,employment agencies construction,employment agencies for building,recruitment agencies employment agencies services,employment services recruitment consultants,employment agencies teaching,employment agencies engineering,employment agencies for manufacturers business,employment agencies legal,agencies employment,Employment & Recruitment Agencies,employment services temporary,recruitment employment agencies,employment services recruitment job specialist,employment--labour hire contractors manual labourers,employment agencies mining,employment--labour hire contractors recruitment,employment consultants,employment agencies accounting,hospitality employment agencies,temporary employment agencies,employment agencies"
    },
    {
        "id": 777,
        "categoryId": 29,
        "subCategoryName": "Engineering & Engineers",
        "icon": null,
        "clues": "general engineering,engineering general,Engineering & Engineers,engineering"
    },
    {
        "id": 779,
        "categoryId": 29,
        "subCategoryName": "Engineering Consultants & Services",
        "icon": null,
        "clues": "hydraulic engineering consultants,plastic engineering services,environmental engineering consultants,engineering placement consultants,engineering employment services,consultants engineering,engineering recruitment services,mechanical engineering consultants,building services engineering,hydraulic & engineering services,electrical engineering consultants,construction engineering consultants,drafting services engineering,drafting services civil engineering,building engineering services,civil engineering consultants,traffic engineering consultants,drafting services structural engineering drafting,engineering recruitment consultants,engineering design consultants,engineering services,engineering consultants,Engineering Consultants & Services"
    },
    {
        "id": 788,
        "categoryId": 29,
        "subCategoryName": "Environmental Consultants",
        "icon": null,
        "clues": "environmental engineering consultants,consultants environmental,Environmental Consultants,environmental consultants"
    },
    {
        "id": 789,
        "categoryId": 29,
        "subCategoryName": "Environmental Law",
        "icon": null,
        "clues": "environmental law,planning & environmental lawyers,Environmental Law"
    },
    {
        "id": 811,
        "categoryId": 29,
        "subCategoryName": "Export Agents",
        "icon": null,
        "clues": "Export Agents,export import,import export agents,export agents"
    },
    {
        "id": 818,
        "categoryId": 29,
        "subCategoryName": "Facilitators",
        "icon": null,
        "clues": "facilitators,Facilitators,facilitators services"
    },
    {
        "id": 819,
        "categoryId": 29,
        "subCategoryName": "Factoring Finance",
        "icon": null,
        "clues": "factoring,debt factoring,finance--factoring,Factoring Finance"
    },
    {
        "id": 823,
        "categoryId": 29,
        "subCategoryName": "Family Law",
        "icon": null,
        "clues": "family law,Family Law,family law solicitors,family law specialist,family law court lawyers,family law courts,family law barrister"
    },
    {
        "id": 825,
        "categoryId": 29,
        "subCategoryName": "Family Welfare Organisations",
        "icon": null,
        "clues": "organisations--family welfare,family organisations,Family Welfare Organisations,family welfare organisations,family support welfare organisers,family welfare"
    },
    {
        "id": 834,
        "categoryId": 29,
        "subCategoryName": "Fashion Agents",
        "icon": null,
        "clues": "fashion sales agents,wholesale fashion agents,fashion agents,fashion agents wholesalers,ladies fashion agents,Fashion Agents,fashion accessories agents"
    },
    {
        "id": 862,
        "categoryId": 29,
        "subCategoryName": "Finance Brokers",
        "icon": null,
        "clues": "finance brokers business finance,debtor finance brokers,finance brokers commercial,Finance Brokers,business finance brokers,commercial finance brokers,finance & insurance brokers,debt consolidation finance brokers,mobile finance brokers,finance brokers anz,finance brokers personal,finance brokers,car finance brokers,equipment finance brokers"
    },
    {
        "id": 863,
        "categoryId": 29,
        "subCategoryName": "Finance--Commercial Bills",
        "icon": null,
        "clues": "finance--commercial bills,Finance--Commercial Bills"
    },
    {
        "id": 864,
        "categoryId": 29,
        "subCategoryName": "Finance--Confirming",
        "icon": null,
        "clues": "finance--confirming,Finance--Confirming"
    },
    {
        "id": 865,
        "categoryId": 29,
        "subCategoryName": "Finance--Short Term Loans",
        "icon": null,
        "clues": "short term finance,finance--short term loans,Finance--Short Term Loans"
    },
    {
        "id": 866,
        "categoryId": 29,
        "subCategoryName": "Financial Planning",
        "icon": null,
        "clues": "financial planning centrelink,financial planning firms,Financial Planning,financial planning services,financial planning"
    },
    {
        "id": 867,
        "categoryId": 29,
        "subCategoryName": "Financiers & Investors",
        "icon": null,
        "clues": "Financiers & Investors,equipment & plant financiers,financiers,mortgage financiers,car financiers"
    },
    {
        "id": 887,
        "categoryId": 29,
        "subCategoryName": "Fleet Management Services",
        "icon": null,
        "clues": "fleet services,fleet management organisations,vehicle fleet management,fleet management services,fleet management,Fleet Management Services,fleet leasing"
    },
    {
        "id": 920,
        "categoryId": 29,
        "subCategoryName": "Foreign Currency Exchange (Forex)",
        "icon": null,
        "clues": "foreign currency exchanges,foreign currency,foreign exchange,foreign currency exchanges international money transfers,Foreign Currency Exchange (Forex),foreign currency exchange,foreign exchange rates,foreign exchange brokers"
    },
    {
        "id": 921,
        "categoryId": 29,
        "subCategoryName": "Forensic Services",
        "icon": null,
        "clues": "forensic engineers,forensic investigators,forensic psychiatrist,forensic psychologist,forensic accountants,forensic science,forensic,forensic psychology,Forensic Services,forensic medicine,forensic testing,forensic pathology,forensic laboratory,forensic scientists,forensic services,Forensic Pathology,forensic accounting,forensic cleaners"
    },
    {
        "id": 922,
        "categoryId": 29,
        "subCategoryName": "Forestry Services & Consultants",
        "icon": null,
        "clues": "forestry nursery,forestry consultants,forestry services,forestry equipment,Forestry Services & Consultants,forestry,forestry contractors,tyres vulcanising extra lugs forestry,forestry supplies,forestry services & consultants,forestry timber trucks"
    },
    {
        "id": 931,
        "categoryId": 29,
        "subCategoryName": "Franchising Law",
        "icon": null,
        "clues": "franchising law,Franchising Law"
    },
    {
        "id": 951,
        "categoryId": 29,
        "subCategoryName": "Fund Raising Consultants",
        "icon": null,
        "clues": "fundraising consultants,Fund Raising Consultants,Superannuation Fund,Annuity & Rollover Consultants"
    },
    {
        "id": 953,
        "categoryId": 29,
        "subCategoryName": "Funeral Celebrants",
        "icon": null,
        "clues": "Funeral Celebrants,funeral celebrants"
    },
    {
        "id": 969,
        "categoryId": 29,
        "subCategoryName": "Futures Brokers",
        "icon": null,
        "clues": "Futures Brokers,futures,futures brokers"
    },
    {
        "id": 998,
        "categoryId": 29,
        "subCategoryName": "Genealogy",
        "icon": null,
        "clues": "Genealogy,genealogy"
    },
    {
        "id": 1005,
        "categoryId": 29,
        "subCategoryName": "Geologists",
        "icon": null,
        "clues": "geologists' & geophysicists' supplies,Geologists,Geologists' & Geophysicists' Supplies,geologists,hydro geologists"
    },
    {
        "id": 1006,
        "categoryId": 29,
        "subCategoryName": "Geophysicists",
        "icon": null,
        "clues": "geologists' & geophysicists' supplies,Geologists' & Geophysicists' Supplies,geophysicists,Geophysicists"
    },
    {
        "id": 1008,
        "categoryId": 29,
        "subCategoryName": "Geotechnical Engineer",
        "icon": null,
        "clues": "flood damage geotechnical engineers,geotechnical testing,geotechnical,geotechnical engineers,geotechnical consultants & engineers,water damage geotechnical engineers,geotechnical engineers & consultants soil testing,geotechnical drilling,engineers geotechnical,geotechnical engineering,geotechnical engineers & consultants,geotechnical engineers--general,consulting engineers geotechnical,Geotechnical Engineer,geotechnical consultants"
    },
    {
        "id": 1041,
        "categoryId": 29,
        "subCategoryName": "Graphic Designers",
        "icon": null,
        "clues": "Graphic Designers,graphic designers logo design,graphic designers for packaging,graphic designers,freelance graphic designers,graphic artists designers,graphic designers print design,graphic designers web site design,designers graphic,graphic designers vehicle graphics,graphic designers signs,graphic designers business cards,graphic designers branding"
    },
    {
        "id": 1042,
        "categoryId": 29,
        "subCategoryName": "Graphics/CAD (Computer Software)",
        "icon": null,
        "clues": "graphics/cad (computer software),graphics/cad (computer software & packages),Graphics/CAD (Computer Software)"
    },
    {
        "id": 1092,
        "categoryId": 29,
        "subCategoryName": "Health/Medical (Computer Software)",
        "icon": null,
        "clues": "health/medical (computer software),Health/Medical (Computer Software),health/medical (computer software & packages)"
    },
    {
        "id": 1108,
        "categoryId": 29,
        "subCategoryName": "Heritage & History Consultants",
        "icon": null,
        "clues": "heritage & history consultants historical societies,heritage & history consultants interpretation,heritage & history consultants advice,Heritage & History Consultants,heritage & history consultants conservation management,heritage & history consultants impact statements,heritage & history consultants australia,heritage building consultants,heritage & history consultants project management,cultural heritage consultants,heritage & history consultants architecture,heritage & history consultants research,heritage & history consultants excavations,heritage consultants,heritage & history consultants anthropology,heritage & history consultants commercial,heritage & history consultants archaeology,heritage & history consultants restorations,heritage & history consultants museums,heritage & history consultants,heritage & history consultants cultural tourism,heritage & history consultants design,heritage & history consultants gis"
    },
    {
        "id": 1130,
        "categoryId": 29,
        "subCategoryName": "Home Loans",
        "icon": null,
        "clues": "Home Loans,home loans - mortgage brokers,home loans,home loans - finance - mortgage"
    },
    {
        "id": 1157,
        "categoryId": 29,
        "subCategoryName": "House Planning",
        "icon": null,
        "clues": "House Planning,house planning,house planning services"
    },
    {
        "id": 1164,
        "categoryId": 29,
        "subCategoryName": "Hydrographic Surveyors",
        "icon": null,
        "clues": "surveyors--hydrographic,hydrographic surveyors,Hydrographic Surveyors,hydrographic"
    },
    {
        "id": 1168,
        "categoryId": 29,
        "subCategoryName": "Hypnotherapy & Hypnosis",
        "icon": null,
        "clues": "hypnotherapy quit smoking,hypnotherapy,Hypnotherapy & Hypnosis"
    },
    {
        "id": 1177,
        "categoryId": 29,
        "subCategoryName": "Illustrators",
        "icon": null,
        "clues": "illustrators,graphic illustrators,Illustrators,3d cg illustrators,illustrators & designers"
    },
    {
        "id": 1178,
        "categoryId": 29,
        "subCategoryName": "Image & Colour Consultants",
        "icon": null,
        "clues": "corporate image consultants,make up & image consultants,personal image consultants,image consultants,image & colour consultants,Image & Colour Consultants"
    },
    {
        "id": 1179,
        "categoryId": 29,
        "subCategoryName": "Immigration Law",
        "icon": null,
        "clues": "immigration law migration consultants,immigration,immigration law,migration consultants & services immigration law,Immigration Law,immigration lawyers"
    },
    {
        "id": 1199,
        "categoryId": 29,
        "subCategoryName": "Information Services",
        "icon": null,
        "clues": "tourist information services,libraries information services,Information Services,information services"
    },
    {
        "id": 1203,
        "categoryId": 29,
        "subCategoryName": "Insolvency & Bankruptcy",
        "icon": null,
        "clues": "Insolvency & Bankruptcy,insolvency,insolvency & bankruptcy,insolvency accountants"
    },
    {
        "id": 1204,
        "categoryId": 29,
        "subCategoryName": "Insolvency & Liquidation Services",
        "icon": null,
        "clues": "insolvency,insolvency accountants,liquidation & insolvency services,liquidation & insolvency,debt consolidation liquidation & insolvency services,Insolvency & Liquidation Services"
    },
    {
        "id": 1215,
        "categoryId": 29,
        "subCategoryName": "Intellectual Property",
        "icon": null,
        "clues": "intellectual property,intellectual property lawyers,Intellectual Property,intellectual,intellectual disability"
    },
    {
        "id": 1219,
        "categoryId": 29,
        "subCategoryName": "International Law",
        "icon": null,
        "clues": "International Law,international lawyers,international law"
    },
    {
        "id": 1220,
        "categoryId": 29,
        "subCategoryName": "International Relations &/or Aid Organisations",
        "icon": null,
        "clues": "international relations,international relations &/or aid organisations,International Relations &/or Aid Organisations,international aid organisations"
    },
    {
        "id": 1225,
        "categoryId": 29,
        "subCategoryName": "Interpreters",
        "icon": null,
        "clues": "Interpreters,interpreters"
    },
    {
        "id": 1230,
        "categoryId": 29,
        "subCategoryName": "IT & Computer Consultants",
        "icon": null,
        "clues": "it systems consultants,it support,it services,consultants it,it management consultants,it security consultants,it employment consultants,it job consultants,IT & Computer Consultants,it consultants,it solutions"
    },
    {
        "id": 1242,
        "categoryId": 29,
        "subCategoryName": "Journalists",
        "icon": null,
        "clues": "Journalists,journalists"
    },
    {
        "id": 1279,
        "categoryId": 29,
        "subCategoryName": "Land Surveyors",
        "icon": null,
        "clues": "land surveyors,land building surveyors,surveyors--land subdivision planning,surveyors land development,surveyors--land civil engineering,consulting land surveyors,Land Surveyors,licensed land surveyors,surveyors--land"
    },
    {
        "id": 1280,
        "categoryId": 29,
        "subCategoryName": "Landscape Architects",
        "icon": null,
        "clues": "landscape design,landscape architects,residential landscape architects,landscape architects & designers,Landscape Architects,landscape architects contractors & designers"
    },
    {
        "id": 1294,
        "categoryId": 29,
        "subCategoryName": "Lawyers & Solicitors",
        "icon": null,
        "clues": "solicitors defence lawyers,solicitors lawyers conveyancing,lawyers solicitors,Lawyers & Solicitors,lawyers,lawyers barristers solicitors,migration lawyers,divorce lawyers,solicitors lawyers,property lawyers & solicitors"
    },
    {
        "id": 1307,
        "categoryId": 29,
        "subCategoryName": "Legal Support & Referral Services",
        "icon": null,
        "clues": "legal support,legal services,legal secretarial & typing services,legal aid services,Legal Support & Referral Services,migration consultants legal services,migration services legal services,legal support & referral services,community legal services,legal services work injury,legal secretarial services,employment services legal services,mediation services legal,legal support services"
    },
    {
        "id": 1309,
        "categoryId": 29,
        "subCategoryName": "Libraries",
        "icon": null,
        "clues": "libraries archives,libraries information services,libraries,libraries public,libraries lending,libraries health,libraries books,libraries--photograph,libraries bookshop,Libraries,libraries local history"
    },
    {
        "id": 1310,
        "categoryId": 29,
        "subCategoryName": "Libraries--Photograph & Film",
        "icon": null,
        "clues": "Libraries--Photograph & Film,libraries--photograph,libraries--photograph & film"
    },
    {
        "id": 1360,
        "categoryId": 29,
        "subCategoryName": "Management Consultants",
        "icon": null,
        "clues": "taxation consultants business management,traffic management consultants,energy management consultants & services origin energy,management systems consultants,management consultants,energy management consultants,project management consultants,management consultants recruiting,Management Consultants,business management consultants,management consultants business coaching,management consultants human resources"
    },
    {
        "id": 1361,
        "categoryId": 29,
        "subCategoryName": "Management Information Services",
        "icon": null,
        "clues": "account information management systems,information management,Management Information Services,project management services,management information services,management services,trusts--management services,property management services,records & information management storage providers,waste management services,building management services"
    },
    {
        "id": 1366,
        "categoryId": 29,
        "subCategoryName": "Manufacturing (Computer Software)",
        "icon": null,
        "clues": "manufacturing (computer software),Manufacturing (Computer Software),manufacturing (computer software & packages)"
    },
    {
        "id": 1367,
        "categoryId": 29,
        "subCategoryName": "Manufacturing Engineers",
        "icon": null,
        "clues": "engineers--manufacturing,consulting engineers manufacturing,heavy engineering manufacturing engineers,Manufacturing Engineers,engineers consulting manufacturing,engineers--manufacturing fabrication"
    },
    {
        "id": 1374,
        "categoryId": 29,
        "subCategoryName": "Marine Engineers",
        "icon": null,
        "clues": "marine engineers metal fabrication,Marine Engineers,marine engineers outboard motors,marine engineers & repair,marine engineers mobile services,marine engineers scania,marine engineers volvo,marine engineers,marine engineers honda"
    },
    {
        "id": 1377,
        "categoryId": 29,
        "subCategoryName": "Maritime Law",
        "icon": null,
        "clues": "Maritime Law,maritime law"
    },
    {
        "id": 1379,
        "categoryId": 29,
        "subCategoryName": "Marketing Consultants & Services",
        "icon": null,
        "clues": "internet marketing services search engines,internet marketing services website optimisation,marketing call centre services,internet marketing services animation,internet marketing services shopping carts,Marketing Consultants & Services,internet & web services online marketing services,marketing services,internet marketing services e-commerce services,event marketing services companies,internet marketing services website design,internet marketing services online marketing,marketing consultants,internet marketing services website hosting,direct marketing services,internet marketing services e-mail,marketing services & consultants"
    },
    {
        "id": 1383,
        "categoryId": 29,
        "subCategoryName": "Marriage, Family & Personal Counselling",
        "icon": null,
        "clues": "marriage celebrant,counselling--marriage,family & personal,marriage celebrants--civil,Marriage,Family & Personal Counselling,marriage,marriage guidance,civil marriage celebrant,marriage counsellors,marriage celebrants--civil justice of the peace"
    },
    {
        "id": 1393,
        "categoryId": 29,
        "subCategoryName": "Mathematical & Statistical Services",
        "icon": null,
        "clues": "mathematical & statistical services,Mathematical & Statistical Services"
    },
    {
        "id": 1399,
        "categoryId": 29,
        "subCategoryName": "Mechanical Engineer",
        "icon": null,
        "clues": "mechanical consultants engineers,mechanical engineering,engineers mechanical,heavy engineering mechanical engineers,mechanical engineering design companies,mechanical engineering consultants,mechanical consulting engineers,Mechanical Engineer,mechanical engineers,engineering mechanical,mechanical engineers & repair,consulting engineers mechanical,consulting mechanical engineers,mechanical consultants,mechanical engineers--general,engineers consulting mechanical"
    },
    {
        "id": 1403,
        "categoryId": 29,
        "subCategoryName": "Media Monitoring Services",
        "icon": null,
        "clues": "Media Monitoring Services,media monitoring,media services,media monitoring services media research,media buying services,media monitoring services"
    },
    {
        "id": 1404,
        "categoryId": 29,
        "subCategoryName": "Mediation",
        "icon": null,
        "clues": "mediation legal,mediation services relationships & counselling,mediation centre,mediation relationships & counselling,Mediation,mediation services legal"
    },
    {
        "id": 1405,
        "categoryId": 29,
        "subCategoryName": "Mediators",
        "icon": null,
        "clues": "Mediators,mediators legal,mediators relationships & counselling"
    },
    {
        "id": 1437,
        "categoryId": 29,
        "subCategoryName": "Meteorological Consultants",
        "icon": null,
        "clues": "meteorological consultants,Meteorological Consultants"
    },
    {
        "id": 1441,
        "categoryId": 29,
        "subCategoryName": "Migrant Organisations",
        "icon": null,
        "clues": "migrant services,migrant,migrant agents,organisations--migrant,migrant support,migrant resource,Migrant Organisations,migrant organisations,migrant resource centre"
    },
    {
        "id": 1442,
        "categoryId": 29,
        "subCategoryName": "Migration Agents & Services",
        "icon": null,
        "clues": "Migration Agents & Services,migration services & consultants,registered migration agents,migration office,migration consultants & services immigration law,migration solicitors,business migration agents,migration lawyers,relocation consultants & services migration assistance,migration consultants & services,migration consultants legal services,migration services legal services,migration agencies,migration consultants,migration agents,migration services centre,migration consultants & services registered migration agent,migration,migration consultants & services visas,migration agents & solicitors,migration services"
    },
    {
        "id": 1518,
        "categoryId": 29,
        "subCategoryName": "Naval Architects",
        "icon": null,
        "clues": "naval architects,Naval Architects,naval architecture"
    },
    {
        "id": 1631,
        "categoryId": 29,
        "subCategoryName": "Patent Attorneys",
        "icon": null,
        "clues": "Patent Attorneys,patent development,patent trademark attorney,Patent Registration,Marketing & Investigation,patent,patent attorney,patent solicitors,patent attorneys,patent lawyers,patent search"
    },
    {
        "id": 1632,
        "categoryId": 29,
        "subCategoryName": "Patent Registration, Marketing & Investigation",
        "icon": null,
        "clues": "patent registration marketing,Patent Registration,Marketing & Investigation,patent registration,marketing & investigation"
    },
    {
        "id": 1786,
        "categoryId": 29,
        "subCategoryName": "Project Management",
        "icon": null,
        "clues": "project management,Project Management,project management consultants,project management services,project managers,construction management project management,project management engineering,project management training,project management construction"
    },
    {
        "id": 1789,
        "categoryId": 29,
        "subCategoryName": "Proofreading",
        "icon": null,
        "clues": "Proofreading,proofreading,proofreading and editing service,editing & proofreading,proofreading services"
    },
    {
        "id": 1807,
        "categoryId": 29,
        "subCategoryName": "Public Relations Consultants",
        "icon": null,
        "clues": "public relations fashion,public relations employment,Public Relations Consultants,public relations agencies,public relations firms,public relations agents,public relations communications,public relations consultants,public relations recruitment,public relations,public relations firm,public relations services,public relations & marketing,public relations media,public relations management"
    },
    {
        "id": 1845,
        "categoryId": 29,
        "subCategoryName": "Real Estate Agents",
        "icon": null,
        "clues": "real estate agents rental property,real estate agents property management,real estate agents commercial,real estate agents homes,Real Estate Agents,real estate agents industrial,real estate agents century 21,real estate agents valuations,real estate agents rental,real estate agents buying,rental real estate agents,real estate buyers' agents,commercial real estate agents,real estate agents holiday rental,real estate agents holiday properties,real estate agents rentals,real estate agents"
    },
    {
        "id": 1846,
        "categoryId": 29,
        "subCategoryName": "Real Estate Auctioneers",
        "icon": null,
        "clues": "real estate auctioneers ray white,Real Estate Auctioneers,valuers--real estate,real estate auctioneers,real estate valuers"
    },
    {
        "id": 1848,
        "categoryId": 29,
        "subCategoryName": "Real Estate Listing Services",
        "icon": null,
        "clues": "relocation consultants & services real estate services,real estate services,real estate listing services,Real Estate Listing Services,real estate listing"
    },
    {
        "id": 1849,
        "categoryId": 29,
        "subCategoryName": "Real Estate Sales Advisory Services",
        "icon": null,
        "clues": "commercial real estate sales,relocation consultants & services real estate services,real estate sales,real estate sales advisory services,real estate services,Real Estate Sales Advisory Services"
    },
    {
        "id": 1866,
        "categoryId": 29,
        "subCategoryName": "Refrigeration Engineers",
        "icon": null,
        "clues": "Refrigeration Engineers,engineers refrigeration design,refrigeration engineers"
    },
    {
        "id": 1869,
        "categoryId": 29,
        "subCategoryName": "Registered Bankruptcy Trustee",
        "icon": null,
        "clues": "Registered Bankruptcy Trustee,registered valuers,bankruptcy trustees--registered"
    },
    {
        "id": 1881,
        "categoryId": 29,
        "subCategoryName": "Research--Industrial & Scientific",
        "icon": null,
        "clues": "research--industrial & scientific,research--industrial,Research--Industrial & Scientific"
    },
    {
        "id": 1891,
        "categoryId": 29,
        "subCategoryName": "Retiree Organisations",
        "icon": null,
        "clues": "organisations--retiree,retirees,Retiree Organisations,retiree organisations"
    },
    {
        "id": 1892,
        "categoryId": 29,
        "subCategoryName": "Retirement Planning & Advisory Services",
        "icon": null,
        "clues": "retirement planning advisor,retirement,retirement planning pension & superannuation,retirement financial planning,retirement villages nursing homes,retirement planning,retirement planning & advisory services,Retirement Planning & Advisory Services,retirement planning & advisor services,retirement homes,retirement services"
    },
    {
        "id": 1898,
        "categoryId": 29,
        "subCategoryName": "Risk Management Consultants",
        "icon": null,
        "clues": "risk management,risk management reports,Risk Management Consultants,risk assessments consultants,risk management consultants,risk management services,financial risk management,injury risk management,risk consultants"
    },
    {
        "id": 1987,
        "categoryId": 29,
        "subCategoryName": "Secretarial & Word Processing Services",
        "icon": null,
        "clues": "secretarial transcribing services,secretarial support,secretarial school,secretarial courses,Secretarial & Word Processing Services,legal secretarial & typing services,secretarial administration,secretarial & word processing services,secretarial training,secretarial,secretarial & typing,secretarial employment agencies,legal secretarial services,secretarial & typing services,secretarial agencies"
    },
    {
        "id": 1989,
        "categoryId": 29,
        "subCategoryName": "Security Guards",
        "icon": null,
        "clues": "shopping centre security guards,security guards,security guards & patrol,Security Guards,security patrols & guards,security guards & patrol agencies,party security guards"
    },
    {
        "id": 2004,
        "categoryId": 29,
        "subCategoryName": "Serviced Offices",
        "icon": null,
        "clues": "Serviced Offices,serviced offices"
    },
    {
        "id": 2005,
        "categoryId": 29,
        "subCategoryName": "Settlement Agents (WA & NT only)",
        "icon": null,
        "clues": "settlement agents ( wa & nt only),settlement agents,settlement agencies,conveyancing settlement agents,business settlement agents,property settlement agents,Settlement Agents (WA & NT only),settlement"
    },
    {
        "id": 2046,
        "categoryId": 29,
        "subCategoryName": "Signwriters",
        "icon": null,
        "clues": "signwriters digital print,signwriters airbrushing,signwriters signboards,signwriters' supplies,signwriters vinyl graphics,signwriters banners,signwriters screen printing,signwriters corflute,signwriters graphic design,signwriters engraving,signwriters,signwriters motor vehicles,signwriters vehicle,signwriters digital printing,signwriters vehicle wrap,Signwriters,signwriters vinyl"
    },
    {
        "id": 2069,
        "categoryId": 29,
        "subCategoryName": "Small Business Advisory Services",
        "icon": null,
        "clues": "computer equipment--home &/or small business,small business advisor,small business advisory services,small business advisory services accounting,small business advisory services taxes,Small Business Advisory Services,accountants & auditors small business,small business,computer equipment--home &/or small business apple,small business accountants,small business services"
    },
    {
        "id": 2071,
        "categoryId": 29,
        "subCategoryName": "Smokers Information & Treatment",
        "icon": null,
        "clues": "Smokers Information & Treatment,smokers information & treatment,smokers information"
    },
    {
        "id": 2074,
        "categoryId": 29,
        "subCategoryName": "Social & Cultural Research",
        "icon": null,
        "clues": "social groups,social events,social & cultural research,social research,social,social dancing,social club,Social & Cultural Research,clubs--social & general,social services"
    },
    {
        "id": 2076,
        "categoryId": 29,
        "subCategoryName": "Social Workers",
        "icon": null,
        "clues": "social workers,Social Workers,employment services social workers"
    },
    {
        "id": 2081,
        "categoryId": 29,
        "subCategoryName": "Soil Testing",
        "icon": null,
        "clues": "soil testing & investigation,soil testing equipment,soil testing services,Soil Testing,soil testing laboratory,soil testing & investigation laboratories,soil testing"
    },
    {
        "id": 2086,
        "categoryId": 29,
        "subCategoryName": "Sound Engineers",
        "icon": null,
        "clues": "sound engineers,acoustic sound engineers,Sound Engineers"
    },
    {
        "id": 2087,
        "categoryId": 29,
        "subCategoryName": "Soundproofing & Acoustic Insulation Services",
        "icon": null,
        "clues": "Soundproofing & Acoustic Insulation Services,sound insulation"
    },
    {
        "id": 2090,
        "categoryId": 29,
        "subCategoryName": "Speakers & Speakers' Agencies",
        "icon": null,
        "clues": "car speakers,hi-fi equipment speakers,speakers agencies,speakers & speakers agencies,public speakers,Speakers & Speakers' Agencies,speakers bureau,speakers & speakers' agencies,motivational speakers,speakers,audio speakers,speakers & entertainers,speakers horns,computer speakers"
    },
    {
        "id": 2137,
        "categoryId": 29,
        "subCategoryName": "Stevedores",
        "icon": null,
        "clues": "Stevedores,stevedores"
    },
    {
        "id": 2138,
        "categoryId": 29,
        "subCategoryName": "Stock & Station Agents",
        "icon": null,
        "clues": "cattle & stock agents,stock & station,Stock & Station Agents,stock & share brokers,stock feeds & supplements hay and chaff,stock agents & supplements,stock agents,Stock Feeds,Fodder & Supplements,stock feed,stock,stock feeds & supplements,stock & station agents"
    },
    {
        "id": 2141,
        "categoryId": 29,
        "subCategoryName": "Stocktakers",
        "icon": null,
        "clues": "stocktakers,Stocktakers"
    },
    {
        "id": 2151,
        "categoryId": 29,
        "subCategoryName": "Structural Engineers",
        "icon": null,
        "clues": "structural engineers,structural engineers--general,consulting structural engineers,structural engineers domestic,Structural Engineers,structural engineers residential,engineers structural,civil & structural engineers"
    },
    {
        "id": 2166,
        "categoryId": 29,
        "subCategoryName": "Surveyors--Aerial",
        "icon": null,
        "clues": "Surveyors--Aerial,surveyors--aerial"
    },
    {
        "id": 2175,
        "categoryId": 29,
        "subCategoryName": "Swimming Pool Safety Inspections",
        "icon": null,
        "clues": "swimming pool covers,swimming pool shop,swimming pool equipment & chemicals,swimming pool services,swimming pool equipment,swimming pool builders,swimming pool safety inspections,swimming pool maintenance,swimming pool maintenance & repair,Swimming Pool Pumps,Accessories & Supplies,swimming pool inspections,swimming pool construction,aquatic centre,swimming pool supplies,swimming pool chemicals,swimming pool fencing contractors,swimming centre,Swimming Pool Safety Inspections"
    },
    {
        "id": 2197,
        "categoryId": 29,
        "subCategoryName": "Tattoo Removal",
        "icon": null,
        "clues": "skin treatment tattoo removal,tattoo removal face,tattoo removal legs,tattoo removal laser treatment,tattoo removal hands,tattoo removalists,tattoo removal back,tattoo removals,tattoo removal chest,tattoo removal feet,tattoo removal cosmetic tattoos,tattoo removal abdomen,cosmetic surgery &/or procedures tattoo removal,Tattoo Removal,tattoo removal,tattoo removal arms"
    },
    {
        "id": 2204,
        "categoryId": 29,
        "subCategoryName": "Taxidermists",
        "icon": null,
        "clues": "taxidermists reptiles,Taxidermists,taxidermists"
    },
    {
        "id": 2241,
        "categoryId": 29,
        "subCategoryName": "Theatrical Managers, Producers & Agents",
        "icon": null,
        "clues": "Radio Programme Producers,theatrical agents,entertainers &/or entertainers' agents theatrical,wine producers,music producers,cattle producers,primary producers,egg producers,milk producers,producers,film producers,Theatrical Managers,Producers & Agents,dairy producers,video producers,food producers,poultry producers,theatrical managers,producers & agents"
    },
    {
        "id": 2259,
        "categoryId": 29,
        "subCategoryName": "Title Searchers",
        "icon": null,
        "clues": "Title Searchers,title searchers"
    },
    {
        "id": 2279,
        "categoryId": 29,
        "subCategoryName": "Trade Commissioners",
        "icon": null,
        "clues": "Trade Commissioners,trade commissioners"
    },
    {
        "id": 2280,
        "categoryId": 29,
        "subCategoryName": "Trade Mark Registration & Investigation",
        "icon": null,
        "clues": "trade mark registration & investigation,Trade Mark Registration & Investigation,trade marks attorneys,trade mark registration"
    },
    {
        "id": 2281,
        "categoryId": 29,
        "subCategoryName": "Trademark Attorneys",
        "icon": null,
        "clues": "trademark lawyers,trademark solicitors,trademark attorney,trademark,Trademark Attorneys"
    },
    {
        "id": 2286,
        "categoryId": 29,
        "subCategoryName": "Traffic Surveys",
        "icon": null,
        "clues": "Traffic Surveys,safety audits traffic surveys,traffic control equipment & services traffic surveys,traffic surveys"
    },
    {
        "id": 2295,
        "categoryId": 29,
        "subCategoryName": "Translators",
        "icon": null,
        "clues": "Translators"
    },
    {
        "id": 2304,
        "categoryId": 29,
        "subCategoryName": "Travel Agents",
        "icon": null,
        "clues": "travel agents & consultants rail,travel agents agents,insurance travel,travel agents & consultants travel insurance,travel agents & consultants ticketing,travel agents & consultants hotels,travel agents & consultants,travel agents africa,travel doctor,travel agents & consultants car hire,travel agents & consultants accommodation,travel agents tour agents,travel agencies,travel agents india,Travel Agents,travel agents,travel agents wholesale,travel agents & consultants cruises,travel agents thailand,travel,travel agents & consultants air"
    },
    {
        "id": 2315,
        "categoryId": 29,
        "subCategoryName": "Trustee Services",
        "icon": null,
        "clues": "trustee,trustee services,Trustee Services"
    },
    {
        "id": 2350,
        "categoryId": 29,
        "subCategoryName": "Union",
        "icon": null,
        "clues": "unions,credit union,Union"
    },
    {
        "id": 2360,
        "categoryId": 29,
        "subCategoryName": "Valuers General",
        "icon": null,
        "clues": "Valuers General,valuers--fine arts,valuers--general,land valuers,valuers--jewellery,valuers--general business valuations,general valuers,valuers--real estate,valuers--fine arts & antiques,valuers property,valuers furniture,valuers,valuers--real,valuers business,real estate valuers"
    },
    {
        "id": 2375,
        "categoryId": 29,
        "subCategoryName": "Vibration Control Consultants",
        "icon": null,
        "clues": "Vibration Control Consultants,vibration control consultants,vibration control"
    },
    {
        "id": 2386,
        "categoryId": 29,
        "subCategoryName": "Visa Services",
        "icon": null,
        "clues": "Visa Services,visa,visa & migration,visa services,visa consultants,visa agents,visa cards"
    },
    {
        "id": 2425,
        "categoryId": 29,
        "subCategoryName": "Web Design & Development",
        "icon": null,
        "clues": "web page development,web design & hosting,Web Design & Development,web design development,internet & web services website design,web development marketing,web design,web page design,web design training,web design courses,internet & web services website development,web development,internet web site development,web application development"
    },
    {
        "id": 2435,
        "categoryId": 29,
        "subCategoryName": "Wedding Photographers",
        "icon": null,
        "clues": "Wedding Photographers,wedding photographers,wedding photographers & video services,wedding photographers & videographers"
    },
    {
        "id": 2436,
        "categoryId": 29,
        "subCategoryName": "Wedding Planning & Planners",
        "icon": null,
        "clues": "Wedding Planning & Planners,wedding planning,wedding arrangement & planning services wedding hair,wedding planners,wedding planners organisers,wedding planners event management,wedding arrangement & planning services decorations,wedding event planners,wedding arrangement & planning services,wedding arrangement & planning services cakes"
    },
    {
        "id": 2461,
        "categoryId": 29,
        "subCategoryName": "Wills, Probates & Estates",
        "icon": null,
        "clues": "solicitors wills probates & estates,wills probates,solicitors wills,probates and estates,wills,probates & estates,Wills,Probates & Estates,wills"
    },
    {
        "id": 2498,
        "categoryId": 29,
        "subCategoryName": "Workers Compensation",
        "icon": null,
        "clues": "Workers Compensation,workers compensation investigation,workers compensation asbestos claims,workers compensation asbestos exposure,workers compensation solicitors,workers compensation,workers compensation insurance,workers compensation advice (business),sheet metal workers,workers compensation lawyers"
    },
    {
        "id": 2502,
        "categoryId": 29,
        "subCategoryName": "Writers, Consultants &/or Services",
        "icon": null,
        "clues": "Writers,Consultants &/or Services,speech writers,writers consultants,sky writers,writers groups,screen writers,freelance writers,writers,writers club,ghost writers,writers,consultants &/or services,writers agents,writers consultants & services"
    }
    ]
},
{
    "id": 30,
    "icon": null,
    "subCategories": [{
    "id": 440,
    "categoryId": 30,
    "subCategoryName": "Christian Science Practitioners",
    "icon": null,
    "clues": "christian cafe,christian school,christian supplies,christian bookstore,christian gifts,christian book shop,christian music,christian college,christian books,christian shop,christian counsellors,christian science practitioners,christian,christian church,Christian Science Practitioners,christian psychologist,christian education"
},
    {
        "id": 442,
        "categoryId": 30,
        "subCategoryName": "Church & Religious Organisations",
        "icon": null,
        "clues": "organisations--church,religious church,Churches,Temples & Mosques,uniting church,church,organisations--church & religious,anglican church,churches,mosques & temples,church organisations,baptist church,Church & Religious Organisations,church mosques,christian church,catholic church"
    },
    {
        "id": 443,
        "categoryId": 30,
        "subCategoryName": "Church & Religious Supplies",
        "icon": null,
        "clues": "church & religious supplies catholic,church supplies,church & religious supplies,Church & Religious Supplies"
    },
    {
        "id": 444,
        "categoryId": 30,
        "subCategoryName": "Church Furniture & Joinery",
        "icon": null,
        "clues": "church furniture,church furniture & joinery,Church Furniture & Joinery"
    },
    {
        "id": 445,
        "categoryId": 30,
        "subCategoryName": "Churches, Temples & Mosques",
        "icon": null,
        "clues": "churches,mosques & temples,churches,mosques & temples christian,mosques & temple,muslim mosques,Churches,Temples & Mosques,church mosques,islamic mosques,mosques,church mosques & temple,arabic mosques,churches"
    },
    {
        "id": 556,
        "categoryId": 30,
        "subCategoryName": "Convents",
        "icon": null,
        "clues": "convents,catholic convents,Convents"
    },
    {
        "id": 1453,
        "categoryId": 30,
        "subCategoryName": "Missions",
        "icon": null,
        "clues": "missions,Missions"
    },
    {
        "id": 1464,
        "categoryId": 30,
        "subCategoryName": "Monasteries",
        "icon": null,
        "clues": "monasteries,Monasteries"
    },
    {
        "id": 2179,
        "categoryId": 30,
        "subCategoryName": "Synagogues",
        "icon": null,
        "clues": "Synagogues,synagogues"
    }
    ]
},
{
    "id": 31,
    "icon": null,
    "subCategories": [{
    "id": 836,
    "categoryId": 31,
    "subCategoryName": "Fast Food",
    "icon": null,
    "clues": "fast food,fast food packaging,Fast Food,fast food franchises,fast food shop,fast food home delivery,fast food chicken,fast food delivery,fast food pizza,fast food outlets,fast food vans,fast food restaurant,fast food & takeaway,fast food chains"
},
    {
        "id": 1886,
        "categoryId": 31,
        "subCategoryName": "Restaurants",
        "icon": null,
        "clues": "takeaways restaurants cafes,restaurants italian,restaurants indian,restaurants cafe,restaurants mexican,restaurants cafes takeaways,restaurants greek,Restaurants,restaurants thai,take away food restaurants,restaurants,restaurants seafood,restaurants australian,restaurants japanese"
    },
    {
        "id": 2239,
        "categoryId": 31,
        "subCategoryName": "Theatre Restaurants",
        "icon": null,
        "clues": "Theatre Restaurants,theatre restaurants"
    }
    ]
},
{
    "id": 32,
    "icon": null,
    "subCategories": [{
    "id": 5,
    "categoryId": 32,
    "subCategoryName": "Aboriginal Art & Crafts",
    "icon": null,
    "clues": "aboriginal organisations,aboriginal artefacts,aboriginal art gallery,aboriginal,aboriginal medical centre,Aboriginal Art & Crafts,aboriginal artwork,aboriginal art & crafts,aboriginal hostel,australian aboriginal art gallery,aboriginal corporation,aboriginal artists,aboriginal services,aboriginal art,aboriginal health"
},
    {
        "id": 16,
        "categoryId": 32,
        "subCategoryName": "Adhesives",
        "icon": null,
        "clues": "Adhesives,adhesives"
    },
    {
        "id": 44,
        "categoryId": 32,
        "subCategoryName": "Air Compressors",
        "icon": null,
        "clues": "air compressors fittings,Air Compressors,air compressors services,air compressors & service,air compressors repair,air compressors parts,air compressors dealers,air compressors manufacturers,air compressors,air compressors maintenance,air compressors hire,air compressors servicing,air compressors sales"
    },
    {
        "id": 46,
        "categoryId": 32,
        "subCategoryName": "Air Conditioning Parts",
        "icon": null,
        "clues": "automotive air conditioning parts,air conditioning,air conditioning repair,air conditioning--installation & service repairs,Air Conditioning Parts,air conditioning services,air conditioning--commercial & industrial,air conditioning-automotive,air conditioning spare parts,air conditioner parts,auto air conditioning,air conditioning-home,air conditioning--installation & service,air compressors parts,air conditioning--parts,automotive air conditioning,air conditioning commercial,air conditioning wholesalers parts,air conditioning--installation & service maintenance"
    },
    {
        "id": 48,
        "categoryId": 32,
        "subCategoryName": "Air Curtains",
        "icon": null,
        "clues": "air curtains,Air Curtains"
    },
    {
        "id": 49,
        "categoryId": 32,
        "subCategoryName": "Air Cushion Vehicles & Equipment",
        "icon": null,
        "clues": "Air Cushion Vehicles & Equipment,air conditioning vehicles,air cushion vehicles & equipment,air conditioning equipment,air equipment,air cushion vehicles"
    },
    {
        "id": 50,
        "categoryId": 32,
        "subCategoryName": "Air Pollution Measuring Equipment",
        "icon": null,
        "clues": "pollution air,air conditioning equipment,Air Pollution Measuring Equipment,Air Tools Accessories,Supply & Service,air equipment,air pollution consultants,air pollution equipment,pollution consultants air monitoring,air pollution measuring equipment,air pollution,air pollution control"
    },
    {
        "id": 51,
        "categoryId": 32,
        "subCategoryName": "Air Purification Equipment",
        "icon": null,
        "clues": "Air Purification Equipment,air conditioning equipment,air purification,Air Tools Accessories,Supply & Service,air purification equipment,air equipment"
    },
    {
        "id": 52,
        "categoryId": 32,
        "subCategoryName": "Air Tools Accessories, Supply & Service",
        "icon": null,
        "clues": "air tools & accessories-supply & service,air tools,air tools distributors,air power tools,air conditioning--installation & service repairs,Air Tools Accessories,Supply & Service,air conditioning services,air conditioning supply,air services,compressed air tools,air conditioning--installation & service,air cargo services australian air express,distributors air tools,air compressors & service,air conditioning--installation & service daikin,air tools & accessories,air powered tools,air conditioning--installation & service maintenance,air conditioning supply & installation"
    },
    {
        "id": 67,
        "categoryId": 32,
        "subCategoryName": "Alpine & Rockery Plants (Nurseries-Retail)",
        "icon": null,
        "clues": "Alpine & Rockery Plants (Nurseries-Retail),alpine & rockery plants (nurseries-retail)"
    },
    {
        "id": 85,
        "categoryId": 32,
        "subCategoryName": "Antiques - Auctions & Dealers",
        "icon": null,
        "clues": "antiques & collectables,antiques dealers book,antiques auctions,antiques shop,antiques dealers,antiques,Antiques - Auctions & Dealers,antiques auctioneers"
    },
    {
        "id": 86,
        "categoryId": 32,
        "subCategoryName": "Antiques Reproduction & Restoration",
        "icon": null,
        "clues": "antiques--reproductions & restoration,antiques restoration,Antiques Reproduction & Restoration,reproduction antiques,antiques--reproductions"
    },
    {
        "id": 93,
        "categoryId": 32,
        "subCategoryName": "Aquatic Plants (Nurseries-Retail)",
        "icon": null,
        "clues": "aquatic plants (nurseries-retail),Aquatic Plants (Nurseries-Retail),aquatic plants"
    },
    {
        "id": 105,
        "categoryId": 32,
        "subCategoryName": "Art Gallery",
        "icon": null,
        "clues": "art gallery,Art Gallery,art gallery supplies,art gallery picture framing"
    },
    {
        "id": 106,
        "categoryId": 32,
        "subCategoryName": "Art Metal Work",
        "icon": null,
        "clues": "art metal work,Art Metal Work,hanging art works,art workshops,metal art"
    },
    {
        "id": 107,
        "categoryId": 32,
        "subCategoryName": "Art Restoration & Conservation",
        "icon": null,
        "clues": "art conservation,picture & art cleaning & restoration,art restoration & conservation,art restoration,Art Restoration & Conservation"
    },
    {
        "id": 108,
        "categoryId": 32,
        "subCategoryName": "Art Supplies",
        "icon": null,
        "clues": "art supplies art professionals,art supplies graphic art,art supplies art papers,art supplies painting,art supplies paper,art supplies folk art,art supplies wholesale,art supplies airbrush supplies,art supplies art spectrum,art supplies framing,art supplies,art supplies mosaics,art supplies retail,Art Supplies,art supplies art books,art supplies framing supplies,art supplies calligraphy,art & craft supplies"
    },
    {
        "id": 110,
        "categoryId": 32,
        "subCategoryName": "Artificial Flowers & Plants",
        "icon": null,
        "clues": "artificial flowers,wholesale artificial flowers,artificial plants wholesale,Artificial Flowers & Plants,artificial plants,artificial plants & flowers,plants artificial,artificial flowers wholesale,wholesale artificial plants,flowers artificial,flowers artificial wholesale"
    },
    {
        "id": 112,
        "categoryId": 32,
        "subCategoryName": "Artists",
        "icon": null,
        "clues": "graphic artists,artists,artists' materials,make up artists,artists supplies,artists materials,Artists,tattoo artists"
    },
    {
        "id": 115,
        "categoryId": 32,
        "subCategoryName": "Ashtrays & Bins",
        "icon": null,
        "clues": "Ashtrays & Bins,ashtrays"
    },
    {
        "id": 126,
        "categoryId": 32,
        "subCategoryName": "Australian Natives (Nurseries-Retail)",
        "icon": null,
        "clues": "Australian Natives (Nurseries-Retail),australian natives,australian natives (nurseries-retail)"
    },
    {
        "id": 128,
        "categoryId": 32,
        "subCategoryName": "Australiana Products",
        "icon": null,
        "clues": "australiana gifts,Australiana Products,australiana products,australiana souvenirs,australiana"
    },
    {
        "id": 131,
        "categoryId": 32,
        "subCategoryName": "Automation Systems & Equipment",
        "icon": null,
        "clues": "Automation Systems & Equipment,automation systems & equipment,automation systems & equipment rexroth,automation systems,automation equipment,automation systems & equipment bosch,building automation systems,gates automation systems"
    },
    {
        "id": 134,
        "categoryId": 32,
        "subCategoryName": "Awnings",
        "icon": null,
        "clues": "awnings repair,awnings,awnings metal,awnings windows,awnings & blinds,Awnings,awnings suppliers,awnings fabric,awnings manufacturers,awnings canopies,awnings shade screens,awnings & patio extensions,awnings commercial,awnings canvas,awnings repairs"
    },
    {
        "id": 137,
        "categoryId": 32,
        "subCategoryName": "Babies' Wear Wholesalers and Manufacturers",
        "icon": null,
        "clues": "Babies' Wear Wholesalers and Manufacturers,babies' wear wholesalers,babies' wear wholesalers & manufacturers,babies wear wholesale"
    },
    {
        "id": 138,
        "categoryId": 32,
        "subCategoryName": "Babies' Wear--Retail",
        "icon": null,
        "clues": "Babies' Wear--Retail,babies' wear--retail,babies' wear--retail christening wear"
    },
    {
        "id": 139,
        "categoryId": 32,
        "subCategoryName": "Baby Prams, Furniture & Accessories",
        "icon": null,
        "clues": "baby prams & furniture retail,baby clothing strollers prams,baby products prams & accessories,baby furniture prams,second hand baby prams,baby goods,baby furniture hire,baby furniture wholesale,baby furniture manufacturers,baby prams,furniture & accessories car seats,baby furniture for hire,baby prams,furniture & accessories cots,baby prams & accessories,pre loved baby prams,baby furniture,baby clothes,baby wear,baby prams,furniture & accessories capsule hire,second hand baby furniture,baby strollers & accessories,baby shop,Baby Prams,Furniture & Accessories,baby furniture store,baby prams,furniture & accessories phil&teds,baby furniture second hand,baby accessories wholesale,baby clothing & accessories,baby furniture & accessories,baby prams,furniture & accessories,baby prams,furniture & accessories repairs,baby prams & strollers,baby furniture & equipment,baby furniture & gifts,baby furniture outlets,second hand baby accessories,baby prams,baby prams furniture,baby supplies,baby accessories hire,furniture baby,baby equipment,baby prams,furniture & accessories baby equipment hire,baby accessories"
    },
    {
        "id": 145,
        "categoryId": 32,
        "subCategoryName": "Bags & Sacks",
        "icon": null,
        "clues": "bags & sacks shopping bags,bags & sacks plastic,bags,bags & sacks fertilizer,bags & sacks promotional bags,bags & sacks canvas,bags & sacks bulk bags,bags & sacks security bags,Bags & Sacks,bags & sacks chemicals,bags & sacks polypropylene,bags & sacks sandbags,bags & sacks book bags,bags & sacks,bags & sacks hessian,bean bags,bags & sacks cellophane,bags & sacks courier bags,bags & sacks waste,bags & sacks jute,bags & sacks stock feeds"
    },
    {
        "id": 155,
        "categoryId": 32,
        "subCategoryName": "Bamboo Plants (Nurseries-Retail)",
        "icon": null,
        "clues": "nurseries-retail bamboo,bamboo landscape plants,bamboo plants (nurseries-retail),bamboo plants,Bamboo Plants (Nurseries-Retail)"
    },
    {
        "id": 159,
        "categoryId": 32,
        "subCategoryName": "Barbers & Barber Shops",
        "icon": null,
        "clues": "barbers,Barbers & Barber Shops"
    },
    {
        "id": 171,
        "categoryId": 32,
        "subCategoryName": "Basketware Products & Supplies",
        "icon": null,
        "clues": "basketware products & supplies,Basketware Products & Supplies"
    },
    {
        "id": 173,
        "categoryId": 32,
        "subCategoryName": "Bathroom Accessories & Equipment",
        "icon": null,
        "clues": "bathroom accessories,bathroom vanity,bathroom equipment & accessories--retail vanities,Bathroom Accessories & Equipment,bathroom products,bathroom equipment & accessories--retail,bathroom equipment & accessories--retail spa baths,bathroom equipment & accessories--retail taps,bathroom equipment & accessories--retail toilets,bathroom equipment & accessories--retail baths,bathroom fittings,bathroom equipment,bathroom repair,bathroom accessories suppliers,bathroom equipment & accessories--retail showroom"
    },
    {
        "id": 176,
        "categoryId": 32,
        "subCategoryName": "Battery Charging Equipment",
        "icon": null,
        "clues": "Battery Charging Equipment,battery charging equipment"
    },
    {
        "id": 177,
        "categoryId": 32,
        "subCategoryName": "BBQ & Barbecue Equipment",
        "icon": null,
        "clues": "bbq,bbq catering,bbq restaurant,bbq ribs,BBQ & Barbecue Equipment,bbq retail,bbq equipment,bbq chicken,bbq hire"
    },
    {
        "id": 182,
        "categoryId": 32,
        "subCategoryName": "Bed Wetting Control Systems",
        "icon": null,
        "clues": "bed wetting control systems,bed wetting alarms,pest control bed bugs,bed bugs pest control,Bed Wetting Control Systems,bed wetting services,bed wetting equipment specialists"
    },
    {
        "id": 183,
        "categoryId": 32,
        "subCategoryName": "Beds & Bedding Stores",
        "icon": null,
        "clues": "beds & mattresses,bunks beds,sofa beds,beds & bedding--retail mattresses,Beds & Bedding Stores,beds & bedding,beds & bedding--retail childrens beds,beds & bedding--retail pillows,beds & bedding--retail futons,kids beds,beds & bedding--retail,beds"
    },
    {
        "id": 185,
        "categoryId": 32,
        "subCategoryName": "Bee & Wasp Removal",
        "icon": null,
        "clues": "bee catcher,bee removalists,Bee & Wasp Removal,bee removals,bee & wasps,bee hive removals,bee & wasp removals,bee control"
    },
    {
        "id": 186,
        "categoryId": 32,
        "subCategoryName": "Beekeepers",
        "icon": null,
        "clues": "Beekeepers,beekeepers"
    },
    {
        "id": 187,
        "categoryId": 32,
        "subCategoryName": "Beekeeping Supplies",
        "icon": null,
        "clues": "beekeeping supplies,apiarist & beekeepers supplies,Beekeeping Supplies"
    },
    {
        "id": 189,
        "categoryId": 32,
        "subCategoryName": "Bells & Chimes",
        "icon": null,
        "clues": "bells & chimes,Bells & Chimes"
    },
    {
        "id": 193,
        "categoryId": 32,
        "subCategoryName": "Bicycle Accessories & Repairs",
        "icon": null,
        "clues": "Bicycle repairs,Bicycle Accessories & Repairs,Bicycle Accessories,bicycles racks,Bicycle Repairs,bicycles & accessories--retail & repairs bicycle hire"
    },
    {
        "id": 194,
        "categoryId": 32,
        "subCategoryName": "Bicycles & Accessories Wholesalers & Manufacturers",
        "icon": null,
        "clues": "bicycles & accessories--retail & repairs continental,bicycles parts wholesalers,bicycles & accessories--retail & repairs specialized,Bicycles & Accessories Wholesalers & Manufacturers,bicycles & accessories--retail & repairs shimano,bicycles & accessories--retail & repairs redline,bicycles & accessories--retail & repairs avanti,bicycles manufacturers,bicycles shop,bicycles & accessories--retail & repairs raleigh,bicycles & accessories--retail & repairs giant,bicycles & accessories--retail & repairs apollo,bicycles importers,bicycles & accessories--retail & repairs mongoose,bicycles sales,bicycles & accessories--retail & repairs schwinn,bicycles & accessories--retail & repairs electric,bmx bicycles & accessories,bicycles & accessories - wholesalers  & manufacturers,bicycles & accessories--retail & repairs campagnolo,bicycles and accessories - wholesalers and manufacturers,bicycles & accessories--retail & repairs bicycle hire"
    },
    {
        "id": 195,
        "categoryId": 32,
        "subCategoryName": "Billiard Tables & Accessories",
        "icon": null,
        "clues": "Billiard Tables & Accessories,billiards store,billiards table removalists,billiards tables,billiards table removals,billiards tables & accessories repairs,billiards,billiards cues,billiards removals,billiards supplies,billiards accessories"
    },
    {
        "id": 202,
        "categoryId": 32,
        "subCategoryName": "Biscuits",
        "icon": null,
        "clues": "biscuits,biscuits factory,Biscuits,biscuits manufacturers,biscuits bakers"
    },
    {
        "id": 203,
        "categoryId": 32,
        "subCategoryName": "Bitumen Products",
        "icon": null,
        "clues": "bitumen surfacing,bitumen contractors,bitumen layers,asphalt & bitumen products,bitumen,bitumen paving,bitumen products,Bitumen Products,bitumen suppliers,bitumen sealing,bitumen roads,bitumen driveways,driveway sealing bitumen products,bitumen repair"
    },
    {
        "id": 205,
        "categoryId": 32,
        "subCategoryName": "Blackboard & Chalkboard Art",
        "icon": null,
        "clues": "blackboard,blackboard & chalkboard art,Whiteboards,Blackboards & Chalkboards,Blackboard & Chalkboard Art"
    },
    {
        "id": 209,
        "categoryId": 32,
        "subCategoryName": "Blinds",
        "icon": null,
        "clues": "blinds,blinds patio blinds,curtains & blinds,outdoor blinds,blinds & shutters,blinds & curtains,window blinds,vertical blinds,blinds & awnings,blinds roller,Blinds,venetian blinds"
    },
    {
        "id": 210,
        "categoryId": 32,
        "subCategoryName": "Blinds Cleaning & Repair",
        "icon": null,
        "clues": "blinds patio blinds,curtains & blinds,blinds--cleaning & maintenance venetian,cleaning venetian blinds,Blinds Cleaning & Repair,vertical blinds,blinds--cleaning & maintenance,venetian blinds,outdoor blinds,blinds & shutters,blinds & curtains,window blinds,blinds--cleaning & maintenance vertical,blinds & awnings,blinds roller,blinds repairs"
    },
    {
        "id": 211,
        "categoryId": 32,
        "subCategoryName": "Blinds--Fittings & Supplies",
        "icon": null,
        "clues": "Blinds--Fittings & Supplies,blinds--fittings,blinds--fittings & supplies"
    },
    {
        "id": 229,
        "categoryId": 32,
        "subCategoryName": "Bond & Free Stores",
        "icon": null,
        "clues": "Bond & Free Stores,bond & free stores"
    },
    {
        "id": 230,
        "categoryId": 32,
        "subCategoryName": "Bonsai (Nurseries-Retail)",
        "icon": null,
        "clues": "bonsai,bonsai pots,bonsai farm,bonsai wholesale,bonsai trees,bonsai (nurseries-retail),bonsai shop,Bonsai (Nurseries-Retail),bonsai nursery,bonsai nurseries,bonsai plants"
    },
    {
        "id": 231,
        "categoryId": 32,
        "subCategoryName": "Book Binding",
        "icon": null,
        "clues": "leather book binding,book binding,book binding services,book printers & binding,Book Binding"
    },
    {
        "id": 232,
        "categoryId": 32,
        "subCategoryName": "Book Stores & Shops",
        "icon": null,
        "clues": "comic book store,bookstore,books,book stores second hand,book shop,Book Stores & Shops,book stores educational"
    },
    {
        "id": 233,
        "categoryId": 32,
        "subCategoryName": "Bookbinding Supplies & Equipment",
        "icon": null,
        "clues": "Bookbinding Supplies & Equipment,bookbinding supplies & equipment,bookbinding equipment & supplies,bookbinding supplies,bookbinding equipment"
    },
    {
        "id": 235,
        "categoryId": 32,
        "subCategoryName": "Bookmakers",
        "icon": null,
        "clues": "bookmakers,Bookmakers"
    },
    {
        "id": 236,
        "categoryId": 32,
        "subCategoryName": "Books Online",
        "icon": null,
        "clues": "Books Online"
    },
    {
        "id": 237,
        "categoryId": 32,
        "subCategoryName": "Books--W'sale",
        "icon": null,
        "clues": "books wholesalers,wholesale books,books - wholesale"
    },
    {
        "id": 241,
        "categoryId": 32,
        "subCategoryName": "Bottle Closures",
        "icon": null,
        "clues": "Bottle Closures,bottle closures"
    },
    {
        "id": 243,
        "categoryId": 32,
        "subCategoryName": "Boutiques",
        "icon": null,
        "clues": "Boutiques,boutiques store,boutiques retail,clothing boutiques,boutiques childrenswear,boutiques ladies,boutiques winery,boutiques,boutiques designer labels,boutiques clothes shop,boutiques clothing,boutiques bags,boutiques builders"
    },
    {
        "id": 245,
        "categoryId": 32,
        "subCategoryName": "Bowling Centres' Equipment--Indoor",
        "icon": null,
        "clues": "bowling alley,ten pin bowling equipment,bowling green,bowling,clubs--bowling,bowling centres' equipment--indoor,Bowling Centres' Equipment--Indoor,bowling centre"
    },
    {
        "id": 247,
        "categoryId": 32,
        "subCategoryName": "Bowling Green Construction & Equipment",
        "icon": null,
        "clues": "bowling green construction,bowling green construction & equipment,synthetic turf bowling green construction,bowling alley,bowling green,bowling greens synthetic,bowling,clubs--bowling,bowling centre,Bowling Green Construction & Equipment"
    },
    {
        "id": 248,
        "categoryId": 32,
        "subCategoryName": "Bowls Equipment & Apparel",
        "icon": null,
        "clues": "Bowls Equipment & Apparel,bowls equipment & apparel,lawn bowls equipment,bowls shop,bowls equipment,lawn bowls,bowls apparel,lawn bowls club,bowls accessories,lawn bowls apparel"
    },
    {
        "id": 249,
        "categoryId": 32,
        "subCategoryName": "Boxes - Clear",
        "icon": null,
        "clues": "boxes - clear lids,Boxes - Clear,boxes - clear,boxes - clear pvc,clear plastic boxes,clear boxes"
    },
    {
        "id": 257,
        "categoryId": 32,
        "subCategoryName": "Breastfeeding Support Services",
        "icon": null,
        "clues": "breastfeeding support services,Breastfeeding Support Services,breastfeeding"
    },
    {
        "id": 259,
        "categoryId": 32,
        "subCategoryName": "Brewery Equipment & Supplies",
        "icon": null,
        "clues": "brewery equipment & supplies,brewery supplies,Brewery Equipment & Supplies,brewery machines"
    },
    {
        "id": 261,
        "categoryId": 32,
        "subCategoryName": "Brick Paving",
        "icon": null,
        "clues": "driveway sealing brick paving,paving--brick bricklaying,brick paving layers,recycled brick paving,brick fencing,brick paving services,brick paving contractors,brick paving contractors driveway,Brick Paving,brick paving,paving--brick"
    },
    {
        "id": 263,
        "categoryId": 32,
        "subCategoryName": "Bricklayers",
        "icon": null,
        "clues": "bricklayers contractors,bricklayers supplies,Bricklayers,bricklayers tools,bricklayers blocklayer,bricklayers sand,tuckpointing bricklayers,bricklayers brick cutting,bricklayers services,bricklayers tiling,bricklayers block laying,bricklayers equipment,bricklayers,bricklayers fences"
    },
    {
        "id": 269,
        "categoryId": 32,
        "subCategoryName": "Briefcases",
        "icon": null,
        "clues": "Briefcases,briefcases"
    },
    {
        "id": 272,
        "categoryId": 32,
        "subCategoryName": "Brushes & Brooms",
        "icon": null,
        "clues": "Brushes & Brooms,brushes & brooms,brushes & mops"
    },
    {
        "id": 294,
        "categoryId": 32,
        "subCategoryName": "Bush Foods & Ingredients",
        "icon": null,
        "clues": "bush foods,Bush Foods & Ingredients,bush foods & ingredients"
    },
    {
        "id": 299,
        "categoryId": 32,
        "subCategoryName": "Business Cards",
        "icon": null,
        "clues": "printers-general business cards,business cards,Business Cards,printers--digital business cards,graphic designers business cards"
    },
    {
        "id": 306,
        "categoryId": 32,
        "subCategoryName": "Butcher Supplies",
        "icon": null,
        "clues": "Butcher Supplies,butcher shop supplies,butcher supplies,butchers' & smallgoods mfrs' supplies,butcher equipment supplies"
    },
    {
        "id": 307,
        "categoryId": 32,
        "subCategoryName": "Butchers Shop",
        "icon": null,
        "clues": "Butchers Shop,butcher retail,butchers--w'sale,butchers wholesalers,butchers--retail"
    },
    {
        "id": 309,
        "categoryId": 32,
        "subCategoryName": "Buttons & Buckles--Retail",
        "icon": null,
        "clues": "Buttons & Buckles--Retail,buttons & buckles--retail"
    },
    {
        "id": 313,
        "categoryId": 32,
        "subCategoryName": "Cable & Hose Reels",
        "icon": null,
        "clues": "Cable & Hose Reels,cable reels,reels--cable & hose"
    },
    {
        "id": 314,
        "categoryId": 32,
        "subCategoryName": "Cables--Mechanical",
        "icon": null,
        "clues": "Cables--Mechanical,cables--mechanical,mechanical cables"
    },
    {
        "id": 315,
        "categoryId": 32,
        "subCategoryName": "Cacti & Succulents (Nurseries-Retail)",
        "icon": null,
        "clues": "Cacti & Succulents (Nurseries-Retail),nursery cacti & succulents,cacti,cacti & succulents (nurseries-retail)"
    },
    {
        "id": 320,
        "categoryId": 32,
        "subCategoryName": "Calculators",
        "icon": null,
        "clues": "Calculators,calculators,calculators & adding machines"
    },
    {
        "id": 321,
        "categoryId": 32,
        "subCategoryName": "Calendars",
        "icon": null,
        "clues": "fundraising calendars,calendars,Calendars,promotional calendars,calendars printing,desktop calendars,advertising calendars"
    },
    {
        "id": 324,
        "categoryId": 32,
        "subCategoryName": "Calligraphy",
        "icon": null,
        "clues": "Calligraphy,calligraphy"
    },
    {
        "id": 325,
        "categoryId": 32,
        "subCategoryName": "Camera Shops, Photography Equipment & Repairs",
        "icon": null,
        "clues": "photography equipment,video cameras,video camera equipment,camera equipment hire,cameras,photography equipment hire,camera shop,video camera repair,Camera Shops,Photography Equipment & Repairs,camera equipment,camera photography,camera repair,camera equipment rental,digital cameras,digital camera repair"
    },
    {
        "id": 329,
        "categoryId": 32,
        "subCategoryName": "Camping Equipment Wholesalers & Manufacturers",
        "icon": null,
        "clues": "camping equipment - wholesalers & manufacturers,camping equipment wholesalers,camping equipment wholesale,Camping Equipment Wholesalers & Manufacturers,camping supplies"
    },
    {
        "id": 330,
        "categoryId": 32,
        "subCategoryName": "Camping Gear & Outdoor Equipment",
        "icon": null,
        "clues": "Camping Gear & Outdoor Equipment,camping equipment--retail tents,camping equipment store,camping & outdoor equipment,camping gear,camping gear hire,camping,camping equipment repair,outdoor camping equipment,camping equipment--retail hiking gear,camping & outdoor,camping equipment--retail,camping equipment--retail clothing,camping equipment--retail swags,camping store,camping equipment hire,camping equipment"
    },
    {
        "id": 333,
        "categoryId": 32,
        "subCategoryName": "Candles",
        "icon": null,
        "clues": "Candles,candles"
    },
    {
        "id": 334,
        "categoryId": 32,
        "subCategoryName": "Cane & Bamboo Products",
        "icon": null,
        "clues": "cane bamboo,cane & bamboo products,cane furniture,Cane & Bamboo Products,cane,cane products,cane baskets"
    },
    {
        "id": 335,
        "categoryId": 32,
        "subCategoryName": "Cane & Wicker Furniture",
        "icon": null,
        "clues": "cane wicker,cane wicker furniture,Cane & Wicker Furniture,furniture--cane & wicker"
    },
    {
        "id": 337,
        "categoryId": 32,
        "subCategoryName": "Canvas & Synthetic Fabric Products",
        "icon": null,
        "clues": "canvas & synthetic fabric products vehicle canopies & covers,Canvas & Synthetic Fabric Products,canvas & synthetic fabric products,canvas & synthetic fabric,canvas & vinyl products,canvas & synthetic fabric products repairs,canvas & synthetic fabric products blinds,canvas & synthetic fabric products awnings"
    },
    {
        "id": 338,
        "categoryId": 32,
        "subCategoryName": "Canvas Wholesalers & Manufacturers",
        "icon": null,
        "clues": "canvas retail manufacturers,canvas wholesalers & manufacturers,canvas supplies,canvas wholesalers,Canvas Wholesalers & Manufacturers,canvas goods manufacturers,canvas manufacturers"
    },
    {
        "id": 366,
        "categoryId": 32,
        "subCategoryName": "Carbon--Activated",
        "icon": null,
        "clues": "carbon--activated,activated carbon,Carbon--Activated"
    },
    {
        "id": 370,
        "categoryId": 32,
        "subCategoryName": "Cards--Greeting--Retail",
        "icon": null,
        "clues": "Cards--Greeting--Retail,cards--greeting--retail"
    },
    {
        "id": 374,
        "categoryId": 32,
        "subCategoryName": "Carpet & Rug Overlocking Services",
        "icon": null,
        "clues": "Carpet & Rug Overlocking Services,overlocking carpet,carpet overlocking"
    },
    {
        "id": 377,
        "categoryId": 32,
        "subCategoryName": "Carpet Laying Supplies",
        "icon": null,
        "clues": "carpet laying supplies,Carpet Laying Supplies,carpet laying"
    },
    {
        "id": 379,
        "categoryId": 32,
        "subCategoryName": "Carpet Tiles & Carpet Retailers",
        "icon": null,
        "clues": "carpet tiles retail,carpet & carpet tiles--retail commercial,Carpet Tiles & Carpet Retailers,carpet & carpet tiles--retail"
    },
    {
        "id": 380,
        "categoryId": 32,
        "subCategoryName": "Carpets & Rugs--Dyeing",
        "icon": null,
        "clues": "carpets & rugs--dyeing,Carpets & Rugs--Dyeing"
    },
    {
        "id": 381,
        "categoryId": 32,
        "subCategoryName": "Carports & Pergolas",
        "icon": null,
        "clues": "carports & garages,carports handyman,carports extensions,pergolas & carports,carports,carports & pergolas,carports construction,carports & awnings,carports suppliers,carports kits,carports manufacturers,Carports & Pergolas,carports door,garages carports pergolas,carports builders,carports installation,awnings carports patios pergolas roofing services"
    },
    {
        "id": 383,
        "categoryId": 32,
        "subCategoryName": "Cash Registers",
        "icon": null,
        "clues": "Cash Registers,cash registers"
    },
    {
        "id": 384,
        "categoryId": 32,
        "subCategoryName": "Casinos",
        "icon": null,
        "clues": "Casinos,casinos,hire--party equipment casinos"
    },
    {
        "id": 387,
        "categoryId": 32,
        "subCategoryName": "Castors",
        "icon": null,
        "clues": "Castors,castors,wheels & castors"
    },
    {
        "id": 396,
        "categoryId": 32,
        "subCategoryName": "CCTV & Security Cameras",
        "icon": null,
        "clues": "cctv suppliers,security cctv,cctv camera trading companies,cctv products wholesale,CCTV & Security Cameras,cctv security,cctv cameras,cctv security systems,cctv installers,cctv systems,cctv,cctv sewer,cctv products,cctv inspections,cctv repair,cctv surveillance,cctv wholesale,cctv installation,cctv security video surveillance,cctv importers,cctv equipment"
    },
    {
        "id": 397,
        "categoryId": 32,
        "subCategoryName": "Ceilings",
        "icon": null,
        "clues": "ceilings suspended,ceilings tiles,Ceilings,plasterers--plasterboard fixers ceilings,ceilings cornices,ceilings plaster,rendering ceilings,ceilings plasterboard,ceilings gyprock,ceilings,ceilings linings"
    },
    {
        "id": 398,
        "categoryId": 32,
        "subCategoryName": "Celebrants--Naming",
        "icon": null,
        "clues": "celebrants--naming,celebrants--naming or funerals,Celebrants--Naming"
    },
    {
        "id": 401,
        "categoryId": 32,
        "subCategoryName": "Centrifugal Machines",
        "icon": null,
        "clues": "Centrifugal Machines,centrifugal machines,horizontal centrifugal pump,centrifugal fans,multistage centrifugal booster pumps,pumps - manufacturers & merchants centrifugal pumps,centrifugal pumps"
    },
    {
        "id": 402,
        "categoryId": 32,
        "subCategoryName": "Ceramics--Art & Hobby",
        "icon": null,
        "clues": "Ceramics--Art & Hobby,ceramics--art,ceramics--art & hobby"
    },
    {
        "id": 403,
        "categoryId": 32,
        "subCategoryName": "Ceramics--Equipment & Supplies",
        "icon": null,
        "clues": "ceramics--equipment & supplies,ceramics--equipment,Ceramics--Equipment & Supplies"
    },
    {
        "id": 404,
        "categoryId": 32,
        "subCategoryName": "Ceramics--Industrial & Scientific",
        "icon": null,
        "clues": "Ceramics--Industrial & Scientific,ceramics -- industrial,ceramics--industrial & scientific"
    },
    {
        "id": 405,
        "categoryId": 32,
        "subCategoryName": "Chains & Chain Equipment",
        "icon": null,
        "clues": "chains & lifting equipment,Chains & Chain Equipment,chains & chain equipment,lifting chains,snow chains"
    },
    {
        "id": 406,
        "categoryId": 32,
        "subCategoryName": "Chainsaws & Brush Cutters",
        "icon": null,
        "clues": "chainsaws repair,chainsaws blades,chainsaws services,chainsaws sales,chainsaws & brushcutters,chainsaws & brushcutters repairs,chainsaws dealers,chainsaws & brushcutters stihl,chainsaws servicing,chainsaws chain,chainsaws,Chainsaws & Brush Cutters,chainsaws hire,chainsaws parts,chainsaws suppliers,chainsaws sharpening,chainsaws training"
    },
    {
        "id": 408,
        "categoryId": 32,
        "subCategoryName": "Charcoal",
        "icon": null,
        "clues": "charcoal chicken shop,charcoal chicken,charcoal,charcoal take away outlets,Charcoal"
    },
    {
        "id": 410,
        "categoryId": 32,
        "subCategoryName": "Charts & Charting Equipment",
        "icon": null,
        "clues": "Charts & Charting Equipment,charts & charting equipment,flip charts,maps & charts,marine charts,nautical charts"
    },
    {
        "id": 420,
        "categoryId": 32,
        "subCategoryName": "Cheque Cashing Services",
        "icon": null,
        "clues": "cheque cashing services,cheque cashing,Cheque Cashing Services,instant cheque cashing,cheque,cheque printing"
    },
    {
        "id": 421,
        "categoryId": 32,
        "subCategoryName": "Cheque Printing & Encoding",
        "icon": null,
        "clues": "cheque cashing,cheque printing & encoding,Cheque Printing & Encoding,cheque,cheque printing"
    },
    {
        "id": 427,
        "categoryId": 32,
        "subCategoryName": "Child Restraints",
        "icon": null,
        "clues": "child restraints fitting,Child Restraints,mobile child restraints,child restraints fitted,child car restraints,seat belts & child restraints,child seat restraints,child safety restraints,child restraints"
    },
    {
        "id": 429,
        "categoryId": 32,
        "subCategoryName": "Children's Play Programmes",
        "icon": null,
        "clues": "children's programmes,children's play programmes,children's play programmes indoor play centres,Children's Play Programmes,children's play programmes indoor playground,children's play programmes birthdays"
    },
    {
        "id": 430,
        "categoryId": 32,
        "subCategoryName": "Children's Wear W'salers & Mfrs",
        "icon": null,
        "clues": "children's wear--retail shoes,Children's Wear W'salers & Mfrs,children's formal wear,children's wear formal,children's wear,children's wear--retail christening,children's wear wholesalers manufacturers suppliers,children's wear retail,children's wholesalers,children's wear--retail formal,children's wear wholesalers & manufacturers,children's wear wholesalers,children's wear--retail communion,children's clothing manufacturers,children's clothing wholesalers,clothing manufacturers children's,children's wear boutiques,children's wear wholesale"
    },
    {
        "id": 431,
        "categoryId": 32,
        "subCategoryName": "Children's Wear--Retail",
        "icon": null,
        "clues": "children's wear--retail shoes,children's wear retail,children 27 s wear--retail,children's wear--retail formal,children's wear--retail communion,Children's Wear--Retail,retail children's wear,children's wear--retail christening"
    },
    {
        "id": 433,
        "categoryId": 32,
        "subCategoryName": "China & Glass Decoration",
        "icon": null,
        "clues": "china,glassware & earthenware,China,Glassware & Earthenware,China & Glass Decoration,china,glassware & earthenware wholesaler,china & glass decoration,china glassware,alessi brand china glassware earthenware"
    },
    {
        "id": 434,
        "categoryId": 32,
        "subCategoryName": "China & Glass Repairs & Restorations",
        "icon": null,
        "clues": "china & glass repairs & restorations,China & Glass Repairs & Restorations,china restoration"
    },
    {
        "id": 435,
        "categoryId": 32,
        "subCategoryName": "China, Glassware & Earthenware",
        "icon": null,
        "clues": "laboratory glassware,scientific glassware,china,glassware & earthenware,hire glassware,China,Glassware & Earthenware,glassware wholesalers,personalised glassware,earthenware,china,glassware & earthenware wholesaler,glassware wholesale,alessi brand china glassware earthenware,glassware suppliers,glassware,earthenware wholesaler,polycarbonate glassware,glassware printing,glassware hire,china glassware,glassware importers,wholesale glassware"
    },
    {
        "id": 438,
        "categoryId": 32,
        "subCategoryName": "Chlorinators",
        "icon": null,
        "clues": "salt water chlorinators,chlorinators,salt chlorinators,salt pool chlorinators,Chlorinators,pool chlorinators"
    },
    {
        "id": 441,
        "categoryId": 32,
        "subCategoryName": "Christmas Trees & Decorations",
        "icon": null,
        "clues": "christmas trees wholesale,christmas trees & decorations,Christmas Trees & Decorations,christmas party,live christmas trees,gift shop supplies christmas decorations,christmas,real christmas trees,christmas functions,christmas shop,christmas trees,christmas decorations,christmas hampers,christmas lights"
    },
    {
        "id": 446,
        "categoryId": 32,
        "subCategoryName": "Cigarette Lighters &/or Repairs",
        "icon": null,
        "clues": "cigarettes shop,cigarettes,cigarettes vending,Cigarette Lighters &/or Repairs,cigarette lighters or repairs,cigarettes cabinets,cigarettes lighters"
    },
    {
        "id": 453,
        "categoryId": 32,
        "subCategoryName": "Clean Room Installation & Maintenance",
        "icon": null,
        "clues": "clean room,Clean Room Installation & Maintenance,clean rooms--installation equipment,clean rooms--installation,equipment & maintenance"
    },
    {
        "id": 454,
        "categoryId": 32,
        "subCategoryName": "Cleaning Cloth Supplies",
        "icon": null,
        "clues": "Cleaning Cloth Supplies,cleaning cloth supplies"
    },
    {
        "id": 458,
        "categoryId": 32,
        "subCategoryName": "Clocks & Repairs",
        "icon": null,
        "clues": "clocks,clocks restoration,clocks movements,clocks supplies,clocks--retail,clocks repair,clocks--retail & repairs,Clocks & Repairs,clocks parts,clocks shop"
    },
    {
        "id": 459,
        "categoryId": 32,
        "subCategoryName": "Clocks Wholesalers & Manufacturers",
        "icon": null,
        "clues": "clocks - wholesalers,Clocks Wholesalers & Manufacturers,clocks manufacturers"
    },
    {
        "id": 460,
        "categoryId": 32,
        "subCategoryName": "Clothes Hangers",
        "icon": null,
        "clues": "clothes hangers,Clothes Hangers,promotional products clothes hangers"
    },
    {
        "id": 461,
        "categoryId": 32,
        "subCategoryName": "Clothes Lines",
        "icon": null,
        "clues": "clothes lines repairs,clothes lines,Clothes Lines"
    },
    {
        "id": 462,
        "categoryId": 32,
        "subCategoryName": "Clothes Online",
        "icon": null,
        "clues": "Clothes Online"
    },
    {
        "id": 463,
        "categoryId": 32,
        "subCategoryName": "Clothing Alterations & Mending",
        "icon": null,
        "clues": "Clothing Alterations & Mending,clothing mending & alterations,clothing alterations & mending leather,alterations clothing,clothing alterations & mending,clothing repair & alterations,clothing alterations,clothing alterations & mending invisible mending,clothing alterations & mending tailors"
    },
    {
        "id": 464,
        "categoryId": 32,
        "subCategoryName": "Clothing Labels",
        "icon": null,
        "clues": "labels--clothing,Clothing Labels,woven labels clothing,printed clothing labels,clothing labels,woven clothing labels"
    },
    {
        "id": 465,
        "categoryId": 32,
        "subCategoryName": "Clothing Pattern Services",
        "icon": null,
        "clues": "clothing patterns,Clothing Pattern Services,clothing pattern services,clothing pattern makers"
    },
    {
        "id": 466,
        "categoryId": 32,
        "subCategoryName": "Clothing--Binding Suppliers",
        "icon": null,
        "clues": "Clothing--Binding Suppliers,clothing--binding suppliers"
    },
    {
        "id": 467,
        "categoryId": 32,
        "subCategoryName": "Clothing--Custom Made",
        "icon": null,
        "clues": "customised clothing,custom clothing,custom made clothing,printing--clothing & textile custom designed,clothing--custom made,custom corporate clothing,Clothing--Custom Made"
    },
    {
        "id": 468,
        "categoryId": 32,
        "subCategoryName": "Clothing--Makers-Up",
        "icon": null,
        "clues": ""
    },
    {
        "id": 472,
        "categoryId": 32,
        "subCategoryName": "Coasters & Mats--Table & Serving",
        "icon": null,
        "clues": "bomboniere coasters,coasters souvenir,coasters drink,coasters beer,coasters table & serving,manufacturers coasters,coasters promotional,coasters bomboniere,coasters,coasters advertising,drink coasters,beer coasters,drink coasters table & serving,coasters & mats--table & serving,bar coasters,Coasters & Mats--Table & Serving,coasters manufacturers,coasters bar,promotional products coasters"
    },
    {
        "id": 473,
        "categoryId": 32,
        "subCategoryName": "Coatings--Decorative",
        "icon": null,
        "clues": "coatings--decorative,graffiti coating decorative coatings,graffiti resistant decorative coatings,graffiti decorative coatings,Coatings--Decorative,graffiti protection decorative coatings,decorative concrete coatings"
    },
    {
        "id": 474,
        "categoryId": 32,
        "subCategoryName": "Coats",
        "icon": null,
        "clues": "Coats,fur coats,dog coats,coats,coats - ladies' - wholesalers & manufacturers,laboratory coats,leather coats,coats - ladies' - wholesalers,men's coats,coats capes"
    },
    {
        "id": 478,
        "categoryId": 32,
        "subCategoryName": "Coin & Note Changing Equipment",
        "icon": null,
        "clues": "Coin & Note Changing Equipment,coin & note changing equipment"
    },
    {
        "id": 479,
        "categoryId": 32,
        "subCategoryName": "Coin & Note Counting & Sorting Equipment",
        "icon": null,
        "clues": "coin counting machines,Coin & Note Counting & Sorting Equipment,coin & note counting & sorting equipment"
    },
    {
        "id": 480,
        "categoryId": 32,
        "subCategoryName": "Coin Dealers & Rare Coins",
        "icon": null,
        "clues": "coin dealers,rare coin dealers,rare coin,coin & stamp dealers,Coin Dealers & Rare Coins,coin & medal dealers,stamp & coin dealers"
    },
    {
        "id": 481,
        "categoryId": 32,
        "subCategoryName": "Cold Storage",
        "icon": null,
        "clues": "Cold Storage,cold store,cold storage freezers,portable cold storage,cold storage,storage cold,transport - light cold storage,cold rooms,cold storage transport,cold storage warehouse"
    },
    {
        "id": 485,
        "categoryId": 32,
        "subCategoryName": "Colours & Pigments",
        "icon": null,
        "clues": "Colours & Pigments,colours & pigments"
    },
    {
        "id": 494,
        "categoryId": 32,
        "subCategoryName": "Commercial Diving",
        "icon": null,
        "clues": "diving--commercial,commercial diving,commercial diving contractors,commercial diving services,Commercial Diving"
    },
    {
        "id": 495,
        "categoryId": 32,
        "subCategoryName": "Commercial Kitchen Equipment",
        "icon": null,
        "clues": "commercial kitchen equipment,commercial kitchen cleaning,kitchen equipment commercial,commercial catering equipment,commercial kitchen,commercial kitchen appliances,commercial equipment finance,Commercial Kitchen Equipment,commercial kitchen supplies,commercial equipment,used commercial kitchen equipment,commercial equipment hire,second hand commercial kitchen equipment,commercial kitchen design,catering equipment,supplies &/or service commercial kitchens"
    },
    {
        "id": 496,
        "categoryId": 32,
        "subCategoryName": "Commercial Laundry Equipment & Supplies",
        "icon": null,
        "clues": "Commercial Laundry Equipment & Supplies,laundry commercial,commercial catering equipment,commercial equipment finance,commercial laundry services,commercial laundry,commercial equipment,commercial kitchen supplies,commercial laundry appliances,commercial equipment hire,commercial cleaning supplies,catering equipment,supplies &/or service commercial kitchens,commercial laundry equipment"
    },
    {
        "id": 499,
        "categoryId": 32,
        "subCategoryName": "Communication Antennas",
        "icon": null,
        "clues": "Communication Antennas,communication equipment,antennas-communication,antennas-communication installation,communication consultants,antennas-communication satellite"
    },
    {
        "id": 505,
        "categoryId": 32,
        "subCategoryName": "Compasses &/or Compass Adjusters",
        "icon": null,
        "clues": "Compasses &/or Compass Adjusters,compass adjusters,compasses &/or compass adjusters,compasses"
    },
    {
        "id": 510,
        "categoryId": 32,
        "subCategoryName": "Computer & Video Games--Retail & Hire",
        "icon": null,
        "clues": "hire computer,computer hire &/or leasing,computer hire &/or leasing apple,computer & video games--retail & hire,computer hire,computer games hire,computer equipment hire,computer room hire,computer & video games--retail & hire repairs,Computer & Video Games--Retail & Hire,computer training room hire,computer rooms for hire,computer hire & leasing"
    },
    {
        "id": 512,
        "categoryId": 32,
        "subCategoryName": "Computer Equipment - Home & Office",
        "icon": null,
        "clues": "computer equipment--home &/or small business printers,computer equipment--installation & networking,computer equipment--hardware apple,computer equipment--installation & networking cabling,computer equipment--home &/or small business,computer equipment--hardware pcs,computer equipment--home &/or small business apple,computer equipment--home &/or small business laptops,computer equipment,computer equipment--repairs,service & upgrades printers,computer equipment--home & small business,computer equipment - hardware,computer equipment--home,computer equipment--hardware computer peripherals,computer equipment--repairs,service & upgrades apple,computer equipment--home &/or small business desktops,Computer Equipment - Home & Office,computer equipment--repairs,service & upgrades,computer equipment--installation & networking data cabling,computer equipment--hardware printers"
    },
    {
        "id": 513,
        "categoryId": 32,
        "subCategoryName": "Computer Equipment Supplies",
        "icon": null,
        "clues": "computer supplies retail,computer equipment supplies ink cartridges,computer equipment supplies,computer equipment supplies printers,computer supplies,Computer Equipment Supplies,computer equipment supplies apple"
    },
    {
        "id": 514,
        "categoryId": 32,
        "subCategoryName": "Computer Equipment--Secondhand",
        "icon": null,
        "clues": "computer equipment--secondhand laptops,Computer Equipment--Secondhand,computer equipment--secondhand,computer equipment--secondhand desktops"
    },
    {
        "id": 515,
        "categoryId": 32,
        "subCategoryName": "Computer Laptop Hire & Leasing",
        "icon": null,
        "clues": "Computer Laptop Hire & Leasing,computer hire &/or leasing,computer hire &/or leasing apple,computer leasing,computer hire & leasing"
    },
    {
        "id": 517,
        "categoryId": 32,
        "subCategoryName": "Computer On-Line Service Providers",
        "icon": null,
        "clues": "computer on-line service providers technical support,computer sales online,computer on-line service providers,online computer sales,Computer On-Line Service Providers"
    },
    {
        "id": 518,
        "categoryId": 32,
        "subCategoryName": "Computer Parts & Equipment",
        "icon": null,
        "clues": "computer parts & repair,Computer Parts & Equipment,second hand computer parts,computer parts sales,computer parts wholesale,computer spare parts,computer parts,computer parts manufacturers,computer parts for sales,computer parts & accessories,computer parts & services,computer parts second hand,computer parts & peripherals shop,computer parts retail,computer parts suppliers,computer parts supply,computer parts wholesalers"
    },
    {
        "id": 519,
        "categoryId": 32,
        "subCategoryName": "Computer Repairs, Service & Upgrades",
        "icon": null,
        "clues": "computer upgrades,computer upgrades & repair,computer equipment--repairs,service & upgrades laptops,computer networking upgrades,computer equipment--repairs,service & upgrades printers,Computer Repairs,Service & Upgrades,computer repair & upgrades,computer equipment--repairs,service & upgrades dell,computer equipment--repairs,service & upgrades apple,computer equipment--repairs,service & upgrades data recovery,computer equipment,repairs,service & upgrades hewlett-packard,computer equipment--repairs,service & upgrades ipod,computer equipment--repairs,service & upgrades pcs,computer equipment repair services upgrades,computer equipment--repairs,service & upgrades mobile technicians,computer equipment--repairs,service & upgrades,computer equipment--repairs,service & upgrades on-site services,computer equipment--repairs,service & upgrades desktops"
    },
    {
        "id": 534,
        "categoryId": 32,
        "subCategoryName": "Concrete Products",
        "icon": null,
        "clues": "Concrete Products,precast concrete products,concrete products manufacturers,concrete products pre-cast concrete,concrete garden products,concrete reinforcement products,reinforced concrete products,concrete stump products,concrete products"
    },
    {
        "id": 536,
        "categoryId": 32,
        "subCategoryName": "Concrete Pumps & Equipment",
        "icon": null,
        "clues": "concrete pumps & equipment concrete batching plants,concrete pumps & equipment,Concrete Pumps & Equipment"
    },
    {
        "id": 552,
        "categoryId": 32,
        "subCategoryName": "Continuous Stationery Printers & Equipment",
        "icon": null,
        "clues": "printers--continuous stationery,continuous kerbing,continuous printers,Continuous Stationery Printers & Equipment,continuous forms,continuous garden edging,printers--continuous stationery & equipment,continuous guttering,continuous stationery"
    },
    {
        "id": 554,
        "categoryId": 32,
        "subCategoryName": "Control Systems--Manual",
        "icon": null,
        "clues": "control systems--manual,Control Systems--Manual"
    },
    {
        "id": 555,
        "categoryId": 32,
        "subCategoryName": "Controllers--Automatic",
        "icon": null,
        "clues": "controllers--automatic,Controllers--Automatic,automatic gates controllers"
    },
    {
        "id": 559,
        "categoryId": 32,
        "subCategoryName": "Conveyor Belt & Elevating Systems",
        "icon": null,
        "clues": "conveyor systems & equipment,belts conveyor,Conveyor Belt & Elevating Systems,conveyor belts repair,conveyor belts cleaning,rubber conveyor belts,conveyor systems,conveyor belts splicing,conveyor belts,woven wire conveyor belts"
    },
    {
        "id": 562,
        "categoryId": 32,
        "subCategoryName": "Cooling Towers",
        "icon": null,
        "clues": "Cooling Towers,cooling tower consultants,cooling towers,cooling tower refitters,cooling tower manufacturers,cooling tower water treatment"
    },
    {
        "id": 569,
        "categoryId": 32,
        "subCategoryName": "Cork Merchants",
        "icon": null,
        "clues": "cork boards,cork floor sanding,cork tiles,cork tile resurfacing,cork products,cork,cork flooring,cork floor,cork sheet,cork suppliers,Cork Merchants,cork floor coverings,cork merchants"
    },
    {
        "id": 570,
        "categoryId": 32,
        "subCategoryName": "Corrosion Consultants",
        "icon": null,
        "clues": "corrosion,corrosion protection,Corrosion Consultants,corrosion engineers,corrosion consultants"
    },
    {
        "id": 578,
        "categoryId": 32,
        "subCategoryName": "Counting Equipment",
        "icon": null,
        "clues": "Counting Equipment,coin counting machines,tablet counting machine,counting equipment,money counting machine"
    },
    {
        "id": 580,
        "categoryId": 32,
        "subCategoryName": "Courier Services",
        "icon": null,
        "clues": "couriers,transport - light courier services,courier services,courier services air,international couriers,courier services international,courier services furniture,courier services local,courier services express courier international,courier services interstate,carriers--light courier services,interstate couriers,courier services refrigerated transport,Courier Services,same day courier services"
    },
    {
        "id": 583,
        "categoryId": 32,
        "subCategoryName": "Craft Supplies & Craft Shop",
        "icon": null,
        "clues": "crafts--retail & supplies paper,craft supplies clay,craft & wool shop,crafts--retail & supplies patchwork,crafts--retail & supplies beads,Craft Supplies & Craft Shop,craft supplies wholesale,bead & craft shop,craft shop,quilting craft shop,paper craft shop,craft wholesalers,craft supplies retail,art & craft,art & craft supplies"
    },
    {
        "id": 584,
        "categoryId": 32,
        "subCategoryName": "Crafts Wholesalers & Manufacturers",
        "icon": null,
        "clues": "crafts - wholesalers & manufacturers,crafts - wholesalers & manufacturers beads,crafts - wholesalers,Crafts Wholesalers & Manufacturers"
    },
    {
        "id": 587,
        "categoryId": 32,
        "subCategoryName": "Credit Cards",
        "icon": null,
        "clues": "credit cards,Credit Cards"
    },
    {
        "id": 588,
        "categoryId": 32,
        "subCategoryName": "Credit Management & Reporting Services",
        "icon": null,
        "clues": "credit management & reporting services credit reporting,credit management & reporting services international credit reports,Credit Management & Reporting Services,credit management & reporting services consultations,credit management & reporting services company research,credit management & reporting services debt collection,credit services,credit management & reporting services risk management reports,credit management & reporting services credit management,credit management & reporting services,credit management & reporting services adverse checks,credit reporting services,credit reporting agencies"
    },
    {
        "id": 589,
        "categoryId": 32,
        "subCategoryName": "Credit Unions & Societies",
        "icon": null,
        "clues": "credit unions & societies,Credit Unions & Societies,credit societies,bank credit unions,credit societies & unions"
    },
    {
        "id": 595,
        "categoryId": 32,
        "subCategoryName": "Crushers & Crushing Services",
        "icon": null,
        "clues": "crushers dust,crushers mining,Crushers & Crushing Services"
    },
    {
        "id": 598,
        "categoryId": 32,
        "subCategoryName": "Curtain Tracks & Accessories",
        "icon": null,
        "clues": "curtains curtain fabrics & accessories,curtain materials,curtain tracks,curtain accessories,Curtain Tracks & Accessories"
    },
    {
        "id": 599,
        "categoryId": 32,
        "subCategoryName": "Curtain Wholesalers & Manufacturers",
        "icon": null,
        "clues": "drapery & curtain manufacturers,curtain blind manufacturers,curtain shop & manufacturers,curtain fabric wholesalers,Curtain Wholesalers & Manufacturers,curtain manufacturers & wholesale"
    },
    {
        "id": 600,
        "categoryId": 32,
        "subCategoryName": "Curtains",
        "icon": null,
        "clues": "Curtains,curtains & blinds,blinds & curtains,curtains,curtains & curtain fabrics--retail,curtains retail"
    },
    {
        "id": 602,
        "categoryId": 32,
        "subCategoryName": "Customs Brokers",
        "icon": null,
        "clues": "customs clearance,customs brokers,ship brokers,charters & consultants customs clearance,Customs Brokers,customs agents brokers,customs clearance agents,customs agents,customs consultants,customs agencies,customs clearing"
    },
    {
        "id": 603,
        "categoryId": 32,
        "subCategoryName": "Cutlery",
        "icon": null,
        "clues": "hire cutlery,disposable cutlery,plastic cutlery,cutlery,cutlery hire,wholesale restaurant cutlery,Cutlery,cutlery supplies"
    },
    {
        "id": 610,
        "categoryId": 32,
        "subCategoryName": "Dancewear & Dance Costumes",
        "icon": null,
        "clues": "dancewear fabric,dancewear,accessories & supplies ballet,dancewear,accessories & supplies costumes,dancewear,accessories & supplies footwear,dancewear,accessories & supplies ballroom,dancewear wholesale,Dancewear & Dance Costumes,dancewear,accessories & supplies"
    },
    {
        "id": 616,
        "categoryId": 32,
        "subCategoryName": "Dating & Introduction Services",
        "icon": null,
        "clues": "dating & introduction services international,dating & introduction services matchmaking,dating & introduction services dinners,dating & introduction services relationship building,dating & introduction services,dating & introduction services online dating,dating & introduction services singles events,dating & introduction services theatre,dating & introduction services members events,dating & introduction services confidential,Dating & Introduction Services,dating,dating agencies,dating services,dating & introduction services travel"
    },
    {
        "id": 620,
        "categoryId": 32,
        "subCategoryName": "Deciduous Shrubs & Trees (Nurseries-Retail)",
        "icon": null,
        "clues": "Deciduous Shrubs & Trees (Nurseries-Retail),deciduous shrubs & trees (nurseries-retail)"
    },
    {
        "id": 622,
        "categoryId": 32,
        "subCategoryName": "Decorations--Suppliers &/or Contractors",
        "icon": null,
        "clues": "Decorations--Suppliers &/or Contractors,decorations--suppliers &/or contractors,decorations--suppliers"
    },
    {
        "id": 623,
        "categoryId": 32,
        "subCategoryName": "Degreasing Equipment",
        "icon": null,
        "clues": "degreasing equipment,truck detailing ad degreasing,degreasing,engine degreasing,Degreasing Equipment"
    },
    {
        "id": 624,
        "categoryId": 32,
        "subCategoryName": "Dehumidifiers",
        "icon": null,
        "clues": "Dehumidifiers,dehumidifiers,dehumidifiers products"
    },
    {
        "id": 636,
        "categoryId": 32,
        "subCategoryName": "Department Stores",
        "icon": null,
        "clues": "Department Stores,department store,department stores"
    },
    {
        "id": 640,
        "categoryId": 32,
        "subCategoryName": "Detectors - Electronic",
        "icon": null,
        "clues": "detectors - electronic underwater detectors,detectors - electronic leaks,detectors - electronic blocked drains,detectors - electronic repairs,detectors - electronic cable locating,detectors - electronic fisher,detectors - electronic gold detectors,Detectors - Electronic,detectors - electronic pipeline detectors,detectors - electronic industrial,detectors - electronic mine detectors,detectors - electronic leak detectors,detectors--electronic,detectors - electronic metal detectors,detectors - electronic camera inspections,detectors - electronic minelab metal detectors,detectors - electronic treasure detectors,detectors - electronic coin detectors,detectors - electronic mining,detectors - electronic plumbing,detectors - electronic hire"
    },
    {
        "id": 644,
        "categoryId": 32,
        "subCategoryName": "Diamond Tools",
        "icon": null,
        "clues": "diamond tools,diamond tool suppliers,Diamond Tools"
    },
    {
        "id": 645,
        "categoryId": 32,
        "subCategoryName": "Diamonds Merchant",
        "icon": null,
        "clues": "diamond jewellers,diamond wholesalers,Diamonds Merchant,wholesale diamonds,diamond merchants,diamond dealers"
    },
    {
        "id": 647,
        "categoryId": 32,
        "subCategoryName": "Dictating Machines",
        "icon": null,
        "clues": "Dictating Machines,dictating machines"
    },
    {
        "id": 648,
        "categoryId": 32,
        "subCategoryName": "Die Casting",
        "icon": null,
        "clues": "die casting mould manufacturers,Die Casting,die casting mould,die casting"
    },
    {
        "id": 653,
        "categoryId": 32,
        "subCategoryName": "Digital Printers",
        "icon": null,
        "clues": "digital printing,printers--digital canvas printing,printers--digital offset printing,printers offset & digital,printers--digital business cards,Digital Printers,printers-general digital printing,printers--digital large format,printers--digital"
    },
    {
        "id": 654,
        "categoryId": 32,
        "subCategoryName": "Direct Marketing",
        "icon": null,
        "clues": "Direct Marketing,direct marketing,direct marketing services"
    },
    {
        "id": 655,
        "categoryId": 32,
        "subCategoryName": "Directories",
        "icon": null,
        "clues": "Directories,business directories,funeral directories,directories"
    },
    {
        "id": 656,
        "categoryId": 32,
        "subCategoryName": "Directory & Notice Boards",
        "icon": null,
        "clues": "directory & notice boards,directory,Directory & Notice Boards,directory boards,telephone directory,solicitors directory,international directory,funeral directory,exhaust company directory,business directory,street directory,phone directory,architects directory,builders directory"
    },
    {
        "id": 660,
        "categoryId": 32,
        "subCategoryName": "Discount Stores",
        "icon": null,
        "clues": "discount stores,discount store,Discount Stores,discount variety store,discount stores retail"
    },
    {
        "id": 661,
        "categoryId": 32,
        "subCategoryName": "Dishwasher Service & Parts",
        "icon": null,
        "clues": "dishwasher technicians,dishwasher servicing,dishlex dishwasher spare parts,dishwasher seals,simpson dishwasher spare parts,dishwasher plumbers,dishwasher manufacturers,dishwasher installation,dishwasher parts,fisher & paykel dishwasher parts,dishwasher services,Dishwasher Service & Parts,lg dishwasher parts,dishwasher services & repair,lg dishwasher parts & repair,dishwasher spare parts,lg dishwasher spare parts,dishwasher service &/or parts,dishwasher repair,dishwasher supplies,simpson dishwasher parts"
    },
    {
        "id": 662,
        "categoryId": 32,
        "subCategoryName": "Dishwashers",
        "icon": null,
        "clues": "dishwashers,fisher & paykel dishwashers,delonghi dishwashers,Dishwashers,electrical appliances--retail dishwashers"
    },
    {
        "id": 663,
        "categoryId": 32,
        "subCategoryName": "Dispensing Equipment, Supplies & Service",
        "icon": null,
        "clues": "Dispensing Equipment,Supplies & Service,beer dispensing equipment,dispensing equipment,supplies & service,dispensing equipment supplies,coffee dispensing machines,dispensing equipment"
    },
    {
        "id": 666,
        "categoryId": 32,
        "subCategoryName": "Disposable Paper Products",
        "icon": null,
        "clues": "disposable glasses,disposable cups,party supplies disposable products,disposable medical products,paper products--disposable,paper products--disposable cups,disposable products suppliers,paper products--disposable food packaging,disposable paper,disposable paper products,disposable cutlery,disposable plates,disposable products,Disposable Paper Products,disposable packaging,paper disposable"
    },
    {
        "id": 667,
        "categoryId": 32,
        "subCategoryName": "Disposal Stores",
        "icon": null,
        "clues": "disposal stores,disposal waste,waste reduction & disposal services,disposal services,disposal store,Disposal Stores,waste reduction & disposal services skips,rubbish disposal,disposal bins,disposal camping"
    },
    {
        "id": 668,
        "categoryId": 32,
        "subCategoryName": "Distilled Water",
        "icon": null,
        "clues": "Distilled Water,distilled water,distilled"
    },
    {
        "id": 673,
        "categoryId": 32,
        "subCategoryName": "Document Scanning",
        "icon": null,
        "clues": "document scanning,Document Scanning"
    },
    {
        "id": 674,
        "categoryId": 32,
        "subCategoryName": "Document Shredding",
        "icon": null,
        "clues": "mobile document shredding,Document Shredding,security document shredding,document shredding,secure document shredding"
    },
    {
        "id": 682,
        "categoryId": 32,
        "subCategoryName": "Dolls & Doll Houses",
        "icon": null,
        "clues": "Dolls & Doll Houses,dolls & doll houses,porcelain dolls,dolls clothes,dolls pram,antique dolls,babushka dolls,dolls,russian dolls"
    },
    {
        "id": 684,
        "categoryId": 32,
        "subCategoryName": "Door & Gate Operating Equipment",
        "icon": null,
        "clues": "door & gate operating equipment,Door & Gate Operating Equipment,door & gates operating,door & gate operating equipment glass doors,door & gate operating equipment remote controls,door & gate operating equipment installation,security doors,windows & equipment door locks,door & gate operating equipment roller doors,door & gate operating equipment repairs,door & gate operating equipment garage doors,door & gates operating systems,Security Doors,Windows & Equipment,door & gate operating equipment sliding gates"
    },
    {
        "id": 685,
        "categoryId": 32,
        "subCategoryName": "Doors & Door Fittings",
        "icon": null,
        "clues": "doors & door fittings hume doors & timber,doors & door fittings timber,doors & door fittings knobs,doors & door fittings screen,doors & door fittings sliding,doors & door fittings handles,doors & fittings,Doors & Door Fittings,garage doors & fittings b&d,doors & door fittings wardrobe,doors & door fittings,doors & door fittings aluminium,doors & door fittings repairs,doors & doors fittings,doors & door fittings bi-fold,doors & door fittings hinges,doors & door fittings fittings,doors & door fittings internal"
    },
    {
        "id": 686,
        "categoryId": 32,
        "subCategoryName": "Double Glazed Windows",
        "icon": null,
        "clues": "double hung windows,double glazing windows & door,windows--double glazed,windows--double glazed sliding windows,Double Glazed Windows,double decker bus,double glazed windows,double decker,double beds,double glazed,double glazing windows,double glazing,doors & windows double glazed,windows--timber double hung windows,double bass"
    },
    {
        "id": 693,
        "categoryId": 32,
        "subCategoryName": "Dress Materials",
        "icon": null,
        "clues": "Dress Materials,wholesale dress materials,fabric dress materials,dress materials,dress materials wholesale,dress materials retail,dress materials fabrics"
    },
    {
        "id": 694,
        "categoryId": 32,
        "subCategoryName": "Dress Patterns & Dressmakers' Models",
        "icon": null,
        "clues": "Dress Patterns & Dressmakers' Models,dress patterns,dress patterns & dressmakers' models,wedding dress patterns"
    },
    {
        "id": 695,
        "categoryId": 32,
        "subCategoryName": "Dress W'salers & Mfrs",
        "icon": null,
        "clues": "dress wholesalers & manufacturers,dress fabric wholesalers,wedding dress wholesalers,dress wholesalers,dress manufacturers,national dress wholesalers & manufacturers,fancy dress wholesalers"
    },
    {
        "id": 696,
        "categoryId": 32,
        "subCategoryName": "Dressmakers",
        "icon": null,
        "clues": "dressmakers,dressmakers couture,Dressmakers,dressmakers formal wear"
    },
    {
        "id": 705,
        "categoryId": 32,
        "subCategoryName": "Duct Tape & Adhesive Tape",
        "icon": null,
        "clues": "Duct Tape & Adhesive Tape"
    },
    {
        "id": 706,
        "categoryId": 32,
        "subCategoryName": "Ducting & Ductwork",
        "icon": null,
        "clues": "ducting & ventilation,ducting vacuum cleaners,ductwork & ducting,ducting supplies,ducting heating repair,ducting & ducting,ducting manufacturers,ducting & ducting manufacturers,air conditioning ducting,Ducting & Ductwork,ducting"
    },
    {
        "id": 707,
        "categoryId": 32,
        "subCategoryName": "Dust & Fume Extraction Systems",
        "icon": null,
        "clues": "dust & fume,fume & dust extraction,Dust & Fume Extraction Systems,dust extraction systems,dust & fume control equipment,wet dust control systems,dust & fume control,dust extraction,dust & fume extraction"
    },
    {
        "id": 708,
        "categoryId": 32,
        "subCategoryName": "Duty Free Shops",
        "icon": null,
        "clues": "duty free,duty free cameras,Duty Free Shops,duty free shops,duty free shop,duty free shopping"
    },
    {
        "id": 711,
        "categoryId": 32,
        "subCategoryName": "Dyers",
        "icon": null,
        "clues": "Dyers,dyers,furskin dressers & dyers,textile dyers"
    },
    {
        "id": 712,
        "categoryId": 32,
        "subCategoryName": "Dyes & Dyestuffs",
        "icon": null,
        "clues": "food dyes,Dyes & Dyestuffs,dyes & dyestuffs"
    },
    {
        "id": 720,
        "categoryId": 32,
        "subCategoryName": "EFTPOS &/or Merchants Facilities",
        "icon": null,
        "clues": "EFTPOS &/or Merchants Facilities,eftpos &/or merchants facilities,eftpos machines,eftpos,eftpos accepted"
    },
    {
        "id": 726,
        "categoryId": 32,
        "subCategoryName": "Electric Fences",
        "icon": null,
        "clues": "Electric Fences,electric fences"
    },
    {
        "id": 727,
        "categoryId": 32,
        "subCategoryName": "Electric Hoists",
        "icon": null,
        "clues": "Electric Hoists,hoists -- electric,electric hoists"
    },
    {
        "id": 728,
        "categoryId": 32,
        "subCategoryName": "Electric Lamps, Globes & Tubes",
        "icon": null,
        "clues": "automotive globes,light globes,Electric Lamps,Globes & Tubes,electric lamps,globes & tubes,projectors globes,electric lamps globes,electric lamps,globes"
    },
    {
        "id": 729,
        "categoryId": 32,
        "subCategoryName": "Electric Meters",
        "icon": null,
        "clues": "electric meters,Electric Meters"
    },
    {
        "id": 730,
        "categoryId": 32,
        "subCategoryName": "Electric Motor Repairs & Generator Repairs",
        "icon": null,
        "clues": "Electric Motor Repairs & Generator Repairs,electric motors & generators--repairs rewinding,electric motors & generators--repairs,electric generators,electric motor services,electric motors & generators"
    },
    {
        "id": 731,
        "categoryId": 32,
        "subCategoryName": "Electric Motors",
        "icon": null,
        "clues": "Electric Motors,second hand electric motors,dc electric motors,pope electric motors,electric motors,240v electric motors"
    },
    {
        "id": 734,
        "categoryId": 32,
        "subCategoryName": "Electrical & Home Appliance Stores",
        "icon": null,
        "clues": "electrical homewares,electrical appliance sales,Electrical & Home Appliance Stores,electrical appliances--retail,home electrical appliance,electrical stores & appliances,electrical appliances"
    },
    {
        "id": 737,
        "categoryId": 32,
        "subCategoryName": "Electrical Appliances Services & Parts",
        "icon": null,
        "clues": "electrical appliances--repairs,service &/or parts microwaves,electrical appliances spare parts,electrical appliances - ovens,electrical appliances--repairs,service &/or parts samsung,Electrical Appliances Services & Parts,electrical appliances parts,electrical appliances repair,electrical appliances--repairs,service &/or parts stoves,electrical appliances--repairs,service &/or parts,electrical appliances--repairs services,electrical appliances - wholesalers & manufacturers,electrical appliances--retail small appliances,electrical appliances--retail televisions,electrical appliances--repairs,service &/or parts ovens,electrical appliances--retail,electrical appliances--service & parts,electrical appliances--retail dishwashers,electrical appliances--retail factory seconds,electrical appliances"
    },
    {
        "id": 741,
        "categoryId": 32,
        "subCategoryName": "Electrical Switches & Control Equipment",
        "icon": null,
        "clues": "electrical control & automation,electrical control,electrical switches,electrical switches & control equipment,Electrical Switches & Control Equipment,electrical contractors safety switches,electrical control equipment,electrical control systems"
    },
    {
        "id": 743,
        "categoryId": 32,
        "subCategoryName": "Electrical Testing Equipment",
        "icon": null,
        "clues": "electrical testing services,Electrical Testing Equipment"
    },
    {
        "id": 744,
        "categoryId": 32,
        "subCategoryName": "Electrical Wholesalers",
        "icon": null,
        "clues": ""
    },
    {
        "id": 750,
        "categoryId": 32,
        "subCategoryName": "Electronic & LED Signs",
        "icon": null,
        "clues": "Electronic & LED Signs,electronic signs,signs--electronic"
    },
    {
        "id": 753,
        "categoryId": 32,
        "subCategoryName": "Electronic Parts & Equipment",
        "icon": null,
        "clues": "Electronic Parts & Equipment,electronic parts - wholesalers & manufacturers,electronic equipment & parts--retail or service jaycar,electronic equipment & parts--retail or service,electronic equipment & parts--retail or service electrical components,electronic parts assembly,electronic parts suppliers,electronic parts - wholesalers,electronic spare parts,electronic equipment & parts,electronic equipment & parts--retail or service bosch"
    },
    {
        "id": 754,
        "categoryId": 32,
        "subCategoryName": "Electronic Parts Assembly Services",
        "icon": null,
        "clues": "Electronic Parts Assembly Services,electronic parts assembly services,electronic assembly,electronic parts assembly,electronic services"
    },
    {
        "id": 756,
        "categoryId": 32,
        "subCategoryName": "Electronic Rust Protection & Rustproofing",
        "icon": null,
        "clues": "Electronic Rust Protection & Rustproofing,electronic rust protection,electronic rust proofing"
    },
    {
        "id": 763,
        "categoryId": 32,
        "subCategoryName": "Embossed Seals & Labels",
        "icon": null,
        "clues": "embossed seals & labels,Embossed Seals & Labels,embossed seals"
    },
    {
        "id": 764,
        "categoryId": 32,
        "subCategoryName": "Embroidery",
        "icon": null,
        "clues": "Embroidery,embroidery services,embroidery"
    },
    {
        "id": 765,
        "categoryId": 32,
        "subCategoryName": "Embroidery Supplies & Equipment",
        "icon": null,
        "clues": "embroidery services,embroidery supplies & equipment,embroidery supplies,Embroidery Supplies & Equipment,crafts--retail & supplies embroidery"
    },
    {
        "id": 781,
        "categoryId": 32,
        "subCategoryName": "Engineering Supplies",
        "icon": null,
        "clues": "industrial engineering supplies,Engineering Supplies,engineering supplies"
    },
    {
        "id": 782,
        "categoryId": 32,
        "subCategoryName": "Engraving",
        "icon": null,
        "clues": "engraving,Engraving,engraving services"
    },
    {
        "id": 783,
        "categoryId": 32,
        "subCategoryName": "Engraving Machines",
        "icon": null,
        "clues": "engraving machines,Engraving Machines,engraving services,engraving machines & accessories"
    },
    {
        "id": 787,
        "categoryId": 32,
        "subCategoryName": "Envelope Wholesalers & Manufacturers",
        "icon": null,
        "clues": "envelope manufacturers,Envelope Wholesalers & Manufacturers,envelope wholesalers,envelope printing,envelope stuffing,envelope wholesalers & manufacturers,envelope suppliers,manufacturers envelope,envelopes product"
    },
    {
        "id": 790,
        "categoryId": 32,
        "subCategoryName": "Environmental Products & Equipment",
        "icon": null,
        "clues": "environmental monitoring equipment,environmental products,environmental products & equipment,Environmental Products & Equipment,environmental friendly products,environmental equipment"
    },
    {
        "id": 791,
        "categoryId": 32,
        "subCategoryName": "Ergonomics",
        "icon": null,
        "clues": "Ergonomics,ergonomics"
    },
    {
        "id": 792,
        "categoryId": 32,
        "subCategoryName": "Erosion Control & Soil Stabilisation",
        "icon": null,
        "clues": "Erosion Control & Soil Stabilisation,erosion,erosion control & soil stabilisation,water damage erosion control,flood damage erosion control,erosion control devices,soil erosion,erosion control flood damage,erosion control,erosion control & soil stabilisation hydromulching,soil erosion control devices"
    },
    {
        "id": 793,
        "categoryId": 32,
        "subCategoryName": "Escalators &/or Maintenance Services",
        "icon": null,
        "clues": "elevator & escalators,lifts & escalators,escalators &/or maintenance services,Escalators &/or Maintenance Services,escalators"
    },
    {
        "id": 798,
        "categoryId": 32,
        "subCategoryName": "Evening Wear",
        "icon": null,
        "clues": "evening wear cocktail dresses,evening shoes,evening wear ladies,evening wear ball gowns,evening wear boutiques,bridal evening wear hire,evening wear--retail & hire,evening wear women,evening gowns,Evening Wear,evening wear purchases,evening wear--retail & for hire,bridal evening wear,evening wear for hire,evening wear & bridal,evening dress,evening wear store,evening wear formal,ladies evening wear,evening wear dress,evening wear plus sizes,evening wear,evening wear hire"
    },
    {
        "id": 800,
        "categoryId": 32,
        "subCategoryName": "Evergreen Shrubs & Trees (Nurseries-Retail)",
        "icon": null,
        "clues": "evergreen shrubs & trees (nurseries-retail),Evergreen Shrubs & Trees (Nurseries-Retail)"
    },
    {
        "id": 807,
        "categoryId": 32,
        "subCategoryName": "Exotic & Tropical Plants (Nurseries-Retail)",
        "icon": null,
        "clues": "exotic plants,exotic & tropical plants (nurseries-retail),Exotic & Tropical Plants (Nurseries-Retail)"
    },
    {
        "id": 809,
        "categoryId": 32,
        "subCategoryName": "Expansion Joints",
        "icon": null,
        "clues": "expansion joints,Expansion Joints,concrete expansion joints"
    },
    {
        "id": 812,
        "categoryId": 32,
        "subCategoryName": "Exporters",
        "icon": null,
        "clues": "export auto spares used,export of products,export packaging,export pallets,export consultants,Exporters,Fruit,Vegetable & Grain Exporters,meat exporters,export shipping,export auto spares,export used car batteries,export freight,export international,fruit,vegetable & grain exporters,exporters,export frozen chicken wings"
    },
    {
        "id": 813,
        "categoryId": 32,
        "subCategoryName": "Ex-Service & Service Organisations",
        "icon": null,
        "clues": "Ex-Service & Service Organisations"
    },
    {
        "id": 817,
        "categoryId": 32,
        "subCategoryName": "Fabrics-Knitted or Woven",
        "icon": null,
        "clues": "Fabrics-Knitted or Woven,fabrics-knitted or woven"
    },
    {
        "id": 821,
        "categoryId": 32,
        "subCategoryName": "Factory Clearance Outlets",
        "icon": null,
        "clues": "factory clearance,shoes factory outlets,shoe factory outlets,handbags factory outlets,maternity factory outlets,factory outlets shoes,mattresses factory outlets,factory clearance outlets,factory outlets store,factory outlets clothing,furniture factory outlets,surf factory outlets,Factory Clearance Outlets,clothing factory outlets,manchester factory outlets,chocolate factory outlets,factory outlets,factory seconds outlets,jeans factory outlets"
    },
    {
        "id": 822,
        "categoryId": 32,
        "subCategoryName": "Fairy Shops",
        "icon": null,
        "clues": "fairy supplies,fairy dress,fairy shop,fairy wings,fairy lights,fairy shops,fairy entertainers,Fairy Shops,fairy floss machine hire,fairy party,fairy costume,fairy floss hire,fairy floss,fairy entertainment,fairy clothing,fairy,fairy floss machine,fairy figurines"
    },
    {
        "id": 826,
        "categoryId": 32,
        "subCategoryName": "Fans & Blowers",
        "icon": null,
        "clues": "fans ceiling,fans retail,fans & blowers ceiling,Fans & Blowers,fans & blowers,ceiling fans,industrial fans,fans & blowers exhaust,fans & blowers industrial,fans,exhaust fans"
    },
    {
        "id": 833,
        "categoryId": 32,
        "subCategoryName": "Fashion Accessories",
        "icon": null,
        "clues": "fashion accessories wholesale,Fashion Accessories,fashion accessories,fashion & accessories store,fashion accessories hair accessories,promotional products fashion accessories,fashion accessories agents,fashion accessories--retail"
    },
    {
        "id": 835,
        "categoryId": 32,
        "subCategoryName": "Fashion Jewellery",
        "icon": null,
        "clues": "fashion costume jewellery,fashion jewellery wholesalers,wholesale fashion jewellery,Fashion Jewellery,fashion jewellery distributors,fashion jewellery importers,fashion jewellery,beads & fashion jewellery"
    },
    {
        "id": 837,
        "categoryId": 32,
        "subCategoryName": "Fasteners--Slide or Snap",
        "icon": null,
        "clues": "Fasteners--Slide or Snap,fasteners--slide,fasteners--slide or snap"
    },
    {
        "id": 838,
        "categoryId": 32,
        "subCategoryName": "Fax Equipment, Supplies & Service",
        "icon": null,
        "clues": "fax repair,fax machine repair,fax broadcasting,fax equipment,supplies & service,fax machines lease,Fax Equipment,Supplies & Service,fax machines,fax toner,fax retailer,fax supplies,fax cartridge,fax machines services,fax,fax servicing,fax services,fax machine services"
    },
    {
        "id": 839,
        "categoryId": 32,
        "subCategoryName": "Feathers & Down--Supplies & Products",
        "icon": null,
        "clues": "Feathers & Down--Supplies & Products,feathers & down--supplies & products,feathers"
    },
    {
        "id": 840,
        "categoryId": 32,
        "subCategoryName": "Feedlots",
        "icon": null,
        "clues": "Feedlots,feedlots"
    },
    {
        "id": 846,
        "categoryId": 32,
        "subCategoryName": "Feng Shui",
        "icon": null,
        "clues": "feng sui,feng shui,Feng Shui,feng shui consultants"
    },
    {
        "id": 850,
        "categoryId": 32,
        "subCategoryName": "Fibre & Cardboard Boxes",
        "icon": null,
        "clues": "boxes & cartons-cardboard & fibre,Fibre & Cardboard Boxes"
    },
    {
        "id": 852,
        "categoryId": 32,
        "subCategoryName": "Fibre Optics",
        "icon": null,
        "clues": "Fibre Optics,fibre optics,computer equipment--installation & networking fibre optics,data cabling fibre optics,computer networking fibre optics"
    },
    {
        "id": 860,
        "categoryId": 32,
        "subCategoryName": "Filtering Materials & Supplies",
        "icon": null,
        "clues": "Filtering Materials & Supplies,filtering materials,filtering materials & supplies"
    },
    {
        "id": 861,
        "categoryId": 32,
        "subCategoryName": "Filters & Filter Service",
        "icon": null,
        "clues": "filters & filter service,Filters & Filter Service"
    },
    {
        "id": 868,
        "categoryId": 32,
        "subCategoryName": "Fine Arts & Antiques Valuers",
        "icon": null,
        "clues": "valuers--fine arts,Fine Arts & Antiques Valuers,valuers--fine arts & antiques,fine art valuers"
    },
    {
        "id": 873,
        "categoryId": 32,
        "subCategoryName": "Fire Safety Equipment & Consultants",
        "icon": null,
        "clues": "fire protection equipment & consultants fire safety,fire protection equipment & consultants fire safety training,fire safety,fire protection equipment & consultants,fire safety engineers,fire safety equipment,fire protection equipment & consultants extinguishers,fire protection equipment & consultants bushfire consultants,fire protection consultants,fire protection equipment & consultants smoke detectors,fire safety training,fire safety services,fire safety inspections,fire safety consultants,fire safety systems,Fire Safety Equipment & Consultants,fire consultants,fire protection equipment & consultants sprinklers"
    },
    {
        "id": 874,
        "categoryId": 32,
        "subCategoryName": "Fireplaces & Fireplace Accessories",
        "icon": null,
        "clues": "fireplaces & accessories gas,fireplaces & accessories,fireplaces & accessories gas log heaters,fireplaces & accessories slow combustion,fireplaces & accessories repairs,Fireplaces & Fireplace Accessories,gas fireplaces,fireplaces & accessories installation,fireplaces & accessories wood heaters,fireplaces,fireplaces & accessories wood"
    },
    {
        "id": 875,
        "categoryId": 32,
        "subCategoryName": "Firewood",
        "icon": null,
        "clues": "firewood suppliers,Firewood,firewood"
    },
    {
        "id": 876,
        "categoryId": 32,
        "subCategoryName": "Fireworks",
        "icon": null,
        "clues": "fireworks wedding,fireworks,Fireworks"
    },
    {
        "id": 881,
        "categoryId": 32,
        "subCategoryName": "Fishing Nets",
        "icon": null,
        "clues": "Fishing Nets,fishing nets"
    },
    {
        "id": 882,
        "categoryId": 32,
        "subCategoryName": "Fishing Tackle",
        "icon": null,
        "clues": "tackle fishing,fishing tackle supply,fishing bait & tackle,fishing tackle equipment,fishing tackle shop,fishing tackle bait,fishing tackle,fishing tackle rods,fishing tackle wholesalers,Fishing Tackle,fishing tackle & bait supply,fishing tackle wholesale,fishing tackle supplies"
    },
    {
        "id": 886,
        "categoryId": 32,
        "subCategoryName": "Flags, Banners & Pennants",
        "icon": null,
        "clues": "banners & flags,Flags,Banners & Pennants,banners stands,banners printing,banners manufacturers,banners design,banners display,pennants,flags,flags of the world,banners & signs,flags & banners,banners,flags,pennants & banners,banners advertising,flags pennants,banners poles,flags & bunting,flags & flagpoles"
    },
    {
        "id": 888,
        "categoryId": 32,
        "subCategoryName": "Flexible Shafting & Equipment",
        "icon": null,
        "clues": "Flexible Shafting & Equipment,flexible shafting & equipment"
    },
    {
        "id": 889,
        "categoryId": 32,
        "subCategoryName": "Flock & Suede Finishing",
        "icon": null,
        "clues": "flock & suede finishing,Flock & Suede Finishing,flock wholesalers & manufacturers"
    },
    {
        "id": 892,
        "categoryId": 32,
        "subCategoryName": "Floor Machines",
        "icon": null,
        "clues": "floor sanding machines,floor machines,Floor Machines"
    },
    {
        "id": 895,
        "categoryId": 32,
        "subCategoryName": "Floor Treatment Products",
        "icon": null,
        "clues": "sheepskin products floor coverings,floor cleaning products,floor treatment,Floor Treatment Products,floor treatment products anti-slip treatments,floor products,floor treatment products"
    },
    {
        "id": 897,
        "categoryId": 32,
        "subCategoryName": "Florist Supplies",
        "icon": null,
        "clues": "florist supplies,Florist Supplies,florists' supplies,wholesale florist supplies"
    },
    {
        "id": 898,
        "categoryId": 32,
        "subCategoryName": "Florists",
        "icon": null,
        "clues": "florists retail hampers,florists retail dried,florists retail,florists retail delivery,Florists,florists,florists wholesale,florists retail orchids,florists retail interflora,florists retail roses,florists retail preservation,florists retail free delivery"
    },
    {
        "id": 900,
        "categoryId": 32,
        "subCategoryName": "Fly Screens",
        "icon": null,
        "clues": "security fly screens,flyscreen,timber fly screens,fly screen repair,fly screens,Fly Screens,fly screen door,fly fishing,window fly screens,magnetic fly screens,flying schools"
    },
    {
        "id": 904,
        "categoryId": 32,
        "subCategoryName": "Foam & Plastic Signs",
        "icon": null,
        "clues": "Foam & Plastic Signs"
    },
    {
        "id": 906,
        "categoryId": 32,
        "subCategoryName": "Foil Printing & Stamping",
        "icon": null,
        "clues": "hot foil printing,hot stamping foil,foil printing,foil balloons,Foil Printing & Stamping,hot foil stamping,foil bags,foil printing & stamping,Labels--Metal Foil,Paper & Plastic,foil packaging,foil stamping,foil & foil products,foil trays,foil container,foil insulation,foil stamping machine,foil,foil printing machine"
    },
    {
        "id": 907,
        "categoryId": 32,
        "subCategoryName": "Foil Printing & Stamping Equipment & Supplies",
        "icon": null,
        "clues": "Foil Printing & Stamping Equipment & Supplies,foil printing & stamping equipment & supplies"
    },
    {
        "id": 926,
        "categoryId": 32,
        "subCategoryName": "Formal Wear Hire--Men's",
        "icon": null,
        "clues": "school formal mens formal hire,formal wear,formal wear hire--men's,men's formal suit hire,men's formal wear,men's formal hire,formal wear shop,formal men's hire,hire men's formal wear,prom night mens formal hire,formal hire men's,men's formal clothing hire,Formal Wear Hire--Men's,men's formal wear hire,formal men's wear hire,formal dress shop,formal dress hire,formal suit hire,formal wear evening wear,formal wear men's,men's formal hire shop"
    },
    {
        "id": 930,
        "categoryId": 32,
        "subCategoryName": "Franchises & Franchising",
        "icon": null,
        "clues": "real estate franchises,restaurant franchises,Franchises & Franchising,fast food franchises"
    },
    {
        "id": 935,
        "categoryId": 32,
        "subCategoryName": "French Polishing",
        "icon": null,
        "clues": "french polishing supplies,french polishing,French Polishing,french polishers"
    },
    {
        "id": 936,
        "categoryId": 32,
        "subCategoryName": "Fridge Repairs",
        "icon": null,
        "clues": "second hand fridge repairs & sales,Fridge Repairs,fridge repair,fridge parts"
    },
    {
        "id": 946,
        "categoryId": 32,
        "subCategoryName": "Fuchsias (Nurseries-Retail)",
        "icon": null,
        "clues": "fuchsias (nurseries-retail),Fuchsias (Nurseries-Retail)"
    },
    {
        "id": 947,
        "categoryId": 32,
        "subCategoryName": "Fuel & Oil Additives",
        "icon": null,
        "clues": "fuel & oil additives,Fuel & Oil Additives,fuel additives"
    },
    {
        "id": 949,
        "categoryId": 32,
        "subCategoryName": "Fuel Pumps & Marketing Equipment",
        "icon": null,
        "clues": "Fuel Pumps & Marketing Equipment"
    },
    {
        "id": 952,
        "categoryId": 32,
        "subCategoryName": "Fund Raising Equipment & Supplies",
        "icon": null,
        "clues": "Fund Raising Equipment & Supplies,fund raising equipment & supplies"
    },
    {
        "id": 954,
        "categoryId": 32,
        "subCategoryName": "Funeral Directors",
        "icon": null,
        "clues": "sustainable funeral directors,greek funeral directors,funeral directors crematoriums,Funeral Directors,funeral directors cremation,funeral directors chinese,funeral homes & directors,funeral directors,funeral directors cemeteries"
    },
    {
        "id": 955,
        "categoryId": 32,
        "subCategoryName": "Funeral Plans & Benefit Funds",
        "icon": null,
        "clues": "funeral plans,funeral funds,funeral benefits funds,funeral plans & benefit funds,Funeral Plans & Benefit Funds"
    },
    {
        "id": 956,
        "categoryId": 32,
        "subCategoryName": "Funeral Supplies",
        "icon": null,
        "clues": "funeral director supplies,funeral supplies,Funeral Supplies"
    },
    {
        "id": 957,
        "categoryId": 32,
        "subCategoryName": "Furnaces",
        "icon": null,
        "clues": "Furnaces,furnaces"
    },
    {
        "id": 958,
        "categoryId": 32,
        "subCategoryName": "Furnishings--Retail",
        "icon": null,
        "clues": "furnishings--retail,Furnishings--Retail"
    },
    {
        "id": 959,
        "categoryId": 32,
        "subCategoryName": "Furnishings--W'sale",
        "icon": null,
        "clues": "furnishings wholesale,soft furnishings wholesale"
    },
    {
        "id": 960,
        "categoryId": 32,
        "subCategoryName": "Furniture & Appliance Hire",
        "icon": null,
        "clues": "hire--household appliances & furniture,appliance & furniture hire,hire--office equipment & furniture,hire--household appliances & furniture refrigerators,Furniture & Appliance Hire,hire furniture,hire--household appliances & furniture rental,baby furniture hire,furniture hire & sales,furniture trailer hire,furniture hire,baby prams,furniture & accessories baby equipment hire"
    },
    {
        "id": 961,
        "categoryId": 32,
        "subCategoryName": "Furniture Assembly Services",
        "icon": null,
        "clues": "Furniture Assembly Services,furniture assembly services harvey norman,furniture assembly services,furniture assembly services fantastic furniture"
    },
    {
        "id": 962,
        "categoryId": 32,
        "subCategoryName": "Furniture Design & Custom Furniture",
        "icon": null,
        "clues": "custom made timber furniture,furniture designers & custom builders wardrobes,furniture designers & custom builders lounge suites,designers furniture,furniture custom builders,furniture custom made,furniture designers,furniture custom,furniture designers & custom builders,custom made furniture,custom made pine furniture,design furniture,custom furniture,furniture designers & custom builders tables,custom design furniture,furniture designers & custom builders timber,custom made office furniture,custom built furniture,Furniture Design & Custom Furniture,custom furniture makers"
    },
    {
        "id": 966,
        "categoryId": 32,
        "subCategoryName": "Furniture Restoration & Repairs",
        "icon": null,
        "clues": "furniture repairs & restorations upholstery,baby prams,furniture & accessories repairs,restoration furniture,furniture restoration supplies,furniture repairs & restorations all timber furniture,Furniture Restoration & Repairs,furniture repairs,antique furniture restoration,furniture repairs & restorations,furniture restoration,furniture repairs & restorations antique furniture"
    },
    {
        "id": 967,
        "categoryId": 32,
        "subCategoryName": "Furniture Stores & Shops",
        "icon": null,
        "clues": "timber furniture shops,Furniture Stores & Shops,nursery furniture stores for babies"
    },
    {
        "id": 973,
        "categoryId": 32,
        "subCategoryName": "Gaming Machines & Supplies",
        "icon": null,
        "clues": "gaming machines,gaming machines & supplies,gaming supplies,Gaming Machines & Supplies"
    },
    {
        "id": 977,
        "categoryId": 32,
        "subCategoryName": "Garbage Disposal Units",
        "icon": null,
        "clues": "garbage disposal units,garbage disposal,Garbage Disposal Units"
    },
    {
        "id": 978,
        "categoryId": 32,
        "subCategoryName": "Garden Nurseries",
        "icon": null,
        "clues": "nurseries garden centre,garden centre nurseries,native garden nurseries,Garden Nurseries,garden nurseries & supplies,nurseries garden,nurseries & garden supplies,garden nurseries"
    },
    {
        "id": 979,
        "categoryId": 32,
        "subCategoryName": "Garden Pots",
        "icon": null,
        "clues": "pots--garden,garden pots,wholesale garden pots,garden equipment & supplies pots,ceramic garden pots,Garden Pots,garden pots & ornaments,garden supplies pots,plastic garden pots"
    },
    {
        "id": 980,
        "categoryId": 32,
        "subCategoryName": "Garden Sheds",
        "icon": null,
        "clues": "garden sheds,timber garden sheds,cedar garden sheds,garden sheds aviaries,garden sheds cubby houses,hardware--retail garden sheds,Garden Sheds,garden sheds dog kennels"
    },
    {
        "id": 981,
        "categoryId": 32,
        "subCategoryName": "Garden Supplies & Equipment",
        "icon": null,
        "clues": "garden equipment & supplies statues,garden equipment & supplies pavers,garden supplies sleepers,garden equipment & supplies garden furniture,garden equipment hire,hire garden equipment,Garden Supplies & Equipment,Indoor Plants,Gardens & Supplies,garden supplies & or equipment,garden supplies pots,garden equipment & supplies water features,garden equipment & supplies ponds,garden equipment,garden equipment & supply,garden supplies wholesale,garden equipment & supplies,garden equipment & supplies pots,garden supplies,garden supplies plants,garden equipment & supplies mulch"
    },
    {
        "id": 982,
        "categoryId": 32,
        "subCategoryName": "Gardeners",
        "icon": null,
        "clues": "landscape gardeners,gardeners,gardeners garden maintenance,Gardeners"
    },
    {
        "id": 984,
        "categoryId": 32,
        "subCategoryName": "Gas Appliances",
        "icon": null,
        "clues": "gas appliances services,Gas Appliances,gas appliances & equipment heaters,gas appliances repair,gas appliances installation,gas appliances,gas appliances & equipment,gas appliances & equipment hot water"
    },
    {
        "id": 985,
        "categoryId": 32,
        "subCategoryName": "Gas Burners",
        "icon": null,
        "clues": "Gas Burners,gas burners & equipment,gas burners"
    },
    {
        "id": 986,
        "categoryId": 32,
        "subCategoryName": "Gas Cylinder Testing",
        "icon": null,
        "clues": "lpg & natural gas equipment & services testing,gas tank testing,gas cylinder testing,Gas Cylinder Testing,gas testing,gas bottle testing,lpg gas tank testing"
    },
    {
        "id": 987,
        "categoryId": 32,
        "subCategoryName": "Gas Detectors",
        "icon": null,
        "clues": "Gas Detectors,gas detectors"
    },
    {
        "id": 988,
        "categoryId": 32,
        "subCategoryName": "Gas Struts",
        "icon": null,
        "clues": "gas struts,gas struts repair,re-gas struts,motor vehicle gas struts,gas struts re-gas,struts re-gas,Gas Struts"
    },
    {
        "id": 990,
        "categoryId": 32,
        "subCategoryName": "Gaskets, Seals & Valves",
        "icon": null,
        "clues": "seals--oil,valves,valves industrial,valves-equipment & service,valves equipment,ball valves,seals & bearings,gaskets & seals,seals--oil & mechanical o-rings,seals--security,Gaskets,Seals & Valves,seals,seals--oil & mechanical,rubber gaskets,valves & fittings,automotive gaskets,head gaskets,gaskets,fridge seals,refrigerator seals,window gaskets"
    },
    {
        "id": 993,
        "categoryId": 32,
        "subCategoryName": "Gauges",
        "icon": null,
        "clues": "pressure gauges,vdo gauges,pin gauges,gauges,gauges pressure,automotive gauges,precision gauges,spline gauges,Gauges,car gauges,instruments gauges,gauges precision,temperature gauges,auto gauges,marine gauges"
    },
    {
        "id": 994,
        "categoryId": 32,
        "subCategoryName": "Gazebos & Shade Houses",
        "icon": null,
        "clues": "gazebos decorations,gazebos & shade houses,gazebos kits,gazebos manufacturers,bali gazebos,gazebos & shade houses bali huts,spa gazebos,Gazebos & Shade Houses,portable gazebos,gazebos,thatch gazebos"
    },
    {
        "id": 997,
        "categoryId": 32,
        "subCategoryName": "Gem Merchants",
        "icon": null,
        "clues": "gem dealers,gemstones,gem merchants,gem buyers,gem cutters,gem store,gem wholesalers,Gem Merchants"
    },
    {
        "id": 999,
        "categoryId": 32,
        "subCategoryName": "General & Precision Grinding",
        "icon": null,
        "clues": "General & Precision Grinding"
    },
    {
        "id": 1000,
        "categoryId": 32,
        "subCategoryName": "General Instruments",
        "icon": null,
        "clues": "instruments--general,General Instruments"
    },
    {
        "id": 1001,
        "categoryId": 32,
        "subCategoryName": "General Insurance",
        "icon": null,
        "clues": "General Insurance,general insurance underwriters,insurance fire,marine,accident & general property,general insurance brokers,insurance general,general insurance,insurance--fire,marine,accident & general travel insurance,insurance fire,marine,accident & general"
    },
    {
        "id": 1002,
        "categoryId": 32,
        "subCategoryName": "General Machinery",
        "icon": null,
        "clues": "General Machinery,machinery--general,machinery--general earthmoving"
    },
    {
        "id": 1004,
        "categoryId": 32,
        "subCategoryName": "Generators",
        "icon": null,
        "clues": "generators & generating sets honda,generators rental,generators hire,Generators,generators sales,generators dealers,generators manufacturers,generators set,generators repair,generators maintenance,generators & generating sets,generators servicing,diesel generators,generators,generators parts,generators services,generators plants,generators suppliers"
    },
    {
        "id": 1007,
        "categoryId": 32,
        "subCategoryName": "Geosynthetic Products",
        "icon": null,
        "clues": "Geosynthetic Products,geosynthetic,geosynthetic products"
    },
    {
        "id": 1009,
        "categoryId": 32,
        "subCategoryName": "Geriatrics",
        "icon": null,
        "clues": "geriatrics,Geriatrics"
    },
    {
        "id": 1010,
        "categoryId": 32,
        "subCategoryName": "Gift Baskets & Hampers",
        "icon": null,
        "clues": "giftware,gift shops,baby gift hampers,florist gift baskets,gift boxes,gift bags,gift baskets,gift shops baskets,gift baskets & hampers,gifts,gift hampers delivery,gift & homewares,gift services hospital deliveries,gift hampers baskets,gift services baskets,baby gift baskets,chocolate gift baskets,gift baskets delivery,gift baskets florist,gift hampers,gift baskets supplies,gift services,gift wrapping,Gift Baskets & Hampers,gourmet gift baskets"
    },
    {
        "id": 1011,
        "categoryId": 32,
        "subCategoryName": "Gift Shop",
        "icon": null,
        "clues": "gift shops,Gift Shop,gift shop"
    },
    {
        "id": 1012,
        "categoryId": 32,
        "subCategoryName": "Gift Shop Supplies",
        "icon": null,
        "clues": "gift supplies wholesale,gift shops,gift shop supplies novelties,gift shop supplies candles,wholesale gift supplies,gift shop supplies & importers,gift shop supplies christmas decorations,gift shop supplies bomboniere,gift packaging supplies,gift baskets supplies,gift supplies,gift wrapping supplies,wholesale gift shop supplies,gift shop supplies,Gift Shop Supplies"
    },
    {
        "id": 1013,
        "categoryId": 32,
        "subCategoryName": "Gift Wrappings",
        "icon": null,
        "clues": "Gift Wrappings,gift wrappings"
    },
    {
        "id": 1015,
        "categoryId": 32,
        "subCategoryName": "Glass Blowing",
        "icon": null,
        "clues": "Glass Blowing,glass blowing classes,glass blowing"
    },
    {
        "id": 1020,
        "categoryId": 32,
        "subCategoryName": "Glass Washing Machines",
        "icon": null,
        "clues": "Glass Washing Machines,glass washing machines"
    },
    {
        "id": 1021,
        "categoryId": 32,
        "subCategoryName": "Glazier & Glass Replacement Services",
        "icon": null,
        "clues": "glass merchants & glaziers splashbacks,glazier mirrors,glazier,glass merchants & glaziers,pet doors glaziers,Glazier & Glass Replacement Services,glazier glass merchants"
    },
    {
        "id": 1022,
        "categoryId": 32,
        "subCategoryName": "Global Positioning System (GPS)",
        "icon": null,
        "clues": "global positioning,Global Positioning System (GPS),global positioning systems,global positioning systems navigation systems"
    },
    {
        "id": 1023,
        "categoryId": 32,
        "subCategoryName": "Gloves Wholesalers & Manufacturers",
        "icon": null,
        "clues": "boxing gloves,gloves,Gloves Wholesalers & Manufacturers,gloves--w'salers & mfrs,leather gloves manufacturers,gloves - wholesalers,leather gloves"
    },
    {
        "id": 1026,
        "categoryId": 32,
        "subCategoryName": "Go-Karts & Karting Supplies",
        "icon": null,
        "clues": "Go-Karts & Karting Supplies,go-karts,parts & supplies,go-karts parts"
    },
    {
        "id": 1027,
        "categoryId": 32,
        "subCategoryName": "Gold & Silver Merchants",
        "icon": null,
        "clues": "gold merchants,Gold & Silver Merchants,gold & silver merchants"
    },
    {
        "id": 1028,
        "categoryId": 32,
        "subCategoryName": "Gold Buyers & Refiners",
        "icon": null,
        "clues": "gold buyers,Gold Buyers & Refiners,gold buyers &/or refiners"
    },
    {
        "id": 1029,
        "categoryId": 32,
        "subCategoryName": "Gold Leaf",
        "icon": null,
        "clues": "Gold Leaf,gold leaf suppliers,gold leaf"
    },
    {
        "id": 1031,
        "categoryId": 32,
        "subCategoryName": "Goldsmiths & Silversmiths",
        "icon": null,
        "clues": "Goldsmiths & Silversmiths,goldsmiths,goldsmiths & silversmiths"
    },
    {
        "id": 1034,
        "categoryId": 32,
        "subCategoryName": "Golf Course Construction & Equipment",
        "icon": null,
        "clues": "golf courses public or private,golf equipment,golf equipment wholesalers,golf courses public,golf course,golf equipment & supplies golf carts,Golf Course Construction & Equipment,golf course construction & equipment,golf courses construction,golf equipment & supplies greg norman,golf courses equipment,golf equipment & supplies,golf courses private,golf equipment repair,golf courses,golf equipment retail"
    },
    {
        "id": 1035,
        "categoryId": 32,
        "subCategoryName": "Golf Course Designers",
        "icon": null,
        "clues": "golf courses public or private,golf courses private,golf courses public,golf course,golf course designers,golf courses,Golf Course Designers"
    },
    {
        "id": 1037,
        "categoryId": 32,
        "subCategoryName": "Golf Equipment & Gear",
        "icon": null,
        "clues": "golf gear,golf club equipment,golf equipment,golf equipment wholesalers,golf equipment & supplies golf carts,golf equipment & supplies greg norman,golf courses equipment,golf equipment & supplies,golf equipment repair,Golf Equipment & Gear,golf equipment retail"
    },
    {
        "id": 1038,
        "categoryId": 32,
        "subCategoryName": "Graffiti Removal",
        "icon": null,
        "clues": "graffiti removals,graffiti paint removal,graffiti removal abrasive blasting,graffiti removal brick cleaning,paint removal services & supplies graffiti removal,graffiti removal cleaning contractors,paint removal graffiti removal,cleaning contractors--steam,pressure,chemical etc. graffiti removal,graffiti removal building maintenance & repairs,graffiti cleaning,graffiti removal chemical cleaning,graffiti removal home cleaning,steam cleaning graffiti removal,Graffiti Removal"
    },
    {
        "id": 1043,
        "categoryId": 32,
        "subCategoryName": "Gratings & Access Covers",
        "icon": null,
        "clues": "Gratings & Access Covers,access covers & gratings"
    },
    {
        "id": 1044,
        "categoryId": 32,
        "subCategoryName": "Graziers",
        "icon": null,
        "clues": "graziers cattle,graziers outstation,farmers & graziers,Graziers,graziers"
    },
    {
        "id": 1045,
        "categoryId": 32,
        "subCategoryName": "Grease Trap Cleaning Services",
        "icon": null,
        "clues": "grease,Grease Trap Cleaning Services,grease trap cleaning,grease filter cleaning,grease trap & septic tank cleaning,grease trap cleaning services,grease trap"
    },
    {
        "id": 1046,
        "categoryId": 32,
        "subCategoryName": "Greenhouse Supplies & Equipment",
        "icon": null,
        "clues": "Greenhouse Supplies & Equipment,greenhouse supplies"
    },
    {
        "id": 1047,
        "categoryId": 32,
        "subCategoryName": "Greeting Cards Wholesalers & Manufacturers",
        "icon": null,
        "clues": "greeting cards,greeting,greeting card manufacturers,Greeting Cards Wholesalers & Manufacturers,wholesale greeting cards,greeting card wholesalers,greeting card publishers,greeting card distributors,cards - greeting - wholesalers & manufacturers,greeting card suppliers,greeting card printing factory,greeting card printers"
    },
    {
        "id": 1051,
        "categoryId": 32,
        "subCategoryName": "Grocery Wholesalers",
        "icon": null,
        "clues": "asian grocery wholesalers,wholesalers grocery,asian grocery,grocery,indian grocery,Grocery Wholesalers,indian grocery wholesalers,grocery store"
    },
    {
        "id": 1054,
        "categoryId": 32,
        "subCategoryName": "Guns & Ammunition",
        "icon": null,
        "clues": "Guns & Ammunition,guns rifles,guns & ammunition rifles,guns & ammunition hunting,guns & ammunition gun safes,guns & ammunition"
    },
    {
        "id": 1059,
        "categoryId": 32,
        "subCategoryName": "Haberdashery",
        "icon": null,
        "clues": "fabric haberdashery wholesale,haberdashery wholesalers,haberdashery supplies,craft & haberdashery,haberdashery,haberdashery shop,wholesale haberdashery,haberdashery wholesale,fabric & haberdashery,Haberdashery"
    },
    {
        "id": 1066,
        "categoryId": 32,
        "subCategoryName": "Hairdressing Supplies",
        "icon": null,
        "clues": "Hairdressing Supplies,hairdressing colleges"
    },
    {
        "id": 1069,
        "categoryId": 32,
        "subCategoryName": "Hand Dryers--Electric",
        "icon": null,
        "clues": "Hand Dryers--Electric,hand dryers--electric,electric hand dryers"
    },
    {
        "id": 1072,
        "categoryId": 32,
        "subCategoryName": "Handbags Wholesalers & Manufacturers",
        "icon": null,
        "clues": "handbags-w'salers & mfrs,handbags factory,handbags & luggage,handbags - wholesalers & manufacturers,handbags distributors,handbags factory outlets,handbags suppliers,Handbags Wholesalers & Manufacturers,handbags outlets,handbags manufacturers,handbags importers,handbags warehouse"
    },
    {
        "id": 1073,
        "categoryId": 32,
        "subCategoryName": "Handbags-Retail & Repairs",
        "icon": null,
        "clues": "handbags,handbags & luggage,handbags repair,handbags factory outlets,handbags retail,Handbags-Retail & Repairs,handbags importers,handbags-retail & repairs"
    },
    {
        "id": 1075,
        "categoryId": 32,
        "subCategoryName": "Handkerchieves Wholesalers & Manufacturers",
        "icon": null,
        "clues": "Handkerchieves Wholesalers & Manufacturers,handkerchieves--w'salers & mfrs"
    },
    {
        "id": 1076,
        "categoryId": 32,
        "subCategoryName": "Handle W'salers & Mfrs",
        "icon": null,
        "clues": "handles,door handles,handles & knobs,Handle W'salers & Mfrs,handle w'salers & mfrs"
    },
    {
        "id": 1080,
        "categoryId": 32,
        "subCategoryName": "Hardware Store",
        "icon": null,
        "clues": "building hardware store,trade hardware store,hardware store,hardware retail store,Hardware Store,iron hardware store"
    },
    {
        "id": 1081,
        "categoryId": 32,
        "subCategoryName": "Hardware--W'sale",
        "icon": null,
        "clues": "hardware wholesalers,computer hardware wholesalers,wholesale hardware,Hardware--W'sale,wholesale distributors hardware it products,computer equipment--hardware wholesale"
    },
    {
        "id": 1082,
        "categoryId": 32,
        "subCategoryName": "Hats & Caps - Retail & Repairs",
        "icon": null,
        "clues": "hats & caps � retail & repairs,hats & caps,Hats & Caps - Retail & Repairs"
    },
    {
        "id": 1083,
        "categoryId": 32,
        "subCategoryName": "Hats & Caps Wholesalers & Manufacturers",
        "icon": null,
        "clues": "Hats & Caps Wholesalers & Manufacturers,hats & caps - wholesalers & manufacturers"
    },
    {
        "id": 1084,
        "categoryId": 32,
        "subCategoryName": "Headsets & Headphones",
        "icon": null,
        "clues": "telephone headsets,phone headsets,wireless headsets,Headsets & Headphones,bluetooth headsets,claria headsets,radio headsets,headsets"
    },
    {
        "id": 1089,
        "categoryId": 32,
        "subCategoryName": "Health Insurance",
        "icon": null,
        "clues": "private health insurance,health insurance,health insurance hcf,health insurance funds,insurance health,health insurance providers,health insurance brokers,health insurance ambulance,Health Insurance"
    },
    {
        "id": 1094,
        "categoryId": 32,
        "subCategoryName": "Hearing Conservation Consultants & Services",
        "icon": null,
        "clues": "hearing consultants,hearing aids,equipment & services,hearing aids,equipment & services audiological assessments,hearing aid services,hearing conservation consultants,hearing conservation consultants & services,Hearing Aids,Equipment & Services,Hearing Conservation Consultants & Services"
    },
    {
        "id": 1095,
        "categoryId": 32,
        "subCategoryName": "Heat Exchange Equipment",
        "icon": null,
        "clues": "heat exchange,heat exchange equipment,heat exchangers,heat exchange manufacturers,Heat Exchange Equipment"
    },
    {
        "id": 1096,
        "categoryId": 32,
        "subCategoryName": "Heat Tracing Systems",
        "icon": null,
        "clues": "Heat Tracing Systems,heat tracing systems"
    },
    {
        "id": 1097,
        "categoryId": 32,
        "subCategoryName": "Heat Treatment",
        "icon": null,
        "clues": "metal heat treatment,Heat Treatment,heat treatment,heat treatment--metal,metal heat treatment systems"
    },
    {
        "id": 1098,
        "categoryId": 32,
        "subCategoryName": "Heating Appliances & Systems",
        "icon": null,
        "clues": "heating appliances & systems floor,heating appliances gas,heating appliances & systems-repairs & service,heating appliances & systems gas,heating appliances & systems ducted,heating appliances services,heating appliances rental,heating systems,Heating Appliances & Systems,heating appliances & systems hydronic,heating appliances & systems central,heating appliances & systems ducts,heating appliances"
    },
    {
        "id": 1099,
        "categoryId": 32,
        "subCategoryName": "Heating Appliances Repair & Service",
        "icon": null,
        "clues": "heating appliances & systems-repairs & service brivis,gas central heating repair,heating appliances & systems-repairs & service,gas ducted heating repair,Heating Appliances Repair & Service,solar pool heating repair,ducted heating repair,ducted gas heating repair,hydronic heating repair,heating & cooling repair,central heating repair,heating appliances & systems-repairs & service vulcan,heating repair,ducting heating repair,heating appliances repair,brivis heating repair,central heating services repair"
    },
    {
        "id": 1102,
        "categoryId": 32,
        "subCategoryName": "Hemp Fibre & Oil Products",
        "icon": null,
        "clues": "hemp,hemp fibre & oil products,Hemp Fibre & Oil Products"
    },
    {
        "id": 1104,
        "categoryId": 32,
        "subCategoryName": "Heraldry",
        "icon": null,
        "clues": "Heraldry,heraldry consultants & association,heraldry"
    },
    {
        "id": 1105,
        "categoryId": 32,
        "subCategoryName": "Herbalists",
        "icon": null,
        "clues": "herbalists chinese methods,herbalists chinese,herbalists herbal remedies,herbalists,Herbalists"
    },
    {
        "id": 1106,
        "categoryId": 32,
        "subCategoryName": "Herbs",
        "icon": null,
        "clues": "Herbs,chinese herbs,herbs & spices,herbs spices store,herbs"
    },
    {
        "id": 1107,
        "categoryId": 32,
        "subCategoryName": "Herbs (Nurseries-Retail)",
        "icon": null,
        "clues": "chinese herbs,herbs (nurseries-retail),herbs & spices,Herbs (Nurseries-Retail),herbs spices store"
    },
    {
        "id": 1109,
        "categoryId": 32,
        "subCategoryName": "Hessian",
        "icon": null,
        "clues": "hessian,hessian suppliers,Hessian,hessian bags,hessian sacks,hessian chaff bags,bags & sacks hessian"
    },
    {
        "id": 1110,
        "categoryId": 32,
        "subCategoryName": "Hide & Skin Merchants",
        "icon": null,
        "clues": "hide,Hide & Skin Merchants,leather hide,hides,hide & skin merchants"
    },
    {
        "id": 1111,
        "categoryId": 32,
        "subCategoryName": "Hi-Fi Equipment",
        "icon": null,
        "clues": ""
    },
    {
        "id": 1112,
        "categoryId": 32,
        "subCategoryName": "Hi-Fi Equipment & Service",
        "icon": null,
        "clues": "hi-fi equipment repairs,hi-fi equipment installation,amplifier hi-fi equipment,hi-fi equipment accessories,hi-fi equipment receivers,hi-fi equipment speakers,hi-fi equipment rental,hi-fi equipment dj equipment,hi-fi equipment headphones,hi-fi equipment home theatre,hi-fi equipment hire,hi-fi equipment mixers,hi-fi equipment microphones,hi-fi equipment--repair,Hi-Fi Equipment & Service,hi-fi equipment--service,hi-fi equipment advice,hi-fi equipment--service speakers"
    },
    {
        "id": 1115,
        "categoryId": 32,
        "subCategoryName": "Hire--Nursery Equipment",
        "icon": null,
        "clues": "Hire--Nursery Equipment,plant nursery hire,hire--nursery equipment,hire--medical & nursery equipment,nursery hire,nursery equipment hire"
    },
    {
        "id": 1118,
        "categoryId": 32,
        "subCategoryName": "Hobbies & Hobby Shops",
        "icon": null,
        "clues": "Hobbies & Hobby Shops"
    },
    {
        "id": 1119,
        "categoryId": 32,
        "subCategoryName": "Hobbies & Model Construction Supplies Wholesalers & Manufacturers",
        "icon": null,
        "clues": "Hobbies & Model Construction Supplies Wholesalers & Manufacturers,hobbies & model construction supplies - wholesalers & manufacturers,hobbies & model construction supplies--retail rc,hobbies & model construction supplies--retail,hobbies & model construction supplies--retail radio controlled"
    },
    {
        "id": 1121,
        "categoryId": 32,
        "subCategoryName": "Hoisting & Rigging Equipment",
        "icon": null,
        "clues": "hoisting & rigging equipment,hoisting & rigging,hoisting equipment,Hoisting & Rigging Equipment,crane & hoisting"
    },
    {
        "id": 1123,
        "categoryId": 32,
        "subCategoryName": "Home Air Conditioning",
        "icon": null,
        "clues": "home air conditioning,home air conditioning repair,air conditioning-home,air conditioning repair home,air conditioning-home evaporative systems,air conditioning-home maintenance,air conditioning-home ducted refrigeration systems,fisher & paykel home air conditioning,air conditioning-home bosch,air conditioning-home split systems,air conditioning home repair,Home Air Conditioning,air conditioning-home ducted systems,air conditioning-home ducted evaporated systems,home air conditioning installation,air conditioning-home daikin,home air conditioner,portable home air conditioning units,air conditioning-home installation,home air conditioning services,air conditioning home services"
    },
    {
        "id": 1124,
        "categoryId": 32,
        "subCategoryName": "Home Automation",
        "icon": null,
        "clues": "home automation,Home Automation"
    },
    {
        "id": 1125,
        "categoryId": 32,
        "subCategoryName": "Home Brewing",
        "icon": null,
        "clues": "barley home brewing supplies,home brewing shop,home brewing suppliers,home brewing equipment,home brewing centre,Home Brewing,home brewing supplies,home brewing kits,home brewing store,home brewing,home brewing beer"
    },
    {
        "id": 1126,
        "categoryId": 32,
        "subCategoryName": "Home Cinema & Theatre",
        "icon": null,
        "clues": "home theatre specialist,home theatre furniture,home theatre cabinets,home theatre installers,home theatre,home cinema & theatre,home theatre setup,home cinema & theatre audiovisual equipment,Home Cinema & Theatre,home theatre projection,home theatre supplies,home theatre seating,home theatre installation,home cinema installation,home cinema & theatre projection systems,home theatre wiring,home theatre sales,home theatre equipment,home theatre systems,home cinema equipment,home theatre speaker,home theatre services,home theatre hi-fi equipment,home theatre store,home cinema,home theatre repair"
    },
    {
        "id": 1128,
        "categoryId": 32,
        "subCategoryName": "Home Health Care Equipment",
        "icon": null,
        "clues": "home health care aids & equipment scooters,home health care aids & equipment rehabilitation equipment,Home Health Care Equipment,home health care equipment,fisher & paykel home health care aids & equipment,home health care aids & equipment,home health care aids & equipment wheelchairs,home health care aids & equipment mobility aids,home health equipment,home health care aids & equipment equipment hire"
    },
    {
        "id": 1135,
        "categoryId": 32,
        "subCategoryName": "Homewares",
        "icon": null,
        "clues": "homewares wholesalers,homewares importers,homewares supplies,Homewares,homewares--retail,homewares store,homewares,homewares wholesale,homewares franchise,homewares & giftware retail,homewares & gifts"
    },
    {
        "id": 1144,
        "categoryId": 32,
        "subCategoryName": "Horticultural Consultants",
        "icon": null,
        "clues": "Horticultural Consultants,horticultural consultants"
    },
    {
        "id": 1145,
        "categoryId": 32,
        "subCategoryName": "Hoses & Hose Fittings",
        "icon": null,
        "clues": "gas hoses,pressure hoses,automotive hoses,radiator hoses,hoses belts automotive,hoses & fittings,hoses supplies,hydraulic hoses,hydraulic hoses & fittings,hoses & fittings--supplies & service enzed,Hoses & Hose Fittings,rexroth hoses & fittings,hoses & fittings--supplies & service,hoses,hoses & fittings--supplies & service hydraulic,hydraulic equipment & supplies hoses,hoses & fittings--supplies & service rexroth,industrial hoses & fittings"
    },
    {
        "id": 1151,
        "categoryId": 32,
        "subCategoryName": "Hot Water Systems",
        "icon": null,
        "clues": "hot water systems,hot water systems bosch,hot water systems services,hot water systems retail,hot water systems gas,hot water systems solar,hot water systems repairs,solar hot water systems,gas hot water systems,Hot Water Systems,hot water systems electric,electric hot water systems,hot water systems solar hot water,rheem hot water systems,dux hot water systems,repair hot water systems,hot water systems parts,hot water systems repair,hot water systems suppliers,hot water systems installation"
    },
    {
        "id": 1160,
        "categoryId": 32,
        "subCategoryName": "Housing Societies--Co-Operative",
        "icon": null,
        "clues": "Housing Societies--Co-Operative,housing societies--co-operative"
    },
    {
        "id": 1162,
        "categoryId": 32,
        "subCategoryName": "Humidifying Equipment",
        "icon": null,
        "clues": "Humidifying Equipment,humidifying equipment"
    },
    {
        "id": 1163,
        "categoryId": 32,
        "subCategoryName": "Hydraulic Equipment & Supplies",
        "icon": null,
        "clues": "hydraulic supplies,hydraulic equipment & supplies,hydraulic equipment & supplies hydraulics,hydraulic equipment & supplies rexroth,hydraulic equipment,Hydraulic Equipment & Supplies,hydraulic equipment & supplies hoses,rexroth hydraulic equipment"
    },
    {
        "id": 1174,
        "categoryId": 32,
        "subCategoryName": "Ice Suppliers",
        "icon": null,
        "clues": "dry ice suppliers,Ice Suppliers"
    },
    {
        "id": 1175,
        "categoryId": 32,
        "subCategoryName": "Identification Equipment & Services",
        "icon": null,
        "clues": "Identification Equipment & Services,identification equipment,identification tags,identification,identification equipment & services"
    },
    {
        "id": 1176,
        "categoryId": 32,
        "subCategoryName": "Identification Systems",
        "icon": null,
        "clues": "identification systems,Identification Systems"
    },
    {
        "id": 1181,
        "categoryId": 32,
        "subCategoryName": "Importers & Imported Products",
        "icon": null,
        "clues": "importers herbal,tile importers,importers of asian products,importers garden,importers cane,food importers,importers gifts,food importers & imported products,importers garden products,importers india,importers & exporters,importers &/or imported products asian foods,importers toys,importers &/or imported products,automotive products importers,importers rugs,importers novelty,furniture importers,importers giftware,importers textiles,importers,Importers & Imported Products,importers &/or imported products food,importers & imported products,importers wholesalers"
    },
    {
        "id": 1182,
        "categoryId": 32,
        "subCategoryName": "Incinerators",
        "icon": null,
        "clues": "incinerators,Incinerators"
    },
    {
        "id": 1184,
        "categoryId": 32,
        "subCategoryName": "Indoor Plant Hire",
        "icon": null,
        "clues": "indoor plants,gardens & supplies,indoor plant hire,indoor plants gardens,indoor plant rental,Indoor Plants,Gardens & Supplies,indoor plants,Indoor Plant Hire,indoor plants wholesale"
    },
    {
        "id": 1186,
        "categoryId": 32,
        "subCategoryName": "Induction Heating Equipment",
        "icon": null,
        "clues": "induction heating equipment,Induction Heating Equipment"
    },
    {
        "id": 1187,
        "categoryId": 32,
        "subCategoryName": "Industrial & Medical Gas",
        "icon": null,
        "clues": "gas industrial,gas--industrial or medical,industrial gas,Industrial & Medical Gas,industrial medical centre,industrial gas suppliers"
    },
    {
        "id": 1190,
        "categoryId": 32,
        "subCategoryName": "Industrial Drying Equipment & Services",
        "icon": null,
        "clues": "drying equipment & services--industrial,industrial equipment supplies,hire--builders',contractors' & handyman's equipment industrial,Industrial Drying Equipment & Services,industrial equipment,industrial equipment hire"
    },
    {
        "id": 1191,
        "categoryId": 32,
        "subCategoryName": "Industrial Fasteners",
        "icon": null,
        "clues": "industrial fasteners,Industrial Fasteners,fasteners--industrial"
    },
    {
        "id": 1192,
        "categoryId": 32,
        "subCategoryName": "Industrial Flooring",
        "icon": null,
        "clues": "industrial flooring epoxy,flooring industrial,industrial flooring,Industrial Flooring"
    },
    {
        "id": 1193,
        "categoryId": 32,
        "subCategoryName": "Industrial Relations Consultants",
        "icon": null,
        "clues": "industrial relations consultants human resources,industrial relations consultants,Industrial Relations Consultants,industrial relocation consultants & services"
    },
    {
        "id": 1194,
        "categoryId": 32,
        "subCategoryName": "Industrial Sewing Machines",
        "icon": null,
        "clues": "industrial sewing machines,industrial sewing,industrial sewing machine services,Industrial Sewing Machines,sewing machines--industrial,industrial sewing machine repair"
    },
    {
        "id": 1196,
        "categoryId": 32,
        "subCategoryName": "Industrial Wheels",
        "icon": null,
        "clues": "industrial wheels,wheels--industrial,Industrial Wheels,wheels--industrial & general"
    },
    {
        "id": 1200,
        "categoryId": 32,
        "subCategoryName": "Ink & Toner Cartridges",
        "icon": null,
        "clues": "ink cartridge,Ink & Toner Cartridges,toner ink,cartridges--toner & ink,toner & ink dealers,ink manufacturers,ink,ink refills,toner & ink cartridge,ink stamps,ink toner cartridge,printer ink,ink toner,ink supplies,toner and ink cartridges,ink cartridge refills"
    },
    {
        "id": 1205,
        "categoryId": 32,
        "subCategoryName": "Inspection & Testing Services",
        "icon": null,
        "clues": "inspection & testing services,building inspections,safety audits inspection services,Inspection & Testing Services,building inspection services building certification,motor vehicle inspection & testing,building inspection services dilapidation,termite inspections,pest inspections,building & pest inspections,inspection & testing services calibrations"
    },
    {
        "id": 1207,
        "categoryId": 32,
        "subCategoryName": "Instrument Cases",
        "icon": null,
        "clues": "instrument cases,Instrument Cases"
    },
    {
        "id": 1210,
        "categoryId": 32,
        "subCategoryName": "Insurance Agents",
        "icon": null,
        "clues": "insurance agents,Insurance Agents,life insurance agents,agents insurance,insurance agents business insurance,travel agents & consultants travel insurance"
    },
    {
        "id": 1211,
        "categoryId": 32,
        "subCategoryName": "Insurance Assessors & Loss Adjusters",
        "icon": null,
        "clues": "insurance assessors & loss adjusters,insurance assessors,insurance loss assessors,water damage insurance assessors,insurance assessors marine,Insurance Assessors & Loss Adjusters,insurance adjusters,insurance loss adjusters,flood damage insurance assessors"
    },
    {
        "id": 1212,
        "categoryId": 32,
        "subCategoryName": "Insurance Brokers",
        "icon": null,
        "clues": "Insurance Brokers,insurance brokers cgu,insurance brokers allianz,insurance brokers vehicle,insurance brokers commercial,insurance brokers marine,insurance brokers suncorp,insurance brokers flood,insurance brokers,insurance brokers public liability,insurance brokers amp,insurance brokers qbe"
    },
    {
        "id": 1213,
        "categoryId": 32,
        "subCategoryName": "Insurance--Credit",
        "icon": null,
        "clues": "credit insurance,Insurance--Credit,insurance--credit"
    },
    {
        "id": 1214,
        "categoryId": 32,
        "subCategoryName": "Insurance--Premium Finance",
        "icon": null,
        "clues": "Insurance--Premium Finance,insurance--premium finance"
    },
    {
        "id": 1216,
        "categoryId": 32,
        "subCategoryName": "Intercom Systems",
        "icon": null,
        "clues": "Intercom Systems,intercom installation,intercom systems repair,intercom installers,intercom systems,intercom repair,video intercom systems,intercom,intercom security,home intercom systems"
    },
    {
        "id": 1217,
        "categoryId": 32,
        "subCategoryName": "Interior Decorators",
        "icon": null,
        "clues": "interior decorators architects residential,interior decorators curtains,interior decorators rugs,interior decorators colour consulting,interior decorators designers,interior decorators design,Interior Decorators,interior decorators bathroom kitchens,interior designers & decorators,interior decorators blinds,interior decorators,interior decorators soft furnishings,interior decorators window treatments"
    },
    {
        "id": 1218,
        "categoryId": 32,
        "subCategoryName": "Interior Designers",
        "icon": null,
        "clues": "designers interior,interior designers & architects,interior designers colour consulting,interior designers & art consultants,interior designers for furniture,interior designers,interior designers & decorators,Interior Designers,interior designers blinds,interior designers commercial,interior designers residential,interior designers hotels,interior designers domestic"
    },
    {
        "id": 1222,
        "categoryId": 32,
        "subCategoryName": "Internet Cafes",
        "icon": null,
        "clues": "Internet Cafes,internet cafes"
    },
    {
        "id": 1223,
        "categoryId": 32,
        "subCategoryName": "Internet Marketing Services",
        "icon": null,
        "clues": "internet marketing services website development,internet marketing services search engines,internet marketing services website optimisation,internet marketing services animation,internet marketing services,internet marketing services shopping carts,internet marketing services internet applications,internet marketing services domain name registration,internet marketing,internet marketing services e-commerce services,Internet Marketing Services,internet marketing services self-managed websites,internet marketing services payment gateway solutions,internet marketing services website design,internet marketing services online marketing,internet marketing services website hosting,internet marketing services website solutions,internet marketing services e-mail,internet marketing services internet solutions,internet marketing services mobile devices,internet marketing services mobile service"
    },
    {
        "id": 1224,
        "categoryId": 32,
        "subCategoryName": "Internet Service Providers",
        "icon": null,
        "clues": "Internet Service Providers,internet service providers telstra,internet service providers broadband,internet service providers bigpond,internet service providers,internet services providers residential"
    },
    {
        "id": 1226,
        "categoryId": 32,
        "subCategoryName": "Investing & Investment Services",
        "icon": null,
        "clues": "Investing & Investment Services"
    },
    {
        "id": 1227,
        "categoryId": 32,
        "subCategoryName": "Investment Bankers",
        "icon": null,
        "clues": "investment bankers,Investment Bankers,investment bank"
    },
    {
        "id": 1231,
        "categoryId": 32,
        "subCategoryName": "Jacks",
        "icon": null,
        "clues": "Jacks,pallet jacks,jacks,jacks forklifts,hydraulic jacks,trolleys jacks,floor jacks,jacks automotive"
    },
    {
        "id": 1232,
        "categoryId": 32,
        "subCategoryName": "Jeans--Retail",
        "icon": null,
        "clues": "Jeans--Retail,jeans--retail"
    },
    {
        "id": 1233,
        "categoryId": 32,
        "subCategoryName": "Jeans--W'salers & Mfrs",
        "icon": null,
        "clues": "jeans wholesalers"
    },
    {
        "id": 1235,
        "categoryId": 32,
        "subCategoryName": "Jewellers' Supplies",
        "icon": null,
        "clues": "Jewellers' Supplies,jewellers supplies,jewellers' supplies & services"
    },
    {
        "id": 1236,
        "categoryId": 32,
        "subCategoryName": "Jewellery Case & Display Pads Wholesalers & Manufacturers",
        "icon": null,
        "clues": "jewellery display equipment,jewellery display stands,silver jewellery wholesalers,jewellery wholesalers,Jewellery Case & Display Pads Wholesalers & Manufacturers,costume jewellery manufacturers,jewellery display supplies,fashion jewellery wholesalers,jewellery display cases,jewellery case,jewellery display cabinets,jewellery case & display pads - wholesalers & manufacturers,diamond jewellery wholesalers,costume jewellery wholesalers,jewellery manufacturers"
    },
    {
        "id": 1237,
        "categoryId": 32,
        "subCategoryName": "Jewellery Designers",
        "icon": null,
        "clues": "jewellery designers,Jewellery Designers,jewellery designers repairs"
    },
    {
        "id": 1238,
        "categoryId": 32,
        "subCategoryName": "Jewellery Stores",
        "icon": null,
        "clues": "Jewellery Stores"
    },
    {
        "id": 1239,
        "categoryId": 32,
        "subCategoryName": "Jewellery Valuation",
        "icon": null,
        "clues": "jewellery valuers,jewellery valuations,Jewellery Valuation"
    },
    {
        "id": 1244,
        "categoryId": 32,
        "subCategoryName": "Jukeboxes",
        "icon": null,
        "clues": "hire--party equipment jukeboxes,jukeboxes,Jukeboxes"
    },
    {
        "id": 1247,
        "categoryId": 32,
        "subCategoryName": "Key Registration",
        "icon": null,
        "clues": "Key Registration,key registration"
    },
    {
        "id": 1250,
        "categoryId": 32,
        "subCategoryName": "Kindergarten & Nursery Equipment & Supplies",
        "icon": null,
        "clues": "kindergarten equipment,kindergarten supplies,kindergarten furniture,Kindergarten & Nursery Equipment & Supplies,kindergarten & nursery equipment & supplies"
    },
    {
        "id": 1253,
        "categoryId": 32,
        "subCategoryName": "Kit Homes",
        "icon": null,
        "clues": "kit homes houses,steel kit homes systems,kit homes sheds,transportable kit homes,kit homes builders,steel kit homes,steel frame kit homes,builders kit homes,Kit Homes,timber kit homes,kit homes"
    },
    {
        "id": 1254,
        "categoryId": 32,
        "subCategoryName": "Kitchen Renovations & Designs",
        "icon": null,
        "clues": "kitchen & bathroom renovations,budget kitchen renovations,kitchen makers,Kitchen Renovations & Designs,kitchen renovations & design,kitchen renovations,kitchen benchtops,kitchen manufacturers & renovations,kitchen renovations & equipment,kitchen design,kitchen renovations supplies,kitchens-renovations & equipment benchtops,kitchen supplies,pre-built kitchen renovations,kitchens renovations,kitchen renovations & makeover,kitchens-renovations & equipment,kitchen cabinets"
    },
    {
        "id": 1255,
        "categoryId": 32,
        "subCategoryName": "Kitchenware",
        "icon": null,
        "clues": ""
    },
    {
        "id": 1256,
        "categoryId": 32,
        "subCategoryName": "Kitchenware Wholesalers & Manufacturers",
        "icon": null,
        "clues": "Kitchenware Wholesalers & Manufacturers,kitchenware--retail knives,kitchenware - wholesalers,kitchenware--w'salers & mfrs,kitchenware supplies,kitchenware wholesale,kitchenware spare parts,kitchenware importers,kitchenware--retail"
    },
    {
        "id": 1257,
        "categoryId": 32,
        "subCategoryName": "Kites",
        "icon": null,
        "clues": "kites,kites kite surfing,Kites,custom kites"
    },
    {
        "id": 1258,
        "categoryId": 32,
        "subCategoryName": "Knitting Wool & Accessories",
        "icon": null,
        "clues": "knitting classes,knitting wool patterns,knitting needles,knitting wool & accessories - retail,knitting wool & accessories - wholesalers & manufacturers,Knitting Wool & Accessories,knitting books,knitting,knitting machines,knitting patterns,knitting shop,knitting wool,knitting wool & accessories,knitting supplies,wool knitting,knitting mills,knitting yarn wool,knitting wool shop,knitting yarn"
    },
    {
        "id": 1259,
        "categoryId": 32,
        "subCategoryName": "Knitwear Wholesalers & Manufacturers",
        "icon": null,
        "clues": "Knitwear Wholesalers & Manufacturers,knitwear--w'salers & mfrs,knitwear - wholesalers,knitwear manufacturers,knitwear,knitwear wholesale"
    },
    {
        "id": 1260,
        "categoryId": 32,
        "subCategoryName": "Knitwear--Retail",
        "icon": null,
        "clues": "Knitwear--Retail,knitwear--retail"
    },
    {
        "id": 1262,
        "categoryId": 32,
        "subCategoryName": "Labelling Equipment",
        "icon": null,
        "clues": "labelling equipment,bottle labelling,Labelling Equipment,labelling machines"
    },
    {
        "id": 1263,
        "categoryId": 32,
        "subCategoryName": "Labels--Metal Foil, Paper & Plastic",
        "icon": null,
        "clues": "Labels--Metal Foil,Paper & Plastic,metal labels,labels--metal foil,paper & plastic,labels--metal foil paper"
    },
    {
        "id": 1264,
        "categoryId": 32,
        "subCategoryName": "Laboratory Equipment & Supplies",
        "icon": null,
        "clues": "photo laboratory,laboratory equipment & services,Laboratory Equipment & Supplies,laboratory equipment,general laboratory equipment,laboratory supplies,laboratory coats,laboratory equipment repair,laboratory chemical supplies,laboratory equipment & supplies,second hand laboratory equipment,dental laboratory supplies,scientific laboratory supplies"
    },
    {
        "id": 1265,
        "categoryId": 32,
        "subCategoryName": "Laboratory Furniture",
        "icon": null,
        "clues": "laboratory furniture,Laboratory Furniture"
    },
    {
        "id": 1267,
        "categoryId": 32,
        "subCategoryName": "Lace Merchants",
        "icon": null,
        "clues": "Lace Merchants,lace material,lace wholesalers,lace,lace wholesale,lace fabric,lace wigs,lace suppliers,lace merchants,lace importers,lace curtains"
    },
    {
        "id": 1269,
        "categoryId": 32,
        "subCategoryName": "Ladders, Step Ladders & Trestles",
        "icon": null,
        "clues": "trestles,ladders,step ladders,ladders steps trestles,step treads,ladders,steps,trestles & accessories,ladders steps,trestles sales,ladders racks,trestles & planks,Ladders,Step Ladders & Trestles,ladders,steps,trestles & accessories attics,attic ladders,step ladders & extension ladders"
    },
    {
        "id": 1270,
        "categoryId": 32,
        "subCategoryName": "Ladies' Wear-Retail",
        "icon": null,
        "clues": "ladies' wear-retail special occasion wear,ladies wear,larger ladies' wear--retail,ladies' wear-retail,large ladies' wear--retail,Ladies' Wear-Retail,ladies hairdresser,ladies clothing retail,ladies fashion,ladies' wear--retail larger sizes,ladies shoes,ladies fashion boutiques,ladies fashion retail,ladies' wear-retail large sizes,ladies' wear--retail fuller figure sizes,ladies retail,ladies boutiques,ladies evening wear,ladies clothing,ladies footwear"
    },
    {
        "id": 1271,
        "categoryId": 32,
        "subCategoryName": "Ladies' Wear--Specialised Fittings",
        "icon": null,
        "clues": "ladies' wear--specialised fittings,ladies wear specialised fittings,Ladies' Wear--Specialised Fittings"
    },
    {
        "id": 1272,
        "categoryId": 32,
        "subCategoryName": "Laminates & Laminated Panels",
        "icon": null,
        "clues": "laminates & laminated panels,laminates & laminated panels flooring,laminates & laminated panels laminex,Laminates & Laminated Panels,laminates & laminated panels benchtops"
    },
    {
        "id": 1273,
        "categoryId": 32,
        "subCategoryName": "Laminating Services",
        "icon": null,
        "clues": "Laminating Services,photocopying services laminating,laminating services"
    },
    {
        "id": 1274,
        "categoryId": 32,
        "subCategoryName": "Laminating Supplies & Equipment",
        "icon": null,
        "clues": "laminating & mounting,laminating posters,laminating supplies & equipment,Laminating Supplies & Equipment,laminating machines,laminating film,laminating stationery,laminating pouches,laminating equipment,laminating supplies"
    },
    {
        "id": 1275,
        "categoryId": 32,
        "subCategoryName": "Lamps--Fuel Burning",
        "icon": null,
        "clues": "lamps--fuel burning,Lamps--Fuel Burning"
    },
    {
        "id": 1276,
        "categoryId": 32,
        "subCategoryName": "Lampshades",
        "icon": null,
        "clues": "Lampshades,lampshades"
    },
    {
        "id": 1278,
        "categoryId": 32,
        "subCategoryName": "Land Salesmen ( S.A. only )",
        "icon": null,
        "clues": "Land Salesmen ( S.A. only ),land salesmen ( s.a. only )"
    },
    {
        "id": 1281,
        "categoryId": 32,
        "subCategoryName": "Landscape Supplies",
        "icon": null,
        "clues": "landscape supplies blocks,landscape supplies rocks,landscape supplies pebbles,landscape supplies boulders,Landscape Supplies,landscape supplies mulch,landscape supplies compost,landscape supplies concrete sleepers,landscape supplies sand,landscape supplies,landscape supplies pavers,landscape supplies sleepers,landscape supplies garden mix,landscape supplies retaining walls,landscape supplies limestone,landscape supplies railway sleepers,landscape supplies nurseries,landscape supplies manure,landscape supplies concrete,landscape supply"
    },
    {
        "id": 1284,
        "categoryId": 32,
        "subCategoryName": "Lapidaries & Gem Cutters",
        "icon": null,
        "clues": "Lapidaries & Gem Cutters,lapidaries & gem cutters,lapidaries'"
    },
    {
        "id": 1285,
        "categoryId": 32,
        "subCategoryName": "Lapidaries' & Gem Cutters' Supplies",
        "icon": null,
        "clues": "lapidaries' & gem cutters' supplies,Lapidaries' & Gem Cutters' Supplies,lapidaries'"
    },
    {
        "id": 1286,
        "categoryId": 32,
        "subCategoryName": "Laser Cutting Services",
        "icon": null,
        "clues": "laser cutting machine,Laser Cutting Services,laser cutting plastics,laser cutting,laser metal cutting,laser cutting 3d,laser cutting 2d,laser cutting metal,metal cutting services laser,metal laser cutting,cutting services metal laser,laser cutting services,laser cutting equipment"
    },
    {
        "id": 1287,
        "categoryId": 32,
        "subCategoryName": "Lasers & Laser Equipment",
        "icon": null,
        "clues": "Lasers & Laser Equipment"
    },
    {
        "id": 1288,
        "categoryId": 32,
        "subCategoryName": "Lattice",
        "icon": null,
        "clues": "lattice,lattice fencing,lattice door,lattice aluminium,lattice suppliers,lattice makers,lattice panels,Lattice,lattice screens"
    },
    {
        "id": 1291,
        "categoryId": 32,
        "subCategoryName": "Lawn Mower Shops & Repairs",
        "icon": null,
        "clues": "lawn mower spares,Lawn Mower Shops & Repairs,lawn mower suppliers,lawn mower services,lawn mower parts,lawn mowers--retail & repairs honda,lawn mowers--retail & repairs,lawn & turf supplies,lawn mowers retail,lawn mowers--retail & repairs spare parts,lawn mower repair,lawn mowers--retail & repairs ride-on,lawn cutting & maintenance garden maintenance,lawn mowers--retail & repairs servicing,lawn mowers--retail & repairs victa"
    },
    {
        "id": 1292,
        "categoryId": 32,
        "subCategoryName": "Lawn Mowers Wholesalers & Manufacturers",
        "icon": null,
        "clues": "Lawn Mowers Wholesalers & Manufacturers,lawn mowers - wholesalers & manufacturers"
    },
    {
        "id": 1293,
        "categoryId": 32,
        "subCategoryName": "Lawn Mowing Services",
        "icon": null,
        "clues": "lawn mowing services,lawn mowing & handyman,lawn services,lawn mowing & gardening,lawn mowing,Lawn Mowing Services,lawn mowing contractors,lawn mowing & maintenance"
    },
    {
        "id": 1296,
        "categoryId": 32,
        "subCategoryName": "Leadlight",
        "icon": null,
        "clues": "Leadlight,leadlight,leadlight glass,leadlight windows,leadlight supplies"
    },
    {
        "id": 1297,
        "categoryId": 32,
        "subCategoryName": "Leak Detection Services",
        "icon": null,
        "clues": "Leak Detection Services,leak detection underground services,leak detection equipment suppliers,leak detection,leak detection plumbers,leak detectors"
    },
    {
        "id": 1298,
        "categoryId": 32,
        "subCategoryName": "Leasing Services & Consultants",
        "icon": null,
        "clues": "leasing,leasing agents,leasing finance,leasing cars,leasing services,leasing consultants,Leasing Services & Consultants,leasing services & consultants"
    },
    {
        "id": 1300,
        "categoryId": 32,
        "subCategoryName": "Leather Clothing--Retail",
        "icon": null,
        "clues": "Leather Clothing--Retail,leather clothing--retail"
    },
    {
        "id": 1301,
        "categoryId": 32,
        "subCategoryName": "Leather Goods Wholesalers & Manufacturers",
        "icon": null,
        "clues": "Leather Goods Wholesalers & Manufacturers,leather goods - w'salers & mfrs,leather goods importers,leather furniture manufacturers,leather manufacturers,leather lounge manufacturers,leather goods repair,leather wholesalers,leather goods - wholesalers & manufacturers,leather clothing - wholesalers & manufacturers,leather goods,leather goods manufacturers,leather goods - wholesalers,leather goods production factory,leather gloves manufacturers,leather goods--reconditioning"
    },
    {
        "id": 1302,
        "categoryId": 32,
        "subCategoryName": "Leather Goods-Retail",
        "icon": null,
        "clues": "leather goods-retail,Leather Goods-Retail"
    },
    {
        "id": 1303,
        "categoryId": 32,
        "subCategoryName": "Leather Merchants",
        "icon": null,
        "clues": "leather merchants,leather merchants andrew muirhead,Leather Merchants"
    },
    {
        "id": 1304,
        "categoryId": 32,
        "subCategoryName": "Leather Restoration",
        "icon": null,
        "clues": "leather restoration,Leather Restoration,leather lounge restoration"
    },
    {
        "id": 1305,
        "categoryId": 32,
        "subCategoryName": "Leather, Suede & Fur Cleaning",
        "icon": null,
        "clues": "leather suede,shoes - retail suede,suede cleaning,suede dry cleaners,Leather,Suede & Fur Cleaning,leather,suede & fur cleaning,automotive suede,suede"
    },
    {
        "id": 1306,
        "categoryId": 32,
        "subCategoryName": "Legal Stationery",
        "icon": null,
        "clues": "legal stationery suppliers,Legal Stationery,legal stationery"
    },
    {
        "id": 1308,
        "categoryId": 32,
        "subCategoryName": "Lettering",
        "icon": null,
        "clues": "car lettering,lettering,acrylic lettering,gold lettering,adhesive lettering,Lettering,vinyl lettering,magnetic lettering,vinyl cut lettering,computer cut lettering"
    },
    {
        "id": 1315,
        "categoryId": 32,
        "subCategoryName": "Life Saving Equipment",
        "icon": null,
        "clues": "Life Saving Equipment,life saving equipment"
    },
    {
        "id": 1328,
        "categoryId": 32,
        "subCategoryName": "Lingerie & Sleepwear Wholesalers & Manufacturers",
        "icon": null,
        "clues": "lingerie sleepwear & hosiery,lingerie,lingerie,sleepwear & hosiery--retail,lingerie,sleepwear & hosiery--retail bras,lingerie wholesalers,Lingerie & Sleepwear Wholesalers & Manufacturers,manufacturers of lingerie,lingerie sleepwear,lingerie sleepwear & or hosiery,sleepwear lingerie,lingerie & sleepwear wholesalers & manufacturers,Lingerie,Sleepwear & Hosiery,lingerie manufacturers,lingerie & sleepwear retail,lingerie retail"
    },
    {
        "id": 1329,
        "categoryId": 32,
        "subCategoryName": "Lingerie, Sleepwear & Hosiery",
        "icon": null,
        "clues": "lingerie,lingerie,sleepwear & hosiery--retail,lingerie,sleepwear & hosiery--retail bras,ladies sleepwear,sleepwear manufacturers,sleepwear wholesale,lingerie sleepwear,sleepwear lingerie,Lingerie,Sleepwear & Hosiery,sleepwear retail,sleepwear,lingerie retail"
    },
    {
        "id": 1331,
        "categoryId": 32,
        "subCategoryName": "Lithographic Printers",
        "icon": null,
        "clues": "lithographic,Lithographic Printers,printers--lithographic,lithographic printers"
    },
    {
        "id": 1338,
        "categoryId": 32,
        "subCategoryName": "Locksmiths' Supplies",
        "icon": null,
        "clues": "locksmiths supplies,Locksmiths' Supplies,locksmiths' supplies"
    },
    {
        "id": 1339,
        "categoryId": 32,
        "subCategoryName": "Lottery Agents",
        "icon": null,
        "clues": "Lottery Agents,lottery,lottery agents"
    },
    {
        "id": 1343,
        "categoryId": 32,
        "subCategoryName": "Lubricants",
        "icon": null,
        "clues": "lubricants manufacturers,Lubricants,lubricants distributors,lubricants,lubricants oil,lubricants chemists"
    },
    {
        "id": 1345,
        "categoryId": 32,
        "subCategoryName": "Machine Knives",
        "icon": null,
        "clues": "Machine Knives,machine knives"
    },
    {
        "id": 1350,
        "categoryId": 32,
        "subCategoryName": "Magnetic Therapy",
        "icon": null,
        "clues": "magnetic therapy,Magnetic Therapy,magnetic therapy products"
    },
    {
        "id": 1351,
        "categoryId": 32,
        "subCategoryName": "Magnets & Magnetic Materials",
        "icon": null,
        "clues": "magnets,fridge magnets,magnets & magnetic materials,Magnets & Magnetic Materials"
    },
    {
        "id": 1352,
        "categoryId": 32,
        "subCategoryName": "Mail Boxes",
        "icon": null,
        "clues": "Mail Boxes,mail boxes"
    },
    {
        "id": 1353,
        "categoryId": 32,
        "subCategoryName": "Mail Contractors",
        "icon": null,
        "clues": "mail contractors,Mail Contractors"
    },
    {
        "id": 1354,
        "categoryId": 32,
        "subCategoryName": "Mail Order Services--Retail",
        "icon": null,
        "clues": "mail order services,Mail Order Services--Retail,mail order services--retail,mail order"
    },
    {
        "id": 1355,
        "categoryId": 32,
        "subCategoryName": "Mailing Machines & Equipment",
        "icon": null,
        "clues": "mailing machines,mailing machines & equipment,mailing equipment,Mailing Machines & Equipment"
    },
    {
        "id": 1358,
        "categoryId": 32,
        "subCategoryName": "Maltsters",
        "icon": null,
        "clues": "maltsters,Maltsters"
    },
    {
        "id": 1362,
        "categoryId": 32,
        "subCategoryName": "Manchester",
        "icon": null,
        "clues": "Manchester,manchester,manchester--retail"
    },
    {
        "id": 1368,
        "categoryId": 32,
        "subCategoryName": "Maps & Mapping",
        "icon": null,
        "clues": "international maps,aeronautical maps & or mapping,antique maps,maps,road maps,maps & charts,gps maps,marine maps,world maps,meteorology maps,travel maps,maps & mapping,Maps & Mapping,topographical maps,maps gps"
    },
    {
        "id": 1372,
        "categoryId": 32,
        "subCategoryName": "Marine Consultants",
        "icon": null,
        "clues": "Marine Consultants,marine consultants"
    },
    {
        "id": 1373,
        "categoryId": 32,
        "subCategoryName": "Marine Contractors",
        "icon": null,
        "clues": "electrical contractors marine,marine electrical contractors,Marine Contractors,marine contractors"
    },
    {
        "id": 1375,
        "categoryId": 32,
        "subCategoryName": "Marine Sail Makers",
        "icon": null,
        "clues": "Marine Sail Makers,sail makers--marine"
    },
    {
        "id": 1376,
        "categoryId": 32,
        "subCategoryName": "Marine Salvage &/or Equipment",
        "icon": null,
        "clues": "marine salvage & equipment,marine safety equipment,Marine Salvage &/or Equipment,marine salvage,marine salvage &/or equipment,marine equipment,marine supplies,marine equipment supplies,marine"
    },
    {
        "id": 1378,
        "categoryId": 32,
        "subCategoryName": "Market Research",
        "icon": null,
        "clues": "paid market research,market research,Market Research"
    },
    {
        "id": 1380,
        "categoryId": 32,
        "subCategoryName": "Markets",
        "icon": null,
        "clues": "markets,markets -organic & farmers markets,Markets"
    },
    {
        "id": 1381,
        "categoryId": 32,
        "subCategoryName": "Marquees",
        "icon": null,
        "clues": "marquees for hire,marquees for sale,tents marquees,marquees,corporate marquees,party marquees,marquees hire,wedding marquees,Marquees"
    },
    {
        "id": 1382,
        "categoryId": 32,
        "subCategoryName": "Marriage Celebrants",
        "icon": null,
        "clues": "marriage celebrants--civil,civil marriage celebrant,Marriage Celebrants,marriage celebrants--civil justice of the peace"
    },
    {
        "id": 1385,
        "categoryId": 32,
        "subCategoryName": "Massage Tables & Equipment",
        "icon": null,
        "clues": "massage equipment & supplies,portable massage tables,massage equipment,massage tables,massage equipment hire,massage equipment & supplies massage tables,Massage Tables & Equipment"
    },
    {
        "id": 1386,
        "categoryId": 32,
        "subCategoryName": "Massage Therapy",
        "icon": null,
        "clues": "massage therapy hot stone,massage therapy relaxation,Massage Therapy,massage therapy mobile massaging service,massage therapy pregnancy,massage therapy courses remedial,hawaiian massage therapy,massage therapy sports,massage therapy,massage therapy deep tissue,massage therapy full-body,massage therapy relaxation massage,massage therapy trigger point therapy,massage therapy remedial"
    },
    {
        "id": 1388,
        "categoryId": 32,
        "subCategoryName": "Masts & Flagpoles",
        "icon": null,
        "clues": "Masts & Flagpoles,masts,yacht masts,flagpoles & masts"
    },
    {
        "id": 1389,
        "categoryId": 32,
        "subCategoryName": "Materials Handling Consultants",
        "icon": null,
        "clues": "hazardous materials consultants,Materials Handling Consultants,materials handling consultants"
    },
    {
        "id": 1390,
        "categoryId": 32,
        "subCategoryName": "Materials Handling Equipment",
        "icon": null,
        "clues": "materials handling equipment drums,Materials Handling Equipment,materials handling equipment"
    },
    {
        "id": 1391,
        "categoryId": 32,
        "subCategoryName": "Maternity Wear W'salers & Mfrs",
        "icon": null,
        "clues": "Maternity Wear W'salers & Mfrs,maternity wear w'salers & mfrs"
    },
    {
        "id": 1392,
        "categoryId": 32,
        "subCategoryName": "Maternity Wear--Retail",
        "icon": null,
        "clues": "maternity hospital,maternity wear--retail evening wear,maternity shop,maternity wear--retail,maternity,Maternity Wear--Retail,maternity clothes,maternity wear"
    },
    {
        "id": 1394,
        "categoryId": 32,
        "subCategoryName": "Mats & Matting",
        "icon": null,
        "clues": "mats,mats & matting,Mats & Matting,coir mats & matting,mats & matting rubber"
    },
    {
        "id": 1396,
        "categoryId": 32,
        "subCategoryName": "Mattresses",
        "icon": null,
        "clues": "beds & mattresses,mattresses protectors,mattresses warehouse,mattresses removals,mattresses cleaning,mattresses retail,mattresses,mattresses wholesalers,mattresses bed,mattresses manufacturers,mattresses sales,mattresses factory,Mattresses,mattresses sanitising,mattresses repair"
    },
    {
        "id": 1397,
        "categoryId": 32,
        "subCategoryName": "Measuring Machines & Equipment",
        "icon": null,
        "clues": "measuring,measuring wheels,measuring system,measuring machines,measuring machines & equipment,measuring tools,measuring instruments,precision measuring,measuring instrument,Measuring Machines & Equipment,measuring equipment,measuring devices"
    },
    {
        "id": 1413,
        "categoryId": 32,
        "subCategoryName": "Memorabilia & Collectables",
        "icon": null,
        "clues": "sports memorabilia,memorabilia framing,memorabilia,collectables & memorabilia,comics collectables & memorabilia,Memorabilia & Collectables,memorabilia shop"
    },
    {
        "id": 1415,
        "categoryId": 32,
        "subCategoryName": "Menswear Wholesalers & Manufacturers",
        "icon": null,
        "clues": "Menswear Wholesalers & Manufacturers,menswear manufacturers,menswear--w'salers & mfrs,menswear wholesalers"
    },
    {
        "id": 1416,
        "categoryId": 32,
        "subCategoryName": "Menswear--Retail",
        "icon": null,
        "clues": "wedding--menswear,menswear clothing,Menswear--Retail,menswear,menswear--retail larger sizes,retail menswear,menswear suits,menswear--retail,retail menswear boutiques,menswear--retail ties,menswear hire,menswear retail store,menswear--retail suits"
    },
    {
        "id": 1417,
        "categoryId": 32,
        "subCategoryName": "Mercantile Agents",
        "icon": null,
        "clues": "Mercantile Agents,mercantile agents,mercantile,mercantile agencies"
    },
    {
        "id": 1418,
        "categoryId": 32,
        "subCategoryName": "Merchants--General",
        "icon": null,
        "clues": "merchants--general,Merchants--General"
    },
    {
        "id": 1419,
        "categoryId": 32,
        "subCategoryName": "Metal & Rubber Stamps",
        "icon": null,
        "clues": "rubber &/or metal stamps,rubber & metal stamps,Metal & Rubber Stamps"
    },
    {
        "id": 1420,
        "categoryId": 32,
        "subCategoryName": "Metal & Wood Signs",
        "icon": null,
        "clues": "metal signs,Rollers--Wooden,Metal,Rubber Etc.,metal and wood signs,rollers--wooden,metal,rubber etc.,signs--metal,Metal & Wood Signs"
    },
    {
        "id": 1421,
        "categoryId": 32,
        "subCategoryName": "Metal Boxes & Cases",
        "icon": null,
        "clues": "Metal Boxes & Cases,metal boxes,boxes & cases-metal,sheet metal workers tool boxes,boxes & cases-metal tool boxes,metal tool boxes"
    },
    {
        "id": 1439,
        "categoryId": 32,
        "subCategoryName": "Microfilming Services, Equipment & Supplies",
        "icon": null,
        "clues": "Microfilming Services,Equipment & Supplies,microfilming services,equipment & supplies,microfilming services equipment"
    },
    {
        "id": 1440,
        "categoryId": 32,
        "subCategoryName": "Microwave Oven",
        "icon": null,
        "clues": "microwave ovens,microwave oven repair,microwave repair,microwave oven spare parts,microwave oven services,Microwave Oven,microwave,microwave oven parts"
    },
    {
        "id": 1452,
        "categoryId": 32,
        "subCategoryName": "Mirrors",
        "icon": null,
        "clues": "mirrors repair,mirrors,mirrors glass,Mirrors,mirrors door,mirrors shop"
    },
    {
        "id": 1455,
        "categoryId": 32,
        "subCategoryName": "Mobile Marketing",
        "icon": null,
        "clues": "mobile marketing billboards,Mobile Marketing,mobile marketing internet,internet marketing services mobile devices,internet marketing services mobile service"
    },
    {
        "id": 1456,
        "categoryId": 32,
        "subCategoryName": "Mobile Phone Repairs & Service",
        "icon": null,
        "clues": "mobile telephones & accessories repairs,mobile phone batteries,mobile phone dealers,mobile telephones-repairs & service optus,mobile phones,mobile phone installation,service stations mobile services,Mobile Phone Repairs & Service,mobile phone sales,mobile telephones-repairs & service nokia,mobile phone store,nokia mobile phones & accessories,computer equipment--repairs,service & upgrades mobile technicians,mobile telephones-repairs & service,mobile phone accessories,mobile phone bluetooth car kits,mobile phones - repairs & service,mobile phone repair,mobile phone car kits"
    },
    {
        "id": 1457,
        "categoryId": 32,
        "subCategoryName": "Mobile Phones & Accessories",
        "icon": null,
        "clues": "mobile phones batteries,samsung mobile phones & accessories,telephones & accessories mobile phones,mobile telephones & accessories batteries,mobile telephones & accessories mobile telephones,mobile telephones & accessories,Nokia mobile phones & accessories,mobile telephones & accessories car kits,mobile telephones & accessories car kit installations,Mobile Phones & Accessories,mobile telephones & accessories telstra mobile dealer,nokia mobile phones & accessories,mobile telephones & accessories optus,mobile telephones & accessories vodafone,mobile phone accessories,mobile & accessories,mobile telephones & accessories mobile accessories,mobile phones & accessories,mnokia mobile phones & accessories"
    },
    {
        "id": 1458,
        "categoryId": 32,
        "subCategoryName": "Mobility Scooters",
        "icon": null,
        "clues": "mobility,mobility scooters retail,scooters--mobility,mobility scooters hire,mobility aids,mobility scooters,Mobility Scooters,mobility scooters importers"
    },
    {
        "id": 1468,
        "categoryId": 32,
        "subCategoryName": "Mortgage Brokers",
        "icon": null,
        "clues": "mortgage brokers bank,mortgage brokers suncorp-metway,mortgage brokers macquarie bank,mortgage brokers st george,mortgage brokers westpac,mortgage finance,mortgage brokers bank west,finance--mortgage loans,mortgage,mortgage brokers commonwealth bank,mortgage brokers business financing,mortgage brokers anz,mortgage loans,mortgage brokers,Mortgage Brokers,mortgage brokers nab,mortgage lenders"
    },
    {
        "id": 1469,
        "categoryId": 32,
        "subCategoryName": "Mosaics",
        "icon": null,
        "clues": "Mosaics,art supplies mosaics,mosaics"
    },
    {
        "id": 1472,
        "categoryId": 32,
        "subCategoryName": "Motor & Boat Canopy",
        "icon": null,
        "clues": "Motor & Boat Canopy"
    },
    {
        "id": 1493,
        "categoryId": 32,
        "subCategoryName": "Mounting & Finishing",
        "icon": null,
        "clues": "Mounting & Finishing,mounting & finishing"
    },
    {
        "id": 1495,
        "categoryId": 32,
        "subCategoryName": "Murals",
        "icon": null,
        "clues": "Murals,murals"
    },
    {
        "id": 1498,
        "categoryId": 32,
        "subCategoryName": "Mushroom & Spawn Suppliers",
        "icon": null,
        "clues": "mushroom & spawn suppliers,mushroom spawn,mushroom suppliers,Mushroom & Spawn Suppliers"
    },
    {
        "id": 1499,
        "categoryId": 32,
        "subCategoryName": "Music & Musical Instruments",
        "icon": null,
        "clues": "music & musical instruments sheet music,musical instruments,Music & Musical Instruments,music & musical instruments guitar strings,music & musical instruments guitars,music & musical instruments drums,music & musical instruments pianos,music & musical instruments repairs,music & musical,music & musical instruments,music & musical instruments violins,music instruments services & repair,music store"
    },
    {
        "id": 1500,
        "categoryId": 32,
        "subCategoryName": "Music Amplification Equipment & Services",
        "icon": null,
        "clues": "Music Amplification Equipment & Services,music equipment hire,music recording equipment,music amplification equipment,music amplification equipment & services,music equipment,music services,music instruments services & repair"
    },
    {
        "id": 1501,
        "categoryId": 32,
        "subCategoryName": "Music Arrangers & Composers",
        "icon": null,
        "clues": "music composers,Music Arrangers & Composers,music arrangers & composers"
    },
    {
        "id": 1502,
        "categoryId": 32,
        "subCategoryName": "Music Stores-Retail",
        "icon": null,
        "clues": "music stores-retail second hand,music stores-retail audio books,music stores-retail,Music Stores-Retail,music stores-retail dvds,music stores-retail records"
    },
    {
        "id": 1504,
        "categoryId": 32,
        "subCategoryName": "Music Therapists",
        "icon": null,
        "clues": "Music Therapists,music therapists"
    },
    {
        "id": 1505,
        "categoryId": 32,
        "subCategoryName": "Musical Instrument Makers & Repairers",
        "icon": null,
        "clues": "musical instrument repair & makers,musical instrument makers,celtic musical instrument makers,musical instrument makers & repairers,Musical Instrument Makers & Repairers,musical instrument makers & repairers guitars,musical instrument makers & repair,musical instrument makers & repairers violins"
    },
    {
        "id": 1506,
        "categoryId": 32,
        "subCategoryName": "Music-Background",
        "icon": null,
        "clues": "Music-Background,music-background"
    },
    {
        "id": 1507,
        "categoryId": 32,
        "subCategoryName": "Musicians & Musicians' Agents",
        "icon": null,
        "clues": "musicians agencies,musicians agents,musicians management,musicians instruments,musicians hire,musicians,Musicians & Musicians' Agents,wedding musicians,musicians managers & agents,musicians & musicians' agents"
    },
    {
        "id": 1508,
        "categoryId": 32,
        "subCategoryName": "Music-Sheet",
        "icon": null,
        "clues": "music-sheet,Music-Sheet"
    },
    {
        "id": 1511,
        "categoryId": 32,
        "subCategoryName": "Name Plates",
        "icon": null,
        "clues": "Name Plates,name plates,brass name plates,house name plates,name tags,name badges,promotional products name plates & plaques"
    },
    {
        "id": 1513,
        "categoryId": 32,
        "subCategoryName": "Nappy Services",
        "icon": null,
        "clues": "nappy bags,nappy cleaning services,nappy cleaning,nappy factory,nappy services,Nappy Services,cloth nappy services,nappy wholesale,nappy wash,disposable nappy services,baby nappy services,nappy cakes,nappy warehouse,nappy delivery,nappy bag manufacturers,nappy,nappy wholesalers,nappy shop,nappy supplies"
    },
    {
        "id": 1514,
        "categoryId": 32,
        "subCategoryName": "National Dress",
        "icon": null,
        "clues": "national dress--retail,national dress,national dress wholesalers & manufacturers,National Dress"
    },
    {
        "id": 1515,
        "categoryId": 32,
        "subCategoryName": "Natural Resources Consultants",
        "icon": null,
        "clues": "natural resource consultants,natural resources consultants,natural resources,Natural Resources Consultants"
    },
    {
        "id": 1520,
        "categoryId": 32,
        "subCategoryName": "Neon & LED Signs",
        "icon": null,
        "clues": "signs--neon & illuminated,neon and illuminated signs,neon signs,neon,signs--neon,Neonatology,opal neon signs,Neon & LED Signs"
    },
    {
        "id": 1522,
        "categoryId": 32,
        "subCategoryName": "Nets & Netting",
        "icon": null,
        "clues": "sports nets,golf nets,cargo nets,mosquito nets,nets & netting bird nets,nets & netting,Nets & Netting,cricket nets,tennis nets,soccer nets,halieutics nets"
    },
    {
        "id": 1525,
        "categoryId": 32,
        "subCategoryName": "New Age Products & Services",
        "icon": null,
        "clues": "new age shop,new age products & services,New Age Products & Services,new age products & services crystals,new age products,new age"
    },
    {
        "id": 1527,
        "categoryId": 32,
        "subCategoryName": "Newsagents",
        "icon": null,
        "clues": "newsagents art supplies,newsagents supplies,newsagents groups,newsagents,newsagents & mobile phones,Newsagents,newsagents brokers,newsagents agents"
    },
    {
        "id": 1528,
        "categoryId": 32,
        "subCategoryName": "Newspapers",
        "icon": null,
        "clues": "newspapers japanese,newspapers turkish,newspapers african,newspapers arabic,Distributors--Newspapers,Magazines &/or Directories,newspapers,newspapers weekly,newspapers korean,newspapers italian,newspapers vietnamese,newspapers asian,newspapers subscriptions,newspapers publishing,newspapers advertising,Newspapers,newspapers community,newspapers indian,newspapers ethnic,newspapers business,newspapers classifieds"
    },
    {
        "id": 1531,
        "categoryId": 32,
        "subCategoryName": "Noise Control Equipment",
        "icon": null,
        "clues": "noise,noise control,noise control equipment,Noise Control Equipment"
    },
    {
        "id": 1532,
        "categoryId": 32,
        "subCategoryName": "Novelties--Dance, Carnival & Magic",
        "icon": null,
        "clues": "magic,carnival equipment,carnival products,carnival,magic shop,magic entertainment,magic supplies,magic cards,novelties--dance,carnival & magic,carnival games,carnival amusements,novelties--dance carnival,magic show,carnival rides,magic wands,Novelties--Dance,Carnival & Magic,carnival hire,magician,magic tricks,carnival supplies"
    },
    {
        "id": 1533,
        "categoryId": 32,
        "subCategoryName": "Novelty Message Services",
        "icon": null,
        "clues": "novelty cakes,balloons--advertising & novelty,novelty messages,novelty,novelty shop,novelty message services,novelty items,novelty toys,Novelty Message Services,novelty gifts,novelty hats"
    },
    {
        "id": 1536,
        "categoryId": 32,
        "subCategoryName": "Nursery & Garden Supplies Wholesale",
        "icon": null,
        "clues": "plant nursery wholesale,nursery wholesale,wholesale native nursery,nursery retail,garden centre nursery,wholesale nursery,garden centre & retail nursery,boutiques garden nursery,nursery plants,Nursery & Garden Supplies Wholesale,nursery furniture,nursery garden,garden nursery,plant nursery supplies,native nursery,plant nursery,nursery garden centre,nurseries - wholesale nursery supplies,wholesale tropical plant nursery,nursery garden supplies,wholesale plant nursery,garden nursery supplies,wholesale tree nursery,nursery wholesalers,nursery,nursery supplies wholesale,wholesale nursery supplies"
    },
    {
        "id": 1537,
        "categoryId": 32,
        "subCategoryName": "Nursery Supplies",
        "icon": null,
        "clues": ""
    },
    {
        "id": 1538,
        "categoryId": 32,
        "subCategoryName": "Nurses & Nursing Services",
        "icon": null,
        "clues": "nurses call,employment services nurses,nurses agencies,nurses & nursing services nursing homes,nurses & nursing services,nurses,nurses & nursing services mental health,nurses & nursing services aged care,Nurses & Nursing Services,nurses uniforms"
    },
    {
        "id": 1540,
        "categoryId": 32,
        "subCategoryName": "Nuts & Bolts",
        "icon": null,
        "clues": "Nuts & Bolts,bolts & nuts,nuts,bolts & nuts screws,Fruits,Nuts & Berries (Nurseries-Retail),nuts bolts screws,nuts--edible &/or products,nuts bolts fasteners,nuts & bolts"
    },
    {
        "id": 1544,
        "categoryId": 32,
        "subCategoryName": "Office & Business Furniture Reconditioning & Repairs",
        "icon": null,
        "clues": "home office furniture,office fax repairs,office furniture rental,office furniture manufacturers,office chair repairs,office & business furniture,Office & Business Furniture Reconditioning & Repairs,office & business furniture sebel,office & business furniture reconditioning & repairs,second hand office furniture"
    },
    {
        "id": 1545,
        "categoryId": 32,
        "subCategoryName": "Office & Business Systems",
        "icon": null,
        "clues": "office & business furniture sebel,office security systems,shop & office fitting shelving systems,Office & Business Systems,office systems,office storage systems,office filing systems,office & business furniture,office & business systems"
    },
    {
        "id": 1546,
        "categoryId": 32,
        "subCategoryName": "Office Equipment & Furniture Hire",
        "icon": null,
        "clues": "hire--office equipment & furniture,office machine hire,Office Equipment & Furniture Hire,portable office hire,office plant hire,transportable office hire,hire equipment office furniture"
    },
    {
        "id": 1547,
        "categoryId": 32,
        "subCategoryName": "Office Equipment--Fire Resisting",
        "icon": null,
        "clues": "office equipment--fire resisting,Office Equipment--Fire Resisting"
    },
    {
        "id": 1548,
        "categoryId": 32,
        "subCategoryName": "Office Furniture",
        "icon": null,
        "clues": ""
    },
    {
        "id": 1549,
        "categoryId": 32,
        "subCategoryName": "Office Supplies",
        "icon": null,
        "clues": "Office Supplies,office supplies,promotional products stationery & office supplies"
    },
    {
        "id": 1551,
        "categoryId": 32,
        "subCategoryName": "Oil & Mechanical Seals",
        "icon": null,
        "clues": "seals--oil & mechanical o-rings,seals--oil,seals--oil & mechanical,Oil & Mechanical Seals,oil seals"
    },
    {
        "id": 1552,
        "categoryId": 32,
        "subCategoryName": "Oil Cooler",
        "icon": null,
        "clues": "oil coolers,Oil Cooler"
    },
    {
        "id": 1556,
        "categoryId": 32,
        "subCategoryName": "Oil Reconditioning",
        "icon": null,
        "clues": "oil reconditioning,Oil Reconditioning"
    },
    {
        "id": 1557,
        "categoryId": 32,
        "subCategoryName": "Oil--Fuel & Heating",
        "icon": null,
        "clues": "oil--fuel & heating,oil--fuel,fuel oil,Oil--Fuel & Heating"
    },
    {
        "id": 1559,
        "categoryId": 32,
        "subCategoryName": "Oils--Eucalyptus, Tea Tree & Essential",
        "icon": null,
        "clues": "tea,tea tree oil,oils--eucalyptus,tea tree & essential aromatherapy products,Oils--Eucalyptus,Tea Tree & Essential,oils--eucalyptus,tea tree & essential,tea room,oils--eucalyptus tea tree,tea shop,essential oils,essential oil suppliers"
    },
    {
        "id": 1561,
        "categoryId": 32,
        "subCategoryName": "Opals",
        "icon": null,
        "clues": "opals,opals retail,Opals"
    },
    {
        "id": 1570,
        "categoryId": 32,
        "subCategoryName": "Orchids (Nurseries-Retail)",
        "icon": null,
        "clues": "orchids,silk orchids,orchids (nurseries-retail),Orchids (Nurseries-Retail),florists retail orchids"
    },
    {
        "id": 1571,
        "categoryId": 32,
        "subCategoryName": "Organic Products",
        "icon": null,
        "clues": "organic products,organic fruit,organic food store,organic vegetables,Organic Products,organic meat,organic hair products,health foods & products--retail organic,organic cleaning products,organic grocers,organic restaurant,organic butcher,organic produce,organic food,hairdressers organic hair products,organic shop,organic cafe,organic"
    },
    {
        "id": 1572,
        "categoryId": 32,
        "subCategoryName": "Organising Services--Home & Office",
        "icon": null,
        "clues": "Organising Services--Home & Office,organising services,organising services--home & office"
    },
    {
        "id": 1573,
        "categoryId": 32,
        "subCategoryName": "Organs & Keyboards-Tuning & Repairing",
        "icon": null,
        "clues": "organs & keyboards-tuning & repairing,pianos,keyboards & organs,Pianos,Keyboards & Organs,pianos & organs,Organs & Keyboards-Tuning & Repairing,electric organs"
    },
    {
        "id": 1574,
        "categoryId": 32,
        "subCategoryName": "Oriental Rugs & Handmade Carpets",
        "icon": null,
        "clues": "Oriental Rugs & Handmade Carpets,carpets & rugs--oriental & handmade,carpets & rugs--oriental & handmade persian,oriental carpet,oriental rugs"
    },
    {
        "id": 1582,
        "categoryId": 32,
        "subCategoryName": "Outdoor Adventure Activities & Supplies",
        "icon": null,
        "clues": "outdoor camping supplies,outdoor adventure activities & supplies camping,outdoor activities,outdoor adventure activities & supplies fishing,Outdoor Adventure Activities & Supplies,outdoor adventure,outdoor adventure store,outdoor adventure activities & supplies,outdoor adventure supplies,outdoor adventure clothing,outdoor adventure activities,children's outdoor activities,outdoor adventure equipment,adventure outdoor"
    },
    {
        "id": 1583,
        "categoryId": 32,
        "subCategoryName": "Outdoor Furniture",
        "icon": null,
        "clues": "Outdoor Furniture,outdoor furniture,furniture--outdoor,outdoor furniture retail"
    },
    {
        "id": 1584,
        "categoryId": 32,
        "subCategoryName": "Ovens--Industrial",
        "icon": null,
        "clues": "Ovens--Industrial,ovens--industrial,industrial ovens"
    },
    {
        "id": 1585,
        "categoryId": 32,
        "subCategoryName": "Overall Hire Services",
        "icon": null,
        "clues": "overall hire services,Overall Hire Services,overalls"
    },
    {
        "id": 1587,
        "categoryId": 32,
        "subCategoryName": "Packaging Consultants",
        "icon": null,
        "clues": "packaging consultants,Packaging Consultants"
    },
    {
        "id": 1588,
        "categoryId": 32,
        "subCategoryName": "Packaging Supplies & Materials",
        "icon": null,
        "clues": "industrial packaging materials,paper packaging materials,Packaging Supplies & Materials,packaging materials takeaway containers,packaging materials plastic,packaging supplies,industrial packaging tape materials,food packaging supplies,wrapping & packaging materials,packaging materials food product packaging,packaging materials,packaging materials bags,packaging materials supplies,packaging materials boxes"
    },
    {
        "id": 1589,
        "categoryId": 32,
        "subCategoryName": "Packaging, Filling & Assembling Services",
        "icon": null,
        "clues": "Packaging,Filling & Sealing Equipment,Packaging,Filling & Assembling Services,packaging,filling & assembling services,packaging,filling & assembling services packing,Aerosol Containers,Valves &/or Filling"
    },
    {
        "id": 1590,
        "categoryId": 32,
        "subCategoryName": "Packaging, Filling & Sealing Equipment",
        "icon": null,
        "clues": "Packaging,Filling & Sealing Equipment,sealing equipment"
    },
    {
        "id": 1591,
        "categoryId": 32,
        "subCategoryName": "Pad Printers",
        "icon": null,
        "clues": "Pad Printers,printers--pad"
    },
    {
        "id": 1603,
        "categoryId": 32,
        "subCategoryName": "Painters' Supplies",
        "icon": null,
        "clues": "painters' supplies,Painters' Supplies,panel beaters & painters,house painters,painters & decorators homes,painters,painters & decorators commercial"
    },
    {
        "id": 1604,
        "categoryId": 32,
        "subCategoryName": "Pallets & Platforms",
        "icon": null,
        "clues": "pallets & crates,plastic pallets,timber pallets,Pallets & Platforms,pallets plastic,pallets manufacturing,pallets,metal pallets,wooden pallets,pallets recyclers,pallets & platforms,pallets & creates"
    },
    {
        "id": 1606,
        "categoryId": 32,
        "subCategoryName": "Palms (Nurseries-Retail)",
        "icon": null,
        "clues": "Palms (Nurseries-Retail),palms (nurseries-retail),wholesale palms,palms"
    },
    {
        "id": 1610,
        "categoryId": 32,
        "subCategoryName": "Paper & Plastic Containers",
        "icon": null,
        "clues": "paper & plastic containers,paper & plastic container,Paper & Plastic Containers"
    },
    {
        "id": 1611,
        "categoryId": 32,
        "subCategoryName": "Paper Bags",
        "icon": null,
        "clues": "recycled paper bags,brown paper bags,bags & sacks paper,paper carry bags,paper bags wholesale,paper bags suppliers,printed paper bags,paper shopping bags,retail paper bags,bags paper,wholesale paper bags,Paper Bags,paper bags,paper gift bags"
    },
    {
        "id": 1612,
        "categoryId": 32,
        "subCategoryName": "Paper Converters",
        "icon": null,
        "clues": "Paper Converters,paper converters"
    },
    {
        "id": 1613,
        "categoryId": 32,
        "subCategoryName": "Paper Making & Converting Machinery",
        "icon": null,
        "clues": "paper making & converting machinery,Paper Making & Converting Machinery,paper making"
    },
    {
        "id": 1614,
        "categoryId": 32,
        "subCategoryName": "Paper Merchants",
        "icon": null,
        "clues": "Paper Merchants,paper merchants"
    },
    {
        "id": 1616,
        "categoryId": 32,
        "subCategoryName": "Paper Shredder & Paper Cutting",
        "icon": null,
        "clues": "paper shredder hire,paper shredder repair,paper cutting machine,Paper Shredder & Paper Cutting,paper cutting,paper cutting & shredding machines"
    },
    {
        "id": 1618,
        "categoryId": 32,
        "subCategoryName": "Parachutes & Parachute Equipment",
        "icon": null,
        "clues": "parachutes & parachute equipment,Parachutes & Parachute Equipment"
    },
    {
        "id": 1621,
        "categoryId": 32,
        "subCategoryName": "Parking Systems & Equipment",
        "icon": null,
        "clues": "parking systems,parking station systems & equipment,car parking systems,Parking Systems & Equipment,parking station systems"
    },
    {
        "id": 1628,
        "categoryId": 32,
        "subCategoryName": "Party Plan Selling",
        "icon": null,
        "clues": "party plan representatives,adult party plan,party plan selling,party plan business,party plan,lingerie party plan,party plan jewellery,adult products party plan,party plan clothing,Party Plan Selling,party plan sales,party plan ladies clothing"
    },
    {
        "id": 1629,
        "categoryId": 32,
        "subCategoryName": "Party Supplies",
        "icon": null,
        "clues": "party supplies disposable products,party supplies birthdays,party supplies costume accessories,Party Supplies,party supplies christmas parties,party supplies,party supplies weddings,party supplies shop,party supplies costumes,party supplies children's parties,party supplies novelties,party supplies hire,party supplies wholesale,party supplies balloons,party supplies hens parties"
    },
    {
        "id": 1636,
        "categoryId": 32,
        "subCategoryName": "Patternmakers -- Engineering",
        "icon": null,
        "clues": "patternmakers - engineering,Patternmakers -- Engineering"
    },
    {
        "id": 1637,
        "categoryId": 32,
        "subCategoryName": "Patternmakers' Equipment & Supplies",
        "icon": null,
        "clues": "patternmakers' equipment & supplies,patternmakers - engineering,Patternmakers' Equipment & Supplies"
    },
    {
        "id": 1639,
        "categoryId": 32,
        "subCategoryName": "Pawn Shop & Pawnbrokers",
        "icon": null,
        "clues": "pawn dealers,Pawn Shop & Pawnbrokers,pawn shop,pawn,pawnbrokers"
    },
    {
        "id": 1641,
        "categoryId": 32,
        "subCategoryName": "Payroll Services",
        "icon": null,
        "clues": "payroll software,payroll preparation &/or deduction services,payroll systems,payroll outsourcing,payroll training,payroll management,Payroll Services,payroll,payroll services,payroll preparation,payroll bureau"
    },
    {
        "id": 1642,
        "categoryId": 32,
        "subCategoryName": "Pearlers",
        "icon": null,
        "clues": "Pearlers,pearlers"
    },
    {
        "id": 1643,
        "categoryId": 32,
        "subCategoryName": "Pens & Pencils Wholesalers & Manufacturers",
        "icon": null,
        "clues": "pens & pencils,promotional pens,Pens & Pencils Wholesalers & Manufacturers,pens & pencils - wholesalers & manufacturers"
    },
    {
        "id": 1644,
        "categoryId": 32,
        "subCategoryName": "Pens & Pencils--Retail & Repairs",
        "icon": null,
        "clues": "pens & pencils--retail & repairs,Pens & Pencils--Retail & Repairs"
    },
    {
        "id": 1645,
        "categoryId": 32,
        "subCategoryName": "Perforation & Perforated Metal",
        "icon": null,
        "clues": "Perforation & Perforated Metal"
    },
    {
        "id": 1649,
        "categoryId": 32,
        "subCategoryName": "Permaculture",
        "icon": null,
        "clues": "permaculture,Permaculture"
    },
    {
        "id": 1666,
        "categoryId": 32,
        "subCategoryName": "Photo Electric Equipment & Supplies",
        "icon": null,
        "clues": "photo electric equipment & supplies,photo equipment,photo supplies,Photo Electric Equipment & Supplies"
    },
    {
        "id": 1667,
        "categoryId": 32,
        "subCategoryName": "Photo Frames & Picture Framing",
        "icon": null,
        "clues": "wholesale photo frames,digital photo frames,photo frames,silver photo frames,photo frames wholesalers,Photo Frames & Picture Framing,picture framing & frames photo frames"
    },
    {
        "id": 1668,
        "categoryId": 32,
        "subCategoryName": "Photo Processing Services",
        "icon": null,
        "clues": "photo processing services,photo processing,digital photo processing,one hour photo processing,photo services,photo processing laboratory,print on canvas photo processing,kodak photo processing,printing on canvas photo processing,photo printing services,professional photo processing,Photo Processing Services,photo processing shop"
    },
    {
        "id": 1669,
        "categoryId": 32,
        "subCategoryName": "Photo Restoration & Retouching",
        "icon": null,
        "clues": "Photo Restoration & Retouching,photo retouching,photo restoration"
    },
    {
        "id": 1670,
        "categoryId": 32,
        "subCategoryName": "Photocopiers & Photocopying",
        "icon": null,
        "clues": "photocopiers,Photocopiers & Photocopying"
    },
    {
        "id": 1671,
        "categoryId": 32,
        "subCategoryName": "Photocopying & Copying",
        "icon": null,
        "clues": "photocopying hire,colour photocopying,photocopying,photocopying paper,photocopying services plan printing,photocopying centre,photocopying machines,photocopying sales,photocopying repair,photocopying services laminating,photocopying shop,photocopying services,photocopying printing,Photocopying & Copying,photocopying services binding,photocopying technicians"
    },
    {
        "id": 1672,
        "categoryId": 32,
        "subCategoryName": "Photocopying Supplies",
        "icon": null,
        "clues": "photocopying hire,colour photocopying,photocopying,photocopying paper,photocopying services plan printing,Photocopying Supplies,photocopying centre,photocopying machines,photocopying sales,photocopying supplies,photocopying repair,photocopying services laminating,photocopying shop,photocopying services,photocopying printing,photocopying services binding,photocopying technicians"
    },
    {
        "id": 1673,
        "categoryId": 32,
        "subCategoryName": "Photogrammetry",
        "icon": null,
        "clues": "photogrammetry,Photogrammetry"
    },
    {
        "id": 1674,
        "categoryId": 32,
        "subCategoryName": "Photographers",
        "icon": null,
        "clues": "photographers--general passport,photographers--general,photographers--aerial,Photographers,photographers,photographers--portrait babies,photographers--commercial & industrial,photographers--commercial,photographers--portrait glamour,photographers--portrait studio"
    },
    {
        "id": 1675,
        "categoryId": 32,
        "subCategoryName": "Photographers--Advertising & Fashion",
        "icon": null,
        "clues": "Photographers--Advertising & Fashion,photographers--advertising,advertising photographers,photographers fashion advertising,photographers--advertising & fashion"
    },
    {
        "id": 1676,
        "categoryId": 32,
        "subCategoryName": "Photographers--Portrait",
        "icon": null,
        "clues": "photographers--portrait,photographers modelling portrait,family portrait photographers,photographers--portrait passport,photographers--portrait babies,photographers--portrait children,Photographers--Portrait,print on canvas portrait photographers,photographers--portrait glamour,photographers--portrait studio"
    },
    {
        "id": 1678,
        "categoryId": 32,
        "subCategoryName": "Photographic Equipment & Supplies Wholesalers & Manufacturers",
        "icon": null,
        "clues": "photographic supplies,photographic equipment & supplies,photographic equipment & supplies--retail & repairs digital,professional photographic supplies,photographic equipment & supplies--retail & repairs hire,photographic equipment & supplies--retail & repairs video cameras,photographic equipment & supplies--retail & repairs canon,professional photographic equipment,photographic equipment & supplies--retail &,photographic equipment & supplies - wholesalers & manufacturers,photographic equipment & supplies--retail & repairs cameras,photographic equipment & supplies--retail & repairs repairs,Photographic Equipment & Supplies Wholesalers & Manufacturers,photographic equipment & supplies--retail & repairs,photographic lighting supplies,photographic equipment"
    },
    {
        "id": 1679,
        "categoryId": 32,
        "subCategoryName": "Photographic Processing Services--Professional",
        "icon": null,
        "clues": "photographic processing services prints on canvas,photos on canvas photographic processing,Photographic Processing Services--Professional,photographic processing services,photographic processing services kodak,photographic processing services--professional,photographic processing,photographic processing services passport photos,photographic processing services printing,photographic processing professionals,photographic processing laboratory"
    },
    {
        "id": 1682,
        "categoryId": 32,
        "subCategoryName": "Physiotherapy Equipment",
        "icon": null,
        "clues": "physiotherapy equipment,Physiotherapy Equipment"
    },
    {
        "id": 1684,
        "categoryId": 32,
        "subCategoryName": "Piano Tuning & Piano Repair",
        "icon": null,
        "clues": "piano repair,piano tuning & repair,Piano Tuning & Piano Repair"
    },
    {
        "id": 1685,
        "categoryId": 32,
        "subCategoryName": "Pianos, Keyboards & Organs",
        "icon": null,
        "clues": "grand pianos,pianos,keyboards & organs,roland pianos,Pianos,Keyboards & Organs,boston pianos,retail pianos,pianos & organs,steinway & sons pianos,essex pianos,electric organs"
    },
    {
        "id": 1686,
        "categoryId": 32,
        "subCategoryName": "Picture Framing Supplies",
        "icon": null,
        "clues": "picture framing shop,wholesale picture framing supplies,picture framing,picture framing supplies,picture framing & frames mounting,picture framing equipment,picture framing & frames picture hanging,picture frame supplies,Picture Framing Supplies,picture framing & frames,picture framing & frames memorabilia"
    },
    {
        "id": 1687,
        "categoryId": 32,
        "subCategoryName": "Pictures, Posters & Prints Wholesale",
        "icon": null,
        "clues": "Pictures,Prints & Posters--Retail,prints on canvas wholesale,pictures,prints & posters - wholesale,Pictures,Posters & Prints Wholesale"
    },
    {
        "id": 1688,
        "categoryId": 32,
        "subCategoryName": "Pictures, Prints & Posters--Retail",
        "icon": null,
        "clues": "pictures,prints & posters--retail,posters shop,prints & paintings,posters printing,posters laminating,posters design,posters mounting,prints posters displays,posters display,prints pictures,posters framing,Pictures,Prints & Posters--Retail,posters suppliers,pictures,prints & posters--retail paintings,posters distributors,prints on canvas - pictures & posters"
    },
    {
        "id": 1693,
        "categoryId": 32,
        "subCategoryName": "Pilots' Supplies",
        "icon": null,
        "clues": "escorts pilots,pilots' supplies,Pilots' Supplies,pilot supplies,overdimensional pilots,road pilots,safe wide pilots,transport pilots"
    },
    {
        "id": 1703,
        "categoryId": 32,
        "subCategoryName": "Plan Printing Equipment & Services",
        "icon": null,
        "clues": "plan printing equipment,plan printing equipment & services,Plan Printing Equipment & Services"
    },
    {
        "id": 1704,
        "categoryId": 32,
        "subCategoryName": "Planter Tubs",
        "icon": null,
        "clues": "Planter Tubs,planter tubs,planter boxes"
    },
    {
        "id": 1705,
        "categoryId": 32,
        "subCategoryName": "Plaques",
        "icon": null,
        "clues": "plaques engraving,plaques makers,plaques trophy,plaques,plaques & trophies,plaques & honour boards,Plaques"
    },
    {
        "id": 1710,
        "categoryId": 32,
        "subCategoryName": "Plastic Bags",
        "icon": null,
        "clues": "bags plastic,promotional products plastic bags,bags & sacks plastic,Plastic Bags,hdpe plastic bags,plastic bags packaging,plastic carry bags,plastic bags,resealable plastic bags,plastic bags wholesale,plastic shopping bags"
    },
    {
        "id": 1711,
        "categoryId": 32,
        "subCategoryName": "Plastic Cards & Equipment",
        "icon": null,
        "clues": "plastic business cards,plastic identity cards,plastic cards & equipment,Plastic Cards & Equipment,plastic cards"
    },
    {
        "id": 1716,
        "categoryId": 32,
        "subCategoryName": "Plastic Products",
        "icon": null,
        "clues": "polycarbonate plastic products retail,promotional products plastic bags,plastic products retail,wholesale of plastic products,Plastic Products,plastic products plastic accessories plastic homeware,recycled plastic products,plastic products,plastic products tanks,paper & plastic products,household plastic products"
    },
    {
        "id": 1724,
        "categoryId": 32,
        "subCategoryName": "Playground Equipment",
        "icon": null,
        "clues": "Playground Equipment,backyard playground equipment,children's playground equipment,playground equipment swings,indoor playground,playground equipment trampolines,playground equipment cubby houses,playground,indoor playground equipment,playground equipment,playground equipment softfall,outdoor playground equipment"
    },
    {
        "id": 1725,
        "categoryId": 32,
        "subCategoryName": "Pleaters",
        "icon": null,
        "clues": "pleaters,fabric pleaters,permanent pleaters,Pleaters"
    },
    {
        "id": 1731,
        "categoryId": 32,
        "subCategoryName": "Podiatry Supplies",
        "icon": null,
        "clues": "podiatry supplies,Podiatry Supplies,mobile podiatry,sports podiatry,podiatry"
    },
    {
        "id": 1732,
        "categoryId": 32,
        "subCategoryName": "Point of Sale (POS) Systems & Service",
        "icon": null,
        "clues": "displays--point of sale display systems,point of sale systems,point of sale hardware,point of purchase,Point of Sale (POS) Systems & Service,point of sale equipment,point of sale software,point of sale advertising,point of sale equipment & services,point of sale printers,point of sales,point of sale suppliers,point of sale merchandising"
    },
    {
        "id": 1733,
        "categoryId": 32,
        "subCategoryName": "Point of Sale Displays",
        "icon": null,
        "clues": "displays--point of sale display systems,point of sale displays,Point of Sale Displays,displays - point of sale,perspex point of sale displays,point of sales displays,displays--point of sale visual merchandising"
    },
    {
        "id": 1735,
        "categoryId": 32,
        "subCategoryName": "Pollution Consultants & Services",
        "icon": null,
        "clues": "pollution consultants & services,Pollution Consultants & Services,pollution consultants,air pollution consultants,envy pollution consultants,pollution,environmental &/or pollution consultants asbestos,pollution consultants air monitoring"
    },
    {
        "id": 1738,
        "categoryId": 32,
        "subCategoryName": "Pontoons",
        "icon": null,
        "clues": "bbq pontoons,party pontoons,Pontoons,pontoons,pontoons club"
    },
    {
        "id": 1743,
        "categoryId": 32,
        "subCategoryName": "Postage Meters",
        "icon": null,
        "clues": "postage,Postage Meters,postage meters"
    },
    {
        "id": 1748,
        "categoryId": 32,
        "subCategoryName": "Potteries",
        "icon": null,
        "clues": "Potteries,potteries"
    },
    {
        "id": 1759,
        "categoryId": 32,
        "subCategoryName": "Power Transmission Equipment & Supplies",
        "icon": null,
        "clues": "power transmission products,power transmission equipment & supplies,power equipment,power transmission,power transmission supplies,Power Transmission Equipment & Supplies,power transmission equipment"
    },
    {
        "id": 1765,
        "categoryId": 32,
        "subCategoryName": "Press Knives & Press Dies",
        "icon": null,
        "clues": "press knives & press dies,press brake,pressure cleaning,press printers,cleaning contractors--steam,pressure,chemical etc.,Press Knives & Press Dies,press tools,press knives,press studs,press agents"
    },
    {
        "id": 1768,
        "categoryId": 32,
        "subCategoryName": "Pressure Sensitive Adhesive Materials--W'salers & Mfrs",
        "icon": null,
        "clues": "pressure sensitive labels,Pressure Sensitive Adhesive Materials--W'salers & Mfrs,pressure sensitive adhesive materials - wholesalers & manufacturers,cleaning equipment--steam,pressure,chemical etc.,pressure cleaning,high pressure cleaning,pressure,pressure washers,cleaning contractors--steam,pressure,chemical etc.,cleaning--home pressure cleaning,pressure vessel manufacturers"
    },
    {
        "id": 1769,
        "categoryId": 32,
        "subCategoryName": "Pressure Vessels",
        "icon": null,
        "clues": "lpg & natural gas equipment & services pressure vessels,pressure vessels--industrial,cleaning equipment--steam,pressure,chemical etc.,pressure cleaning,high pressure cleaning,pressure,pressure washers,Pressure Vessels,cleaning contractors--steam,pressure,chemical etc.,cleaning--home pressure cleaning,pressure vessels"
    },
    {
        "id": 1770,
        "categoryId": 32,
        "subCategoryName": "Pre-Stressing Equipment & Supplies",
        "icon": null,
        "clues": "prestressing,pre-stressing equipment,Pre-Stressing Equipment & Supplies,pre-stressing equipment & supplies"
    },
    {
        "id": 1772,
        "categoryId": 32,
        "subCategoryName": "Printed Circuits",
        "icon": null,
        "clues": "Printed Circuits,printed circuits"
    },
    {
        "id": 1773,
        "categoryId": 32,
        "subCategoryName": "Printers & Printing Services",
        "icon": null,
        "clues": "Printers & Printing Services,printers-general web printing,printers-general offset printing,printers--digital canvas printing,printers--digital offset printing,services printers,printers-general instant printing,printers' supplies & services repairs,printers-general digital printing"
    },
    {
        "id": 1774,
        "categoryId": 32,
        "subCategoryName": "Printers' Supplies & Services",
        "icon": null,
        "clues": "laser printer services,printer ink supplies,printers' supplies & services,printer cartridge supplies,printer supplies & services,laser printer supplies,canon printer services,printer services & repair,printer repair services,Printers' Supplies & Services,epson printer services,computer printer services,brother printer services,printers' supplies & services repairs,computer equipment supplies printers,printer services centre,computer printer supplies,hp printer services,printer services"
    },
    {
        "id": 1776,
        "categoryId": 32,
        "subCategoryName": "Printers--Plastic &/or Flexographic",
        "icon": null,
        "clues": "Printers--Plastic &/or Flexographic,printers plastic,printers--plastic &/or flexographic,plastic bag printers"
    },
    {
        "id": 1777,
        "categoryId": 32,
        "subCategoryName": "Printing Consultants & Brokers",
        "icon": null,
        "clues": "printing consultants,Printing Consultants & Brokers,printing consultants & brokers"
    },
    {
        "id": 1778,
        "categoryId": 32,
        "subCategoryName": "Printing Engineers",
        "icon": null,
        "clues": "Printing Engineers,printing engineers"
    },
    {
        "id": 1779,
        "categoryId": 32,
        "subCategoryName": "Printing Ink",
        "icon": null,
        "clues": "Printing Ink,printing ink,screen printing ink suppliers,screen printing ink"
    },
    {
        "id": 1780,
        "categoryId": 32,
        "subCategoryName": "Printing Machinery",
        "icon": null,
        "clues": "printing machinery sales,printing machinery supplies,used printing machinery,screen printing machinery,printing machinery,Printing Machinery"
    },
    {
        "id": 1781,
        "categoryId": 32,
        "subCategoryName": "Private Detectives & Investigators",
        "icon": null,
        "clues": "private investigators,Private Detectives & Investigators"
    },
    {
        "id": 1783,
        "categoryId": 32,
        "subCategoryName": "Process Control Instruments",
        "icon": null,
        "clues": "trimec process control instruments,process control detector,process control consoles,process control instruments,process control gauges,fisher & paykel process control instruments,process control electrodes,process control monitors,process control scales,process control,process control remote displays,Process Control Instruments,process control meter,digital process control instrument,instruments--process control,process control engineers,contrec process control instruments"
    },
    {
        "id": 1784,
        "categoryId": 32,
        "subCategoryName": "Process Servers",
        "icon": null,
        "clues": "Process Servers,process servers"
    },
    {
        "id": 1785,
        "categoryId": 32,
        "subCategoryName": "Product & Industrial Designers",
        "icon": null,
        "clues": "Product & Industrial Designers,product & industrial designers,design product industrial"
    },
    {
        "id": 1787,
        "categoryId": 32,
        "subCategoryName": "Promotional Products",
        "icon": null,
        "clues": "promotional products drinkware & bar accessories,promotional products home & kitchen accessories,promotional products fashion accessories,promotional products toys & novelties,promotional products personal accessories,Promotional Products,promotional products,promotional products fridge magnets,promotional products supplier distributors,promotional products hats & caps,promotional products stubby holders,promotional products stationery & office supplies,promotional products screen printing,promotional products computer accessories"
    },
    {
        "id": 1788,
        "categoryId": 32,
        "subCategoryName": "Promotions Personnel",
        "icon": null,
        "clues": "promotions sales,Promotions Personnel,promotions agencies,promotions & advertising,promotions personnel"
    },
    {
        "id": 1792,
        "categoryId": 32,
        "subCategoryName": "Property Styling",
        "icon": null,
        "clues": "property styling,Property Styling"
    },
    {
        "id": 1793,
        "categoryId": 32,
        "subCategoryName": "Property Valuers",
        "icon": null,
        "clues": "property valuers,licensed property valuers,Property Valuers,sworn property valuers,independent property valuers,valuers property,residential property valuers,commercial property valuers,registered property valuers"
    },
    {
        "id": 1797,
        "categoryId": 32,
        "subCategoryName": "Protective Coatings",
        "icon": null,
        "clues": "coatings--protective,protective coatings,graffiti resistant protective coatings,Protective Coatings,floor coatings protective coatings,spray painting services protective coatings,graffiti protective coatings,graffiti coating protective coatings,protective,graffiti treatment protective coatings,industrial protective coatings"
    },
    {
        "id": 1802,
        "categoryId": 32,
        "subCategoryName": "Public Address (PA) Sound Systems",
        "icon": null,
        "clues": "public address systems hire,public address systems sound systems,public address,public address hire,electronic public address,public address systems,Public Address (PA) Sound Systems"
    },
    {
        "id": 1806,
        "categoryId": 32,
        "subCategoryName": "Public Notary",
        "icon": null,
        "clues": "notary public,solicitors notary public,Public Notary,public notary"
    },
    {
        "id": 1814,
        "categoryId": 32,
        "subCategoryName": "Pulleys",
        "icon": null,
        "clues": "Pulleys,timing pulleys,bearings & bushings pulleys,belts & pulleys,pulleys & belts,pulleys"
    },
    {
        "id": 1816,
        "categoryId": 32,
        "subCategoryName": "Pump Repairs & Repairers",
        "icon": null,
        "clues": "Pump Repairs & Repairers,pump repairers,pump repairers irrigation"
    },
    {
        "id": 1817,
        "categoryId": 32,
        "subCategoryName": "Pumping Contractors",
        "icon": null,
        "clues": "waste pumping,pumping equipment,concreters pumping,Pumping Contractors,pumping machine,soil pumping,pumping contractors"
    },
    {
        "id": 1819,
        "categoryId": 32,
        "subCategoryName": "Quality Assurance Services",
        "icon": null,
        "clues": "quality assurance services,quality assurance,Quality Assurance Services,quality assurance consultants,quality management services"
    },
    {
        "id": 1820,
        "categoryId": 32,
        "subCategoryName": "Quantity Surveyors",
        "icon": null,
        "clues": "quantity estimator,quantity surveyors tax depreciation,quantity surveyors depreciation schedules,surveyors quantity,quantity surveyors depreciation,quantity surveyors,Quantity Surveyors"
    },
    {
        "id": 1823,
        "categoryId": 32,
        "subCategoryName": "Quilting",
        "icon": null,
        "clues": "quilting,quilting lessons,quilting & embroidery store,quilting materials,quilting patchwork,quilting classes,quilting craft,quilting services,quilting machines,quilting craft shop,quilting fabric,crafts--retail & supplies quilting,quilting supplies,quilting shop,Quilting,quilting kits,patchwork & quilting"
    },
    {
        "id": 1824,
        "categoryId": 32,
        "subCategoryName": "Quilts & Bedspreads",
        "icon": null,
        "clues": "cleaning quilts,Quilts & Bedspreads,feather quilts,quilts & bedspreads"
    },
    {
        "id": 1827,
        "categoryId": 32,
        "subCategoryName": "Racquet Restringers & Repairers",
        "icon": null,
        "clues": "racquet restringers & repairers,racquet stringing,racquet,Racquet Restringers & Repairers,tennis racquet restring,racquet repair,racquet restring"
    },
    {
        "id": 1838,
        "categoryId": 32,
        "subCategoryName": "Raincoats",
        "icon": null,
        "clues": "raincoats,Raincoats,raincoats disposable"
    },
    {
        "id": 1840,
        "categoryId": 32,
        "subCategoryName": "Range Hoods",
        "icon": null,
        "clues": "Range Hoods,rangehood,range hoods installation,range hoods"
    },
    {
        "id": 1841,
        "categoryId": 32,
        "subCategoryName": "Rare & Unusual Plants (Nurseries-Retail)",
        "icon": null,
        "clues": "rare trees,Rare & Unusual Plants (Nurseries-Retail),rare & unusual plants (nurseries-retail)"
    },
    {
        "id": 1842,
        "categoryId": 32,
        "subCategoryName": "Razor & Industrial Blades Wholesalers & Manufacturers",
        "icon": null,
        "clues": "Razor & Industrial Blades Wholesalers & Manufacturers,razor & industrial blades - wholesalers & manufacturers"
    },
    {
        "id": 1850,
        "categoryId": 32,
        "subCategoryName": "Rechargeable Batteries & Dry Cell Battery",
        "icon": null,
        "clues": "batteries-dry cell & rechargeable computers,rechargeable batteries,batteries rechargeable,batteries-dry cell & rechargeable rebuilding,batteries-dry cell & rechargeable,Rechargeable Batteries & Dry Cell Battery"
    },
    {
        "id": 1851,
        "categoryId": 32,
        "subCategoryName": "Recording Equipment & Supplies",
        "icon": null,
        "clues": "sound recording equipment,Recording Equipment & Supplies,music recording equipment,audio recording equipment,recording equipment & supplies,recording equipment"
    },
    {
        "id": 1854,
        "categoryId": 32,
        "subCategoryName": "Rectifiers & Inverters",
        "icon": null,
        "clues": "rectifiers & inverters,Rectifiers & Inverters,rectifiers"
    },
    {
        "id": 1855,
        "categoryId": 32,
        "subCategoryName": "Recycled Clothing",
        "icon": null,
        "clues": "Recycled Clothing,recycled clothing wholesalers,recycled clothing opportunity shop,recycled clothing children's,recycled clothing vintage,recycled clothing boutiques,recycled clothing maternity,recycled clothing wholesale,recycled clothing shop,recycled clothing retro,recycled clothing"
    },
    {
        "id": 1856,
        "categoryId": 32,
        "subCategoryName": "Recycling & Waste Management Equipment",
        "icon": null,
        "clues": "recycling services ferrous metals,recycling services concrete,scrap metal merchants recycling,recycling,waste reduction & disposal services recycling,recycling services plastic,Recycling & Waste Management Equipment,recycling services paper,recycling services it equipment,recycling services waste"
    },
    {
        "id": 1857,
        "categoryId": 32,
        "subCategoryName": "Recycling Depots Beverage Containers ( S.A. & NT. Only )",
        "icon": null,
        "clues": "recycling depots--beverage containers (sa & nt),Recycling Depots Beverage Containers ( S.A. & NT. Only )"
    },
    {
        "id": 1858,
        "categoryId": 32,
        "subCategoryName": "Recycling Equipment",
        "icon": null,
        "clues": "Recycling Equipment,recycling equipment"
    },
    {
        "id": 1859,
        "categoryId": 32,
        "subCategoryName": "Recycling Services",
        "icon": null,
        "clues": "recycling services ferrous metals,recycling services concrete,recycling services pallets,recycling services,recycling services it equipment,Recycling Services,waste reduction & disposal services recycling,recycling services plastic,recycling services paper,recycling services waste,recycling services removal"
    },
    {
        "id": 1860,
        "categoryId": 32,
        "subCategoryName": "Reflective & Luminous Products",
        "icon": null,
        "clues": "reflective,reflective & luminous products,reflective traffic cones,reflective foil,reflective blinds,reflective tape,Reflective & Luminous Products"
    },
    {
        "id": 1865,
        "categoryId": 32,
        "subCategoryName": "Refrigeration Domestic & Retail",
        "icon": null,
        "clues": "refrigeration - commercial and industrial - retail and service,domestic refrigeration sales,refrigeration--domestic--repairs,refrigeration--commercial & industrial--retail & service cool rooms,refrigeration--commercial & industrial--retail & service,refrigeration retail,refrigeration--commercial & industrial--retail & service display fridges,refrigeration--domestic--repairs & service,refrigeration--commercial & industrial--retail & service freezers,refrigeration repair domestic,domestic refrigeration,refrigeration services domestic,refrigeration--domestic--repairs & service doors,refrigeration--commercial & industrial--retail & service freezer rooms,refrigeration--commercial & industrial--retail & service supermarkets,refrigeration--commercial & industrial--retail & service refrigeration engineers,refrigeration--commercial & industrial--retail & service repairs,domestic refrigeration repair,refrigeration--domestic--repairs & services second hand,refrigeration--domestic--repairs & service freezers,refrigeration commercial & industrial retail,refrigeration--commercial & industrial--retail & service display cabinets,refrigeration -- domestic -- retail,refrigeration--commercial & industrial--retail & service chest freezer,refrigeration--commercial & industrial--retail & service wine chiller,refrigeration--commercial & industrial--retail & service ice cream freezer,refrigeration--domestic--repairs & service westinghouse,commercial refrigeration,refrigeration--commercial & industrial--retail & service strip curtains,refrigeration domestic,refrigeration--domestic--repairs & service seals,Refrigeration Domestic & Retail,refrigeration--commercial & industrial--retail & service trucks,refrigeration--domestic--repairs & service refrigerators"
    },
    {
        "id": 1867,
        "categoryId": 32,
        "subCategoryName": "Refrigeration Parts & Equipment Wholesaler & Manufacturer",
        "icon": null,
        "clues": "refrigeration spare parts,refrigeration equipment & parts--w'salers & mfrs,refrigeration equipment,refrigeration parts,refrigeration equipment manufacturers,refrigeration equipment & parts wholesalers,Refrigeration Parts & Equipment Wholesaler & Manufacturer,refrigeration manufacturers,refrigeration equipment & parts,refrigeration wholesalers"
    },
    {
        "id": 1868,
        "categoryId": 32,
        "subCategoryName": "Regalia & Academic Wear",
        "icon": null,
        "clues": "Regalia & Academic Wear,regalia & academic wear,academic regalia,masonic regalia,regalia"
    },
    {
        "id": 1870,
        "categoryId": 32,
        "subCategoryName": "Registered Liquidators",
        "icon": null,
        "clues": "liquidators--registered,Registered Liquidators,registered liquidators,registered company liquidators"
    },
    {
        "id": 1873,
        "categoryId": 32,
        "subCategoryName": "Reiki",
        "icon": null,
        "clues": "reiki,Reiki,reiki healing"
    },
    {
        "id": 1874,
        "categoryId": 32,
        "subCategoryName": "Reinsurance",
        "icon": null,
        "clues": "Reinsurance,reinsurance"
    },
    {
        "id": 1875,
        "categoryId": 32,
        "subCategoryName": "Relaxation Therapy",
        "icon": null,
        "clues": "relaxation spa,alternative & natural therapies relaxation therapy,relaxation retreats,massage therapy relaxation massage,massage therapy relaxation,relaxation,relaxation centre,relaxation therapy,relaxation massage,Relaxation Therapy,relaxation services,relaxation massage therapy"
    },
    {
        "id": 1876,
        "categoryId": 32,
        "subCategoryName": "Relocation Consultants & Services",
        "icon": null,
        "clues": "international relocation consultants & services,relocation consultants & services,interstate relocation consultants & services,relocation consultants & services migration assistance,computer relocation consultants & services,executive relocation services,relocation assistance,relocation consultants & services safes,relocation consultants & services cultural training programs,house relocation services,relocation specialist,relocation consultants & services visas,relocation agencies,relocation consultants & services school search,relocation consultants & services real estate services,relocation houses,relocation consultants,relocation services,relocation migration assistance,industrial relocation services,relocation,relocation consultants & services equipment relocation,commerical relocation consultants & services,relocation consultants & services customs clearance,Relocation Consultants & Services,shipping container relocation consultants & services,relocation agents,relocation companies international,relocation consultants & services pet relocations,industrial relocation consultants & services,local relocation consultants & services"
    },
    {
        "id": 1879,
        "categoryId": 32,
        "subCategoryName": "Repetition Engineering",
        "icon": null,
        "clues": "Repetition Engineering,repetition engineers,repetition engineering,engineers repetition,repetition"
    },
    {
        "id": 1880,
        "categoryId": 32,
        "subCategoryName": "Rescue Services",
        "icon": null,
        "clues": "rescue helicopters,wildlife rescue,rescue equipment,rescue training,animal rescue,rescue services,rescue,Rescue Services"
    },
    {
        "id": 1882,
        "categoryId": 32,
        "subCategoryName": "Resellers (Computer Software)",
        "icon": null,
        "clues": "resellers (computer software & packages),computer resellers,mac resellers,apple computer resellers,Resellers (Computer Software),apple resellers,telecom resellers,it resellers,resellers,printer resellers,resellers (computer software),software resellers"
    },
    {
        "id": 1885,
        "categoryId": 32,
        "subCategoryName": "Resistors",
        "icon": null,
        "clues": "Resistors,resistors"
    },
    {
        "id": 1887,
        "categoryId": 32,
        "subCategoryName": "Resume Writing Services",
        "icon": null,
        "clues": "resume,resume services selection criteria,resume writing services,writing resume,Resume Writing Services,resume writing,employment services resume design,resume services"
    },
    {
        "id": 1888,
        "categoryId": 32,
        "subCategoryName": "Retail Insulation Materials",
        "icon": null,
        "clues": "artists materials--retail,insulation retail,insulation materials--retail batts,insulation materials--retail insulation solutions,dress materials retail,insulation materials--retail,insulation materials--retail csr,Retail Insulation Materials"
    },
    {
        "id": 1889,
        "categoryId": 32,
        "subCategoryName": "Retail/POS (Computer Software)",
        "icon": null,
        "clues": "Retail/POS (Computer Software),retail/pos (computer software & packages)"
    },
    {
        "id": 1895,
        "categoryId": 32,
        "subCategoryName": "Ribbon, Tape Wholesalers & Manufacturers",
        "icon": null,
        "clues": "ribbon manufacturers,tape recorders - wholesalers & manufacturers,ribbon wholesalers,tape dubbing,ribbon & trimming,ribbon printing,ribbon,tape recorders,tape packaging,tape supplies,ribbon supplies,tape recorders--retail,tape,tape recorders--retail & repairs,tape conversions,ribbon & craft,ribbon importers,Ribbon,Tape Wholesalers & Manufacturers,tape duplication,tape - tying - wholesalers & manufacturers,ribbon wholesale,ribbon shop"
    },
    {
        "id": 1904,
        "categoryId": 32,
        "subCategoryName": "Roadside Assistance",
        "icon": null,
        "clues": "roadside assistance,roadside,roadside assistance towing,Roadside Assistance"
    },
    {
        "id": 1905,
        "categoryId": 32,
        "subCategoryName": "Robots",
        "icon": null,
        "clues": "robots,Robots"
    },
    {
        "id": 1908,
        "categoryId": 32,
        "subCategoryName": "Roller Shutters",
        "icon": null,
        "clues": "roller doors & shutters,roller shutters & grilles pet doors,industrial roller shutters,roller shutters & grilles,security roller shutters,roller shutters,roller shutters & grilles repairs,commercial roller shutters,roller shutters & grilles emergency,Roller Shutters"
    },
    {
        "id": 1910,
        "categoryId": 32,
        "subCategoryName": "Rollers--Wooden, Metal, Rubber Etc.",
        "icon": null,
        "clues": "rubber manufacturers,Rollers--Wooden,Metal,Rubber Etc.,rollers--wooden,metal,rubber etc.,rubber flooring,rubber matting,rubber stamps,foam rubber,rubber"
    },
    {
        "id": 1916,
        "categoryId": 32,
        "subCategoryName": "Rope & Twine",
        "icon": null,
        "clues": "rope merchants,Rope & Twine,rope access,rope,rope & twine"
    },
    {
        "id": 1917,
        "categoryId": 32,
        "subCategoryName": "Roses",
        "icon": null,
        "clues": "roses nursery,Roses,roses nurseries,roses wholesale,roses"
    },
    {
        "id": 1918,
        "categoryId": 32,
        "subCategoryName": "Rotary Hoeing",
        "icon": null,
        "clues": "rotary hoeing,Rotary Hoeing"
    },
    {
        "id": 1921,
        "categoryId": 32,
        "subCategoryName": "Rubber Products",
        "icon": null,
        "clues": "rubber manufacturers,Rubber Products,Rollers--Wooden,Metal,Rubber Etc.,rubber flooring,rubber products - wholesalers,rubber matting,rubber stamps,foam rubber,rubber products,rubber products--retail,rubber,rubber products--w'salers & mfrs,rubber products manufacturers"
    },
    {
        "id": 1923,
        "categoryId": 32,
        "subCategoryName": "Rubbish Removal & Skip Bins",
        "icon": null,
        "clues": "mini skip rubbish,rubbish removers mimi skip bins,rubbish skip hire,bins rubbish,rubbish removers skip bins,rubbish removers building site bins,bulk rubbish bins,rubbish removalists,mini skip rubbish bins,waste bins rubbish,plastic rubbish bins,rubbish bins,rubbish removal man,commercial rubbish bins,rubbish removal domestic,rubbish skips,rubbish removals,rubbish removal skids,rubbish removal bins,rubbish removers bulk bins,rubbish removal furniture,industrial rubbish bins,skip rubbish bins,rubbish removal services,rubbish removal & disposal,rubbish wheelie bins,rubbish removal skips,rubbish bins skips,rubbish waste bins,skip rubbish removals,rubbish removal mini skips,skip hire rubbish removals,rubbish removers wheelie bins,rubbish skip bins,Rubbish Removal & Skip Bins"
    },
    {
        "id": 1926,
        "categoryId": 32,
        "subCategoryName": "Rugs",
        "icon": null,
        "clues": "Rugs,rugs"
    },
    {
        "id": 1927,
        "categoryId": 32,
        "subCategoryName": "Rural & Industrial Sheds",
        "icon": null,
        "clues": "rural contractors,rural merchandise,rural store,rural fencing,sheds--rural,rural gates,rural sheds,sheds--rural & industrial horse stables,Rural & Industrial Sheds,sheds--rural & industrial industrial,rural fencing supplies,sheds--rural & industrial,rural fencing contractors,rural supplies,rural produce"
    },
    {
        "id": 1928,
        "categoryId": 32,
        "subCategoryName": "Saddlery & Riding",
        "icon": null,
        "clues": "Saddlery & Riding,saddlery,saddlery repair,saddlery store,saddlery wholesale,saddlery import,saddlery & or riding outfitters,saddlery & riding outfitters,saddlery wholesalers,horse riding saddlery,saddlery supplies"
    },
    {
        "id": 1929,
        "categoryId": 32,
        "subCategoryName": "Safes & Vaults",
        "icon": null,
        "clues": "safes & strongroom doors removal,safes & strongroom door,safes opening,safes & strongroom doors firearms,safes,safes locksmiths,safes & strongroom doors,gun safes,Safes & Vaults"
    },
    {
        "id": 1930,
        "categoryId": 32,
        "subCategoryName": "Safety Deposit Services",
        "icon": null,
        "clues": "safety deposit services,Safety Deposit Services"
    },
    {
        "id": 1931,
        "categoryId": 32,
        "subCategoryName": "Safety Equipment & Accessories",
        "icon": null,
        "clues": "safety equipment hire,safety equipment & accessories boots,safety equipment,safety equipment & accessories footwear,Safety Equipment & Accessories,safety equipment-road or traffic,safety accessories,safety equipment & accessories,safety equipment & accessories roof access systems,safety equipment & accessories fire extinguishers,safety equipment & accessories rubber,safety equipment-road or traffic guard rails,safety equipment & accessories height safety,safety equipment & clothing,safety equipment & accessories fall protection"
    },
    {
        "id": 1932,
        "categoryId": 32,
        "subCategoryName": "Safety Glass",
        "icon": null,
        "clues": "safety glasses,safety glass,glass safety film,Safety Glass"
    },
    {
        "id": 1933,
        "categoryId": 32,
        "subCategoryName": "Safety Signs & Road Signs",
        "icon": null,
        "clues": "road safety barriers,road safety certificates,safety equipment-road or traffic,road safety,Safety Signs & Road Signs,consulting engineers road safety,road safety hire,safety signs & equipment,road safety signs,safety equipment-road or traffic guard rails,road safety equipment,safety signs,signs--safety & traffic,road worthy safety certificates"
    },
    {
        "id": 1934,
        "categoryId": 32,
        "subCategoryName": "Sailboarding Equipment & Supplies",
        "icon": null,
        "clues": "Sailboarding Equipment & Supplies,sailboarding equipment & supplies"
    },
    {
        "id": 1937,
        "categoryId": 32,
        "subCategoryName": "Sales Promotion & Incentive Consultants",
        "icon": null,
        "clues": "sales promotion & incentive consultants,promotions sales,sales promotions,sales consultants,Sales Promotion & Incentive Consultants"
    },
    {
        "id": 1938,
        "categoryId": 32,
        "subCategoryName": "Salespeople",
        "icon": null,
        "clues": "Salespeople,salespeople"
    },
    {
        "id": 1939,
        "categoryId": 32,
        "subCategoryName": "Saleyards",
        "icon": null,
        "clues": "Saleyards,saleyards"
    },
    {
        "id": 1940,
        "categoryId": 32,
        "subCategoryName": "Salt Damp Curers ( S.A. only )",
        "icon": null,
        "clues": "salt damp curers,salt damp repair,salt damp curers ( s.a. only ),Salt Damp Curers ( S.A. only ),salt damp removals,salt damp"
    },
    {
        "id": 1942,
        "categoryId": 32,
        "subCategoryName": "Sample Books & Cards",
        "icon": null,
        "clues": "Sample Books & Cards,sample books & cards"
    },
    {
        "id": 1943,
        "categoryId": 32,
        "subCategoryName": "Sand, Soil & Gravel Supplies",
        "icon": null,
        "clues": "sand,soil & gravel--retail pebbles,sand supplies,gravel quarries,sand & gravel,sand,soil & gravel--retail,Sand,Soil & Gravel Supplies,gravel pit,sand & soil,sand soil gravel,sand,gravel driveways,gravel rock,gravel supplies,gravel supply,gravel & pebbles,gravel & sand,soil supplies"
    },
    {
        "id": 1944,
        "categoryId": 32,
        "subCategoryName": "Sand, Soil & Gravel Wholesalers",
        "icon": null,
        "clues": "Sand,Soil & Gravel Wholesalers"
    },
    {
        "id": 1946,
        "categoryId": 32,
        "subCategoryName": "Satellite Equipment & Services",
        "icon": null,
        "clues": "satellite services,satellite installation services,satellite dish equipment,satellite equipment & services,satellite antennas services,satellite equipment,satellite tv services,satellite services & or equipment,satellite equipment & services satellite phone rental,satellite equipment & or services,satellite internet services providers,Satellite Equipment & Services,satellite tv equipment,television antenna services satellite dishes"
    },
    {
        "id": 1947,
        "categoryId": 32,
        "subCategoryName": "Sauna Baths",
        "icon": null,
        "clues": "sauna facilities,Sauna Baths,sauna,sauna baths,sauna rooms,sauna spa"
    },
    {
        "id": 1948,
        "categoryId": 32,
        "subCategoryName": "Sauna Equipment & Accessories",
        "icon": null,
        "clues": "Sauna Equipment & Accessories,sauna repair,sauna equipment,sauna equipment & accessories,sauna suppliers"
    },
    {
        "id": 1949,
        "categoryId": 32,
        "subCategoryName": "Sawdust & Shaving Suppliers",
        "icon": null,
        "clues": "sawdust or shaving suppliers,sawdust,sawdust & shavings suppliers,sawdust & shaving suppliers,sawdust or shavings suppliers,Sawdust & Shaving Suppliers"
    },
    {
        "id": 1952,
        "categoryId": 32,
        "subCategoryName": "Saws",
        "icon": null,
        "clues": "cold saws,power saws,Saws,saws,saws sharpening"
    },
    {
        "id": 1953,
        "categoryId": 32,
        "subCategoryName": "Scaffolding",
        "icon": null,
        "clues": "scaffolding,scaffolding guard rail,scaffolding planks,scaffolding contractors,Scaffolding,scaffolding suppliers,scaffolding training,scaffolding edge protection,scaffolding sales,scaffolding hire"
    },
    {
        "id": 1954,
        "categoryId": 32,
        "subCategoryName": "Scales & Weighing Equipment",
        "icon": null,
        "clues": "scales & weighing equipment,scales & weighing,weighing scales,scales,scales weights,scales calibration,Scales & Weighing Equipment"
    },
    {
        "id": 1955,
        "categoryId": 32,
        "subCategoryName": "Scarves & Ties Wholesalers & Manufacturers",
        "icon": null,
        "clues": "ties & scarves--w'salers & mfrs,scarves,Scarves & Ties Wholesalers & Manufacturers,scarves made"
    },
    {
        "id": 1957,
        "categoryId": 32,
        "subCategoryName": "School Supplies",
        "icon": null,
        "clues": "school supplies books,school supplies art,school book supplies,School Supplies,school supplies"
    },
    {
        "id": 1958,
        "categoryId": 32,
        "subCategoryName": "School Uniforms Suppliers & Wholesalers",
        "icon": null,
        "clues": "school uniforms suppliers,school uniforms--w'salers & mfrs,school uniforms manufacturers,school uniforms,School Uniforms Suppliers & Wholesalers,school uniforms - wholesalers & manufacturers,school uniforms shop"
    },
    {
        "id": 1959,
        "categoryId": 32,
        "subCategoryName": "School Uniforms--Retail",
        "icon": null,
        "clues": "School Uniforms--Retail,school uniforms--retail"
    },
    {
        "id": 1966,
        "categoryId": 32,
        "subCategoryName": "Scientific Instruments",
        "icon": null,
        "clues": "scientific testing,scientific equipment hire,scientific technical recruitment agencies,scientific glassware,scientific research,scientific supply,scientific glass,scientific instruments,scientific,scientific equipment supplies,scientific equipment,Scientific Instruments,instruments--scientific,scientific supplies,scientific laboratory,scientific analysis,scientific instrument hire,scientific laboratory supplies,scientific scales"
    },
    {
        "id": 1968,
        "categoryId": 32,
        "subCategoryName": "Scrapbooking",
        "icon": null,
        "clues": "Scrapbooking,scrapbooking party,scrapbooking craft,scrapbooking shop,scrapbooking supplies,scrapbooking warehouse,scrapbooking paper,scrapbooking wholesalers,scrapbooking & invitations,scrapbooking wholesale,scrapbooking classes"
    },
    {
        "id": 1969,
        "categoryId": 32,
        "subCategoryName": "Screen Printers",
        "icon": null,
        "clues": "screen printers textile,screen printers clothing,screen printers overlays,screen printers fascias,screen printers t shirts,Screen Printers,screen printers,screen printers membrane"
    },
    {
        "id": 1970,
        "categoryId": 32,
        "subCategoryName": "Screen Printing Supplies",
        "icon": null,
        "clues": "screen printing classes,silk screen supplies,silk screen printing supplies,screen printing equipment,screen printing shirts,screen printing t-shirt,screen printing machinery,screen printing embroidery,Screen Printing Supplies,screen printing clothing,screen printing ink,screen printing supply,t-shirt screen printing,screen printing business,screen printing ink suppliers,screen printing machines,screen printing,screen printing supplies"
    },
    {
        "id": 1971,
        "categoryId": 32,
        "subCategoryName": "Screening & Grading Machinery",
        "icon": null,
        "clues": "screening & grading machinery,wire mesh screening and grading machinery,Screening & Grading Machinery,screening equipment,screening plant"
    },
    {
        "id": 1972,
        "categoryId": 32,
        "subCategoryName": "Screening--Radio Frequency Interference",
        "icon": null,
        "clues": "screening--radio frequency interference,Screening--Radio Frequency Interference"
    },
    {
        "id": 1976,
        "categoryId": 32,
        "subCategoryName": "Scuba Gear",
        "icon": null,
        "clues": "scuba gear,scuba diving gear,Scuba Gear,scuba dive gear"
    },
    {
        "id": 1977,
        "categoryId": 32,
        "subCategoryName": "Sculptors",
        "icon": null,
        "clues": "ice sculptors,sculptors,Sculptors"
    },
    {
        "id": 1979,
        "categoryId": 32,
        "subCategoryName": "Seat Belts",
        "icon": null,
        "clues": "seat belts fitting,seat belts installers,seat belts repair,motor accessories--retail seat belts,car seat belts,seat belts & child restraints,seat belts fitting in vans,Seat Belts,seat belts installation,seat belts"
    },
    {
        "id": 1980,
        "categoryId": 32,
        "subCategoryName": "Seating & Seating Systems",
        "icon": null,
        "clues": "banquet seating,tiered seating,beam seating,seating hire,office seating,seating & seating systems,transport seating,grandstand seating,Seating & Seating Systems,seating,stadium seating,outdoor seating,event seating,seating systems,booth seating,commercial seating,aluminium seating,temporary seating"
    },
    {
        "id": 1981,
        "categoryId": 32,
        "subCategoryName": "Second Hand Books & Antiquarian",
        "icon": null,
        "clues": "second books,second hand books to sell,second hand goods,second hand timber,second hand school text books,second hand school books,Second Hand Books & Antiquarian,second hand academic books,second hand travel books,second hand book shop,second hand university books,second bookstore,second hand tyres,second hand computers,second hand text books,books second,second hand bookstore,second hand clothing,second hand shop,second hand books,second hand white goods,second hand appliances,second hand office furniture"
    },
    {
        "id": 1982,
        "categoryId": 32,
        "subCategoryName": "Second Hand Building Materials",
        "icon": null,
        "clues": "Second Hand Building Materials,second hand building equipment,second hand goods,second hand timber,second hand building materials,second hand building supplies,second building,second hand book shop,second hand tyres,building materials second,second hand computers,second hand materials,second hand bookstore,second hand clothing,second hand shop,second building materials,second hand books,second hand white goods,second hand appliances,second hand building,second hand office furniture,second hand building products"
    },
    {
        "id": 1983,
        "categoryId": 32,
        "subCategoryName": "Second Hand Cardboard Boxes & Cartons",
        "icon": null,
        "clues": "second hand book shop,second hand tyres,second hand boxes,second hand goods,second hand timber,second hand computers,second hand cartons,second hand bookstore,second hand clothing,second hand shop,second hand books,second hand white goods,Second Hand Cardboard Boxes & Cartons,second hand appliances,second hand office furniture"
    },
    {
        "id": 1984,
        "categoryId": 32,
        "subCategoryName": "Second Hand Dealers",
        "icon": null,
        "clues": "second hand tyres dealers,second hand goods,Second Hand Dealers,second hand timber,second hand tank dealers,second hand machinery dealers,second hand truck dealers,pawnbrokers second hand dealers,second hand dealers furniture,second hand furniture dealers,second hand book shop,second hand book dealers,second hand tyres,second hand goods dealers,second hand computers,second hand bookstore,second hand clothing,second hand shop,second hand books,second hand white goods,second hand appliances,second hand office furniture,second dealers,second hand car dealers"
    },
    {
        "id": 1985,
        "categoryId": 32,
        "subCategoryName": "Second Hand Furniture",
        "icon": null,
        "clues": "second hand goods,second hand business furniture,second hand timber,sell second hand furniture,second hand furniture sales,second hand baby furniture,baby furniture second hand,second hand furniture dealers,second hand book shop,second hand tyres,second hand furniture auctions,buy second hand furniture,second furniture,second hand furniture shop,second hand home furniture,second hand computers,second hand bookstore,second hand furniture,second hand clothing,second hand shop,second hand books,second hand white goods,second hand furniture buyers,second hand appliances,second hand furniture & whitegoods,Second Hand Furniture,second hand office furniture"
    },
    {
        "id": 1986,
        "categoryId": 32,
        "subCategoryName": "Second Hand Machinery",
        "icon": null,
        "clues": "second hand cnc machinery,second hand goods,second hand timber,Second Hand Machinery,second hand machinery dealers,second hand book shop,second hand tyres,second hand machinery sellers,second hand machinery,second hand computers,second hand bookstore,second hand clothing,second hand shop,second hand books,second hand white goods,second hand appliances,second hand farm machinery,second hand office furniture"
    },
    {
        "id": 1988,
        "categoryId": 32,
        "subCategoryName": "Security Doors, Windows & Equipment",
        "icon": null,
        "clues": "security doors,windows & equipment sliding,electronic security equipment,security equipment,security doors,windows & equipment screens,security doors,windows & equipment crimsafe,security doors,windows & equipment door locks,security doors,windows & equipment screen,security windows & door,security screens windows,security screens windows fabrication stainless,Security Doors,Windows & Equipment,security doors,windows & equipment window grilles,security doors & screens,pet doors security doors,security doors,windows & equipment aluminium,security doors,windows & equipment window screens,security doors,windows & equipment repairs,security doors,windows & equipment doors,security doors,windows & equipment sales,security doors,windows & equipment residential,security doors,windows & equipment,security doors windows,security guard equipment,security alarms equipment,security doors,windows & equipment installation,security windows"
    },
    {
        "id": 1990,
        "categoryId": 32,
        "subCategoryName": "Security Seals",
        "icon": null,
        "clues": "seals--security,Security Seals,security seals"
    },
    {
        "id": 1991,
        "categoryId": 32,
        "subCategoryName": "Security Shuttering Services",
        "icon": null,
        "clues": "security shuttering services,event security guard & patrol services,employment services security,security & proactive services,security alarms services,security guard services,security dog services,security training services security,Security Shuttering Services,security services,investment services social security,security guard & patrol services"
    },
    {
        "id": 1992,
        "categoryId": 32,
        "subCategoryName": "Security Systems",
        "icon": null,
        "clues": "security systems & consultants intercoms,security systems repair,security systems & consultants,security systems & consultants chubb security,security systems & security consultants,security systems & consultants surveillance,security systems recruitment consultants,security systems installers,security systems & consultants alarms,security systems & consultants access control,security systems & consultants monitoring,security systems installation,security systems & consultants cameras,security systems for consultants,security systems,security systems & consultants commercial,Security Systems"
    },
    {
        "id": 1994,
        "categoryId": 32,
        "subCategoryName": "Seed Cleaning, Drying & Grading",
        "icon": null,
        "clues": "Seed Cleaning,Drying & Grading,drying equipment & services--industrial,seed cleaning drying,freeze drying,freeze drying services,carpet drying,drying equipment,kiln drying,seed grading,flower drying,seed cleaning,drying & grading,seed cleaning & grading services,seed cleaning drying & grading services,grain drying"
    },
    {
        "id": 1996,
        "categoryId": 32,
        "subCategoryName": "Seed/s (Nurseries-Retail)",
        "icon": null,
        "clues": "seeds,seeds mining,Seed/s (Nurseries-Retail),seed/s (nurseries-retail)"
    },
    {
        "id": 1998,
        "categoryId": 32,
        "subCategoryName": "Self-Adhesive Labels",
        "icon": null,
        "clues": "labels--self-adhesive,Self-Adhesive Labels"
    },
    {
        "id": 2002,
        "categoryId": 32,
        "subCategoryName": "Service Stations",
        "icon": null,
        "clues": "motor service stations,Service Stations,service stations,automotive service stations,service stations tweed heads,service stations mobile services,shell service stations,service stations shell,service stations bp,motor service stations & garages,service stations mobil,mobil service stations"
    },
    {
        "id": 2006,
        "categoryId": 32,
        "subCategoryName": "Sewage & Waste Water Treatment",
        "icon": null,
        "clues": "waste water & sewage treatment,sewage pumps,sewage & waste water treatment,sewage treatment,sewage connections,sewage disposal,sewage waste,sewage systems,sewage services,sewage removals,sewage plumbers,sewage tanks,waste water & sewage treatment septic tanks,sewage cleaning,Sewage & Waste Water Treatment,sewage treatment systems,sewage treatment plant,sewage conversions,sewage & waste water,sewage,sewage recyclers"
    },
    {
        "id": 2007,
        "categoryId": 32,
        "subCategoryName": "Sewer Cleaning Equipment",
        "icon": null,
        "clues": "sewer camera,sewer pipe,sewer pumps,sewer blockage,sewer construction,sewer conversions,sewer & drainage,sewer location,sewer mains plumbers,Sewer Cleaning Equipment,sewer connection,sewer reticulation,sewer cleaning equipment,sewer cleaning,sewer plumbers"
    },
    {
        "id": 2010,
        "categoryId": 32,
        "subCategoryName": "Sewing Machine Repairs & Service",
        "icon": null,
        "clues": "Sewing Machine Repairs & Service,sewing machines-domestic,sewing machines--service,sewing machines-service & repairs,sewing machine retail,sewing services,sewing machine repair,sewing machine services"
    },
    {
        "id": 2011,
        "categoryId": 32,
        "subCategoryName": "Sewing Machines",
        "icon": null,
        "clues": "sewing machines,sewing machines retail,sewing machines-service & repairs,sewing machines-domestic horn,sewing machines parts,sewing machines-domestic,sewing machines--service,sewing machines sales,sewing machines repair,Sewing Machines,sewing machines--industrial,sewing machines pfaff,sewing machines & used"
    },
    {
        "id": 2012,
        "categoryId": 32,
        "subCategoryName": "Sewing Threads",
        "icon": null,
        "clues": "Sewing Threads,sewing threads"
    },
    {
        "id": 2014,
        "categoryId": 32,
        "subCategoryName": "Shade Sails",
        "icon": null,
        "clues": "Shade Sails,shade structures & sails,shade structure & sails,shade sails,outdoor shade sails,shade cloth sails,sun shade sails,poles for shade sails,shade sails & structures"
    },
    {
        "id": 2015,
        "categoryId": 32,
        "subCategoryName": "Shadecloth Supplies",
        "icon": null,
        "clues": "shadecloth blinds,Shadecloth Supplies,pool landscapes shadecloth,shadecloth supplies"
    },
    {
        "id": 2016,
        "categoryId": 32,
        "subCategoryName": "Share Registrars",
        "icon": null,
        "clues": "share accommodation,share market brokers,share farming,share trading,stock & share brokers,share registrars,share market,Share Registrars,share registry,share,share brokers,share house,share registrars ato reporting"
    },
    {
        "id": 2017,
        "categoryId": 32,
        "subCategoryName": "Sharpening Service",
        "icon": null,
        "clues": "sharpening tools wood,tool sharpening,sharpening scissors,blade sharpening,sharpening,sharpening blades,sharpening knives,sharpening services clippers,saws sharpening,sharpening services mobile services,saw sharpening,sharpening services,sharpening services knives,mobile sharpening services,scissor sharpening,knife sharpening,knife sharpening services,Sharpening Service,chainsaws sharpening,saw sharpening services,sharpening stones,sharpening tools"
    },
    {
        "id": 2018,
        "categoryId": 32,
        "subCategoryName": "Shavers",
        "icon": null,
        "clues": "Shavers,shavers"
    },
    {
        "id": 2019,
        "categoryId": 32,
        "subCategoryName": "Shavers -- Electric -- Repairs",
        "icon": null,
        "clues": "Shavers -- Electric -- Repairs,shavers -- electric -- repairs"
    },
    {
        "id": 2022,
        "categoryId": 32,
        "subCategoryName": "Sheepskin Products",
        "icon": null,
        "clues": "sheepskin products slippers,sheepskin products export,sheepskin covers,sheepskin products floor coverings,sheepskin products footwear,sheepskin products ugg boots,sheepskin products mittens,sheepskin products seat covers,sheepskin products medical,Sheepskin Products,sheepskin boots,sheepskin products,sheepskin products shearing shed,sheepskin coats,sheepskin rugs,sheepskin footwear,sheepskin slippers,sheepskin seat covers,sheepskin wholesalers,sheepskin products sheepskins,sheepskin products clothing,sheepskin products car seat covers,sheepskin,sheepskin products lambskins"
    },
    {
        "id": 2026,
        "categoryId": 32,
        "subCategoryName": "Shells--Marine",
        "icon": null,
        "clues": "shells--marine,Shells--Marine"
    },
    {
        "id": 2028,
        "categoryId": 32,
        "subCategoryName": "Shelving Solutions",
        "icon": null,
        "clues": "shelving systems,shelving hire,shelving & storage systems racks,shelving supplies,shelving units,shelving & storage,shelving racks,shelving retail,Shelving Solutions,shelving,shelving installation,shelving & storage systems,shelving manufacturers,shelving & storage systems lockers,shelving brackets,shelving used,shelving & racking"
    },
    {
        "id": 2029,
        "categoryId": 32,
        "subCategoryName": "Shelving Units & Racks",
        "icon": null,
        "clues": "Shelving Units & Racks,shelving storage racks,shelving & storage systems racks,shelving units,racks & shelving,shelving racks"
    },
    {
        "id": 2030,
        "categoryId": 32,
        "subCategoryName": "Shiatsu Therapy",
        "icon": null,
        "clues": "shiatsu therapy,shiatsu therapists,shiatsu massage,Shiatsu Therapy"
    },
    {
        "id": 2033,
        "categoryId": 32,
        "subCategoryName": "Ship Chandlers",
        "icon": null,
        "clues": "ship chandlers,Ship Chandlers"
    },
    {
        "id": 2034,
        "categoryId": 32,
        "subCategoryName": "Ship Provedores",
        "icon": null,
        "clues": "Ship Provedores,ship provedores"
    },
    {
        "id": 2036,
        "categoryId": 32,
        "subCategoryName": "Shoe Repairs",
        "icon": null,
        "clues": "shoe repairs,shoe repairs re-colouring,Shoe Repairs"
    },
    {
        "id": 2037,
        "categoryId": 32,
        "subCategoryName": "Shoe Shops & Stores",
        "icon": null,
        "clues": "dance shoes,children's shoes,shoe maker,shoe stores retail,shoe shop,Shoe Shops & Stores,bridal shoes,shoes - retail children,carpet shoe stores"
    },
    {
        "id": 2038,
        "categoryId": 32,
        "subCategoryName": "Shoes Online",
        "icon": null,
        "clues": "dance shoes,sports shoes,wedding shoes,shoes,ladies shoes,children's shoes,bridal shoes,shoes - retail children,Shoes Online,shoes - retail,shoe"
    },
    {
        "id": 2041,
        "categoryId": 32,
        "subCategoryName": "Shopping Centres & Malls",
        "icon": null,
        "clues": "shopping centres,shopping centres cinema,malls & shopping centre,Shopping Centres & Malls,shopping malls,shopping centres bunnings,shopping centres flower power,general shopping malls,shopping centres safeway,shopping centres kfc,shopping centres bing lee"
    },
    {
        "id": 2043,
        "categoryId": 32,
        "subCategoryName": "Show Cases & Glass Counters",
        "icon": null,
        "clues": "kitchens-renovations & equipment showroom,shower,show cases & glass counters,showgrounds,Show Cases & Glass Counters"
    },
    {
        "id": 2044,
        "categoryId": 32,
        "subCategoryName": "Shower Screens",
        "icon": null,
        "clues": "frameless shower screens,glass shower screens,shower screens repairs,shower screens frameless,Shower Screens,shower screens,bathroom renovations shower screens"
    },
    {
        "id": 2045,
        "categoryId": 32,
        "subCategoryName": "Shutters & Louvres",
        "icon": null,
        "clues": "shutters & louvres vinyl,shutters & louvres pine,shutters & louvres venetian,shutters & louvres security,shutters & louvres plantation,roller shutters & grilles,shutters,shutters & louvres automatic,shutters & louvres,shutters & louvres ventilation,shutters & louvres sun louvres,shutters & louvres rolling,shutters & louvres sliding,plantation shutters,Shutters & Louvres,shutters & louvres internal,shutters & louvres external,shutters & louvres vergola,window shutters,shutters manufacturers,shutters & louvres acoustic,shutters & blinds,shutters & louvres commercial,shutters & louvres electric,shutters & louvres aluminium,shutters & louvres just blinds"
    },
    {
        "id": 2047,
        "categoryId": 32,
        "subCategoryName": "Signwriters Supplies",
        "icon": null,
        "clues": "signwriters digital print,signwriters airbrushing,signwriters signboards,signwriters' supplies,signwriters vinyl graphics,signwriters banners,signwriters screen printing,signwriters corflute,signwriters graphic design,signwriters engraving,signwriters motor vehicles,signwriters vehicle,signwriters digital printing,signwriters vehicle wrap,Signwriters Supplies,signwriters vinyl"
    },
    {
        "id": 2049,
        "categoryId": 32,
        "subCategoryName": "Silks",
        "icon": null,
        "clues": "silks wholesale distributors,india silks,silks,Silks"
    },
    {
        "id": 2051,
        "categoryId": 32,
        "subCategoryName": "Silver Plating",
        "icon": null,
        "clues": "Silver Plating,silver plating,silver jewellery,silver,silver jewellers"
    },
    {
        "id": 2052,
        "categoryId": 32,
        "subCategoryName": "Silver Reclamation--Photographic",
        "icon": null,
        "clues": "Silver Reclamation--Photographic,silver reclamation--photographic"
    },
    {
        "id": 2054,
        "categoryId": 32,
        "subCategoryName": "Sinks",
        "icon": null,
        "clues": "laundry sinks,Sinks,sinks,sinks & taps,baths sinks,kitchens sinks,portable sinks,pedestal sinks,bathroom sinks,sinks repair,commercial sinks,sinks trader,sinks polishing,kitchen sinks,stainless steel sinks"
    },
    {
        "id": 2055,
        "categoryId": 32,
        "subCategoryName": "Sirens & Warning Lights",
        "icon": null,
        "clues": "sirens hooters,Sirens & Warning Lights,sirens,hooters & warning lights"
    },
    {
        "id": 2056,
        "categoryId": 32,
        "subCategoryName": "Ski & Snowboard Equipment",
        "icon": null,
        "clues": "ski equipment,bonfire snow ski equipment,spy snow ski equipment,ski equipment for hire,ski equipment wholesalers & manufacturers,ski equipment sales,ski equipment retail,ski & snowboard hire,ski equipment and clothing,snow ski equipment hire,water ski equipment,snow ski equipment repairs,childrens snow ski equipment,ski equipment liquidators,snowboard ski,snow ski equipment,ski snowboard warehouse,ski & snowboard,ski equipment stores,sos snow ski equipment,Ski & Snowboard Equipment,adult snow ski equipment,ski & snowboard equipment,quiksilver snow ski equipment"
    },
    {
        "id": 2062,
        "categoryId": 32,
        "subCategoryName": "Skylights",
        "icon": null,
        "clues": "skylights installation,skylights solatube,solatube skylights,roof construction skylights,Skylights,skylights"
    },
    {
        "id": 2067,
        "categoryId": 32,
        "subCategoryName": "Slicers",
        "icon": null,
        "clues": "Slicers,meat slicers,slicers"
    },
    {
        "id": 2068,
        "categoryId": 32,
        "subCategoryName": "Slot Cars & Equipment",
        "icon": null,
        "clues": "Slot Cars & Equipment,slot cars,slot cars and equipment,slot cars & equipment"
    },
    {
        "id": 2072,
        "categoryId": 32,
        "subCategoryName": "Soaps & Detergents",
        "icon": null,
        "clues": "Soaps & Detergents,soaps & detergents"
    },
    {
        "id": 2082,
        "categoryId": 32,
        "subCategoryName": "Solar Energy",
        "icon": null,
        "clues": "solar energy,solar energy industrial,solar energy equipment insulation,solar power,solar pool heating,solar water heater,solar,solar hot water,solar hot water systems,solar heating,hot water systems solar hot water,solar energy equipment grid systems,Solar Energy,solar energy equipment panels,solar energy equipment power systems,solar energy equipment pool heating,solar energy equipment,solar panels,solar energy equipment electrical contractors,solar energy equipment solar installers"
    },
    {
        "id": 2083,
        "categoryId": 32,
        "subCategoryName": "Soldering Equipment & Services",
        "icon": null,
        "clues": "Soldering Equipment & Services,soldering services,soldering equipment & services,soldering equipment & supplies,soldering equipment"
    },
    {
        "id": 2088,
        "categoryId": 32,
        "subCategoryName": "Souvenirs",
        "icon": null,
        "clues": "australian souvenirs,souvenirs suppliers,souvenirs outlets,souvenirs shop,souvenirs importers,souvenirs wholesale,souvenirs,souvenirs distributors,Souvenirs"
    },
    {
        "id": 2089,
        "categoryId": 32,
        "subCategoryName": "Spas & Hot Tubs",
        "icon": null,
        "clues": "spas,hot tubs & equipment spa pools,spas,hot tubs & equipment outdoor spa,spas,hot tubs & equipment,hotspring spas,spas & pools,spas hot tubs,hot tubs spas & whirlpool,spas beauty salon,spas,spas & swimming pools,spas,hot tubs & equipment covers,spas fibreglass,Spas & Hot Tubs"
    },
    {
        "id": 2093,
        "categoryId": 32,
        "subCategoryName": "Speed Variation Equipment",
        "icon": null,
        "clues": "speed variation equipment,speed equipment,speed queen equipment sales,Speed Variation Equipment"
    },
    {
        "id": 2094,
        "categoryId": 32,
        "subCategoryName": "Speedometers",
        "icon": null,
        "clues": "speedometers,Speedometers"
    },
    {
        "id": 2096,
        "categoryId": 32,
        "subCategoryName": "Splashbacks",
        "icon": null,
        "clues": "splashbacks,benchtops kitchens splashbacks,Splashbacks,splashbacks producers,kitchens-renovations & equipment splashbacks,glass merchants & glaziers splashbacks,kitchen splashbacks,kitchen glass splashbacks,stainless steel splashbacks,splashbacks glass,glass kitchen splashbacks,glass splashbacks,acrylic splashbacks"
    },
    {
        "id": 2097,
        "categoryId": 32,
        "subCategoryName": "Sponges",
        "icon": null,
        "clues": "sponges,Sponges"
    },
    {
        "id": 2101,
        "categoryId": 32,
        "subCategoryName": "Sports Cards & Memorabilia",
        "icon": null,
        "clues": "sports cards & memorabilia,sports memorabilia framing,Sports Cards & Memorabilia,sports memorabilia,sports trading cards,sports memorabilia shop,sports cards"
    },
    {
        "id": 2103,
        "categoryId": 32,
        "subCategoryName": "Sports Equipment, Goods & Repairs",
        "icon": null,
        "clues": "sports equipment sales,sports equipment,sports injury equipment,sports goods importers,sports goods wholesalers.distributers,sports medical equipment,Sports Equipment,Goods & Repairs,sports equipment hire,sports equipment suppliers,water sports equipment,hire sports equipment,manufacturers of sports goods"
    },
    {
        "id": 2111,
        "categoryId": 32,
        "subCategoryName": "Sportswear & Activewear Wholesalers & Manufacturers",
        "icon": null,
        "clues": "Sportswear & Activewear Wholesalers & Manufacturers,sportswear-men's-wholesalers & manufacturers,sportswear - women's - wholesalers & manufacturers,sportswear uniforms,sportswear wholesalers,sportswear,sportswear - men's - wholesalers,sportswear manufacturers,sportswear - wholesalers & manufacturers,sportswear suppliers,sportswear retailer,sportswear men's manufacturers,sportswear - men's - wholesalers & manufacturers printing,sportswear--retail footwear,sportswear - men's - wholesalers & manufacturers team wear"
    },
    {
        "id": 2112,
        "categoryId": 32,
        "subCategoryName": "Sportswear--Retail",
        "icon": null,
        "clues": "sportswear--retail,sportswear retailer,Sportswear--Retail,sportswear--retail footwear"
    },
    {
        "id": 2115,
        "categoryId": 32,
        "subCategoryName": "Spray Tanning",
        "icon": null,
        "clues": "Spray Tanning,spray tanning salon,spray tanning &/or solariums,beauty salons with spray tanning,mobile spray tanning"
    },
    {
        "id": 2118,
        "categoryId": 32,
        "subCategoryName": "Spring Water Supply & Accessories",
        "icon": null,
        "clues": "Spring Water Supply & Accessories,spring water suppliers,natural spring water,spring water delivery,spring water supplies & accessories,spring racing fashion accessories,natural spring water suppliers,bottled spring water,spring water,spring water bottlers"
    },
    {
        "id": 2124,
        "categoryId": 32,
        "subCategoryName": "Stainless Steel Products & Equipment Suppliers",
        "icon": null,
        "clues": "stainless products,stainless steel,stainless steel handrails,stainless steel products & equipment tanks,stainless steel wire products,stainless steel products,stainless steel supplies,stainless steel products & equipment tubing,stainless steel products & equipment benches,stainless steel products & equipment handrails,manufacturers stainless steel products,stainless steel pipe suppliers,Stainless Steel Products & Equipment Suppliers,stainless steel pipe,stainless steel merchants,stainless steel products & equipment,stainless steel manufacturers,stainless steel balustrades,stainless steel tube,stainless steel tube suppliers,stainless steel fabrication,stainless steel equipment,stainless steel benchtops,security doors,windows & equipment stainless steel mesh,stainless steel fasteners,steel merchants stainless steel,stainless steel products & equipment pipes,stainless steel sheet suppliers"
    },
    {
        "id": 2125,
        "categoryId": 32,
        "subCategoryName": "Staircases & Handrails",
        "icon": null,
        "clues": "spiral staircases,staircases,Staircases & Handrails,staircases & handrails,timber staircases"
    },
    {
        "id": 2126,
        "categoryId": 32,
        "subCategoryName": "Stamp Collectors & Dealers",
        "icon": null,
        "clues": "Stamp Collectors & Dealers,stamp dealers international,coin & stamp dealers,stamp & coin dealers,stamp dealers"
    },
    {
        "id": 2127,
        "categoryId": 32,
        "subCategoryName": "Staples & Stapling Equipment",
        "icon": null,
        "clues": "staples,Staples & Stapling Equipment,staples & stapling equipment"
    },
    {
        "id": 2128,
        "categoryId": 32,
        "subCategoryName": "Static Electricity Control",
        "icon": null,
        "clues": "Static Electricity Control,static electricity control,static electricity"
    },
    {
        "id": 2129,
        "categoryId": 32,
        "subCategoryName": "Stationery",
        "icon": null,
        "clues": ""
    },
    {
        "id": 2136,
        "categoryId": 32,
        "subCategoryName": "Stencils",
        "icon": null,
        "clues": "stencils,Stencils,paint stencils"
    },
    {
        "id": 2145,
        "categoryId": 32,
        "subCategoryName": "Storage Solutions",
        "icon": null,
        "clues": "furniture removals & storage,storage facilities,storage--general self storage,storage--general mobile service,storage,storage--general,furniture removals & storage local,furniture removals & storage interstate,self storage,water storage solutions,Storage Solutions,storage units,plastic storage solutions"
    },
    {
        "id": 2146,
        "categoryId": 32,
        "subCategoryName": "Storage--Liquid--Bulk",
        "icon": null,
        "clues": "Storage--Liquid--Bulk,storage--liquid--bulk"
    },
    {
        "id": 2147,
        "categoryId": 32,
        "subCategoryName": "Stored Value Card Services",
        "icon": null,
        "clues": "stored value card services,Stored Value Card Services"
    },
    {
        "id": 2148,
        "categoryId": 32,
        "subCategoryName": "Stove & Rangehood Parts & Service",
        "icon": null,
        "clues": "stoves,stove elements,gas stove spare parts,Stove & Rangehood Parts & Service,chef stove replacement parts,stove repair,stove installation,stove replacement parts,stove services,stove spare parts,gas stove parts,omega stove parts,stove,electric stove parts,gas stove services,chef stove parts,stove parts"
    },
    {
        "id": 2149,
        "categoryId": 32,
        "subCategoryName": "Stoves & Ranges",
        "icon": null,
        "clues": "stoves & ranges - service & parts,stoves,stoves smeg,stoves & ranges--service & parts blanco,stoves & ranges - whsalers & manufacturers,stoves delonghi,stoves & ranges--service & parts gas,stoves & ranges--service & parts westinghouse,stoves & ranges--service & parts spare parts,stoves seconds,stoves & ranges - retail,stoves repair,stoves & ranges--service & parts heating elements,stoves & ranges--service & parts installation,stoves & ranges--service & parts bosch,stoves & ranges,stoves & ranges--service & parts ovens,stoves & ovens,stoves retail,Stoves & Ranges,stoves ilve,stoves & ranges--service & parts chef,stoves parts,stoves & ranges - wholesalers & manufacturers"
    },
    {
        "id": 2153,
        "categoryId": 32,
        "subCategoryName": "Suits",
        "icon": null,
        "clues": "sumo suits,wedding suits,suits men's,suits accessories for hire,men's suits,menswear--retail suits,suits,Suits"
    },
    {
        "id": 2154,
        "categoryId": 32,
        "subCategoryName": "Sun Protection Products",
        "icon": null,
        "clues": "sun protection products,Sun Protection Products,sun protection"
    },
    {
        "id": 2155,
        "categoryId": 32,
        "subCategoryName": "Sunglasses",
        "icon": null,
        "clues": "sunglasses,sunglasses dragon,sunglasses wholesalers,sunglasses repair,sunglasses shop,optometrists sunglasses,sunglasses wholesale,Sunglasses"
    },
    {
        "id": 2157,
        "categoryId": 32,
        "subCategoryName": "Supermarkets & Grocery Stores",
        "icon": null,
        "clues": "Supermarkets & Grocery Stores,supermarkets & grocery stores japanese,supermarkets & grocery stores,art supplies supermarkets,supermarkets & grocery stores indian,chinese supermarkets & grocery stores"
    },
    {
        "id": 2158,
        "categoryId": 32,
        "subCategoryName": "Surfboards, Surfing Equipment Wholesalers & Manufacturers",
        "icon": null,
        "clues": "surfing equipment & accessories - wholesalers & manufacturers surfboards,Surfboards,Surfing Equipment Wholesalers & Manufacturers,used surfboards,surfboards,surfing equipment & supplies - wholesalers & manufacturers"
    },
    {
        "id": 2159,
        "categoryId": 32,
        "subCategoryName": "Surfing Equipment & Accessories--Retail",
        "icon": null,
        "clues": "surfing equipment & accessories--retail surfwear,surfing equipment & accessories - wholesalers & manufacturers surfboards,surfing equipment,surfing equipment & accessories--w'salers & mfrs,surfing equipment & accessories--retail billabong,surfing equipment & accessories--retail,surfing equipment & accessories--retail wetsuits,surfing equipment & accessories,Surfing Equipment & Accessories--Retail,surfing equipment supplies wholesale manufacturers,surfing equipment & supplies--retail,Surfboards,Surfing Equipment Wholesalers & Manufacturers,surfing equipment & accessories--retail surf boards,surfing equipment & accessories--retail board bags,surfing equipment & supplies - wholesalers & manufacturers,surfing equipment & supplies"
    },
    {
        "id": 2164,
        "categoryId": 32,
        "subCategoryName": "Surplus Merchandise Brokers",
        "icon": null,
        "clues": "Surplus Merchandise Brokers,surplus merchandise brokers"
    },
    {
        "id": 2165,
        "categoryId": 32,
        "subCategoryName": "Surveyors' Equipment & Supply",
        "icon": null,
        "clues": "Surveyors' Equipment & Supply,surveyors' equipment & supplies"
    },
    {
        "id": 2169,
        "categoryId": 32,
        "subCategoryName": "Sweeping Machines & Services",
        "icon": null,
        "clues": "sweeping machines & services,Sweeping Machines & Services,sweeping machines,power sweeping & scrubbing services,sweeping services,surrounding sweeping machines"
    },
    {
        "id": 2173,
        "categoryId": 32,
        "subCategoryName": "Swimming Pool Maintenance & Repairs",
        "icon": null,
        "clues": "swimming pool covers,swimming pool shop,swimming pool equipment & chemicals,swimming maintenance & repair,swimming pool services,swimming pool equipment,swimming pool builders,swimming pool maintenance,swimming pool maintenance & repair,Swimming Pool Pumps,Accessories & Supplies,swimming pool construction,aquatic centre,swimming pool supplies,swimming pool chemicals,swimming pool fencing contractors,swimming centre,Swimming Pool Maintenance & Repairs,swimming pool light repairs"
    },
    {
        "id": 2174,
        "categoryId": 32,
        "subCategoryName": "Swimming Pool Pumps, Accessories & Supplies",
        "icon": null,
        "clues": "swimming pool accessories,swimming pool pumps,swimming pool filter & pumps,swimming pool equipment & chemicals,swimming pool accessories & chemicals,Air Tools Accessories,Supply & Service,swimming pool services,swimming pool maintenance,swimming pool maintenance & repair,swimming pool equipment & supplies,swimming pool construction,swimming accessories,swimming pool chemicals,swimming pool fencing contractors,swimming pool davey pumps,swimming supplies,Baby Prams,Furniture & Accessories,swimming pool covers,swimming pool shop,swimming pool equipment,swimming pool builders,Concrete Formwork,Form Ties & Accessories,Swimming Pool Pumps,Accessories & Supplies,aquatic centre,swimming pool supplies,swimming centre"
    },
    {
        "id": 2176,
        "categoryId": 32,
        "subCategoryName": "Swimming Pools",
        "icon": null,
        "clues": "in ground swimming pools,swimming pools & spas,swimming pools leisure centres,public swimming pools,Swimming Pools,above ground swimming pools,fibreglass swimming pools,swimming pools,swimming pools aquatic centres,swimming pools aquaerobics,swimming pools swimming lessons,swimming pools hydrotherapy,indoor swimming pools,swimming pools steel,concrete swimming pools,swimming pools & spa"
    },
    {
        "id": 2177,
        "categoryId": 32,
        "subCategoryName": "Swimwear",
        "icon": null,
        "clues": ""
    },
    {
        "id": 2180,
        "categoryId": 32,
        "subCategoryName": "Synthetic Grass & Turf Sport Surfaces",
        "icon": null,
        "clues": "fake grass,artificial grass,landscape contractors & designers synthetic grass,sporting surfaces--synthetic multipurpose courts,lawn & turf supplies synthetic grass,synthetic turf bowling green construction,synthetic grass tennis courts,synthetic turf landscapers,synthetic surfaces,synthetic turf,synthetic grass,astro synthetic turf,synthetic turf sporting surfaces,synthetic grass suppliers,Synthetic Grass & Turf Sport Surfaces,sporting surfaces--synthetic,synthetic sporting surfaces,sporting surfaces--synthetic synthetic grass,synthetic turf suppliers"
    },
    {
        "id": 2181,
        "categoryId": 32,
        "subCategoryName": "T.A.B.'s",
        "icon": null,
        "clues": "spring racing tabs,T.A.B.'s,t.a.b.'s"
    },
    {
        "id": 2184,
        "categoryId": 32,
        "subCategoryName": "Tailors--Ladies'",
        "icon": null,
        "clues": "tailors--ladies',Tailors--Ladies'"
    },
    {
        "id": 2185,
        "categoryId": 32,
        "subCategoryName": "Tailors-Men's",
        "icon": null,
        "clues": "tailors--mens,tailors-men's suits,tailors-men's,Tailors-Men's"
    },
    {
        "id": 2186,
        "categoryId": 32,
        "subCategoryName": "Tallow Merchants",
        "icon": null,
        "clues": "Tallow Merchants,tallow,tallow merchants"
    },
    {
        "id": 2191,
        "categoryId": 32,
        "subCategoryName": "Tanning Equipment",
        "icon": null,
        "clues": "Tanning Equipment,solarium & tanning equipment,tanning equipment"
    },
    {
        "id": 2193,
        "categoryId": 32,
        "subCategoryName": "Tap Wholesalers & Manufacturers",
        "icon": null,
        "clues": "Tap Wholesalers & Manufacturers,tap wholesalers,tap manufacturers"
    },
    {
        "id": 2194,
        "categoryId": 32,
        "subCategoryName": "Tapestries",
        "icon": null,
        "clues": "tapestries,Tapestries"
    },
    {
        "id": 2196,
        "categoryId": 32,
        "subCategoryName": "Tarpaulins",
        "icon": null,
        "clues": "Tarpaulins,tarpaulins repair,tarpaulins truck,truck tarpaulins,canvas tarpaulins,tarpaulins hire,tarpaulins canvas,tarpaulins tent,ute tarpaulins,tarpaulins manufacturers,poly tarpaulins,tarpaulins"
    },
    {
        "id": 2198,
        "categoryId": 32,
        "subCategoryName": "Tattooist & Tattoo Shop",
        "icon": null,
        "clues": "Tattooist & Tattoo Shop,tattooist,cosmetic tattooist,maori tattooist,tattooing female tattooists"
    },
    {
        "id": 2205,
        "categoryId": 32,
        "subCategoryName": "Tea Suppliers",
        "icon": null,
        "clues": "tea,tea room,Tea Suppliers,tea suppliers,tea shop"
    },
    {
        "id": 2213,
        "categoryId": 32,
        "subCategoryName": "Telemarketing",
        "icon": null,
        "clues": "telemarketing call centre,telemarketing jobs,telemarketing bureaus,telemarketing services,telemarketing outbound call centre,telemarketing centre,Telemarketing,telemarketing customer service,telemarketing"
    },
    {
        "id": 2214,
        "categoryId": 32,
        "subCategoryName": "Telephone Answering Services",
        "icon": null,
        "clues": "telephone answering services,telephone answering,mobile telephones,telephone installation,mobile telephones & accessories vodafone,telephones & systems--installation & maintenance,telephones & accessories telstra,telephone answer services,Telephone Installation,Repairs & Maintenance,Telephone Answering Services,mobile telephones & accessories,telephone,telephone systems,telephone interpreter services,telephone answering & call diversion equipment,telephone systems services,mobile telephones & accessories optus,telephone technicians,telephone services providers,mobile telephone services,telephone answering & message services,telephone cleansing services"
    },
    {
        "id": 2215,
        "categoryId": 32,
        "subCategoryName": "Telephone Installation, Repairs & Maintenance",
        "icon": null,
        "clues": "telephone maintenance,mobile telephones,mobile telephone installation,telephone installation & maintenance,telephones & systems--installation & maintenance,telephone & systems installation & or maintenance,Telephone Installation,Repairs & Maintenance,telephone line installation,telephone,telephone systems maintenance,telephone technicians,telephone line maintenance,telephone installation contractors,Computer Repairs,Service & Upgrades,telephone cable installation,telephone installation,telephone lines installation,mobile telephones & accessories vodafone,telephones & accessories telstra,mobile telephones & accessories,telephone systems,mobile telephones & accessories optus,telephone systems installation,Camera Shops,Photography Equipment & Repairs"
    },
    {
        "id": 2216,
        "categoryId": 32,
        "subCategoryName": "Telephone Recorded Information Services",
        "icon": null,
        "clues": "Telephone Recorded Information Services,mobile telephones,telephone installation,mobile telephones & accessories vodafone,telephones & systems--installation & maintenance,telephones & accessories telstra,telephone answer services,telephone recorded,Telephone Installation,Repairs & Maintenance,mobile telephones & accessories,telephone,telephone systems,telephone interpreter services,telephone systems services,mobile telephones & accessories optus,telephone technicians,telephone recorded information services,telephone services providers,mobile telephone services,telephone answering & message services,telephone cleansing services"
    },
    {
        "id": 2217,
        "categoryId": 32,
        "subCategoryName": "Telephone Services",
        "icon": null,
        "clues": "mobile telephones,telephone installation,mobile telephones & accessories vodafone,Telephone Services,telephones & systems--installation & maintenance,telephones & accessories telstra,telephone answer services,Telephone Installation,Repairs & Maintenance,mobile telephones & accessories,telephone,telephone systems,telephone interpreter services,telephone systems services,mobile telephones & accessories optus,telephone technicians,telephone services providers,mobile telephone services,telephone services,telephone answering & message services,telephone cleansing services"
    },
    {
        "id": 2218,
        "categoryId": 32,
        "subCategoryName": "Telephone Systems & Equipment",
        "icon": null,
        "clues": "second hand telephone systems,mobile telephones,second hand telephone equipment,telephone installation,Telephone Systems & Equipment,telephone business systems,mobile telephones & accessories vodafone,business telephone systems,telephone systems & equipment,telephones & systems--installation & maintenance,telephone & systems installation & or maintenance,telephones & accessories telstra,nec telephone systems,Telephone Installation,Repairs & Maintenance,telephone systems & equipment cabling,mobile telephones & accessories,telephone,telephone systems,telephone systems services,telephone answering & call diversion equipment,mobile telephones & accessories optus,telephone systems & equipment systems integration,telephone systems maintenance,telephone equipment,telephone & communication systems,telephone systems installation,telephone technicians,samsung telephone systems"
    },
    {
        "id": 2219,
        "categoryId": 32,
        "subCategoryName": "Telephones & Accessories",
        "icon": null,
        "clues": "mobile telephones & accessories telstra corporation,mobile telephones,mobile telephones & accessories car kits,mobile telephones & accessories telstra mobile dealer,mobile telephones-repairs & service,mobile telephones & accessories vodafone,telephones & systems--installation & maintenance cabling,telephones & systems--installation & maintenance,telephones & accessories retail,mobile telephones & accessories mobile accessories,Telephones & Accessories,telephones & accessories telstra,telephones & accessories mobile phones,mobile telephones & accessories batteries,mobile telephones & accessories repairs,telephones & accessories,mobile telephones & accessories mobile telephones,mobile telephones & accessories,mobile telephones & accessories virgin mobile,mobile telephones & accessories car kit installations,telephones & systems,mobile telephones & accessories optus,telephones & systems--installation & maintenance repairs,telephones & systems--installation & maintenance technicians,mobile telephones & accessories hands free kits,mobile telephones & accessories installation"
    },
    {
        "id": 2220,
        "categoryId": 32,
        "subCategoryName": "Television Production Equipment--Testing & Servicing",
        "icon": null,
        "clues": "television film production,television production equipment--testing & servicing,film & television production,film television video production,television servicing,Television Production Equipment--Testing & Servicing,television production"
    },
    {
        "id": 2223,
        "categoryId": 32,
        "subCategoryName": "Television Retailers",
        "icon": null,
        "clues": "television retailers,Television Retailers"
    },
    {
        "id": 2231,
        "categoryId": 32,
        "subCategoryName": "Tents",
        "icon": null,
        "clues": "tents aussie disposals,party tents,Tents,tents marquees,camping tents,tents hire,tents"
    },
    {
        "id": 2233,
        "categoryId": 32,
        "subCategoryName": "Test Equipment",
        "icon": null,
        "clues": "electronic test equipment,electrical test equipment,Test Equipment"
    },
    {
        "id": 2234,
        "categoryId": 32,
        "subCategoryName": "Textile & Clothing Printing",
        "icon": null,
        "clues": "textiles,digital textile printing,printing--clothing & textile promotional products,printing--clothing & textile shirts,printing--clothing & textile workwear,printing--clothing & textile screen printing,textile printing,printing--clothing & textile schools,printing--clothing & textile digital printing,clothing & textile printing,textile importers,printing--clothing & textile sportswear,textile clothing,textile digital printing,digital textile printing equipment,clothing & textile,textile agents,printing--clothing & textile windcheaters,printing--clothing & textile t shirts,textile mills,textile manufacturers,Textile & Clothing Printing,printing--clothing & textile banners,textile design,printing--clothing & textile,printing--clothing & textile uniforms,printing--clothing & textile jackets"
    },
    {
        "id": 2235,
        "categoryId": 32,
        "subCategoryName": "Textile & Fashion Designers",
        "icon": null,
        "clues": "textiles,textile agents,textile mills,textile manufacturers,designers--textile & fashion,textile & fashion designers,textile design,Textile & Fashion Designers,designers--textile,textile printing,textile importers"
    },
    {
        "id": 2237,
        "categoryId": 32,
        "subCategoryName": "Textile Wholesalers",
        "icon": null,
        "clues": "interior textile wholesalers,textile wholesalers,Textile Wholesalers"
    },
    {
        "id": 2242,
        "categoryId": 32,
        "subCategoryName": "Theatrical Supplies & Services",
        "icon": null,
        "clues": "theatrical supplies & services props,hire staging equipment theatrical supplies,theatrical supplies & services,theatrical services,theatrical supplies,Theatrical Supplies & Services,theatrical supplies & services stages"
    },
    {
        "id": 2243,
        "categoryId": 32,
        "subCategoryName": "Thermometers",
        "icon": null,
        "clues": "Thermometers,thermometers,digital thermometers"
    },
    {
        "id": 2244,
        "categoryId": 32,
        "subCategoryName": "Thermostats",
        "icon": null,
        "clues": "thermostats,Thermostats"
    },
    {
        "id": 2246,
        "categoryId": 32,
        "subCategoryName": "Ticket Issuing Machines",
        "icon": null,
        "clues": "ticket issuing machines,Ticket Issuing Machines"
    },
    {
        "id": 2247,
        "categoryId": 32,
        "subCategoryName": "Tickets & Tags",
        "icon": null,
        "clues": "excavators tickets,bobcat tickets,tickets,machinery tickets,sports tickets,printing tickets,event tickets,concert tickets,football tickets,tickets & tags,airline tickets,Tickets & Tags,raffle tickets,theatre tickets,afl tickets"
    },
    {
        "id": 2250,
        "categoryId": 32,
        "subCategoryName": "Timber Supplies",
        "icon": null,
        "clues": "timber supplies,Timber Supplies,timber building supplies"
    },
    {
        "id": 2254,
        "categoryId": 32,
        "subCategoryName": "Time Recording Systems",
        "icon": null,
        "clues": "time recording systems,Time Recording Systems"
    },
    {
        "id": 2256,
        "categoryId": 32,
        "subCategoryName": "Timing Equipment",
        "icon": null,
        "clues": "Timing Equipment,timing equipment"
    },
    {
        "id": 2260,
        "categoryId": 32,
        "subCategoryName": "Tobacco Products & Tobacconists' Supplies",
        "icon": null,
        "clues": "tobacco products,Tobacco Products & Tobacconists' Supplies,tobacco products & tobacconists' supplies"
    },
    {
        "id": 2261,
        "categoryId": 32,
        "subCategoryName": "Tobacconists--Retail",
        "icon": null,
        "clues": "tobacconists--retail,Tobacconists--Retail"
    },
    {
        "id": 2262,
        "categoryId": 32,
        "subCategoryName": "Toilet Paper Wholesalers & Manufacturers",
        "icon": null,
        "clues": "toilet paper manufacturers,toilet paper w'salers & mfrs,toilet manufacturers,Toilet Paper Wholesalers & Manufacturers,toilet paper wholesalers,toilet paper suppliers,toilet paper,bulk toilet paper,portable toilet manufacturers"
    },
    {
        "id": 2263,
        "categoryId": 32,
        "subCategoryName": "Toll Road Operators",
        "icon": null,
        "clues": "Toll Road Operators,toll road operators citylink,toll roads,toll road operators"
    },
    {
        "id": 2264,
        "categoryId": 32,
        "subCategoryName": "Toolmakers",
        "icon": null,
        "clues": "toolmakers moulds,Toolmakers,toolmakers"
    },
    {
        "id": 2268,
        "categoryId": 32,
        "subCategoryName": "Tourist Bureaux",
        "icon": null,
        "clues": "Tourist Bureaux,tourist bureaux"
    },
    {
        "id": 2269,
        "categoryId": 32,
        "subCategoryName": "Towel Supplies",
        "icon": null,
        "clues": "towel hire,towel services,towel supplies,towel rails,towel bathrobes,towel wholesale,towel manufacturers,Towel Supplies,towel racks,towel embroidery,paper towel dispensers,towel warmers,towel importers,towel dispensers,towels,towel wholesalers,towel rental"
    },
    {
        "id": 2272,
        "categoryId": 32,
        "subCategoryName": "Towing Equipment",
        "icon": null,
        "clues": "towing equipment installation,Towing Equipment,towing equipment,hella towing equipment,towing equipment onsite services,towing equipment tow bars,towing equipment hayman reese"
    },
    {
        "id": 2273,
        "categoryId": 32,
        "subCategoryName": "Towing Services",
        "icon": null,
        "clues": "heavy towing services,truck towing services,towing services full on-hook insurance,Towing Services,towing services car body removal,towing services heavy towing,towing services cars,towing equipment onsite services,24 hour towing services,towing services racq,interstate towing services,towing services nrma,car towing services,towing services,towing services wrecking,towing services tilt tray,towing services racv"
    },
    {
        "id": 2277,
        "categoryId": 32,
        "subCategoryName": "Toys--Retail & Repairs",
        "icon": null,
        "clues": "toys--retail & repairs dolls,toys--retail & repairs stuffed animals,toys--retail & repairs puzzles,Toys--Retail & Repairs,toys--retail & repairs games,toys--retail & repair,retail toys,toys--retail & repairs,toys--retail & repairs lego"
    },
    {
        "id": 2292,
        "categoryId": 32,
        "subCategoryName": "Trampolines",
        "icon": null,
        "clues": "Trampolines,playground equipment trampolines,trampolines,water trampolines"
    },
    {
        "id": 2293,
        "categoryId": 32,
        "subCategoryName": "Transfers & Emblems",
        "icon": null,
        "clues": "vhs transfers to dvd,car transfers,Transfers & Emblems,limousine transfers,t-shirt transfers,bus transfers,funeral transfers,transfers & emblems,bus & coach scheduled services airport transfers,airport transfers,transfers,iron on transfers,airport shuttle services airport transfers"
    },
    {
        "id": 2294,
        "categoryId": 32,
        "subCategoryName": "Transformers",
        "icon": null,
        "clues": "transformers,transformers manufacturers,transformers oil,Transformers,transformers hire,transformers repair"
    },
    {
        "id": 2302,
        "categoryId": 32,
        "subCategoryName": "Traps--Animal Or Rodent",
        "icon": null,
        "clues": "traps--animal supplies,Traps--Animal Or Rodent,traps--animal or rodent,animal traps"
    },
    {
        "id": 2303,
        "categoryId": 32,
        "subCategoryName": "Travel Accessories",
        "icon": null,
        "clues": "Travel Accessories,insurance travel,travel agencies,travel accessories,travel agents & consultants,travel doctor,luggage travel accessories,travel"
    },
    {
        "id": 2307,
        "categoryId": 32,
        "subCategoryName": "Travel W'Salers",
        "icon": null,
        "clues": "insurance travel,travel agencies,Travel W'Salers,travel goods - wholesalers & manufacturers,travel agents & consultants,travel wholesalers,travel doctor,travel"
    },
    {
        "id": 2310,
        "categoryId": 32,
        "subCategoryName": "Trestle Tables",
        "icon": null,
        "clues": "trestles & planks,Ladders,Step Ladders & Trestles,trestles,ladders,steps,trestles & accessories attics,trestle tables,trestle table hire,Trestle Tables,trestle hire,ladders steps trestles,ladders,steps,trestles & accessories,trestles sales"
    },
    {
        "id": 2311,
        "categoryId": 32,
        "subCategoryName": "Trophies",
        "icon": null,
        "clues": "Trophies,trophies & medals,trophies & gifts,trophies awards,trophies plaques,trophies engraving,trophies"
    },
    {
        "id": 2316,
        "categoryId": 32,
        "subCategoryName": "T-Shirt Suppliers",
        "icon": null,
        "clues": "t-shirt wholesalers,t-shirt embroidery,t-shirt screen printing,t-shirt printing,t-shirt manufacturers,t-shirt suppliers,t-shirt wholesale,T-Shirt Suppliers,t-shirt"
    },
    {
        "id": 2319,
        "categoryId": 32,
        "subCategoryName": "Tube Cleaning & Expanding Equipment",
        "icon": null,
        "clues": "Tubes--Collapsible,tube stock,Tube Cleaning & Expanding Equipment,tube cleaning & expanding equipment,Electric Lamps,Globes & Tubes,tube,tube rolling,tube supplies,tube fittings"
    },
    {
        "id": 2320,
        "categoryId": 32,
        "subCategoryName": "Tubes--Cardboard & Paper",
        "icon": null,
        "clues": "tubes--cardboard & paper,Tubes--Cardboard & Paper,cardboard tubes"
    },
    {
        "id": 2321,
        "categoryId": 32,
        "subCategoryName": "Tubes--Flexible",
        "icon": null,
        "clues": "tubes--flexible,Tubes--Flexible"
    },
    {
        "id": 2322,
        "categoryId": 32,
        "subCategoryName": "Tubes--Steel",
        "icon": null,
        "clues": "Tubes--Steel,tubes--steel"
    },
    {
        "id": 2327,
        "categoryId": 32,
        "subCategoryName": "Turbochargers",
        "icon": null,
        "clues": "turbochargers dyno tuning,Turbochargers,turbochargers,rear mounted turbochargers"
    },
    {
        "id": 2328,
        "categoryId": 32,
        "subCategoryName": "Turf & Lawn Suppliers",
        "icon": null,
        "clues": "lawn & turf supplies seed,turf,turf layers,turf supplies,lawn & turf supplies turf,lawn & turf supplies instant lawns,lawn & turf supplies sir walter,instant turf,lawn & turf supplies farms,sir walter turf suppliers,synthetic turf suppliers,lawn & turf maintenance,lawn & turf supplies synthetic grass,lawn & turf supplies artificial turf,Turf & Lawn Suppliers,turf & lawn,instant turf suppliers,lawn & turf installation,lawn turf,lawn & turf supplies spray-on lawn,turf lawn suppliers,turf farm,turf club,lawn & turf supplies grass,lawn & turf supplies,turf supply"
    },
    {
        "id": 2329,
        "categoryId": 32,
        "subCategoryName": "TV Antenna Services",
        "icon": null,
        "clues": "tv stations,tv repair & services,tv,tv services,tv retail,tv antennas installation,television antenna services pay tv equipment,TV Antenna Services,tv repair,tv antennas services,pay tv services,tv installation,television antenna services digital tv systems,tv antennas,satellite tv services,tv aerial,tv rental,satellite tv"
    },
    {
        "id": 2330,
        "categoryId": 32,
        "subCategoryName": "TV Hire",
        "icon": null,
        "clues": "tv stations,tv,tv services,TV Hire,tv retail,tv antennas installation,hire tv,tv repair,tv installation,tv hire,tv antennas,tv aerial,tv rental,satellite tv"
    },
    {
        "id": 2331,
        "categoryId": 32,
        "subCategoryName": "TV Repairs & Installation",
        "icon": null,
        "clues": "tv stations,tv,tv services,tv retail,tv antennas installation,plasma tv installation,tv repair,tv installation,tv antennas,satellite tv installation,tv aerial,tv aerial installation,TV Repairs & Installation,tv rental,satellite tv"
    },
    {
        "id": 2332,
        "categoryId": 32,
        "subCategoryName": "Typesetting",
        "icon": null,
        "clues": "Typesetting,typesetting"
    },
    {
        "id": 2333,
        "categoryId": 32,
        "subCategoryName": "Typewriter Dealers & Repairers",
        "icon": null,
        "clues": "Typewriter Dealers & Repairers,typewriter dealers & repairers,typewriter dealers,typewriter repair,typewriter,typewriter supplies,typewriter ribbons"
    },
    {
        "id": 2335,
        "categoryId": 32,
        "subCategoryName": "Tyres",
        "icon": null,
        "clues": ""
    },
    {
        "id": 2340,
        "categoryId": 32,
        "subCategoryName": "Umbrella & Walking Stick Manufacturers & Wholesalers",
        "icon": null,
        "clues": "Umbrella & Walking Stick Manufacturers & Wholesalers,umbrellas"
    },
    {
        "id": 2341,
        "categoryId": 32,
        "subCategoryName": "Umbrellas & Walking Sticks--Retail & Repairs",
        "icon": null,
        "clues": "umbrellas hire,Umbrellas & Walking Sticks--Retail & Repairs,umbrellas & walking sticks - wholesalers & manufacturers,garden umbrellas,lighted umbrellas,umbrellas & walking sticks--retail & repairs,umbrellas manufacturers,shade umbrellas,golf umbrellas,beach umbrellas,umbrellas repair,umbrellas shades,market umbrellas,outdoor umbrellas,umbrellas"
    },
    {
        "id": 2342,
        "categoryId": 32,
        "subCategoryName": "Unaccompanied baggage",
        "icon": null,
        "clues": "unaccompanied luggage,unaccompanied baggage,Unaccompanied baggage"
    },
    {
        "id": 2343,
        "categoryId": 32,
        "subCategoryName": "Unclassified",
        "icon": null,
        "clues": "Unclassified"
    },
    {
        "id": 2344,
        "categoryId": 32,
        "subCategoryName": "Underclothing W'salers & Mfrs",
        "icon": null,
        "clues": "Underclothing W'salers & Mfrs,underclothing w'salers & mfrs"
    },
    {
        "id": 2345,
        "categoryId": 32,
        "subCategoryName": "Underclothing--Retail",
        "icon": null,
        "clues": "underclothing--retail,Underclothing--Retail"
    },
    {
        "id": 2347,
        "categoryId": 32,
        "subCategoryName": "Underpinning Services",
        "icon": null,
        "clues": "underpinning services,underpinning,Underpinning Services,building underpinning,underpinning work contractors,foundation underpinning,house underpinning,concrete underpinning,underpinning walls"
    },
    {
        "id": 2348,
        "categoryId": 32,
        "subCategoryName": "Uniforms",
        "icon": null,
        "clues": "uniforms--retail corporate,uniforms shop,sports uniforms,uniforms,uniforms manufacturers & wholesalers,corporate uniforms,Uniforms,school uniforms,work uniforms,uniforms--retail,uniforms suppliers"
    },
    {
        "id": 2349,
        "categoryId": 32,
        "subCategoryName": "Uniforms Wholesalers & Manufacturers",
        "icon": null,
        "clues": "uniforms--retail corporate,uniforms shop,uniforms manufacturers & wholesalers healthcare,medical uniforms manufacturers,uniforms manufacturers & wholesalers corporate,sports uniforms,uniforms - wholesalers and manufacturers,uniforms manufacturers & wholesalers,uniforms manufacturers,corporate uniforms,school uniforms manufacturers,Uniforms Wholesalers & Manufacturers,school uniforms,work uniforms,school uniforms - wholesalers & manufacturers,uniforms manufacturers & wholesalers work wear,uniforms--retail,uniforms suppliers,softball uniforms manufacturers"
    },
    {
        "id": 2357,
        "categoryId": 32,
        "subCategoryName": "Vacuum Cleaner Parts & Repairs",
        "icon": null,
        "clues": "vacuum cleaners bags,vacuum cleaners parts,vacuum cleaners--repairs & parts kirby,kirby vacuum cleaners parts,Vacuum Cleaner Parts & Repairs,vacuum cleaners services,vacuum cleaners--industrial,vacuum cleaners repair,vacuum cleaners--repairs & parts vacuum bags,vacuum spare parts,vacuum cleaners--domestic ducted,vacuum parts,vacuum cleaners--repairs & parts,vacuum cleaners--domestic,vacuum parts & accessories,vacuum cleaners--repairs & parts ducted,vacuum cleaners spare parts,vacuum cleaners--repairs,vacuum cleaners supplies"
    },
    {
        "id": 2358,
        "categoryId": 32,
        "subCategoryName": "Vacuum Cleaners",
        "icon": null,
        "clues": "vacuum cleaners bags,vacuum cleaners--domestic rainbow,vacuum cleaners parts,vacuum cleaners services,vacuum cleaners hire,vacuum cleaners--industrial,vacuum cleaners repair,vacuum cleaners--repairs & parts vacuum bags,vacuum cleaners--domestic ducted,Vacuum Cleaners,vacuum cleaners,vacuum cleaners--repairs & parts,vacuum cleaners accessories,vacuum cleaners--domestic,vacuum cleaners--repairs & parts ducted,vacuum cleaners spare parts,vacuum cleaners--repairs,vacuum cleaners supplies"
    },
    {
        "id": 2359,
        "categoryId": 32,
        "subCategoryName": "Vacuum System & Equipment",
        "icon": null,
        "clues": "Vacuum System & Equipment,vacuum equipment & systems,vacuum systems,ducted vacuum systems,vacuum equipment"
    },
    {
        "id": 2362,
        "categoryId": 32,
        "subCategoryName": "Variety Stores",
        "icon": null,
        "clues": "Variety Stores,variety store,variety stores,discount variety store"
    },
    {
        "id": 2368,
        "categoryId": 32,
        "subCategoryName": "Vending Machines",
        "icon": null,
        "clues": "food vending machines,coin vending machines,vending machines sales,vending machine wholesalers,vending machine repair,vending suppliers,vending machines,second hand vending machines,vending machine operator,drink vending machines,cold drink vending machines,vending machine distributors,coffee vending machines,vending machine services,vending machines for sales,vending machine suppliers,vending equipment,vending,vending equipment & services,vending coke machine,lolly vending machines,Vending Machines,condoms vending machines,vending machine manufacturers,vending services,vending machine hire,vending machine sales"
    },
    {
        "id": 2369,
        "categoryId": 32,
        "subCategoryName": "Ventilation System & Equipment",
        "icon": null,
        "clues": "ventilation systems,duct exhaust ventilation systems,Ventilation System & Equipment,commercial ventilation systems"
    },
    {
        "id": 2376,
        "categoryId": 32,
        "subCategoryName": "Vibration Control Equipment",
        "icon": null,
        "clues": "Vibration Control Equipment,vibration control equipment,scan vibro vibration equipment,vibration control"
    },
    {
        "id": 2387,
        "categoryId": 32,
        "subCategoryName": "Vitamin Products",
        "icon": null,
        "clues": "vitamin products supplements,Vitamin Products,vitamin products"
    },
    {
        "id": 2389,
        "categoryId": 32,
        "subCategoryName": "Voice Messaging",
        "icon": null,
        "clues": "Voice Messaging,voice,voice over studios,voice coaching,voice messaging,voice lessons,voice over agents,voice signature,voice therapy,voice tuition,voice training,voice teachers,voice recorders,voice & data,voice over,voice over artists,voice recognition,voice over ip,voice over training"
    },
    {
        "id": 2395,
        "categoryId": 32,
        "subCategoryName": "Warehousing",
        "icon": null,
        "clues": "warehousing distribution,warehousing & distributors,Warehousing,warehousing forklifts,warehousing supplies,warehousing & storage,warehousing,warehousing & logistics"
    },
    {
        "id": 2397,
        "categoryId": 32,
        "subCategoryName": "Washers",
        "icon": null,
        "clues": "washers & dryers,glass washers,washers,industrial washers,rubber washers,pressure washers,Washers"
    },
    {
        "id": 2402,
        "categoryId": 32,
        "subCategoryName": "Waste Disposal & Reduction Services",
        "icon": null,
        "clues": "waste disposal facility,waste reduction & disposal services septic tank waste,waste reduction & disposal services,waste disposal,waste disposal equipment,waste disposal centre,waste removal services,waste disposal sites,waste reduction & disposal services skips,waste reduction & disposal services recycling,waste services,waste disposal station,waste reduction & disposal services scrap metal,Waste Disposal & Reduction Services,waste reduction & disposal services asbestos,septic tank cleaning services liquid waste collection,waste disposal depot,recycling services waste,waste management services,waste reduction & disposal services clean fill,waste disposal services,waste reduction & disposal services medical waste,waste reduction & disposal services collections,waste disposal tips,waste reduction & disposal services garden waste,waste reduction & disposal equipment skips,waste reduction & disposal services residential,waste reduction & disposal equipment bins,waste disposal chemical,waste disposal & recyclers,waste reduction & disposal equipment,waste reduction services,waste disposal bins,waste reduction & disposal services landfills,waste reduction,waste reduction & disposal services skip bins,waste disposal facilities,waste collection services,waste reduction & disposal,waste bin services"
    },
    {
        "id": 2403,
        "categoryId": 32,
        "subCategoryName": "Watch Shop & Watch Repairs",
        "icon": null,
        "clues": "Watch Shop & Watch Repairs"
    },
    {
        "id": 2404,
        "categoryId": 32,
        "subCategoryName": "Watches--W'sale",
        "icon": null,
        "clues": "wholesale watches,watches wholesale"
    },
    {
        "id": 2405,
        "categoryId": 32,
        "subCategoryName": "Watchmakers' Equipment & Supplies",
        "icon": null,
        "clues": "watchmakers' equipment & supplies,watches--retail & repair watchmakers,jewellers & watchmakers,watchmakers,Watchmakers' Equipment & Supplies,watchmakers supplies"
    },
    {
        "id": 2406,
        "categoryId": 32,
        "subCategoryName": "Water Beds",
        "icon": null,
        "clues": "mattresses & water beds,water beds,Water Beds"
    },
    {
        "id": 2408,
        "categoryId": 32,
        "subCategoryName": "Water Coolers",
        "icon": null,
        "clues": "office water coolers,water coolers suppliers,water coolers,reverse osmosis water coolers,water coolers hire,Water Coolers"
    },
    {
        "id": 2409,
        "categoryId": 32,
        "subCategoryName": "Water Features, Ponds & Garden Ornaments",
        "icon": null,
        "clues": "water features,ponds,water features designers,water features pumps,garden water,water features,ponds & garden ornaments,nurseries-retail water gardens,water features suppliers,water ponds,garden water pumps,garden water features,garden equipment & supplies water features,garden equipment & supplies ponds,fountains,statuary & sundials ponds,water features & garden statues,garden water fountains,water gardens,garden ponds,water garden supplies,Water Features,Ponds & Garden Ornaments"
    },
    {
        "id": 2410,
        "categoryId": 32,
        "subCategoryName": "Water Filters",
        "icon": null,
        "clues": "water filters,water filters--drinking,Water Filters"
    },
    {
        "id": 2412,
        "categoryId": 32,
        "subCategoryName": "Water Reticulation Contractors & Services",
        "icon": null,
        "clues": "gas hot water services,hot water systems services,water drilling contractors,electric hot water services,electrical contractors & underground power & water services work,water boring contractors,plumbers & gasfitters hot water services,electrical contractors hot water heaters,landscape contractors & designers water features,water bore contractors,water reticulation,water tank services,hot water services units,boring & drilling contractors bore water,water main contractors,hot water services,solar hot water services,water services,boring & drilling contractors water boring,landscape contractors & designers water saving gardens,water cartage contractors,Water Reticulation Contractors & Services,water contractors,hot water services repair,water reticulation contractors & services,metal cutting services water-jet,cleaning contractors--steam,pressure,chemical etc. water blasting,water delivery services,landscapers contractors & water gardens,hot water services installation,water reticulation contractors"
    },
    {
        "id": 2413,
        "categoryId": 32,
        "subCategoryName": "Water Seepage Control",
        "icon": null,
        "clues": "water seepage,water pumps control panel manufacturers,water damage erosion control,water seepage control,hdpe water seepage control,Water Seepage Control"
    },
    {
        "id": 2415,
        "categoryId": 32,
        "subCategoryName": "Water Skiing Gear",
        "icon": null,
        "clues": "water skiing instructors,Water Skiing Gear,water skiing,water skiing suppliers,water skiing centres,water sports gear"
    },
    {
        "id": 2416,
        "categoryId": 32,
        "subCategoryName": "Water Tanks & Tank Supplies",
        "icon": null,
        "clues": "tanks & tank equipment water,water tank installation,water tank,water tanks retail,water tanks installation,water tanks concrete,water tanks steel,Water Tanks & Tank Supplies,concrete water tanks,water tank installers,water tanks,water tanks & pumps,water tanks sales,water tank retail,spring water supplies & accessories,water tank suppliers,water tank manufacturers,water tank repair,water tank cleaning,water tank sales,water tank pumps"
    },
    {
        "id": 2418,
        "categoryId": 32,
        "subCategoryName": "Water Treatment & Equipment",
        "icon": null,
        "clues": "water recyclers equipment,water treatment plant,water treatment & equipment testing,commercial water treatment equipment,gas appliances & equipment hot water,water treatment specialist,water treatment,water blasting equipment,water treatment systems,water ski equipment,water treatment services,waste water & sewage treatment,water treatment & equipment,tanks & tank equipment water,water treatment engineers,waste water treatment,water purification equipment,water sports equipment,water equipment,water treatment equipment supply companies,garden equipment & supplies water features,water tank equipment,water testing equipment,Water Treatment & Equipment"
    },
    {
        "id": 2423,
        "categoryId": 32,
        "subCategoryName": "Wax & Wax Products",
        "icon": null,
        "clues": "Wax & Wax Products,wax stamps,beauty salons brazilian waxing,wax seals,waxing,wax & wax products,waxing salon,brazilian waxing"
    },
    {
        "id": 2424,
        "categoryId": 32,
        "subCategoryName": "Weather Vanes",
        "icon": null,
        "clues": "weather balloons,weather forecasting,weather instruments,weather,weather station,weather vanes,weather seals,weather seals & strips,weatherboards,Weather Vanes,weather shields"
    },
    {
        "id": 2426,
        "categoryId": 32,
        "subCategoryName": "Webbing",
        "icon": null,
        "clues": "nylon webbing,webbing,Webbing"
    },
    {
        "id": 2429,
        "categoryId": 32,
        "subCategoryName": "Wedding Flowers",
        "icon": null,
        "clues": "silk wedding flowers,wedding flowers delivery,wedding flowers,wedding silk flowers,Wedding Flowers"
    },
    {
        "id": 2430,
        "categoryId": 32,
        "subCategoryName": "Wedding Gifts & Registry Services",
        "icon": null,
        "clues": "wedding registry,wedding bomboniere gifts,wedding video services,wedding hair & beauty services mobile services,wedding gifts,wedding services,wedding gifts & registry services,wedding arrangement & planning services,Wedding Gifts & Registry Services,wedding arrangement & planning services cakes,wedding arrangement services,wedding arrangement & planning services wedding hair,wedding arrangement & planning services decorations,wedding arrangement services & supplies,wedding photographers & video services,wedding reception services"
    },
    {
        "id": 2432,
        "categoryId": 32,
        "subCategoryName": "Wedding Invitations & Stationery",
        "icon": null,
        "clues": "wedding invitations cards,wedding stationery printing,Wedding Invitations & Stationery,wedding stationery wholesalers,wholesale wedding stationery,wedding invitations,wedding stationery supplies,wedding invitations printing,wedding invitations & stationery,wedding invitations paper,wedding stationery wholesale,diy wedding invitations,wedding arrangement & planning services stationery,wedding stationery"
    },
    {
        "id": 2433,
        "categoryId": 32,
        "subCategoryName": "Wedding Jewellery & Accessories",
        "icon": null,
        "clues": "wedding accessories,Wedding Jewellery & Accessories,wedding jewellery & accessories,wedding cake accessories"
    },
    {
        "id": 2441,
        "categoryId": 32,
        "subCategoryName": "Weight Loss Treatments",
        "icon": null,
        "clues": "weight loss centre,weight reducing treatments dietitians,Weight Loss Treatments,weight reducing treatments weight loss,weight reducing treatments herbalife,weight loss,weight loss treatments,weight reducing treatments nutritionists,weight loss clinic,weight reducing treatments jenny craig,weight reducing treatments lite n' easy"
    },
    {
        "id": 2444,
        "categoryId": 32,
        "subCategoryName": "Western Style Clothing & Accessories",
        "icon": null,
        "clues": "western style clothing,Western Style Clothing & Accessories,western clothing,western style clothing & accessories"
    },
    {
        "id": 2445,
        "categoryId": 32,
        "subCategoryName": "Wetsuit Wholesalers & Manufacturers",
        "icon": null,
        "clues": "wetsuits repair,wetsuits manufacturers,wetsuit w'salers & mfrs,wetsuit alterations,wetsuits hire,neoprene wetsuit,Wetsuit Wholesalers & Manufacturers,surfing equipment & accessories--retail wetsuits,wetsuits"
    },
    {
        "id": 2448,
        "categoryId": 32,
        "subCategoryName": "Wheelbarrows",
        "icon": null,
        "clues": "Wheelbarrows,wheelbarrows"
    },
    {
        "id": 2450,
        "categoryId": 32,
        "subCategoryName": "Whiteboards, Blackboards & Chalkboards",
        "icon": null,
        "clues": "Whiteboards,Blackboards & Chalkboards,whiteboards repair,whiteboards hire,whiteboards & chalkboards,electronic whiteboards,whiteboards"
    },
    {
        "id": 2454,
        "categoryId": 32,
        "subCategoryName": "Wholesale Florist",
        "icon": null,
        "clues": "wholesale florist,Wholesale Florist,florists wholesale,wholesale florist supplies"
    },
    {
        "id": 2456,
        "categoryId": 32,
        "subCategoryName": "Wholesale Jewellery",
        "icon": null,
        "clues": "Wholesale Jewellery,wholesale costume jewellery,wholesale body jewellery,wholesale fashion jewellery,wholesale fashion jewellery & accessories,wholesale jewellery supplies,wholesale jewellery,costume jewellery wholesale,sterling silver jewellery wholesale"
    },
    {
        "id": 2458,
        "categoryId": 32,
        "subCategoryName": "Wholesale Toys",
        "icon": null,
        "clues": "wholesale toys,Wholesale Toys,toys - wholesale soft toys,wholesale sex toys"
    },
    {
        "id": 2462,
        "categoryId": 32,
        "subCategoryName": "Winches",
        "icon": null,
        "clues": "winches,Winches"
    },
    {
        "id": 2465,
        "categoryId": 32,
        "subCategoryName": "Window Dressing & Supplies",
        "icon": null,
        "clues": "Window Dressing & Supplies,window cleaning supplies,window dressing,window dressing & supplies,window tint supplies"
    },
    {
        "id": 2476,
        "categoryId": 32,
        "subCategoryName": "Wine Racks & Storage",
        "icon": null,
        "clues": "wine storage,Wine Racks & Storage,wine racks & storage,wine racks"
    },
    {
        "id": 2478,
        "categoryId": 32,
        "subCategoryName": "Wire Products",
        "icon": null,
        "clues": "wire products,Wire Products,wire mesh products,stainless steel wire products,wire products spring wire"
    },
    {
        "id": 2479,
        "categoryId": 32,
        "subCategoryName": "Wire Ropes & Synthetic Ropes",
        "icon": null,
        "clues": "wire & synthetic ropes & fittings,Wire Ropes & Synthetic Ropes"
    },
    {
        "id": 2480,
        "categoryId": 32,
        "subCategoryName": "Wire Straighteners & Cutters",
        "icon": null,
        "clues": "Wire Straighteners & Cutters,wire straighteners & cutters"
    },
    {
        "id": 2481,
        "categoryId": 32,
        "subCategoryName": "Wire W'salers & Mfrs",
        "icon": null,
        "clues": "Electric Cable,Wire Wholesalers & Manufacturers,electric cable & wire - wholesalers & manufacturers,wire manufacturers,manufacturers wire & mesh products"
    },
    {
        "id": 2482,
        "categoryId": 32,
        "subCategoryName": "Wire--Non-Ferrous or Stainless Steel",
        "icon": null,
        "clues": ""
    },
    {
        "id": 2489,
        "categoryId": 32,
        "subCategoryName": "Wooden Boxes & Cases",
        "icon": null,
        "clues": "wooden sheds,wooden ladders,Rollers--Wooden,Metal,Rubber Etc.,wooden boxes,wooden gift boxes,wooden blinds,wooden cases,wooden crates,wooden boat,wooden venetians,wooden furniture,wooden toys,wooden shutters,wooden floors,Wooden Boxes & Cases,wooden gates,wooden fencing,wooden pallets,wooden signs,wooden door,wooden cabinets"
    },
    {
        "id": 2497,
        "categoryId": 32,
        "subCategoryName": "Work & Corporate Uniform Hire",
        "icon": null,
        "clues": "elevating work platform hire cherry pickers,work uniforms suppliers,women's work uniforms,employment--labour hire contractors industrial work,work wear,uniforms work,Work & Corporate Uniform Hire,uniforms--retail work wear,elevating work platform hire,work uniforms,uniforms manufacturers & wholesalers work wear,elevating work platforms for hire,work platform hire"
    },
    {
        "id": 2499,
        "categoryId": 32,
        "subCategoryName": "Workwear & Protective Clothing",
        "icon": null,
        "clues": "Workwear & Protective Clothing,safety workwear,printing--clothing & textile workwear,protective clothing & workwear shop,clothing workwear"
    }
    ]
},
{
    "id": 33,
    "icon": null,
    "subCategories": [{
    "id": 23,
    "categoryId": 33,
    "subCategoryName": "Adventure Tours & Holidays Packages",
    "icon": null,
    "clues": "adventure,adventure holidays,Adventure Tours & Holidays Packages,adventure tours & holidays,adventure tours,adventure activity tours"
},
    {
        "id": 81,
        "categoryId": 33,
        "subCategoryName": "Angling Clubs",
        "icon": null,
        "clues": "Angling Clubs,clubs--angling,angling supplies,angling club,angling"
    },
    {
        "id": 98,
        "categoryId": 33,
        "subCategoryName": "Archery Clubs",
        "icon": null,
        "clues": "clubs--archery,archery club,archery shop,Archery Clubs,archery,archery supplies"
    },
    {
        "id": 121,
        "categoryId": 33,
        "subCategoryName": "Athletic Clubs",
        "icon": null,
        "clues": "clubs--athletic,athletic club,athletic coaching,Athletic Clubs"
    },
    {
        "id": 127,
        "categoryId": 33,
        "subCategoryName": "Australian Rules Football Clubs",
        "icon": null,
        "clues": "Australian Rules Football Clubs,australian rules,australian rules football clubs,australian rules club,australian football,clubs-australian rules football,australian football club,australian rules football"
    },
    {
        "id": 144,
        "categoryId": 33,
        "subCategoryName": "Badminton",
        "icon": null,
        "clues": "badminton courts,Badminton,badminton,badminton club"
    },
    {
        "id": 151,
        "categoryId": 33,
        "subCategoryName": "Ballooning Clubs",
        "icon": null,
        "clues": "ballooning club,Ballooning Clubs,clubs--ballooning"
    },
    {
        "id": 168,
        "categoryId": 33,
        "subCategoryName": "Baseball Clubs",
        "icon": null,
        "clues": "baseball uniforms,baseball training,baseball caps,baseball equipment,baseball cleats,baseball bat,baseball cards,clubs--baseball,Baseball Clubs,baseball supplies,baseball gear,baseball & softball store,baseball store,baseball clothing,baseball club,baseball"
    },
    {
        "id": 169,
        "categoryId": 33,
        "subCategoryName": "Basketball Clubs",
        "icon": null,
        "clues": "Basketball Clubs,clubs--basketball"
    },
    {
        "id": 170,
        "categoryId": 33,
        "subCategoryName": "Basketball Equipment, Coaching & Venues",
        "icon": null,
        "clues": "basketball equipment coaching,Basketball Equipment,Coaching & Venues,basketball,basketball uniforms,basketball court,basketball coaching,basketball equipment,basketball club,basketball clothing & equipment retail,basketball equipment,coaching & venues,basketball stadium"
    },
    {
        "id": 192,
        "categoryId": 33,
        "subCategoryName": "Bicycle & Cycling Clubs",
        "icon": null,
        "clues": "Bicycle & Cycling Clubs,clubs--bicycle"
    },
    {
        "id": 196,
        "categoryId": 33,
        "subCategoryName": "Billiards & Snooker Clubs",
        "icon": null,
        "clues": "clubs--billiards & snooker,billiards hall,Billiards & Snooker Clubs"
    },
    {
        "id": 214,
        "categoryId": 33,
        "subCategoryName": "Boat & Yacht Builders & Repairers",
        "icon": null,
        "clues": "boat builders manufacturers,Boat & Yacht Builders & Repairers,ship & boat builders,boat & yacht builders & repairers painting,aluminium boat builders,boat & yacht builders & repair,boat builders fibreglass,boat & yacht builders & repairers aluminium,boat & yacht builders & repairers fibreglass,boat & yacht builders & repairers slipways,boat & yacht builders & repairers,boat builders,boat builders staff"
    },
    {
        "id": 215,
        "categoryId": 33,
        "subCategoryName": "Boat & Yacht Designers & Plans",
        "icon": null,
        "clues": "Boat & Yacht Designers & Plans,boat plans"
    },
    {
        "id": 216,
        "categoryId": 33,
        "subCategoryName": "Boat & Yacht Equipment",
        "icon": null,
        "clues": "boat & yacht equipment wholesale,boat & yacht equipment carpets,boat & yacht equipment rigging,boat safety equipment,trailers & trailer equipment boat,Boat & Yacht Equipment,boat & yacht equipment chandlery,boat & yacht equipment fibreglass,boat equipment,boat & yacht equipment propellers,boat & yacht equipment outboard motors,boat trailers & trailer equipment,boat & yacht equipment,yacht & boat equipment"
    },
    {
        "id": 217,
        "categoryId": 33,
        "subCategoryName": "Boat & Yacht Sales",
        "icon": null,
        "clues": "boat marine sales,boat & yacht sales,boat sales,boat sales & repair,used boat sales,Boat & Yacht Sales,boat sales & services,second hand boat sales,new boat sales,boat sales services outboard,trailer boat sales,power boat sales,ski boat sales"
    },
    {
        "id": 218,
        "categoryId": 33,
        "subCategoryName": "Boat & Yacht Transport Services",
        "icon": null,
        "clues": "transport boat,boat transport,boat & yacht transport,boat & yacht transport services,boat transportation,small boat transport,boat charter services,Boat & Yacht Transport Services"
    },
    {
        "id": 246,
        "categoryId": 33,
        "subCategoryName": "Bowling Club",
        "icon": null,
        "clues": "ten pin bowling club,bowling alley,bowling green,rsl bowling club,bowling,clubs--bowling,Bowling Club,bowling clubs,club bowling,bowling centre,bowling club"
    },
    {
        "id": 250,
        "categoryId": 33,
        "subCategoryName": "Boxing Clubs",
        "icon": null,
        "clues": "Boxing Clubs,kick boxing clubs,boxing,boxing equipment,boxing gyms,martial arts & self defence instruction & supplies boxing,boxing club,boxing classes,boxing supplies,clubs--boxing"
    },
    {
        "id": 295,
        "categoryId": 33,
        "subCategoryName": "Bushwalking Clubs",
        "icon": null,
        "clues": "clubs--bushwalking,bushwalking equipment,bushwalking,bushwalking clothing,Bushwalking Clubs,bushwalking supplies,camping bushwalking,bushwalking club"
    },
    {
        "id": 322,
        "categoryId": 33,
        "subCategoryName": "Calisthenic Clubs",
        "icon": null,
        "clues": "Calisthenic Clubs,calisthenics,calisthenics clubs,clubs--calisthenic"
    },
    {
        "id": 331,
        "categoryId": 33,
        "subCategoryName": "Camps",
        "icon": null,
        "clues": "horse riding camps,christian youth camps,camps,recreation camps,camps for children with a disability,youth camps,school camps,weight loss camps,fitness camps,holiday camps,kids camps,Camps"
    },
    {
        "id": 336,
        "categoryId": 33,
        "subCategoryName": "Canoes & Kayaks",
        "icon": null,
        "clues": "kayaks & canoes,Canoes & Kayaks"
    },
    {
        "id": 363,
        "categoryId": 33,
        "subCategoryName": "Caravan Clubs",
        "icon": null,
        "clues": "Caravan Clubs,clubs--caravan"
    },
    {
        "id": 367,
        "categoryId": 33,
        "subCategoryName": "Card Clubs",
        "icon": null,
        "clues": "Card Clubs,card clubs"
    },
    {
        "id": 395,
        "categoryId": 33,
        "subCategoryName": "Caving Clubs",
        "icon": null,
        "clues": "Caving Clubs,clubs--caving"
    },
    {
        "id": 422,
        "categoryId": 33,
        "subCategoryName": "Chess Clubs",
        "icon": null,
        "clues": "chess club,chess,clubs--chess,Chess Clubs"
    },
    {
        "id": 470,
        "categoryId": 33,
        "subCategoryName": "Clubs--Racing & Hunt",
        "icon": null,
        "clues": "Clubs--Racing & Hunt,clubs--racing,clubs--racing & hunt,clubs--motor racing"
    },
    {
        "id": 482,
        "categoryId": 33,
        "subCategoryName": "Collectibles & Memorabilia Clubs",
        "icon": null,
        "clues": "batman collectibles,clubs--collectibles & memorabilia,Collectibles & Memorabilia Clubs"
    },
    {
        "id": 504,
        "categoryId": 33,
        "subCategoryName": "Community Service Clubs",
        "icon": null,
        "clues": "Community Service Clubs,clubs--community service,community services clubs"
    },
    {
        "id": 511,
        "categoryId": 33,
        "subCategoryName": "Computer Clubs",
        "icon": null,
        "clues": "Computer Clubs,clubs--computer"
    },
    {
        "id": 591,
        "categoryId": 33,
        "subCategoryName": "Cricket Clubs",
        "icon": null,
        "clues": "Cricket Clubs,cricket club,indoor cricket clubs,clubs--cricket"
    },
    {
        "id": 592,
        "categoryId": 33,
        "subCategoryName": "Cricket Coaches",
        "icon": null,
        "clues": "Cricket Coaches,cricket coaching,cricket coaches"
    },
    {
        "id": 594,
        "categoryId": 33,
        "subCategoryName": "Croquet Clubs",
        "icon": null,
        "clues": "croquet,clubs--croquet,Croquet Clubs,croquet club"
    },
    {
        "id": 612,
        "categoryId": 33,
        "subCategoryName": "Dart Clubs",
        "icon": null,
        "clues": "dart club,dart clubs,clubs--dart,Dart Clubs"
    },
    {
        "id": 617,
        "categoryId": 33,
        "subCategoryName": "Day Spas",
        "icon": null,
        "clues": "day spas saunas,Day Spas,day spas facial treatments,day spas massage,day spas solarium,day spas,day spas jane iredale,beauty salons day spa,day spas spa,day spas & beauty salon,day spas spa packages,day spa"
    },
    {
        "id": 843,
        "categoryId": 33,
        "subCategoryName": "Fencing Clubs",
        "icon": null,
        "clues": "Fencing Clubs"
    },
    {
        "id": 880,
        "categoryId": 33,
        "subCategoryName": "Fishing Bait",
        "icon": null,
        "clues": "fishing tackle bait,Fishing Bait,fishing bait & tackle,fishing tackle & bait supply,fishing bait,fishing bait & tackle shop"
    },
    {
        "id": 883,
        "categoryId": 33,
        "subCategoryName": "Fishing Trips",
        "icon": null,
        "clues": "fishing trips deep sea fishing,barramundi fishing trips,fishing trips,deep sea fishing trips,fishing trips charters,Fishing Trips"
    },
    {
        "id": 884,
        "categoryId": 33,
        "subCategoryName": "Fitness Equipment",
        "icon": null,
        "clues": "fitness equipment repair,fitness equipment home gyms,Fitness Equipment,fitness equipment rental,fitness equipment weights,fitness equipment,fitness equipment hire,fitness equipment treadmills,fitness equipment exercise bikes"
    },
    {
        "id": 901,
        "categoryId": 33,
        "subCategoryName": "Flying & Gliding Clubs",
        "icon": null,
        "clues": "Flying & Gliding Clubs,clubs--flying & gliding"
    },
    {
        "id": 929,
        "categoryId": 33,
        "subCategoryName": "Four Wheel Drive Clubs",
        "icon": null,
        "clues": "four wheel drive wreckers,four wheel drive,four wheel drive equipment & accessories,four wheel drive equipment,clubs--four wheel drive,four wheel drive club,four wheel drive accessories,four wheel drive hire,Four Wheel Drive Clubs,four wheel drive equipment & accessories arb"
    },
    {
        "id": 983,
        "categoryId": 33,
        "subCategoryName": "Gardening Clubs",
        "icon": null,
        "clues": "clubs--gardening,gardening,gardening services,organic gardening,Gardening Clubs,gardening contractors,hire--builders',contractors' & handyman's equipment gardening"
    },
    {
        "id": 996,
        "categoryId": 33,
        "subCategoryName": "Gem & Lapidary Clubs",
        "icon": null,
        "clues": "Gem & Lapidary Clubs"
    },
    {
        "id": 1025,
        "categoryId": 33,
        "subCategoryName": "Go-Karting Clubs",
        "icon": null,
        "clues": "go-karting clubs,go-karting club,amusement centres go-karting,Go-Karting Clubs"
    },
    {
        "id": 1032,
        "categoryId": 33,
        "subCategoryName": "Golf Clubs - Private",
        "icon": null,
        "clues": "golf clubs retail,social golf clubs,Golf Clubs - Private,sporting clubs golf courses,clubs--golf,custom golf clubs,golf clubs private"
    },
    {
        "id": 1036,
        "categoryId": 33,
        "subCategoryName": "Golf Driving Range",
        "icon": null,
        "clues": "driving range golf,golf courses--public driving range,golf driving,Golf Driving Range,golf driving range,golf practice ranges,golf range"
    },
    {
        "id": 1048,
        "categoryId": 33,
        "subCategoryName": "Gridiron Clubs",
        "icon": null,
        "clues": "gridiron,clubs--gridiron,Gridiron Clubs"
    },
    {
        "id": 1053,
        "categoryId": 33,
        "subCategoryName": "Gun & Shooting Clubs",
        "icon": null,
        "clues": "Gun & Shooting Clubs,gun club"
    },
    {
        "id": 1057,
        "categoryId": 33,
        "subCategoryName": "Gymnastic Clubs",
        "icon": null,
        "clues": "Gymnastic Clubs,clubs--gymnastic,gymnastics,gymnastics club"
    },
    {
        "id": 1074,
        "categoryId": 33,
        "subCategoryName": "Handball Clubs",
        "icon": null,
        "clues": "Handball Clubs,handball clubs,clubs--handball"
    },
    {
        "id": 1077,
        "categoryId": 33,
        "subCategoryName": "Hang Gliding & Paragliding",
        "icon": null,
        "clues": "cable hang gliding,hang gliding,Hang Gliding & Paragliding,hang gliding & paragliding"
    },
    {
        "id": 1085,
        "categoryId": 33,
        "subCategoryName": "Health & Fitness - Centres & Services",
        "icon": null,
        "clues": "health & fitness centres & services boxing,bodybuilding health & fitness centres,health & fitness centres & services swimming pool,tai chi health & fitness centres,health & fitness club,health & fitness,health & fitness centres & services gymnastics,yoga health & fitness centres,health & fitness centres & services,Health & Fitness - Centres & Services,health & fitness centres & services boxercise,health & fitness centres,health & fitness centres & services pilates"
    },
    {
        "id": 1090,
        "categoryId": 33,
        "subCategoryName": "Health Retreats",
        "icon": null,
        "clues": "Health Retreats,health retreats,health holidays & retreats,health holidays & retreats spas,health spa retreats,detoxification health retreats"
    },
    {
        "id": 1113,
        "categoryId": 33,
        "subCategoryName": "Hire--Camping & Leisure Equipment",
        "icon": null,
        "clues": "camping gear hire,hire--camping,camping equipment hire,hire--camping & leisure equipment,Hire--Camping & Leisure Equipment,hire camping equipment"
    },
    {
        "id": 1116,
        "categoryId": 33,
        "subCategoryName": "Historic Machinery Clubs",
        "icon": null,
        "clues": "historic machinery clubs,Historic Machinery Clubs,clubs--historic machinery"
    },
    {
        "id": 1117,
        "categoryId": 33,
        "subCategoryName": "Historical Re-Enactment Clubs",
        "icon": null,
        "clues": "heritage & history consultants historical societies,historical research,Historical Re-Enactment Clubs,clubs--historical re-enactment"
    },
    {
        "id": 1120,
        "categoryId": 33,
        "subCategoryName": "Hockey Clubs",
        "icon": null,
        "clues": "hockey,hockey equipment,hockey club,Hockey Clubs,clubs--hockey"
    },
    {
        "id": 1150,
        "categoryId": 33,
        "subCategoryName": "Hot Air Balloons",
        "icon": null,
        "clues": "Hot Air Balloons,hot air blowers,hot air balloons rides,hot air,hot air balloons,balloons- hot air,hot air balloon flights"
    },
    {
        "id": 1155,
        "categoryId": 33,
        "subCategoryName": "House Boats--Hire",
        "icon": null,
        "clues": "house boats--hire,House Boats--Hire"
    },
    {
        "id": 1173,
        "categoryId": 33,
        "subCategoryName": "Ice Skating Rinks",
        "icon": null,
        "clues": "ice skating rinks,Ice Skating Rinks,ice skating lessons,ice skating,ice skating rink,ice skating arena"
    },
    {
        "id": 1183,
        "categoryId": 33,
        "subCategoryName": "Indoor Games",
        "icon": null,
        "clues": "indoor tennis,indoor play,indoor netball,indoor games,indoor sports centre,indoor rock climbing,indoor play centre,indoor soccer,indoor cricket,indoor playground,Indoor Games,indoor swimming pools,indoor,games--indoor--supplies"
    },
    {
        "id": 1185,
        "categoryId": 33,
        "subCategoryName": "Indoor Sports",
        "icon": null,
        "clues": "indoor sports centre,Indoor Sports,indoor sports,indoor sports volleyball,indoor sports netball,indoor sports soccer"
    },
    {
        "id": 1234,
        "categoryId": 33,
        "subCategoryName": "Jet Skis",
        "icon": null,
        "clues": "jet ski repair,Jet Skis,jet propelled skis sea-doo,jet propelled skis,jet ski,jet ski hire,jet ski sales"
    },
    {
        "id": 1240,
        "categoryId": 33,
        "subCategoryName": "Jockeys",
        "icon": null,
        "clues": "jockeys,Jockeys"
    },
    {
        "id": 1268,
        "categoryId": 33,
        "subCategoryName": "Lacrosse Clubs",
        "icon": null,
        "clues": "clubs--lacrosse,Lacrosse Clubs,lacrosse"
    },
    {
        "id": 1314,
        "categoryId": 33,
        "subCategoryName": "Life Saving &/or Surfing Clubs",
        "icon": null,
        "clues": "clubs--life saving &/or surfing,clubs--life saving,Life Saving &/or Surfing Clubs,surf life saving club,life saving club,clubs--life saving & surfing"
    },
    {
        "id": 1370,
        "categoryId": 33,
        "subCategoryName": "Marinas",
        "icon": null,
        "clues": "marinas,marinas antifouling painting slipway services travel lift,Marinas,marinas boat repairs"
    },
    {
        "id": 1371,
        "categoryId": 33,
        "subCategoryName": "Marine Clubs",
        "icon": null,
        "clues": "clubs--marine,Marine Clubs"
    },
    {
        "id": 1384,
        "categoryId": 33,
        "subCategoryName": "Martial Arts & Self Defence Supplies",
        "icon": null,
        "clues": "martial arts school,Martial Arts & Self Defence Supplies,martial arts,martial arts equipment,martial arts & self defence instruction & supplies tai chi,martial arts & self defence instruction & supplies karate,self defence & martial arts,martial arts & self defence instruction & supplies,martial arts & self defence instruction & supplies wing chun,martial arts & self defence instruction & supplies jiu jitsu,martial arts & self defence,martial arts & self defence instruction,martial supplies,martial arts & self defence instruction & supplies tae kwon do,martial arts supplies,martial arts & self defence instruction & supplies uniforms,martial arts & self defence instruction & supplies hapkido,martial arts & self defence instruction & supplies kung fu,mixed martial arts instruction & supplies,martial,martial arts & self defence instruction & supplies judo,martial arts & self defence instruction & supplies muay thai,martial arts & self defence instruction & supplies boxing,martial arts store,martial arts & self defence instruction & supplies kickboxing,martial arts supply,martial arts & self defence instruction & supplies ninjutsu,martial arts & self defence supplies"
    },
    {
        "id": 1412,
        "categoryId": 33,
        "subCategoryName": "Meditation",
        "icon": null,
        "clues": "Meditation,meditation supplies,meditation,meditation classes,yoga meditation,meditation courses,meditation centre"
    },
    {
        "id": 1414,
        "categoryId": 33,
        "subCategoryName": "Men's Organisations & Activities",
        "icon": null,
        "clues": "Men's Organisations & Activities,men's organisations & activities,men's organisations"
    },
    {
        "id": 1459,
        "categoryId": 33,
        "subCategoryName": "Model Aeroplane Clubs",
        "icon": null,
        "clues": "Model Aeroplane Clubs,model aircraft clubs,clubs--model aeroplane"
    },
    {
        "id": 1460,
        "categoryId": 33,
        "subCategoryName": "Model Engineers Clubs",
        "icon": null,
        "clues": "model aircraft clubs,Model Engineers Clubs,clubs--model aeroplane"
    },
    {
        "id": 1461,
        "categoryId": 33,
        "subCategoryName": "Model Makers",
        "icon": null,
        "clues": "Model Makers,architectural model makers"
    },
    {
        "id": 1473,
        "categoryId": 33,
        "subCategoryName": "Motor Boat Clubs",
        "icon": null,
        "clues": "clubs--motor boat,clubs--motor cycle,Motor Boat Clubs,motor boat club,clubs--motor racing,motor cycles clubs"
    },
    {
        "id": 1479,
        "categoryId": 33,
        "subCategoryName": "Motor Cycle Clubs",
        "icon": null,
        "clues": "clubs--motor boat,clubs--motor cycle,motor boat club,Motor Cycle Clubs,clubs--motor racing,motor cycles clubs"
    },
    {
        "id": 1482,
        "categoryId": 33,
        "subCategoryName": "Motor Racing Clubs",
        "icon": null,
        "clues": "motor racing,motor racing helmets,Motor Racing Clubs,clubs--motor racing,motor cycles clubs,motor racing supplies,clubs--motor boat,motor racing apparel,clubs--motor cycle,motor racing accessories,motor boat club"
    },
    {
        "id": 1521,
        "categoryId": 33,
        "subCategoryName": "Netball Clubs",
        "icon": null,
        "clues": "netball club,Netball Clubs,clubs--netball,netball"
    },
    {
        "id": 1535,
        "categoryId": 33,
        "subCategoryName": "Nudist Clubs",
        "icon": null,
        "clues": "Nudist Clubs,nudist club,nudist,clubs-nudist"
    },
    {
        "id": 1601,
        "categoryId": 33,
        "subCategoryName": "Paintball & Skirmish",
        "icon": null,
        "clues": "Paintball & Skirmish,paintball skirmish,paintball & skirmish games & equipment laser tag,paintball & skirmish games & equipment,skirmish paintball,paintball"
    },
    {
        "id": 1619,
        "categoryId": 33,
        "subCategoryName": "Parasailing",
        "icon": null,
        "clues": "Parasailing,parasailing"
    },
    {
        "id": 1622,
        "categoryId": 33,
        "subCategoryName": "Parks, Gardens & Reserves",
        "icon": null,
        "clues": "Parks,Gardens & Reserves,recreation reserves,reserves,Zoos,Sanctuaries & Animal Parks,caravan & tourist parks,theme parks,holiday parks,parks,parks,gardens & reserves,wildlife reserves,caravan & tourist parks cabins,parks & reserves"
    },
    {
        "id": 1652,
        "categoryId": 33,
        "subCategoryName": "Personal Trainers & Fitness Training",
        "icon": null,
        "clues": "personal fitness trainers kickboxing,personal trainers,personal fitness trainers,personal fitness trainers boxing,personal fitness trainers mobile services,Personal Trainers & Fitness Training,personal fitness trainers boot camp,personal fitness,personal fitness trainers boxing equipment,personal fitness trainers studio,personal fitness trainers aqua aerobics,mobile personal trainers,personal fitness instructors,personal fitness trainers supplies,personal fitness trainers pilates,fitness personal trainers,personal fitness trainers bone density,personal training studios,personal training courses"
    },
    {
        "id": 1665,
        "categoryId": 33,
        "subCategoryName": "Philatelic Clubs",
        "icon": null,
        "clues": "philatelic supplies,clubs--philatelic,philatelic dealers,philatelic,Philatelic Clubs"
    },
    {
        "id": 1677,
        "categoryId": 33,
        "subCategoryName": "Photographic Clubs",
        "icon": null,
        "clues": "clubs--photographic,Photographic Clubs"
    },
    {
        "id": 1691,
        "categoryId": 33,
        "subCategoryName": "Pilates",
        "icon": null,
        "clues": "pilates centre,pilates equipment,pilates physiotherapist,pilates pregnancy,pilates method floor,Pilates,pilates instructors,pilates classes,pilates,pilates reformer,pilates method,pilates studios,pilates gyms,pilates training,pilates & yoga,health & fitness centres & services pilates"
    },
    {
        "id": 1701,
        "categoryId": 33,
        "subCategoryName": "Pistol Clubs",
        "icon": null,
        "clues": "pistol shooting,clubs--pistol,Pistol Clubs,pistol,pistol club,pistol range"
    },
    {
        "id": 1739,
        "categoryId": 33,
        "subCategoryName": "Pony Clubs",
        "icon": null,
        "clues": "pony club,pony hire,pony party,pony stud,pony rides,Pony Clubs,clubs--pony,pony breeders,pony activities"
    },
    {
        "id": 1803,
        "categoryId": 33,
        "subCategoryName": "Public Golf Courses",
        "icon": null,
        "clues": "golf courses public or private,golf courses--public driving range,public golf club,Public Golf Courses,golf courses public,public golf,public golf courses,golf courses--public pro shop,public golf coarse"
    },
    {
        "id": 1825,
        "categoryId": 33,
        "subCategoryName": "Racecourses",
        "icon": null,
        "clues": "racecourses,Racecourses"
    },
    {
        "id": 1826,
        "categoryId": 33,
        "subCategoryName": "Racing Syndicate Organisers",
        "icon": null,
        "clues": "racing syndicate organisers,racing gear,racing components,racing products,racing car performance workshops,racing fuel,racing stables,racing cars,racing suits,racing engines,racing equipment,Racing Syndicate Organisers,racing helmets,racing tyres,racing tracks,racing supplies,racing equipment & materials"
    },
    {
        "id": 1835,
        "categoryId": 33,
        "subCategoryName": "Railway Clubs",
        "icon": null,
        "clues": "Railway Clubs,clubs--railway"
    },
    {
        "id": 1853,
        "categoryId": 33,
        "subCategoryName": "Recreational Divers & Scuba",
        "icon": null,
        "clues": "recreational activities,recreational vehicles,Recreational Divers & Scuba,divers--recreational,recreational diving,recreational lawn bowling"
    },
    {
        "id": 1906,
        "categoryId": 33,
        "subCategoryName": "Rock Climbing Clubs",
        "icon": null,
        "clues": "rock climbing equipment,indoor rock climbing centre,rock climbing gyms,rock & roll clubs,mobile rock climbing,rock climbing,rock climbing indoor,rock climbing venues,clubs--rock climbing,Rock Climbing Clubs,rock climbing gear,indoor rock climbing,rock climbing supplies,rock climbing walls,rock climbing clubs,rock n roll clubs,rock climbing centre"
    },
    {
        "id": 1907,
        "categoryId": 33,
        "subCategoryName": "Rock Climbing Venues & Equipment",
        "icon": null,
        "clues": "rock climbing equipment,indoor rock climbing centre,rock climbing gyms,mobile rock climbing,rock climbing,rock climbing indoor,rock climbing venues,dance tuition &/or venues rock 'n' roll,rock climbing gear,indoor rock climbing,rock climbing venues & equipment,rock climbing supplies,rock climbing walls,rock climbing centre,Rock Climbing Venues & Equipment"
    },
    {
        "id": 1909,
        "categoryId": 33,
        "subCategoryName": "Roller Skating Rinks",
        "icon": null,
        "clues": "roller skating rinks,roller skating rink,Roller Skating Rinks,roller skating centre,roller skating,roller skating ring"
    },
    {
        "id": 1919,
        "categoryId": 33,
        "subCategoryName": "Rowing Clubs",
        "icon": null,
        "clues": "rowing club,clubs--rowing,Rowing Clubs,rowing"
    },
    {
        "id": 1924,
        "categoryId": 33,
        "subCategoryName": "Rugby League Clubs",
        "icon": null,
        "clues": "rugby league supplies,rugby league merchandise,Rugby League Clubs,rugby league club,rugby league clothing,rugby leagues club,rugby league,clubs--rugby league,rugby league football club,junior rugby league,clubs--rugby union,rugby league store"
    },
    {
        "id": 1925,
        "categoryId": 33,
        "subCategoryName": "Rugby Union Clubs",
        "icon": null,
        "clues": "rugby union club,rugby union betting,Rugby Union Clubs,clubs--rugby union,sports betting rugby union,clubs--rugby league,rugby union"
    },
    {
        "id": 1965,
        "categoryId": 33,
        "subCategoryName": "Science Fiction Clubs",
        "icon": null,
        "clues": "Science Fiction Clubs,science fiction,science fiction bookstore,books science fiction,science fiction books,clubs--science fiction"
    },
    {
        "id": 1974,
        "categoryId": 33,
        "subCategoryName": "Scuba Diving Clubs",
        "icon": null,
        "clues": "scuba diving gear,scuba diving lessons,scuba diving supplies,scuba diving courses,scuba diving shop,scuba diving school,Scuba Diving Clubs,scuba diving,scuba diving centre,scuba diving equipment,clubs--scuba diving,scuba diving training"
    },
    {
        "id": 1999,
        "categoryId": 33,
        "subCategoryName": "Senior Citizens Clubs",
        "icon": null,
        "clues": "senior citizens hall,senior citizens club,senior citizens,clubs--senior citizens,Senior Citizens Clubs"
    },
    {
        "id": 2039,
        "categoryId": 33,
        "subCategoryName": "Shooting Ranges",
        "icon": null,
        "clues": "shooting academy,Shooting Ranges,shooting range,shooting complex,shooting,shooting store,shooting club,shooting gallery,shooting ranges,shooting supplies"
    },
    {
        "id": 2057,
        "categoryId": 33,
        "subCategoryName": "Ski Clubs",
        "icon": null,
        "clues": "Ski Clubs,clubs--ski"
    },
    {
        "id": 2058,
        "categoryId": 33,
        "subCategoryName": "Ski Resorts & Tours",
        "icon": null,
        "clues": "jet ski tours,ski centre tours,ski tours,Ski Resorts & Tours,ski resorts,ski centres,tours & resorts"
    },
    {
        "id": 2061,
        "categoryId": 33,
        "subCategoryName": "Skydiving",
        "icon": null,
        "clues": "Skydiving,skydiving,skydiving tandem,skydiving commercial,skydiving hunter valley,skydiving torquay,skydiving instruction,skydiving display,skydiving private parties,skydiving school,skydiving photography"
    },
    {
        "id": 2073,
        "categoryId": 33,
        "subCategoryName": "Soccer Clubs",
        "icon": null,
        "clues": "soccer,soccer club,indoor soccer,clubs--soccer,soccer equipment,soccer shop,Soccer Clubs"
    },
    {
        "id": 2075,
        "categoryId": 33,
        "subCategoryName": "Social & General Clubs",
        "icon": null,
        "clues": "social groups,social events,social research,social,social dancing,club social & general,social club,Social & General Clubs,clubs--social & general function room,sports & social clubs,social golf clubs,clubs--social & general,social & general clubs,social & business clubs,social services,clubs--social"
    },
    {
        "id": 2077,
        "categoryId": 33,
        "subCategoryName": "Societies--General",
        "icon": null,
        "clues": "societies--general,Societies--General"
    },
    {
        "id": 2079,
        "categoryId": 33,
        "subCategoryName": "Softball Clubs",
        "icon": null,
        "clues": "softball sports store,softball club,softball & baseball equipment,softball gear,Softball Clubs,softball,softball association,clubs--softball,baseball & softball store,softball supplies,softball uniforms,softball equipment,softball uniforms manufacturers"
    },
    {
        "id": 2098,
        "categoryId": 33,
        "subCategoryName": "Sporting Clubs - Miscellaneous",
        "icon": null,
        "clues": "women's sporting clubs,Sporting Clubs - Miscellaneous,sporting clubs golf courses,sporting clubs organisations,clubs - sporting - miscellaneous,sporting clubs & leisure centre,sporting clubs lawn bowls,children's sporting clubs"
    },
    {
        "id": 2099,
        "categoryId": 33,
        "subCategoryName": "Sporting Organisations",
        "icon": null,
        "clues": "Sporting Organisations,sporting clubs organisations,sporting organisations,organisations--sporting"
    },
    {
        "id": 2100,
        "categoryId": 33,
        "subCategoryName": "Sports Betting",
        "icon": null,
        "clues": "sports betting tennis,sports betting cricket,sports betting soccer,sports betting afl,sports betting golf,sports betting nrl,sports betting,sports betting rugby union,Sports Betting"
    },
    {
        "id": 2102,
        "categoryId": 33,
        "subCategoryName": "Sports Centre & Grounds",
        "icon": null,
        "clues": "sports centres &/or grounds indoor,sports centres &/or grounds stadium,sports centre,Sports Centre & Grounds,sports medical centre,super sports centre,indoor sports centre,sports grounds,sports centre & grounds,lighting for sports grounds,sports centres &/or grounds,sports injury centre"
    },
    {
        "id": 2108,
        "categoryId": 33,
        "subCategoryName": "Sports Promoters & Consultants",
        "icon": null,
        "clues": "sports promoters & consultants,Sports Promoters & Consultants,sports consultants,sports promoters"
    },
    {
        "id": 2109,
        "categoryId": 33,
        "subCategoryName": "Sports Tours & Holidays",
        "icon": null,
        "clues": "Sports Tours & Holidays,sports tours & holidays,sports tours"
    },
    {
        "id": 2110,
        "categoryId": 33,
        "subCategoryName": "Sports Training Services",
        "icon": null,
        "clues": "sports training services,sports car services,sports training,Sports Training Services"
    },
    {
        "id": 2120,
        "categoryId": 33,
        "subCategoryName": "Squash Clubs",
        "icon": null,
        "clues": "Squash Clubs,clubs--squash"
    },
    {
        "id": 2121,
        "categoryId": 33,
        "subCategoryName": "Squash Coaches",
        "icon": null,
        "clues": "Squash Coaches,squash coaches"
    },
    {
        "id": 2122,
        "categoryId": 33,
        "subCategoryName": "Squash Courts",
        "icon": null,
        "clues": "squash courts,squash courts for hire,Squash Courts,squash centre"
    },
    {
        "id": 2170,
        "categoryId": 33,
        "subCategoryName": "Swimming Clubs",
        "icon": null,
        "clues": "swimming clubs academy,clubs--swimming,Swimming Clubs"
    },
    {
        "id": 2182,
        "categoryId": 33,
        "subCategoryName": "Table Tennis Clubs",
        "icon": null,
        "clues": "table tennis manufacturers,table tennis equipment,table tennis supplies,table tennis tables,Table Tennis Clubs,table tennis hire,table tennis tables retail,table tennis table,clubs--table tennis,table tennis table retail,table tennis,table tennis club,table tennis bat"
    },
    {
        "id": 2192,
        "categoryId": 33,
        "subCategoryName": "Tantra",
        "icon": null,
        "clues": "massage tantra,Tantra,tantra massage,personal development tantra,tantra techniques,alternative & natural therapies tantra,tantra"
    },
    {
        "id": 2226,
        "categoryId": 33,
        "subCategoryName": "Ten Pin Bowling",
        "icon": null,
        "clues": "ten pin bowling club,bowling ten pin,ten pin bowling,tenpin clubs bowling,ten pin bowling equipment,ten bowling,ten pin bowling centre,ten pin bowling alley,ten bin bowling,Ten Pin Bowling,ten pin,ten pen bowling,ten pin bowling supplies"
    },
    {
        "id": 2227,
        "categoryId": 33,
        "subCategoryName": "Tennis Clubs",
        "icon": null,
        "clues": "tennis clubs,clubs--tennis,Tennis Clubs,clubs--table tennis,tennis courts tennis clubs"
    },
    {
        "id": 2230,
        "categoryId": 33,
        "subCategoryName": "Tennis Court Hire",
        "icon": null,
        "clues": "tennis court resurfacing,Tennis Court Hire,tennis sports centre to hire,tennis court builders,tennis court lighting manufacturers,table tennis hire,tennis court surfaces,tennis court accessories,hire tennis court,tennis court repair,tennis court supplies,tennis court lighting construction,tennis court maintenance,tennis courts,tennis court lighting,tennis court fencing,tennis court cleaning,tennis court hire,tennis court construction & repair,indoor tennis court hire,indoor tennis courts for hire,tennis courts for hire,tennis court construction,tennis courts tennis clubs,tennis hire"
    },
    {
        "id": 2266,
        "categoryId": 33,
        "subCategoryName": "Touch Football Clubs",
        "icon": null,
        "clues": "touch football,Touch Football Clubs,clubs--touch football"
    },
    {
        "id": 2337,
        "categoryId": 33,
        "subCategoryName": "UFO Clubs",
        "icon": null,
        "clues": "UFO Clubs,clubs--ufo"
    },
    {
        "id": 2390,
        "categoryId": 33,
        "subCategoryName": "Volleyball Clubs",
        "icon": null,
        "clues": "clubs--volleyball,volleyball centre,volleyball team,volleyball courts,volleyball club,volleyball equipment,indoor beach volleyball,indoor volleyball,Volleyball Clubs,volleyball,indoor sports volleyball,beach volleyball"
    },
    {
        "id": 2396,
        "categoryId": 33,
        "subCategoryName": "Wargaming Clubs",
        "icon": null,
        "clues": "clubs--wargaming,Wargaming Clubs"
    },
    {
        "id": 2411,
        "categoryId": 33,
        "subCategoryName": "Water Polo Clubs",
        "icon": null,
        "clues": "Water Polo Clubs,water polo,clubs--water polo"
    },
    {
        "id": 2414,
        "categoryId": 33,
        "subCategoryName": "Water Skiing Centres & Resorts",
        "icon": null,
        "clues": "water skiing instructors,water skiing centres & resorts,Water Skiing Centres & Resorts,water skiing,water skiing suppliers,water skiing centres"
    },
    {
        "id": 2422,
        "categoryId": 33,
        "subCategoryName": "Waterskiing Clubs",
        "icon": null,
        "clues": "waterskiing clubs,Waterskiing Clubs"
    },
    {
        "id": 2483,
        "categoryId": 33,
        "subCategoryName": "Women's Organisations & Activities",
        "icon": null,
        "clues": "women's organisations & activities,Women's Organisations & Activities,women's organisations"
    },
    {
        "id": 2490,
        "categoryId": 33,
        "subCategoryName": "Woodworking Clubs",
        "icon": null,
        "clues": "woodworking factory,woodworking tools,woodworking power tools,woodworking machines,Woodworking Clubs,woodworking supplies,clubs - woodworking,woodworking,woodworking machinery & service,woodworking tools & machinery,woodworking equipment"
    },
    {
        "id": 2501,
        "categoryId": 33,
        "subCategoryName": "Wrestling Clubs",
        "icon": null,
        "clues": "wrestling school,clubs--wrestling,freestyle wrestling,wrestling gear,jelly wrestling,sumo wrestling,wrestling,Wrestling Clubs,wrestling clubs,wrestling training,sumo wrestling hire"
    },
    {
        "id": 2505,
        "categoryId": 33,
        "subCategoryName": "Yacht Clubs",
        "icon": null,
        "clues": "yacht trailers,yacht supplies,yacht brokers,yacht charter,yacht rigging,yacht club,yacht hire,boat & yacht builders & repair,yacht,clubs--yacht,Yacht Clubs,boat & yacht trailers"
    },
    {
        "id": 2508,
        "categoryId": 33,
        "subCategoryName": "Yoga",
        "icon": null,
        "clues": "yoga health & fitness centres,bikram yoga,yoga pilates,yoga teachers,yoga classes,yoga school,yoga,Yoga,yoga supplies,yoga mats,yoga centre,yoga studio"
    },
    {
        "id": 2509,
        "categoryId": 33,
        "subCategoryName": "Youth Organisations, Hostels & Activities",
        "icon": null,
        "clues": "youth centre,youth organisations,homes & hostels nursing homes,youth hostel,youth health,youth services,youth camps,youth club,youth accommodation,youth work,youth refuge,Youth Organisations,Hostels & Activities,youth groups,youth organisations h,youth theatre,youth counsellors,youth activities,youth organisations,hostels & activities,youth organisations hostel"
    }
    ]
},
{
    "id": 34,
    "icon": null,
    "subCategories": [{
    "id": 22,
    "categoryId": 34,
    "subCategoryName": "Advanced Shrubs & Trees (Nurseries-Retail)",
    "icon": null,
    "clues": "advanced shrubs & trees (nurseries-retail),advanced plants,advanced tree nurseries,advanced shrubs,advanced motorcycle training,Advanced Shrubs & Trees (Nurseries-Retail),advanced trees,advanced driving,advanced nails,advanced driver training,advanced dental technicians,advanced tree nursery,advanced martial arts,advanced driving courses,advanced driving school"
},
    {
        "id": 45,
        "categoryId": 34,
        "subCategoryName": "Air Conditioning Installation & Service",
        "icon": null,
        "clues": "air tools & accessories-supply & service,air conditioning,air conditioning repair,Air Conditioning Installation & Service,car air conditioning installation,air conditioning--installation & service repairs,Air Tools Accessories,Supply & Service,air conditioning services,air conditioning--commercial & industrial,air conditioning-automotive,home air conditioning installation,compressed air installation,auto air conditioning,air conditioning--installation & service fujitsu,installation air conditioners,air conditioning-home installation,installation air conditioning,air services,air conditioning-home,air conditioning--installation & services electrician,air conditioning--installation & service,air cargo services australian air express,air compressors & service,air conditioning--installation & service daikin,air conditioning--parts,automotive air conditioning,air conditioning commercial,air conditioning--installation & service maintenance,air conditioning--installation & service ducted"
    },
    {
        "id": 88,
        "categoryId": 34,
        "subCategoryName": "Apprenticeships",
        "icon": null,
        "clues": "apprenticeships electricians,apprenticeships centre,apprenticeships services,apprenticeships agencies,apprenticeships fitters,Apprenticeships,apprenticeships,apprenticeships training"
    },
    {
        "id": 96,
        "categoryId": 34,
        "subCategoryName": "Arboriculturists",
        "icon": null,
        "clues": "arboriculturists expert witness,arboriculturists tree surveys,Arboriculturists,arboriculturists tree farm establishment,arboriculturists tree assessments,arboriculturists reports,arboriculturists vegetation management plans,arboriculturists,arboriculturists tree farm designs,arboriculturists tree health,arboriculturists local government,arboriculturists dispute mediation,tree reports arboriculturists"
    },
    {
        "id": 114,
        "categoryId": 34,
        "subCategoryName": "Asbestos Removal",
        "icon": null,
        "clues": "asbestos removal & services,asbestos inspections,asbestos testing,asbestos disposal,asbestos consultants,asbestos demolitions,asbestos removalists,asbestos removal,asbestos removals,asbestos building inspections,Asbestos Removal"
    },
    {
        "id": 116,
        "categoryId": 34,
        "subCategoryName": "Asphalt & Bitumen Paving",
        "icon": null,
        "clues": "driveway sealing asphalt paving,asphalt layers,paving--asphalt & bitumen,paving--asphalt & bitumen driveways,asphalt recyclers,paving--asphalt & bitumen repairs,Asphalt & Bitumen Paving,asphalt paving,asphalt,asphalt & bitumen paving,paving--asphalt,asphalt profiling,asphalt services,asphalt works,paving--asphalt & bitumen spray on paving,asphalt driveways,asphalt bitumen,asphalt repair,paving--asphalt & bitumen construction,asphalt contractors"
    },
    {
        "id": 117,
        "categoryId": 34,
        "subCategoryName": "Asphalt Products & Supplies",
        "icon": null,
        "clues": "asphalt profiling,asphalt products,asphalt & bitumen products,Asphalt Products & Supplies,asphalt supply,asphalt products & supplies"
    },
    {
        "id": 129,
        "categoryId": 34,
        "subCategoryName": "Auto Electrician Services",
        "icon": null,
        "clues": "motorcycle auto electrician,mobile auto services,auto electrical services mobile services,auto electrician,auto mechanical services,mobile auto electrician,auto electrician mobile,automotive auto electrician,auto electrical services alternators,Auto Electrician Services,auto transmission services,mechanic auto electrician,electrician auto,24 hour auto electrician,auto services centre,mobile auto electrical services,auto electrical services,auto electrical services starter motors,auto electricians,auto electrical services trucks,auto car services,auto air & electrician,mobile truck auto electrician,auto gas services,auto services repair,truck auto electrician,services auto,auto services,auto electrical services mobile,auto electrical services repairs"
    },
    {
        "id": 153,
        "categoryId": 34,
        "subCategoryName": "Balustrading",
        "icon": null,
        "clues": "balustrading aluminium,balustrading,Balustrading,balustrading stainless steel,balustrading glass,balustrading wire rope,balustrading handrail fittings,balustrading balustrades"
    },
    {
        "id": 154,
        "categoryId": 34,
        "subCategoryName": "Bamboo & Timber Flooring",
        "icon": null,
        "clues": "Bamboo & Timber Flooring,bamboo floor,bamboo flooring,flooring bamboo,bamboo flooring layers,bamboo fencing"
    },
    {
        "id": 172,
        "categoryId": 34,
        "subCategoryName": "Bath Resurfacing",
        "icon": null,
        "clues": "Bath Resurfacing,bath & basin resurfacing"
    },
    {
        "id": 175,
        "categoryId": 34,
        "subCategoryName": "Bathroom Renovations & Designs",
        "icon": null,
        "clues": "bathroom renovations vanities,Bathroom Renovations & Designs,bathroom renovations plumbing,bathroom renovations,bathroom renovations leaking showers,bathroom renovations tiles,bathroom renovations shower screens,bathroom renovations tiling"
    },
    {
        "id": 204,
        "categoryId": 34,
        "subCategoryName": "Bitumen Spraying",
        "icon": null,
        "clues": "bitumen spraying,Bitumen Spraying"
    },
    {
        "id": 206,
        "categoryId": 34,
        "subCategoryName": "Blacksmiths",
        "icon": null,
        "clues": "Blacksmiths,Blacksmiths &/or Farriers,blacksmiths,blacksmiths &/or farriers,blacksmiths & farriers"
    },
    {
        "id": 208,
        "categoryId": 34,
        "subCategoryName": "Blasting Equipment & Contractors",
        "icon": null,
        "clues": "blasting drilling,Blasting Equipment & Contractors,sand blasting equipment,blasting contractors & equipment,blasting resistant module,blasting hole drilling,water blasting,bead blasting,blasting,water blasting equipment,blasting & painting,blasting equipment"
    },
    {
        "id": 225,
        "categoryId": 34,
        "subCategoryName": "Boiler & Furnace Cleaning",
        "icon": null,
        "clues": "boiler & furnace cleaning,Boiler & Furnace Cleaning"
    },
    {
        "id": 226,
        "categoryId": 34,
        "subCategoryName": "Boiler Inspectors",
        "icon": null,
        "clues": "Boiler Inspectors,boiler inspectors"
    },
    {
        "id": 227,
        "categoryId": 34,
        "subCategoryName": "Boilers & Boiler Service",
        "icon": null,
        "clues": "boiler making,Boilers & Boiler Service,boiler repair,boiler maintenance,boiler fabrication,boiler manufacturers,boiler services,boilers & boiler service"
    },
    {
        "id": 238,
        "categoryId": 34,
        "subCategoryName": "Boring & Drilling Contractors",
        "icon": null,
        "clues": "boring contractors,excavating & earth moving contractors boring,boring & drilling contractors underground work,boring drilling contractors & companies,boring & drilling contractors directional,boring & drilling contractors,water boring contractors,boring & drilling contractors water boring,boring & drilling contractors exploration,boring services,under boring contractors,Boring & Drilling Contractors,drilling boring contractors,boring contractors mineral exploration,boring & drilling contractors bore water,hard rock boring contractors"
    },
    {
        "id": 239,
        "categoryId": 34,
        "subCategoryName": "Boring & Drilling Equipment",
        "icon": null,
        "clues": "boring & drilling equipment & supplies,boring & drilling equipment,boring machine,boring & drilling suppliers,Boring & Drilling Equipment"
    },
    {
        "id": 260,
        "categoryId": 34,
        "subCategoryName": "Brick Cleaning",
        "icon": null,
        "clues": "Brick Cleaning,brick & brick wall cleaning,stain removal brick cleaning,brick cleaning graffiti removal,brick wall cleaning,graffiti removal brick cleaning,graffiti cleaning brick cleaning,brick cleaning"
    },
    {
        "id": 268,
        "categoryId": 34,
        "subCategoryName": "Bridges &/or Bridge Contractors",
        "icon": null,
        "clues": "bridge construction,bridge builders,Bridges &/or Bridge Contractors,bridges &/or bridge contractors,bridge contractors,concrete contractors bridges"
    },
    {
        "id": 274,
        "categoryId": 34,
        "subCategoryName": "Builders & Building Contractors",
        "icon": null,
        "clues": "house builders,Builders & Building Contractors,home builders,building contractors--alterations extensions & renovations master builders association,building contractors--alterations,extensions & renovations contract builders"
    },
    {
        "id": 275,
        "categoryId": 34,
        "subCategoryName": "Builders & Contractors Equipment Hire",
        "icon": null,
        "clues": "hire--builders',contractors' & handyman's equipment scissor lifts,hire--builders',contractors' & handyman's equipment,hire--builders',contractors' & handyman's equipment industrial,builders contractors & handyman equipment,hire--builders',contractors' & handyman's equipment contractors,Builders & Contractors Equipment Hire,hire--builders',contractors' & handyman's equipment fencing,hire builders equipment,builders' & contractors' equipment,hire--builders',contractors' & handyman's equipment dingo,hire--builders',contractors' & handyman's equipment handymen,hire--builders',contractors' & handyman's equipment builders,builders hire equipment,builders' contractors' & handyman's equipment,hire--builders',contractors' & handyman's equipment diggers,hire--builders',contractors' & handyman's equipment gardening"
    },
    {
        "id": 276,
        "categoryId": 34,
        "subCategoryName": "Builders' Equipment",
        "icon": null,
        "clues": ""
    },
    {
        "id": 277,
        "categoryId": 34,
        "subCategoryName": "Building Consultants",
        "icon": null,
        "clues": "building design consultants,building services consultants,team building consultants,Building Consultants,building consultants,building code consultants"
    },
    {
        "id": 278,
        "categoryId": 34,
        "subCategoryName": "Building Contractors--Maintenance & Repairs",
        "icon": null,
        "clues": "building contractors--maintenance,building contractors--maintenance & repairs gyprock,building maintenance contractors,building contractors--maintenance & repairs electricians,building contractors--maintenance & repairs,Building Contractors--Maintenance & Repairs,building maintenance & repairs graffiti removal"
    },
    {
        "id": 279,
        "categoryId": 34,
        "subCategoryName": "Building Design - Extensions, Renovations & Alterations",
        "icon": null,
        "clues": "building contractors alterations additions,building design consultants,building extensions renovations,building extensions,building contractors--alterations,extensions & renovations houses,building contractors alterations,building contractors--alterations,extensions & renovations commercial,building contractors--alterations,extensions & renovations verandahs,building design services,building contractors--alterations,extensions & renovations decks,building contractors extensions,building extensions alterations,building contractors--alterations,extensions & renovations architects,building contractors--alterations,extensions & renovations contract builders,building contractors alterations extensions & renovations,building alterations,building contractors alterations & repair,building contractors alterations & renovations,building renovations,building design drafting,home building renovations,building contractors renovations,building contractors--alterations extensions,building contractors renovations extensions,Building Design - Extensions,Renovations & Alterations,building contractors--alterations,extensions & renovations residential,building contractors--alterations,extensions & renovations carpenters,building contractors--alterations,extensions & renovations construction,building contractors--alterations,extensions & renovations electricians"
    },
    {
        "id": 280,
        "categoryId": 34,
        "subCategoryName": "Building Designers",
        "icon": null,
        "clues": "building designers residential,building designers draftsmen,building designers environmentally sustainable design,building designers renovators,Building Designers,building designers,building designers design"
    },
    {
        "id": 281,
        "categoryId": 34,
        "subCategoryName": "Building Foundation & Excavation",
        "icon": null,
        "clues": "building foundations,Building Foundation & Excavation,building excavations,building excavations & foundations,building foundation repair,building excavations & foundations civil contractors"
    },
    {
        "id": 282,
        "categoryId": 34,
        "subCategoryName": "Building Information Centres",
        "icon": null,
        "clues": "Building Information Centres,building information,building information centres"
    },
    {
        "id": 283,
        "categoryId": 34,
        "subCategoryName": "Building Inspection",
        "icon": null,
        "clues": "building inspection services dilapidation,building inspections reports,Building Inspection,pest & building inspections,building inspections,building inspection services building certification"
    },
    {
        "id": 284,
        "categoryId": 34,
        "subCategoryName": "Building Management Services & Consultants",
        "icon": null,
        "clues": "Building Management Services & Consultants,building design consultants,building services consultants,team building consultants,building management,building management services & consultants,building code consultants,building management services"
    },
    {
        "id": 285,
        "categoryId": 34,
        "subCategoryName": "Building Restoration",
        "icon": null,
        "clues": "Building Restoration,restoration building,building restoration services & supplies,building restoration,building restoration services,building restoration supplies,heritage building restorations"
    },
    {
        "id": 286,
        "categoryId": 34,
        "subCategoryName": "Building Societies",
        "icon": null,
        "clues": "Building Societies,building societies,bank building societies"
    },
    {
        "id": 287,
        "categoryId": 34,
        "subCategoryName": "Building Supplies",
        "icon": null,
        "clues": "building supplies metal,building supplies handymen,building supplies aluminium,building supplies architectural mouldings,building supplies insulation,second hand building supplies,building supplies brick,building supplies tiles,building supplies steel,building supplies concrete,building supplies,Building Supplies,building supplies sheet metal"
    },
    {
        "id": 288,
        "categoryId": 34,
        "subCategoryName": "Building Surveyors",
        "icon": null,
        "clues": "land building surveyors,building surveyors,Building Surveyors,building surveyors pca,private building surveyors,building surveyors accredited,building surveyors building inspections,building surveyors building certification"
    },
    {
        "id": 289,
        "categoryId": 34,
        "subCategoryName": "Buildings - Prefabricated & Transportable - Domestic",
        "icon": null,
        "clues": "prefabricated buildings,steel buildings,buildings - relocatable & transportable - domestic,buildings--prefabricated,demountable buildings,Buildings - Prefabricated & Transportable - Domestic,relocatable transportable buildings,buildings - prefabricated & transportable - domestic,industrial buildings,buildings--relocatable & transportable--domestic granny flats,portable buildings,buildings--relocatable,relocatable buildings,prefabricated transportable commercial industrial buildings,buildings transportable,transportable buildings"
    },
    {
        "id": 290,
        "categoryId": 34,
        "subCategoryName": "Built In Wardrobes",
        "icon": null,
        "clues": "timber built in wardrobes,Built In Wardrobes,built in wall robes,wardrobes--built-in mirrored doors,built in,built in wardrobes,built in wardrobe door,built in storage,wardrobes--built-in,built in robes,built in furniture,built in bookcases,wardrobes--built-in sliding doors,built in cupboards"
    },
    {
        "id": 311,
        "categoryId": 34,
        "subCategoryName": "Cabinet Makers & Designers",
        "icon": null,
        "clues": "Cabinet Makers & Designers,cabinet makers' supplies benchtops,cabinet makers custom built,kitchen cabinet makers,cabinet makers kitchens,cabinet makers diy,cabinet makers display cabinets,cabinet makers,cabinet makers cupboards,cabinet makers equipment,cabinet makers' supplies,cabinet makers vanities,cabinet makers' equipment"
    },
    {
        "id": 312,
        "categoryId": 34,
        "subCategoryName": "Cabinet Making Supplies",
        "icon": null,
        "clues": "kitchen cabinet supplies,cabinet supplies,cabinet makers' supplies benchtops,Cabinet Making Supplies,cabinet maker supplies,cabinet makers' supplies,cabinet hardware supplies"
    },
    {
        "id": 372,
        "categoryId": 34,
        "subCategoryName": "Carpenters & Joiners",
        "icon": null,
        "clues": "roof carpenters,Carpenters & Joiners,carpenters & joiners carpentry,joiners & carpenters,carpenters,carpenters & joiners"
    },
    {
        "id": 376,
        "categoryId": 34,
        "subCategoryName": "Carpet Layers & Planners",
        "icon": null,
        "clues": "carpet layers equipment,Carpet Layers & Planners,flood damage carpet layers,carpet layers tools,carpet layers flood damage,carpet layers,carpet & vinyl layers,commercial carpet layers,carpet layers & repair,carpet layers & planners,carpet retail layers"
    },
    {
        "id": 378,
        "categoryId": 34,
        "subCategoryName": "Carpet Repair",
        "icon": null,
        "clues": "carpet & vinyl repair,Carpet Repair,carpet repairers & restorers,stain removal carpet repair & restoration,carpet layers & repair,auto carpet repair,carpet & furniture cleaning & protection colour repair,carpet repair,persian carpet repair"
    },
    {
        "id": 432,
        "categoryId": 34,
        "subCategoryName": "Chimney Sweep",
        "icon": null,
        "clues": "chimney,chimney sweeps,chimney sweep,Chimney Sweep,chimney cleaning"
    },
    {
        "id": 487,
        "categoryId": 34,
        "subCategoryName": "Commercial & Industrial Cladding",
        "icon": null,
        "clues": "Commercial & Industrial Cladding"
    },
    {
        "id": 490,
        "categoryId": 34,
        "subCategoryName": "Commercial & Industrial Refrigeration",
        "icon": null,
        "clues": "commercial refrigeration hire,refrigeration commercial & industrial,refrigeration--commercial & industrial--retail & service refrigeration engineers,Commercial & Industrial Refrigeration,commercial refrigeration services,refrigeration commercial repair,refrigeration commercial,commercial & industrial refrigeration,commercial refrigeration mechanic,refrigeration--commercial & industrial--retail & service,commercial refrigeration,second hand commercial refrigeration,commercial refrigeration equipment,refrigeration repair commercial,commercial refrigeration repair,commercial air conditioning & refrigeration"
    },
    {
        "id": 491,
        "categoryId": 34,
        "subCategoryName": "Commercial Air Conditioning",
        "icon": null,
        "clues": "Commercial Air Conditioning,commercial air conditioning services,air conditioning--commercial & industrial hire,air conditioning--commercial & industrial,air conditioning--commercial & industrial ducted systems,air conditioning commercial services & maintenance,air conditioning services commercial,commercial air conditioning,air conditioning industrial & commercial,fisher & paykel commercial air conditioning,commercial air conditioning & air handling,air conditioning commercial,air conditioning--commercial & industrial contractors,commercial air conditioning & refrigeration,commercial air conditioning ducting"
    },
    {
        "id": 492,
        "categoryId": 34,
        "subCategoryName": "Commercial Building Maintenance",
        "icon": null,
        "clues": "building maintenance services--commercial rope access,building maintenance services--commercial carpenters,commercial garden maintenance,Commercial Building Maintenance,commercial maintenance,plumbing maintenance services commercial,commercial building maintenance,commercial property maintenance,building maintenance services--commercial,maintenance commercial"
    },
    {
        "id": 506,
        "categoryId": 34,
        "subCategoryName": "Composite & Epoxy Flooring",
        "icon": null,
        "clues": "composite cladding,Composite & Epoxy Flooring,composite suppliers,composite wood,composite decks,composite decking,composite timber,composite materials"
    },
    {
        "id": 507,
        "categoryId": 34,
        "subCategoryName": "Compressed Air & Pneumatic Tools",
        "icon": null,
        "clues": "compressed air tools,Compressed Air & Pneumatic Tools"
    },
    {
        "id": 508,
        "categoryId": 34,
        "subCategoryName": "Compressed Air Control Equipment",
        "icon": null,
        "clues": "Compressed Air Control Equipment,compressed air control equipment"
    },
    {
        "id": 527,
        "categoryId": 34,
        "subCategoryName": "Concrete Contractors",
        "icon": null,
        "clues": "concrete contractors formwork,concrete contractors foundations,concrete contractors patterned,concrete contractors slab concreters,concrete contractors retaining walls,concrete contractors tilt panels,concrete contractors cement,concrete contractors floor levelling,concrete contractors stencilling,concrete contractors spraying,concrete contractors steel fixers,concrete contractors,concrete contractors construction,concrete contractors exposed aggregate,concrete contractors precasters,Concrete Contractors,concrete contractors crushed rock,concrete contractors polishing"
    },
    {
        "id": 530,
        "categoryId": 34,
        "subCategoryName": "Concrete Gutters & Kerbs",
        "icon": null,
        "clues": "Concrete Gutters & Kerbs,concrete kerbs & gutters,concrete gutters,concrete kerbs,concrete kerbs & gutters kwik kerb"
    },
    {
        "id": 531,
        "categoryId": 34,
        "subCategoryName": "Concrete Mixers",
        "icon": null,
        "clues": "reversible concrete mixers,Concrete Mixers,concrete mixers machines,mini concrete mixers,hire--builders',contractors' & handyman's equipment concrete mixers,concrete mixers hire"
    },
    {
        "id": 532,
        "categoryId": 34,
        "subCategoryName": "Concrete Paving",
        "icon": null,
        "clues": "paving--concrete driveways,concrete products paving,Concrete Paving,concrete paving slabs,paving--concrete concreting,paving--concrete poured limestone,concrete paving stones,paving--concrete,concrete spray paving,concrete paving"
    },
    {
        "id": 533,
        "categoryId": 34,
        "subCategoryName": "Concrete Polishing",
        "icon": null,
        "clues": "concrete treatment & repair concrete polishing,Concrete Polishing,concrete floor polishing & preparation,concrete polishing,concrete grinding & polishing,concrete contractors polishing,concrete floor polishing"
    },
    {
        "id": 537,
        "categoryId": 34,
        "subCategoryName": "Concrete Resurfacing & Repair",
        "icon": null,
        "clues": "concrete treatment & repair concrete polishing,resurfacing concrete,concrete resurfacing,concrete repair & sealing,concrete treatment & repair resurfacing,concrete repair,Concrete Resurfacing & Repair,concrete treatment & repair"
    },
    {
        "id": 538,
        "categoryId": 34,
        "subCategoryName": "Concrete Screeding &/or Trowelling Machinery",
        "icon": null,
        "clues": "Concrete Screeding &/or Trowelling Machinery,concrete screeding &/or trowelling machinery,concrete screeding"
    },
    {
        "id": 539,
        "categoryId": 34,
        "subCategoryName": "Concrete Slab Floors",
        "icon": null,
        "clues": "pre cast concrete slab manufacturers,suspended concrete slabs,concrete paving slabs,concrete slab floors,concrete slabs repair,Concrete Slab Floors,concrete slab work"
    },
    {
        "id": 540,
        "categoryId": 34,
        "subCategoryName": "Concrete Sleepers, Reinforcements & Mesh",
        "icon": null,
        "clues": "concrete reinforcements fixers,concrete sleepers,Concrete Sleepers,Reinforcements & Mesh,landscape supplies concrete sleepers,concrete steel mesh,wire mesh concrete reinforcements,concrete mesh,concrete reinforcements,concrete reinforcements steel"
    },
    {
        "id": 547,
        "categoryId": 34,
        "subCategoryName": "Conservatories",
        "icon": null,
        "clues": "Conservatories,conservatories"
    },
    {
        "id": 549,
        "categoryId": 34,
        "subCategoryName": "Construction Management",
        "icon": null,
        "clues": "construction management,construction site management,Construction Management,construction management construction,domestic construction & project management,building construction management,construction management project management,construction & project management,construction management engineering,commercial construction management,project management construction"
    },
    {
        "id": 563,
        "categoryId": 34,
        "subCategoryName": "Coopers",
        "icon": null,
        "clues": "Coopers,coopers"
    },
    {
        "id": 566,
        "categoryId": 34,
        "subCategoryName": "Coppersmiths",
        "icon": null,
        "clues": "coppersmiths,Coppersmiths"
    },
    {
        "id": 585,
        "categoryId": 34,
        "subCategoryName": "Crane Hire",
        "icon": null,
        "clues": "mobile crane hire,crane hire tower,crane truck hire,crane hire,Crane Hire,hiab crane truck hire,hiab crane hire,crane hire crane trucks"
    },
    {
        "id": 621,
        "categoryId": 34,
        "subCategoryName": "Decking Contractors & Builders",
        "icon": null,
        "clues": "Decking Contractors & Builders,decking patios,decking installation,timber decking,patio builders decking,decking repair,decking design,builders decking,decking & pergolas,decking,decking builders,decking tiles,decking supplies,decking contractors"
    },
    {
        "id": 626,
        "categoryId": 34,
        "subCategoryName": "Demolition Contractors",
        "icon": null,
        "clues": "demolition contractors & equipment excavators,demolition contractors & equipment friable asbestos,house demolition,demolition contractors & equipment rubbish removals,Demolition Contractors,demolition contractors & equipment houses,demolition contractors & equipment recycling,demolition contractors & equipment,demolition contractors & equipment machine hire,excavating & earth moving contractors demolition,demolition contractors & equipment asbestos removal,demolition contractors & equipment salvage,demolition,demolition contractors,demolition contractors rock sawing"
    },
    {
        "id": 689,
        "categoryId": 34,
        "subCategoryName": "Drainage & Drain Contractors",
        "icon": null,
        "clues": "excavating & earth moving contractors drainage,drainage contractors,Drainage & Drain Contractors,civil drainage contractors"
    },
    {
        "id": 690,
        "categoryId": 34,
        "subCategoryName": "Drainage Equipment & Systems",
        "icon": null,
        "clues": "drainage products,drainage equipment,drainage pipes,drainage grates,drainage equipment & systems,drainage supplies,drainage systems,civil drainage,Drainage Equipment & Systems"
    },
    {
        "id": 691,
        "categoryId": 34,
        "subCategoryName": "Drawing, Drafting Equipment & Supplies",
        "icon": null,
        "clues": "drawing & drafting equipment & supplies,drafting equipment,Drawing,Drafting Equipment & Supplies,drawing equipment,drafting supplies,drawing supplies,drawing & drafting equipment,drawing boards,drafting services structural engineering drafting,drafting equipment & supplies"
    },
    {
        "id": 698,
        "categoryId": 34,
        "subCategoryName": "Driveways",
        "icon": null,
        "clues": "paving--concrete driveways,gravel driveways,bitumen driveways,paving--asphalt & bitumen driveways,asphalt driveways,Driveways,concrete driveways,driveways,excavating & earth moving contractors driveways,concrete treatment & repair driveways,cement driveways"
    },
    {
        "id": 723,
        "categoryId": 34,
        "subCategoryName": "Electric Cable Ducting Systems",
        "icon": null,
        "clues": "Electric Cable Ducting Systems,electric cable ducting systems,electric cable ducting"
    },
    {
        "id": 724,
        "categoryId": 34,
        "subCategoryName": "Electric Cable, Wire Wholesalers & Manufacturers",
        "icon": null,
        "clues": "Electric Cable,Wire Wholesalers & Manufacturers,electric cable & wire - wholesalers & manufacturers,cable & electric wire,electric cable & wire"
    },
    {
        "id": 725,
        "categoryId": 34,
        "subCategoryName": "Electric Elements",
        "icon": null,
        "clues": "electric heating elements,Electric Elements,electric elements,electric stove elements,elements electric stoves"
    },
    {
        "id": 739,
        "categoryId": 34,
        "subCategoryName": "Electrical Insulators & Insulating Materials",
        "icon": null,
        "clues": "electrical insulators,Electrical Insulators & Insulating Materials,electrical materials,electrical insulators &/or insulating materials"
    },
    {
        "id": 746,
        "categoryId": 34,
        "subCategoryName": "Electricians & Electrical Contractors",
        "icon": null,
        "clues": "Electricians & Electrical Contractors,electricians,building contractors--maintenance & repairs electricians,building contractors electricians,building contractors--alterations,extensions & renovations electricians"
    },
    {
        "id": 761,
        "categoryId": 34,
        "subCategoryName": "Elevated Work Platform Hire",
        "icon": null,
        "clues": "training & development elevated work platform,Elevated Work Platform Hire"
    },
    {
        "id": 766,
        "categoryId": 34,
        "subCategoryName": "Emergency Lighting & Power",
        "icon": null,
        "clues": "lighting & power--emergency,emergency lighting,temporary power hire emergency power,Emergency Lighting & Power,emergency electrician,emergency services lighting"
    },
    {
        "id": 801,
        "categoryId": 34,
        "subCategoryName": "Excavation & Earth Moving Equipment",
        "icon": null,
        "clues": "Excavation & Earth Moving Equipment"
    },
    {
        "id": 802,
        "categoryId": 34,
        "subCategoryName": "Excavation & Earthmoving Contractors",
        "icon": null,
        "clues": "Earthmoving Contractors,Excavation Contractors,Excavation & Earthmoving Contractors"
    },
    {
        "id": 844,
        "categoryId": 34,
        "subCategoryName": "Fencing Contractors",
        "icon": null,
        "clues": "fencing contractors brushwood,fencing contractors picket top,fencing contractors metal,fencing contractors chainlink,glass fencing contractors,agricultural fencing contractors,fencing contractors neighbourhood fencing,fencing contractors barbed wire,aluminium fencing contractors,fencing contractors,colorbond fencing contractors,fencing contractors cyclone,Fencing Contractors,swimming pool fencing contractors,fencing contractors hardifence,fencing contractors palings,fencing contractors garden,fencing contractors boundary,temporary fencing contractors"
    },
    {
        "id": 845,
        "categoryId": 34,
        "subCategoryName": "Fencing Materials",
        "icon": null,
        "clues": "fencing materials residential,aluminium fencing materials,rural fencing materials,Fencing Materials,fencing materials picket,timber fencing materials,wire mesh fencing materials,fencing materials chain wire mesh,fencing materials neighbourhood fencing,fencing materials agricultural,fencing materials"
    },
    {
        "id": 851,
        "categoryId": 34,
        "subCategoryName": "Fibre Cement Building Products",
        "icon": null,
        "clues": "fibre cement,fibre cement building products,fibre cement fencing contractors,hardware--retail fibre cement sheeting,Fibre Cement Building Products,fibre cement sheet,fibre cement cladding"
    },
    {
        "id": 871,
        "categoryId": 34,
        "subCategoryName": "Fire Doors & Fire Windows",
        "icon": null,
        "clues": "fire rated windows,fire windows,fire doors & windows,Fire Doors & Fire Windows,fire doors & frames"
    },
    {
        "id": 872,
        "categoryId": 34,
        "subCategoryName": "Fire Resistant Materials & Supplies",
        "icon": null,
        "clues": "Fire Resistant Materials & Supplies,fire resistant materials,fire resistant,fire resistant paint,fire supplies,fire resistant fabric,fire resistant materials & supplies"
    },
    {
        "id": 890,
        "categoryId": 34,
        "subCategoryName": "Floor Covering Layers",
        "icon": null,
        "clues": "lino floor covering,floor covering retail,floor covering installers,floor covering layers,industrial vinyl floor covering,floor covering wholesalers,Floor Covering Layers"
    },
    {
        "id": 891,
        "categoryId": 34,
        "subCategoryName": "Floor Coverings",
        "icon": null,
        "clues": "floor coverings cork,timber floor coverings,sheepskin products floor coverings,floor coverings installation,floor coverings vinyl,floor coverings retail,natural floor coverings,carpet floor coverings,floor coverings laminate,commercial floor coverings,floor coverings tiles,floor coverings linoleum,floor coverings,vinyl floor coverings,Floor Coverings"
    },
    {
        "id": 893,
        "categoryId": 34,
        "subCategoryName": "Floor Sanding & Polishing",
        "icon": null,
        "clues": "floor sanding & polishing cork,floor sanding & finishing,floor sanding machines,marble floor polishing,timber floor sanding & polishing,floor sanding equipment,floor sanding services,timber floor polishing,floor sanding & polishing floor boards,floor polishing equipment,wood floor polishing,vinyl floor polishing,Floor Sanding & Polishing,floor polishing supplies,concrete floor polishing,floor sanding & polishing,floor polishing,floor sanding contractors,floor grinding & polishing,concrete floor polishing & preparation,floor sanding supplies,timber floor sanding,wooden floor polishing,floor sanding & polishing sanding,floor polishing sanding,floor sanding & polishing services,floor sanding,floor sanding equipment hire"
    },
    {
        "id": 894,
        "categoryId": 34,
        "subCategoryName": "Floor Tiles & Wall Tiles",
        "icon": null,
        "clues": "tiles--wall & floor cork,floor tiles retail,wall & floor tiling,tiles--wall & floor,Floor Tiles & Wall Tiles,tiles--wall & floor bathrooms,floor tiles,tile layers--wall & floor,floor tiles suppliers,tiles--wall & floor wholesale,floor coverings tiles,tiles--wall & floor italian tiles,tiles--wall & floor stone,ceramic floor tiles,tiles floor & wall,tiles--wall & floor floors,tiles--wall & floor vinyl,floor & wall tiles,tiles--wall & floor pools,tiles floor,ceramic tiles wall & floor,floor tiles cleaning"
    },
    {
        "id": 925,
        "categoryId": 34,
        "subCategoryName": "Forklifts & Forklift Repairs",
        "icon": null,
        "clues": "Forklifts & Forklift Repairs,used forklifts,forklifts wreckers parts,driving schools forklifts,toyota forklifts"
    },
    {
        "id": 963,
        "categoryId": 34,
        "subCategoryName": "Furniture Manufacturers & Wholesalers",
        "icon": null,
        "clues": "furniture - wholesalers & manufacturers bedroom furniture,furniture - wholesalers & manufacturers restaurants,furniture retail wholesalers,office furniture manufacturers,furniture - wholesalers & manufacturers tables,event furniture wholesalers,furniture - wholesalers & manufacturers cafes,wholesalers furniture,furniture - wholesalers & manufacturers chairs,furniture - wholesalers & manufacturers wood,furniture - wholesalers & manufacturers outdoor,furniture - wholesalers & manufacturers lounge suites,furniture wholesalers importers,Furniture Manufacturers & Wholesalers,furniture manufacturers & wholesalers,furniture - wholesalers and manufacturers,furniture manufacturers,furniture - wholesalers & manufacturers pine,furniture - wholesalers & manufacturers imported,outdoor furniture wholesalers"
    },
    {
        "id": 964,
        "categoryId": 34,
        "subCategoryName": "Furniture Manufacturers Supplies",
        "icon": null,
        "clues": "hotel restaurant & club furniture supplies,hospitality furniture supplies,Furniture Manufacturers Supplies,garden equipment & supplies garden furniture,furniture manufacturers supplies,furniture restoration supplies,furniture supplies,hotel,restaurant & club supplies furniture,hairdresser supplies & furniture"
    },
    {
        "id": 970,
        "categoryId": 34,
        "subCategoryName": "Galvanising & Tinning",
        "icon": null,
        "clues": "galvanising & tinning,galvanising services,steel galvanising,galvanising,hot dip galvanising,Galvanising & Tinning,metal galvanising"
    },
    {
        "id": 975,
        "categoryId": 34,
        "subCategoryName": "Garage Builders & Prefabricators",
        "icon": null,
        "clues": "garage builders & prefabricators,garage & shed builders,Garage Builders & Prefabricators,garage builders"
    },
    {
        "id": 976,
        "categoryId": 34,
        "subCategoryName": "Garage Doors & Fittings",
        "icon": null,
        "clues": "garage doors & fittings b&d,garage doors & fittings automatic openers,repair garage door,garage doors & fittings tilt-a-door,Garage Doors & Fittings,garage door fittings,garage doors & fittings,garage doors & fittings automatic gates,garage doors"
    },
    {
        "id": 992,
        "categoryId": 34,
        "subCategoryName": "Gates",
        "icon": null,
        "clues": "gates,gates automation,fences & gates,automatic gates,gates metal,security gates,gates & fencing,Gates"
    },
    {
        "id": 1019,
        "categoryId": 34,
        "subCategoryName": "Glass Processing & Handling Equipment",
        "icon": null,
        "clues": "glass processing machinery,glass processing,glass handling,glass processing & manufacturers,badges & badge making equipment & supplies glass,Glass Processing & Handling Equipment,glass processing & handling equipment,glass handling equipment,glass equipment,glass processing & mfrg,door & gate operating equipment glass doors"
    },
    {
        "id": 1055,
        "categoryId": 34,
        "subCategoryName": "Gutter Cleaning",
        "icon": null,
        "clues": "gutter cleaning,guttering & spouting gutter cleaning,Gutter Cleaning,roof repairers & cleaners gutter cleaning"
    },
    {
        "id": 1056,
        "categoryId": 34,
        "subCategoryName": "Guttering & Spouting",
        "icon": null,
        "clues": "guttering & spouting metal roofing,guttering & spouting leaf guards,guttering & spouting fascias,guttering & spouting repairs,guttering & spouting gutter cleaning,guttering & spouting guards,guttering installation,guttering supplies,Guttering & Spouting,guttering & spouting,spouting & guttering,guttering repair,guttering downpipes,guttering services,guttering contractors,guttering cleaning,roof guttering,guttering,polycarbonate guttering & spouting"
    },
    {
        "id": 1078,
        "categoryId": 34,
        "subCategoryName": "Hard Facing",
        "icon": null,
        "clues": "hard facing,Hard Facing"
    },
    {
        "id": 1129,
        "categoryId": 34,
        "subCategoryName": "Home Improvements",
        "icon": null,
        "clues": "home improvements renovations,Home Improvements,home improvements"
    },
    {
        "id": 1131,
        "categoryId": 34,
        "subCategoryName": "Home Maintenance & Handymen",
        "icon": null,
        "clues": "Home Maintenance & Handymen,home maintenance & repairs carpentry,home maintenance & repairs guttering,home maintenance & repairs cleaning,home maintenance & repairs,home maintenance & repairs fencing,home maintenance & repair,home maintenance,home maintenance & repairs insulation,home maintenance & repairs plastering"
    },
    {
        "id": 1156,
        "categoryId": 34,
        "subCategoryName": "House Cladding",
        "icon": null,
        "clues": "House Cladding,house cladding"
    },
    {
        "id": 1158,
        "categoryId": 34,
        "subCategoryName": "House Relocation",
        "icon": null,
        "clues": "house relocation,house relocation services,House Relocation"
    },
    {
        "id": 1159,
        "categoryId": 34,
        "subCategoryName": "House Restumping & Reblocking",
        "icon": null,
        "clues": "reblocking house,house restumping reblocking,House Restumping & Reblocking,house restumping reblocking & raising,house restumping,reblocking or raising,house raising & restumping,house restumping,house restumping reblocking for raising,house reblocking"
    },
    {
        "id": 1206,
        "categoryId": 34,
        "subCategoryName": "Installation & Maintenance Engineers",
        "icon": null,
        "clues": "Installation & Maintenance Engineers,engineers--installation &/or maintenance,heavy engineering installation maintenance,engineers--installation"
    },
    {
        "id": 1208,
        "categoryId": 34,
        "subCategoryName": "Insulation Installers & Contractors",
        "icon": null,
        "clues": "insulation removals,insulation contractors installation,industrial insulation contractors,loft insulation contractors,Insulation Installers & Contractors,insulation batts,insulation contractors insulation dust removal,pipe insulation contractors,insulation installers,roof insulation contractors,ceiling insulation installers,insulation contractors,insulation contractors removal,insulation contractors gold batts,residential insulation contractors,insulation contractors ceiling vacuum,roof insulation installers,insulation contractors acoustic,home insulation installers"
    },
    {
        "id": 1241,
        "categoryId": 34,
        "subCategoryName": "Joinery",
        "icon": null,
        "clues": "Joinery,joinery supplies,joinery carpenters,joinery door,joinery manufacturers,joinery workshops,joinery kitchen,joinery,joinery shop"
    },
    {
        "id": 1266,
        "categoryId": 34,
        "subCategoryName": "Labour & Contractors Hire",
        "icon": null,
        "clues": "labour hire construction,labour hire recruitment,employment--labour hire contractors fitters,employment--labour hire contractors carpenters,employment--labour hire contractors permanent,labour hire,employment--labour hire contractors hospitality,labour,labour hire services,employment--labour hire contractors packers,employment--labour hire contractors apprenticeships,labour hire mining,labour hire hospitality,employment--labour hire contractors cleaners,employment--labour hire contractors,employment--labour hire contractors engineering,employment--labour hire contractors casual,labour hire agencies,labour contractors,employment--labour hire contractors drivers,employment--labour hire contractors construction,labour hire contractors,employment--labour hire contractors manual labourers,employment--labour hire contractors recruitment,labour hire employment agencies,employment--labour hire contractors manufacturing,Labour & Contractors Hire,labour hire firms,employment--labour hire contractors mining,farm labour contractors,labour hire employment,labour hire casual"
    },
    {
        "id": 1277,
        "categoryId": 34,
        "subCategoryName": "Land Clearing & Firebreak Contractors",
        "icon": null,
        "clues": "land clearing & firebreak contractors,land clearing contractors,landscape contractors,Land Clearing & Firebreak Contractors,land clearing,excavating & earth moving contractors land clearing,land clearing firebreak mowing"
    },
    {
        "id": 1282,
        "categoryId": 34,
        "subCategoryName": "Landscaping & Landscape Design",
        "icon": null,
        "clues": "garden landscaping,landscaping solutions,landscaping landscapers,landscaping association,landscaping services,landscaping,Landscaping & Landscape Design,landscaping turf,landscaping & retaining wall,landscaping pergola designs,landscaping & fencing"
    },
    {
        "id": 1316,
        "categoryId": 34,
        "subCategoryName": "Lift Maintenance",
        "icon": null,
        "clues": "lift maintenance,Lift Maintenance"
    },
    {
        "id": 1317,
        "categoryId": 34,
        "subCategoryName": "Lifts & Elevators",
        "icon": null,
        "clues": "stair lifts,hydraulic lifts,residential lifts,lifts domestic,lifts--maintenance & repairs,lifts,ski lifts,car lifts,home lifts,lifts & escalators,wheelchair lifts,scissor lifts,Lifts & Elevators"
    },
    {
        "id": 1318,
        "categoryId": 34,
        "subCategoryName": "Lighting & Power Pole",
        "icon": null,
        "clues": "Lighting & Power Pole,poles--lighting & power"
    },
    {
        "id": 1319,
        "categoryId": 34,
        "subCategoryName": "Lighting Consultants",
        "icon": null,
        "clues": "lighting regulatory consultants,Lighting Consultants,lighting consultants"
    },
    {
        "id": 1320,
        "categoryId": 34,
        "subCategoryName": "Lighting Maintenance",
        "icon": null,
        "clues": "lighting maintenance,Lighting Maintenance"
    },
    {
        "id": 1321,
        "categoryId": 34,
        "subCategoryName": "Lighting Stores",
        "icon": null,
        "clues": "Lighting Stores"
    },
    {
        "id": 1322,
        "categoryId": 34,
        "subCategoryName": "Lighting Wholesalers & Manufacturers",
        "icon": null,
        "clues": "lighting & accessories - wholesalers & manufacturers,lighting manufacturers tennis lights,lighting & accessories wholesalers,garden lighting wholesalers,lighting & accessories - wholesalers & manufacturers sporting facilities,lighting & accessories manufacturers,wholesalers lighting,sports lighting manufacturers,tennis court lighting manufacturers,lighting wholesalers,lighting & accessories - wholesalers & manufacturers lamps,car park lighting manufacturers,Lighting Wholesalers & Manufacturers,lighting manufacturers,sportsground lighting manufacturers"
    },
    {
        "id": 1323,
        "categoryId": 34,
        "subCategoryName": "Lightning & Surge Protection",
        "icon": null,
        "clues": "lightning,lightning engineers,lightning & surge protection,Lightning & Surge Protection,lightning protection"
    },
    {
        "id": 1324,
        "categoryId": 34,
        "subCategoryName": "Lime",
        "icon": null,
        "clues": "Lime,lime suppliers,limestone blocks,limestone,lime spreading,lime,lime stabilisation"
    },
    {
        "id": 1330,
        "categoryId": 34,
        "subCategoryName": "Lintels & Arch Bars",
        "icon": null,
        "clues": "steel lintels,Lintels & Arch Bars,precast concrete lintels,lintels & floor beams,concrete lintels,lintels,lintels & arch bars"
    },
    {
        "id": 1336,
        "categoryId": 34,
        "subCategoryName": "Lock Distributors & Manufacturers",
        "icon": null,
        "clues": "Lock Distributors & Manufacturers,lock manufacturers"
    },
    {
        "id": 1337,
        "categoryId": 34,
        "subCategoryName": "Locksmiths & Locksmith Services",
        "icon": null,
        "clues": "locks & locksmiths,car locksmiths,automotive locksmiths,lock suppliers,Locksmiths & Locksmith Services,lockers,lock hardware,locks,auto locksmiths,lock out,locks & locksmiths lock opening,locks & locksmiths automotive,lock smith,lock repair,mobile locksmiths,lock manufacturers,locksmiths,lock up,lock up storage"
    },
    {
        "id": 1369,
        "categoryId": 34,
        "subCategoryName": "Marble & Granite Suppliers",
        "icon": null,
        "clues": "marble & granite merchants caesar stone,marble & granite suppliers,marble & granite merchants bench tops,marble granite stone,marble polishing,granite & marble suppliers,marble importers,marble & granite,marble stone,marble & granite merchants imported,marble & granite merchants bartops,marble & granite distributors,marble,marble & granite merchants,marble merchants,marble stone suppliers,Marble & Granite Suppliers,marble tiles,marble granite sandstone,marble & granite manufacturers,marble supplies,marble & granite wholesalers"
    },
    {
        "id": 1435,
        "categoryId": 34,
        "subCategoryName": "Metallurgists",
        "icon": null,
        "clues": "Metallurgists,metallurgists"
    },
    {
        "id": 1438,
        "categoryId": 34,
        "subCategoryName": "Meters, Equipment & Services",
        "icon": null,
        "clues": "meters,equipment & services,ph meters,water meters,water flow meters,lux meters,air flow meters,Meters,Equipment & Services,meters,noise meters,meters equipment,electricity meters,meters stove,moisture meters,flow meters,gas meters,parking meters"
    },
    {
        "id": 1444,
        "categoryId": 34,
        "subCategoryName": "Milliners' Supplies",
        "icon": null,
        "clues": "milliners' supplies,Milliners' Supplies"
    },
    {
        "id": 1445,
        "categoryId": 34,
        "subCategoryName": "Millinery",
        "icon": null,
        "clues": "bridal millinery,Millinery,millinery supplies,millinery wholesalers & manufacturers,millinery,millinery - retail"
    },
    {
        "id": 1465,
        "categoryId": 34,
        "subCategoryName": "Monumental Masons",
        "icon": null,
        "clues": "monumental plaques,monumental stone,monumental stonemasons,monumental masons,monumental masonry,gravestones & monumental masons,Monumental Masons,masons monumental,monumental works"
    },
    {
        "id": 1466,
        "categoryId": 34,
        "subCategoryName": "Monumental Masons' Supplies",
        "icon": null,
        "clues": "monumental plaques,monumental stone,monumental stonemasons,monumental masonry,gravestones & monumental masons,Monumental Masons' Supplies,monumental masons' supplies,masons monumental,monumental works"
    },
    {
        "id": 1467,
        "categoryId": 34,
        "subCategoryName": "Mooring Contractors",
        "icon": null,
        "clues": "mooring services,mooring contractors,boat mooring,Mooring Contractors,mooring"
    },
    {
        "id": 1598,
        "categoryId": 34,
        "subCategoryName": "Paint & Painting Equipment",
        "icon": null,
        "clues": "paint spray equipment,Paint & Painting Equipment"
    },
    {
        "id": 1600,
        "categoryId": 34,
        "subCategoryName": "Paint Removal Services & Supplies",
        "icon": null,
        "clues": "graffiti coating after paint removal,graffiti coating paint removal specialists,paint removal services,factory floor cleaning contractors paint removal,graffiti paint removal,paint removalists,graffiti protection after paint removal,lead paint removals,paint removal services & supplies graffiti removal,paint removal graffiti removal,Paint Removal Services & Supplies,paint removal door,paint removals,paint removal services & supplies"
    },
    {
        "id": 1602,
        "categoryId": 34,
        "subCategoryName": "Painters & Decorators",
        "icon": null,
        "clues": "painters & decorators repairs,panel beaters & painters,painters & decorators,painters & decorators re-painting,house painters,painters & decorators homes,painters & decorators spray painting,Painters & Decorators,painters & decorators commercial,painters & decorators graffiti removal,painters,painters & decorators flood damage"
    },
    {
        "id": 1609,
        "categoryId": 34,
        "subCategoryName": "Panelling--Interior",
        "icon": null,
        "clues": "Panelling--Interior,panelling--interior"
    },
    {
        "id": 1623,
        "categoryId": 34,
        "subCategoryName": "Parquetry Flooring",
        "icon": null,
        "clues": "Parquetry Flooring,parquetry,parquetry flooring,parquetry floor"
    },
    {
        "id": 1624,
        "categoryId": 34,
        "subCategoryName": "Particle Board",
        "icon": null,
        "clues": "Particle Board,particle board,timber--trade & retail particle board"
    },
    {
        "id": 1625,
        "categoryId": 34,
        "subCategoryName": "Partitions & Fitouts",
        "icon": null,
        "clues": "partitions,partitions & ceiling,washroom partitions,ceiling & partitions,office fitouts & partitions,office partitions,partitions walls,partitions toilet,Partitions & Fitouts,toilet partitions,aluminium partitions,glass partitions"
    },
    {
        "id": 1635,
        "categoryId": 34,
        "subCategoryName": "Patio Builders",
        "icon": null,
        "clues": "Patio Builders,patio builders decking,patio builders,patio builders council approvals,patio timber builders,patio builders pergolas"
    },
    {
        "id": 1638,
        "categoryId": 34,
        "subCategoryName": "Paving Pebble",
        "icon": null,
        "clues": "pebble paving,Paving Pebble,paving--pebble,paving--pebble pebble mix"
    },
    {
        "id": 1653,
        "categoryId": 34,
        "subCategoryName": "Pest Control",
        "icon": null,
        "clues": "Pest Control,pest control rats,pest exterminators,pest & building inspections,pest control inspections,pest control,pest spray,pest control owner operated,pest control treatments,pest,pest control bed bugs,pest control bird control,building & pest,pest control spiders,pest management,pest control commercial,pest control services,pest control fleas,pest control termites,pest control cockroaches,pest inspections,pest control snakes,pest control possums,pest control fumigation,building & pest inspections,pest control supplies"
    },
    {
        "id": 1683,
        "categoryId": 34,
        "subCategoryName": "Piano Removals",
        "icon": null,
        "clues": "piano removals,piano removalists,Piano Removals"
    },
    {
        "id": 1692,
        "categoryId": 34,
        "subCategoryName": "Pile Driving Contractors & Equipment",
        "icon": null,
        "clues": "pile driving,pile driving contractors or equipment,pile driving contractors or equipment steel foundations contracting,marine pile driving,pile driving contractors or equipment drilled piers,pile driving equipment,pile driving contractors or equipment sheet piling,pile driving contractors & equipment,Pile Driving Contractors & Equipment,pile driving contractors"
    },
    {
        "id": 1694,
        "categoryId": 34,
        "subCategoryName": "Pipe Cutting & Threading",
        "icon": null,
        "clues": "Pipe Cutting & Threading,pipe cutting,pipe cutting & threading"
    },
    {
        "id": 1696,
        "categoryId": 34,
        "subCategoryName": "Pipe Line Equipment",
        "icon": null,
        "clues": "Pipe Line Equipment,pipe line equipment,pipe line contractors"
    },
    {
        "id": 1697,
        "categoryId": 34,
        "subCategoryName": "Pipe Lining & Coating",
        "icon": null,
        "clues": "pipe lining,pipe lining & coating,Pipe Lining & Coating,pipe coating,pipe relining & coating"
    },
    {
        "id": 1699,
        "categoryId": 34,
        "subCategoryName": "Pipeline Contractors",
        "icon": null,
        "clues": "Pipeline Contractors,pipeline testing,pipeline welding,pipeline testing services,pipeline services,civil pipeline contractors,pipeline detectors,pipeline layers,pipeline cctv,pipeline construction,pipeline fabrication"
    },
    {
        "id": 1700,
        "categoryId": 34,
        "subCategoryName": "Pipes - Concrete & Clay",
        "icon": null,
        "clues": "pipes concrete & clay agricultural,clay pipes,Pipes - Concrete & Clay,pipes concrete & clay rocla,concrete pipes,pipes concrete & clay covers,pipes concrete & clay reinforced concrete,pipes concrete & clay irrigation,pipes concrete & clay,pipes concrete & clay stormwater,pipes concrete & clay drainage,concrete & clay pipes,pipes concrete & clay sewerage"
    },
    {
        "id": 1706,
        "categoryId": 34,
        "subCategoryName": "Plaster & Plastering Supplies",
        "icon": null,
        "clues": "plaster,solid plasterers,plaster repair,art supplies plaster,gyprock plaster supplies,plaster supplies,plaster ceiling,plaster & plasterboard supplies,building supplies plaster boards,Plaster & Plastering Supplies,plasterboard,plaster moulds,plaster mouldings,plasterers--plasterboard fixers,plaster cornice,plaster products"
    },
    {
        "id": 1707,
        "categoryId": 34,
        "subCategoryName": "Plasterers",
        "icon": null,
        "clues": "solid plasterers,plasterers--plasterboard fixers ceilings,plasterers--plasterboard fixers repairs,plasterers services,plasterers,plasterers retail,plasterers supplies,plasterers building,Plasterers,plasterers specialty,plasterers products,plasterers solid,plasterers tools,plasterers contractors,plasterers--plasterboard fixers"
    },
    {
        "id": 1708,
        "categoryId": 34,
        "subCategoryName": "Plastering Supplies & Equipment",
        "icon": null,
        "clues": "Plastering Supplies & Equipment,plastering supplies & equipment,home maintenance & repairs plastering"
    },
    {
        "id": 1715,
        "categoryId": 34,
        "subCategoryName": "Plastic Pipes & Fittings",
        "icon": null,
        "clues": "plastic fittings,pipes & fittings--plastic pvc,pipes & fittings--plastic,Plastic Pipes & Fittings,plastic pipe fittings"
    },
    {
        "id": 1718,
        "categoryId": 34,
        "subCategoryName": "Plastic Repairs",
        "icon": null,
        "clues": "Plastic Repairs"
    },
    {
        "id": 1719,
        "categoryId": 34,
        "subCategoryName": "Plastics Consultants",
        "icon": null,
        "clues": "plastics consultants,Plastics Consultants"
    },
    {
        "id": 1726,
        "categoryId": 34,
        "subCategoryName": "Plumbers & Gas Fitters",
        "icon": null,
        "clues": "plumbers & gasfitters tank installation,plumbers & gasfitters maintenance,gas fitters & plumbers,Plumbers & Gas Fitters,plumbers & gasfitters gas appliances,plumbers & gasfitters bathroom renovations,plumbers & gasfitters hot water services,plumbers & gasfitters camera inspections,plumbers & drainers,plumbers & gasfitters drains,plumbers & gasfitters natural gas,plumbers & gasfitters renovations,plumbers & gasfitters reece,commercial plumbers,plumbers & gasfitters blocked drains,gas plumbers,plumbers & gasfitters gas,plumbers & gasfitters heating,plumbers & gasfitters repairs,plumbers & gasfitters leaks,plumbers & gasfitters sewer"
    },
    {
        "id": 1727,
        "categoryId": 34,
        "subCategoryName": "Plumbing Consultants",
        "icon": null,
        "clues": "Plumbing Consultants,plumbing consultants"
    },
    {
        "id": 1728,
        "categoryId": 34,
        "subCategoryName": "Plumbing Supplies",
        "icon": null,
        "clues": "plumbers' supplies,roof plumbing supplies,plumbing supplies wholesale,gas plumbing supplies,plumbing supplies,hardware--retail plumbing supplies,Plumbing Supplies,plumbing supplies retail,bathroom & plumbing supplies,retail plumbing supplies,plumbing & irrigation supplies,plumbing & bathroom supplies"
    },
    {
        "id": 1729,
        "categoryId": 34,
        "subCategoryName": "Plywood & Veneers",
        "icon": null,
        "clues": "plywood wholesalers,Plywood & Veneers,plywood distributors,plywoods & veneers,plywood sheets,plywood curving,plywood manufacturers,marine plywood,plywood,plywood importers,plywood bending"
    },
    {
        "id": 1742,
        "categoryId": 34,
        "subCategoryName": "Post Hole Contractors & Equipment",
        "icon": null,
        "clues": "post hole drilling,hire post hole diggers,post hole contractors & equipment,post hole diggers hire,post hole,post hole diggers contractors,Post Hole Contractors & Equipment,post hole contractors,post hole boring,excavating & earth moving contractors post hole diggers,post hole diggers"
    },
    {
        "id": 1757,
        "categoryId": 34,
        "subCategoryName": "Power Tools & Repairs",
        "icon": null,
        "clues": "Power Tools & Repairs,power tools--retail,power tools,power tools bandsaw,power tools--retail & repairs bosch,power tools hitachi,power tools--retail & repairs,power tools & machinery,power tools - wholesalers & manufacturers,power tools - wholesalers,hardware--retail power tools"
    },
    {
        "id": 1758,
        "categoryId": 34,
        "subCategoryName": "Power Tools Wholesalers & Manufacturers",
        "icon": null,
        "clues": "power tools--retail,power tools,power tools bandsaw,power tools--retail & repairs bosch,power tools hitachi,power tools--retail & repairs,Power Tools Wholesalers & Manufacturers,hardware--retail power tools,dredging contractors & equipment manufacturers power stations,power tool manufacturers,power tools & machinery,power tools - wholesalers & manufacturers,power tools - wholesalers"
    },
    {
        "id": 1760,
        "categoryId": 34,
        "subCategoryName": "Precast Concrete Panels",
        "icon": null,
        "clues": "precast concrete steps,precast,precast concrete tanks,precast concrete & ready mixed concrete,precast tilt panels,concrete pre-cast panels,precast concrete footings,precast concrete pits,Precast Concrete Panels,precast concrete suppliers,precast concrete panels,precast concrete products,precast concrete pile,precast concrete,precast concrete manufacturers,precast concrete contractors,precast concrete water tanks,precast panels,precast pits"
    },
    {
        "id": 1761,
        "categoryId": 34,
        "subCategoryName": "Precision Engineers",
        "icon": null,
        "clues": "precision engineers,precision machining,precision measuring,Precision Engineers,precision concreting,precision gauges,precision tooling,precision turning,precision grinding"
    },
    {
        "id": 1818,
        "categoryId": 34,
        "subCategoryName": "PVC Windows",
        "icon": null,
        "clues": "pvc windows,pvc door,pvc blinds,pvc curtains,PVC Windows,windows--pvc"
    },
    {
        "id": 1839,
        "categoryId": 34,
        "subCategoryName": "Raised Floors",
        "icon": null,
        "clues": "raised printing,raised storage area,raised floor,raised storage platforms,Raised Floors,raised,raised flooring,floors--raised,raised garden beds,raised access floors"
    },
    {
        "id": 1844,
        "categoryId": 34,
        "subCategoryName": "Ready Mix Concrete",
        "icon": null,
        "clues": "Ready Mix Concrete,ready mixed cement,concrete ready mixed,precast concrete & ready mixed concrete,ready mix cement,readymix concrete,ready mix concrete suppliers,ready mixed concrete,ready mixed concrete suppliers"
    },
    {
        "id": 1847,
        "categoryId": 34,
        "subCategoryName": "Real Estate Development",
        "icon": null,
        "clues": "real estate development,market consultants real estate development,Real Estate Development,real estate land development"
    },
    {
        "id": 1878,
        "categoryId": 34,
        "subCategoryName": "Rendering",
        "icon": null,
        "clues": "rendering repair,rendering blockwork,rendering coloured,rendering ceilings,rendering products,rendering polystyrene,cement rendering,rendering,rendering supplies,rendering brickwork,rendering mouldings,Rendering,rendering walls,rendering plasterers,rendering sand and cement,rendering removals,rendering services,rendering cement,rendering material,rendering fencing"
    },
    {
        "id": 1890,
        "categoryId": 34,
        "subCategoryName": "Retaining Walls",
        "icon": null,
        "clues": "retaining walls & fencing,retaining wall design,retaining wall specialist,retaining walls retaining walls,retaining walls concrete,retaining walls,retaining wall engineers,retaining wall construction,retaining wall blocks,retaining wall contractors,landscape supplies retaining walls,retaining wall builders,retaining walls limestone,retaining,landscape contractors & designers retaining walls,retaining walls blocks,Retaining Walls,retaining walls repairs,retaining blocks,retaining wall supplies"
    },
    {
        "id": 1896,
        "categoryId": 34,
        "subCategoryName": "Rigging Services & Erection Contractors",
        "icon": null,
        "clues": "rigging courses,rigging,rigging tools,rigging services,rigging contractors,rigging & erection,rigging equipment,rigging & erection contractors & services,rigging hire,rigging agencies,Rigging Services & Erection Contractors,rigging training,rigging supplies,rigging & manufacturers"
    },
    {
        "id": 1897,
        "categoryId": 34,
        "subCategoryName": "Rising Damp - Repairs & Waterproofing",
        "icon": null,
        "clues": "stain removal rising damp,Rising Damp - Repairs & Waterproofing,rising damp - repairs & waterproofing,rising damp,rising damp - repairs,water damage rising damp repairs,rising damp specialist,flood damage rising damp repairs"
    },
    {
        "id": 1899,
        "categoryId": 34,
        "subCategoryName": "Rivets & Riveting Equipment",
        "icon": null,
        "clues": "rivets & riveting equipment,rivets,blind rivets,pop rivets,Rivets & Riveting Equipment"
    },
    {
        "id": 1900,
        "categoryId": 34,
        "subCategoryName": "Road Building Machinery",
        "icon": null,
        "clues": "road building machinery,Road Building Machinery,road building"
    },
    {
        "id": 1901,
        "categoryId": 34,
        "subCategoryName": "Road Signs & Line Marking",
        "icon": null,
        "clues": "glass bead for road marking,road line,road &/or line marking,road marking,road marking glass beads,road marking paint,road traffic signs,road signs,road safety signs,road marking line,Road Signs & Line Marking,road & linemarking"
    },
    {
        "id": 1911,
        "categoryId": 34,
        "subCategoryName": "Roof Restoration & Repairs",
        "icon": null,
        "clues": "Roof Restoration & Repairs,roof restoration"
    },
    {
        "id": 1912,
        "categoryId": 34,
        "subCategoryName": "Roof Tiles",
        "icon": null,
        "clues": "roof tiles,Roof Tiles,second hand roof tiles,roof tiles suppliers,tiles roof"
    },
    {
        "id": 1913,
        "categoryId": 34,
        "subCategoryName": "Roof Trusses & Wall Frames",
        "icon": null,
        "clues": "roof frame & trusses,roof trusses & wall frames,roof trusses & wall frames manufacturers,steel roof trusses & wall frames,roof trusses & wall frames steel,roof trusses,roof trusses & wall frames timber,roof trusses & frames,timber roof trusses,roof trusses & frames supply installation,steel roof trusses,wall frame & roof trusses,Roof Trusses & Wall Frames"
    },
    {
        "id": 1914,
        "categoryId": 34,
        "subCategoryName": "Roofing Construction & Services",
        "icon": null,
        "clues": "awnings carports roofing services,roofing services,Roofing Construction & Services,awnings carports patios pergolas roofing services"
    },
    {
        "id": 1915,
        "categoryId": 34,
        "subCategoryName": "Roofing Materials",
        "icon": null,
        "clues": "metal roofing,metal roofing materials,Roofing Materials,roofing products,polycarbonate roofing supplies,sheet metal workers roofing materials,colorbond roofing,roofing supplies,roofing,roofing shingles,roofing materials sheet metal,roofing installers,roofing plasterers,roofing materials colorbond,roofing services,roofing materials,roofing manufacturers"
    },
    {
        "id": 1978,
        "categoryId": 34,
        "subCategoryName": "Sealing Compounds & Services",
        "icon": null,
        "clues": "sealing services,sealing compounds & services,Sealing Compounds & Services,sealing compounds"
    },
    {
        "id": 2000,
        "categoryId": 34,
        "subCategoryName": "Septic Tank Cleaning",
        "icon": null,
        "clues": "septic tanks,septic tank services,septic tank repair,septic tank pumping,grease trap & septic tank cleaning,septic tank manufacturers,septic tank removals,septic tank maintenance,septic tank manufacturers & installation services,septic tank cleaning,Septic Tank Cleaning,septic tank pump out,septic cleaning,septic tank cleaning services,septic tank suppliers,septic tank cleaning services liquid waste collection,septic cleaning services,septic tank installation,septic tanks cleaning services"
    },
    {
        "id": 2001,
        "categoryId": 34,
        "subCategoryName": "Septic Tank Installation & Manufacturers",
        "icon": null,
        "clues": "septic tanks,septic tank services,septic tank repair,septic tank pumping,septic tank manufacturers,septic tank removals,septic tank maintenance,Septic Tank Installation & Manufacturers,septic tank manufacturers & installation services,septic tank pump out,septic tank cleaning services,septic tank suppliers,septic installation,septic tank cleaning services liquid waste collection,septic tank installation"
    },
    {
        "id": 2032,
        "categoryId": 34,
        "subCategoryName": "Ship Builders & Repairers",
        "icon": null,
        "clues": "Ship Builders & Repairers,ship builders,ship & boat builders,ship builders & repairers,boat & ship builders,ship builders & repair"
    },
    {
        "id": 2040,
        "categoryId": 34,
        "subCategoryName": "Shop Fittings & Office Fit Outs",
        "icon": null,
        "clues": "shop & office fitting shelving systems,shop & office fitting,shop fittings clothing,shop & office fitting design,retail shop fittings,shop & office fitouts,shop & office fitting second hand,office & shop cleaning services,shop fitting supplies,shop & office fitting racks,office & shop fitting,shop fitters sunglasses,shop fitters real estate,Shop Fittings & Office Fit Outs,used shop fittings,shop & office fitting tattslotto agencies,shop & office fitting joinery,shop fittings second hand,shop & office fitting partitions,shop fitouts supplies,shop fitter & display companies,custom shop & office fit out,shop fitouts,shop fittings,shop & office fitting cabinet making,shop & office fitting post offices,shop & office fitting mannequins,shop fittings used,second hand shop fittings,shop fitter for hair salon,shop & office fitting installation"
    },
    {
        "id": 2063,
        "categoryId": 34,
        "subCategoryName": "Slashing Contractors",
        "icon": null,
        "clues": "slashing contractors,Slashing Contractors,slashing,slashing contractors grass cutting,grass slashing"
    },
    {
        "id": 2064,
        "categoryId": 34,
        "subCategoryName": "Slate",
        "icon": null,
        "clues": "slate & slate products,slate roofing,slate flooring,slate cleaning,Slate,slate,slate sealing"
    },
    {
        "id": 2065,
        "categoryId": 34,
        "subCategoryName": "Slaters & Roof Tilers",
        "icon": null,
        "clues": "roof slaters,Slaters & Roof Tilers,tilers & slaters--roof,slaters"
    },
    {
        "id": 2084,
        "categoryId": 34,
        "subCategoryName": "Solid Plastering",
        "icon": null,
        "clues": "solid surface,solid plasterers,solid timber flooring,Solid Plastering,solid plaster,solid surface countertop"
    },
    {
        "id": 2105,
        "categoryId": 34,
        "subCategoryName": "Sports Grounds Construction",
        "icon": null,
        "clues": "sports grounds,sports construction maintenance,sports centres &/or grounds indoor,lighting for sports grounds,sports centres &/or grounds stadium,sports centres &/or grounds,sports grounds construction,Sports Grounds Construction"
    },
    {
        "id": 2113,
        "categoryId": 34,
        "subCategoryName": "Spray Painting",
        "icon": null,
        "clues": "spray painting supplies,spray painting services protective coatings,auto spray painting,spray painting services,spray painting services furniture,spray painting training,spray painting services two pack,spray painting equipment,car spray painting,spray painting supplies & equipment,panel beaters & painters spray painting,spray painting services industrial,Spray Painting,spray painting,spray painting cars,spray painting booth"
    },
    {
        "id": 2114,
        "categoryId": 34,
        "subCategoryName": "Spray Painting Equipment & Supplies",
        "icon": null,
        "clues": "agricultural spray equipment,spray painting training,panel beaters & painters spray painting,spray painting services industrial,spray booth equipment,airless spray equipment,spray painting booth,spray tan equipment,spray painting supplies,spray painting services protective coatings,auto spray painting,spray painting services,spray painting services furniture,spray painting services two pack,paint spray equipment,Spray Painting Equipment & Supplies,spray equipment,spray booth booth equipment,lawn & turf supplies spray-on lawn,spray painting equipment,car spray painting,spray painting supplies & equipment,beauty salon spray tan supplies,spray painting cars"
    },
    {
        "id": 2116,
        "categoryId": 34,
        "subCategoryName": "Spraying Equipment & Supplies",
        "icon": null,
        "clues": "spraying equipment & supplies,weed spraying equipment,spraying equipment & supplies booth,Spraying Equipment & Supplies"
    },
    {
        "id": 2119,
        "categoryId": 34,
        "subCategoryName": "Sprockets",
        "icon": null,
        "clues": "sprockets,Sprockets,chains & sprockets"
    },
    {
        "id": 2123,
        "categoryId": 34,
        "subCategoryName": "Stained Glass",
        "icon": null,
        "clues": "Stained Glass,stained glass supplies,stained glass,stained glass windows"
    },
    {
        "id": 2135,
        "categoryId": 34,
        "subCategoryName": "Steeplejacks",
        "icon": null,
        "clues": "steeplejacks,Steeplejacks,steeplejacks graffiti removal,graffiti cleaning steeplejacks"
    },
    {
        "id": 2143,
        "categoryId": 34,
        "subCategoryName": "Stone Polishing & Working Machinery",
        "icon": null,
        "clues": "stone polishing,Stone Polishing & Working Machinery,stone polishing & working machinery"
    },
    {
        "id": 2144,
        "categoryId": 34,
        "subCategoryName": "Stonemasons & Stonework",
        "icon": null,
        "clues": "monumental stonemasons,stonemasons supplies,stonemasons headstones,stonemasons grave & headstones,memorial stonemasons,builders stonemasons,stonemasons tools,Stonemasons & Stonework,stonemasons & stonework,tuckpointing stonemasons"
    },
    {
        "id": 2172,
        "categoryId": 34,
        "subCategoryName": "Swimming Pool Designs & Construction",
        "icon": null,
        "clues": "concrete swimming pool construction,swimming pool equipment & chemicals,swimming pool services,swimming pool maintenance,swimming pool maintenance & repair,swimming pool construction,inground swimming pool construction,swimming pool chemicals,swimming pool fencing contractors,Swimming Pool Designs & Construction,swimming pool covers,swimming pool shop,fibreglass swimming pool construction,swimming pool equipment,swimming pool builders,above ground swimming pool construction,Swimming Pool Pumps,Accessories & Supplies,aquatic centre,swimming pool supplies,swimming pool construction lighting,swimming centre"
    },
    {
        "id": 2187,
        "categoryId": 34,
        "subCategoryName": "Tank Calibration",
        "icon": null,
        "clues": "tanks & tank equipment repairs,tanks & tank equipment water,Tank Calibration,tanks & tank equipment,tanks & tank equipment rainwater,tank calibration"
    },
    {
        "id": 2188,
        "categoryId": 34,
        "subCategoryName": "Tank Cleaning",
        "icon": null,
        "clues": "tanks & tank equipment water,tank cleaning,fuel tank cleaning,tanks & tank equipment,Tank Cleaning,tanks & tank equipment repairs,rainwater tank cleaning,oil tank cleaning,tanks & tank equipment rainwater,septic tank cleaning services,septic tank cleaning services liquid waste collection,water tank cleaning"
    },
    {
        "id": 2189,
        "categoryId": 34,
        "subCategoryName": "Tanker Cleaning--Marine or Industrial",
        "icon": null,
        "clues": "tanker cleaning--marine or industrial,road tanker,fuel tanker,recycled water tanker,water tanker hire,Tanker Cleaning--Marine or Industrial,fuel tanker sales,tanker wash,petrol tanker,tanker transport,diesel tanker,water tanker"
    },
    {
        "id": 2195,
        "categoryId": 34,
        "subCategoryName": "Tariff &/or Trade Consultants",
        "icon": null,
        "clues": "tariff &/or trade consultants,Tariff &/or Trade Consultants"
    },
    {
        "id": 2229,
        "categoryId": 34,
        "subCategoryName": "Tennis Court Construction & Repairs",
        "icon": null,
        "clues": "tennis construction,sporting goods--retail & repairs table tennis,Tennis Court Construction & Repairs,tennis court resurfacing,tennis court builders,tennis court lighting manufacturers,tennis court surfaces,tennis court accessories,tennis court repair,tennis court supplies,tennis court construction & repairs,tennis court lighting construction,tennis fence construction,sporting goods--retail & repairs tennis,tennis court maintenance,tennis courts,tennis court lighting,tennis court construction & repairs ses,tennis court fencing,tennis court cleaning,clay tennis court construction,tennis court construction & repair,tennis courts for hire,tennis court construction,tennis courts tennis clubs"
    },
    {
        "id": 2232,
        "categoryId": 34,
        "subCategoryName": "Terrazzo",
        "icon": null,
        "clues": "terrazzo partitions,terrazzo tiles,terrazzo,terrazzo repair,terrazzo polishing concrete,driveway sealing terrazzo,terrazzo polishing,Terrazzo,terrazzo restoration,terrazzo floors"
    },
    {
        "id": 2248,
        "categoryId": 34,
        "subCategoryName": "Tiling Equipment & Supplies",
        "icon": null,
        "clues": "tiling supplies,tiling equipment & supplies,Tiling Equipment & Supplies,tiling equipment"
    },
    {
        "id": 2251,
        "categoryId": 34,
        "subCategoryName": "Timber Treatment & Preservation",
        "icon": null,
        "clues": "timber preservation,Timber Treatment & Preservation,timber treatment & preservation,timber treatment"
    },
    {
        "id": 2252,
        "categoryId": 34,
        "subCategoryName": "Timber Wholesale",
        "icon": null,
        "clues": "timber flooring wholesale,wholesale timber merchants,timber - wholesale decking,wholesale timber floors,Timber Wholesale,timber wholesalers,wholesale timber floor,timber - wholesale fencing"
    },
    {
        "id": 2253,
        "categoryId": 34,
        "subCategoryName": "Timber Windows",
        "icon": null,
        "clues": "windows--timber double hung windows,timber doors & windows,Timber Windows,timber windows & doors,timber windows,timber framed windows,timber casement windows"
    },
    {
        "id": 2258,
        "categoryId": 34,
        "subCategoryName": "Titanium",
        "icon": null,
        "clues": "titanium,titanium welding,Titanium"
    },
    {
        "id": 2265,
        "categoryId": 34,
        "subCategoryName": "Tools & Trade Tools",
        "icon": null,
        "clues": "tradesman tools,power tools,air tools,Air Tools Accessories,Supply & Service,hand tools,tools,Tools & Trade Tools,machine tools,tools trade"
    },
    {
        "id": 2282,
        "categoryId": 34,
        "subCategoryName": "Traffic & Road Safety Equipment",
        "icon": null,
        "clues": "road traffic control,traffic management,safety audits traffic surveys,equipment hire traffic,traffic safety equipment,road traffic management,traffic control,safety equipment-road or traffic guard rails,traffic equipment hire,traffic control equipment &/or services,signs--safety & traffic,traffic control equipment & services traffic surveys,traffic control equipment &/or services main road approvals,traffic safety products,traffic control equipment & services,traffic signs,traffic,traffic consultants,Traffic & Road Safety Equipment,safety equipment-road or traffic,traffic control equipment,traffic equipment,traffic engineers,traffic safety,roads & traffic,road traffic signs,safety audits traffic control services,traffic control equipment & services traffic signals"
    },
    {
        "id": 2308,
        "categoryId": 34,
        "subCategoryName": "Tree & Stump Removal Services",
        "icon": null,
        "clues": "tree removal stump grinding,tree felling services,Oils--Eucalyptus,Tea Tree & Essential,tree stump,tree removalists,tree surgeons,tree stump grinding,tree felling & stump removal tree removal,trees,tree felling & stump removal mulching,tree cutting,tree services,Tree & Stump Removal Services,tree felling & stump removals,tree surgery tree removal,tree removals,tree lopping,tree stumpers,tree removal services,tree stump removers,tree felling & stump removal,tree,tree stump removals,tree felling & stump removal site clearing,tree felling,tree felling & stump removal stump grinding,tree felling & stump removal arborists,tree cutting services,tree pruning,tree & stump removalists,tree lopping services"
    },
    {
        "id": 2309,
        "categoryId": 34,
        "subCategoryName": "Tree Surgery",
        "icon": null,
        "clues": "tree surgery tree felling,tree surgery consulting,tree surgery woodchipping,tree surgery pests,tree surgery palms,tree surgery trimming,tree surgery arborists,tree surgery pruning,tree surgery stump removal,Tree Surgery,tree surgery,tree surgery stump grinding,tree surgery transplanting,tree surgery climbing,tree surgery deadwooding,tree surgery inspection reports,tree surgery tree support system,tree surgery root barriers,tree surgery tree injections,tree surgery tree removal"
    },
    {
        "id": 2352,
        "categoryId": 34,
        "subCategoryName": "Upholstery",
        "icon": null,
        "clues": "auto upholstery,upholstery,motor body trimmers upholstery,Upholstery,upholstery fabric,upholstery repair,upholstery cleaning,furniture upholstery,car upholstery,upholstery materials"
    },
    {
        "id": 2353,
        "categoryId": 34,
        "subCategoryName": "Upholstery Supplies",
        "icon": null,
        "clues": "motor body trimmers upholstery,upholstery repair,Upholstery Supplies,car upholstery,auto upholstery,upholstery fabric,upholstery cleaning,furniture upholstery,upholstery supplies,upholstery materials,automotive upholstery supplies"
    },
    {
        "id": 2361,
        "categoryId": 34,
        "subCategoryName": "Valves Equipment & Service",
        "icon": null,
        "clues": "Valves Equipment & Service,valves-equipment & service,valves equipment"
    },
    {
        "id": 2370,
        "categoryId": 34,
        "subCategoryName": "Verandahs",
        "icon": null,
        "clues": "building contractors--alterations,extensions & renovations verandahs,verandahs,Verandahs,bullnose verandahs"
    },
    {
        "id": 2391,
        "categoryId": 34,
        "subCategoryName": "Wall & Floor Tilers",
        "icon": null,
        "clues": "wall & floor tiles retail,tilers wall & floor,wall floor tiles,tiles--wall & floor,wall cladding,tiles--wall & floor bathrooms,wall tiles,tile layers--wall & floor,tiles floor & wall,tiles--wall & floor floors,tiles--wall & floor vinyl,floor & wall tiles,ceramic tiles wall & floor,tile layers--wall,tiles--wall & floor cork,wallpaper,wall & floor tiling,wall floor & border tiles,tiles--wall & floor wholesale,tiles--wall & floor stone,ceramic wall & floor tilers,wall,Wall & Floor Tilers"
    },
    {
        "id": 2392,
        "categoryId": 34,
        "subCategoryName": "Wall Finishes",
        "icon": null,
        "clues": "wall finishes,wall finishes rendering,Wall Finishes"
    },
    {
        "id": 2393,
        "categoryId": 34,
        "subCategoryName": "Wallpapering & Wallcovering Services",
        "icon": null,
        "clues": "wallpapering,Wallpapering & Wallcovering Services,wallpapering & wallcovering services"
    },
    {
        "id": 2394,
        "categoryId": 34,
        "subCategoryName": "Wallpapers & Wallcoverings",
        "icon": null,
        "clues": "wallpapers & wallcoverings wall borders,vinyl wallpapers & wallcoverings,wallpapers & wallcoverings paper,sanderson wallpapers & wallcoverings,wallpapers & wallcoverings fabric,wallpapers & wallcoverings wholesalers,paper wallpapers & wallcoverings,wallpapers & wallcoverings sanderson,Wallpapers & Wallcoverings,wallpapers & wallcoverings designers guild,wallpapers & wallcoverings porter's,wallpapers & wallcoverings murals,romo wallpapers & wallcoverings,wallpapers & wallcoverings vision wallcoverings,fabric wallpapers & wallcoverings,porter's wallpapers & wallcoverings,wallpapers & wallcoverings signature,wallpapers & wallcoverings vinyl,wallpapers & wallcoverings,wallpapers & wallcoverings harlequin"
    },
    {
        "id": 2398,
        "categoryId": 34,
        "subCategoryName": "Washing Machines & Dryer Repairs, Service & Parts",
        "icon": null,
        "clues": "washing machine spare parts,lg washing machine parts,lg washing machine spare parts,hoover washing machine spare parts,washing machines & dryers--repairs,service & parts kleenmaid,Washing Machines & Dryer Repairs,Service & Parts,washing machine & dryer repair,second hand washing machines,washing machines & dryers--repairs,service & parts simpson,simpson washing machine parts,indesit washing machine spare parts,hoover washing machine parts,washing machines & dryers,simpson washing machine spare parts,second hand washing machine parts,washing machine parts,washing machines & dryers--repairs,service & parts samsung,washing machines & dryers--repairs,service & parts,washing machines & dryers - retail,washing machine repair & parts,washing machines & dryers--repairs,service & parts lg,washing machine services,washing machines,washing machine repairs,washing machines & dryers--retail bosch,whirlpool washing machine spare parts,washing machines & dryers--repairs,service & parts whirlpool,ge washing machine parts,washing machines & dryers--repairs,service & parts hoover"
    },
    {
        "id": 2399,
        "categoryId": 34,
        "subCategoryName": "Washing Machines & Dryers - Commercial & Industrial",
        "icon": null,
        "clues": "washing machines & dryers--repairs,service & parts samsung,washing machines & dryers--repairs,service & parts,washing machines & dryers - retail,washing machines & dryers--repairs,service & parts kleenmaid,washing machines & dryers--repairs,service & parts lg,washing machines,Washing Machines & Dryer Repairs,Service & Parts,second hand washing machines,washing machines & dryers--repairs,service & parts simpson,washing machines & dryers--repairs,service & parts bosch,washing machines & dryers--retail bosch,fisher & paykel commercial washing machines,washing machines & dryers--repairs,service & parts whirlpool,washing machines & dryers--repairs,service & parts maytag,commercial washing machines,washing machines & dryers,washing machines & dryers - commercial & industrial,industrial washing machines,Washing Machines & Dryers - Commercial & Industrial,fisher & paykel washing machines & dryers,washing machines & dryers--repairs,service & parts hoover,fisher & paykel industrial washing machines"
    },
    {
        "id": 2420,
        "categoryId": 34,
        "subCategoryName": "Waterproofing Contractors",
        "icon": null,
        "clues": "waterproofing applicator,graffiti resistant waterproofing contractors,graffiti protection waterproofing contractors,Waterproofing Contractors,waterproofing contractors,graffiti coating waterproofing contractors,waterproofing contractors showers,graffiti treatment waterproofing contractors,waterproofing materials,waterproofing supplies,waterproofing"
    },
    {
        "id": 2421,
        "categoryId": 34,
        "subCategoryName": "Waterproofing Products",
        "icon": null,
        "clues": "waterproofing applicator,Waterproofing Products,waterproofing materials,waterproofing supplies,waterproofing products,waterproofing"
    },
    {
        "id": 2439,
        "categoryId": 34,
        "subCategoryName": "Weed Control Services",
        "icon": null,
        "clues": "weed mat,weed spraying equipment,weed spraying,weed management,weed killer,weed eradication,weed control,Weed Control Services,weed control services,weed eaters,weed spraying contractors,weed removals"
    },
    {
        "id": 2440,
        "categoryId": 34,
        "subCategoryName": "Weighbridges",
        "icon": null,
        "clues": "Weighbridges,weighbridges,transport - heavy weighbridges"
    },
    {
        "id": 2442,
        "categoryId": 34,
        "subCategoryName": "Welding",
        "icon": null,
        "clues": ""
    },
    {
        "id": 2443,
        "categoryId": 34,
        "subCategoryName": "Welding Supplies & Equipment",
        "icon": null,
        "clues": "welding equipment & supplies boc,welding equipment hire,plastic welding equipment,welding equipment repair,Welding Supplies & Equipment,welding equipment & supplies,welding supplies,hire welding equipment,welding equipment,welding supplies esab,welding equipment & supplies harris"
    },
    {
        "id": 2446,
        "categoryId": 34,
        "subCategoryName": "Wharf Construction & Repairs",
        "icon": null,
        "clues": "wharf builders,wharf transport,piercing,wharf construction & repair,wharf cartage,wharf,body piercing,Wharf Construction & Repairs,wharf construction & repairs,wharf construction,ear piercing"
    },
    {
        "id": 2466,
        "categoryId": 34,
        "subCategoryName": "Window Operating Equipment",
        "icon": null,
        "clues": "window operating equipment,Window Operating Equipment,security doors,windows & equipment window screens,security doors,windows & equipment window grilles,window cleaning equipment"
    },
    {
        "id": 2467,
        "categoryId": 34,
        "subCategoryName": "Window Repairs",
        "icon": null,
        "clues": "window repairs,Window Repairs,window repairs flood damage,flood damage window repairs,water damage window repairs"
    },
    {
        "id": 2468,
        "categoryId": 34,
        "subCategoryName": "Window Roller Shutters",
        "icon": null,
        "clues": "wooden window shutters,Window Roller Shutters,window shutters,aluminium window shutters,window roller shutters,window security shutters,window rollers,timber window shutters,automatic window roller shutter wholesalers"
    },
    {
        "id": 2469,
        "categoryId": 34,
        "subCategoryName": "Window Tinting",
        "icon": null,
        "clues": "Window Tinting,window tinting,window tinting commercial,window tinting residential,window tinting car,window tinting industrial,car window tinting"
    },
    {
        "id": 2470,
        "categoryId": 34,
        "subCategoryName": "Windows--Steel",
        "icon": null,
        "clues": "windows--steel,security doors,windows & equipment stainless steel mesh,Windows--Steel,steel frame windows,steel windows,steel framed windows"
    },
    {
        "id": 2484,
        "categoryId": 34,
        "subCategoryName": "Wood & Plastic Moulding",
        "icon": null,
        "clues": "mouldings--plastic & wood,wood flooring,Rollers--Wooden,Metal,Rubber Etc.,wood mouldings,woodfired pizza restaurant,wooden floors,woodworking,wood heater,Wood & Plastic Moulding,wood"
    },
    {
        "id": 2486,
        "categoryId": 34,
        "subCategoryName": "Wood Chippers",
        "icon": null,
        "clues": "Wood Chippers,wood chippers hire,wood chippers"
    },
    {
        "id": 2491,
        "categoryId": 34,
        "subCategoryName": "Woodworking Machinery",
        "icon": null,
        "clues": "woodworking factory,woodworking tools,woodworking power tools,woodworking machines,woodworking supplies,secondhand woodworking machinery,woodworking,woodworking machinery & service,Woodworking Machinery,woodworking tools & machinery,woodworking equipment"
    }
    ]
},
{
    "id": 35,
    "icon": null,
    "subCategories": [{
    "id": 43,
    "categoryId": 35,
    "subCategoryName": "Air Cargo Services",
    "icon": null,
    "clues": "air cargo services tnt express,air services,air cargo services,air cargo agents,Air Cargo Services,air cargo services australian air express,air cargo,air conditioning services,air cargo services freight,air cargo services dhl"
},
    {
        "id": 53,
        "categoryId": 35,
        "subCategoryName": "Aircraft Charter & Hire",
        "icon": null,
        "clues": "aircraft equipment & maintenance,aircraft freight charter,aircraft charter & rental services,aircraft,aircraft charter & rental,aircraft charter,charter aircraft,Aircraft Charter & Hire,aircraft hire,aircraft charter services,aircraft maintenance,aircraft charter & rental services car parking"
    },
    {
        "id": 54,
        "categoryId": 35,
        "subCategoryName": "Aircraft Maintenance & Equipment",
        "icon": null,
        "clues": "aircraft equipment & maintenance,aircraft charter & rental services,aircraft,maintenance aircraft,aircraft equipment,aircraft maintenance facilities,aircraft charter,Aircraft Maintenance & Equipment,aircraft maintenance jobs,aircraft maintenance engineers,aircraft maintenance"
    },
    {
        "id": 55,
        "categoryId": 35,
        "subCategoryName": "Aircraft Manufacturers & Distributors",
        "icon": null,
        "clues": "aircraft equipment & maintenance,aircraft distributors & manufacturers,aircraft manufacturers,aircraft charter & rental services,aircraft,Aircraft Manufacturers & Distributors,aircraft distributors,aircraft manufacturers & distributors,aircraft maintenance,aircraft charter"
    },
    {
        "id": 56,
        "categoryId": 35,
        "subCategoryName": "Airlines & Travel Agents",
        "icon": null,
        "clues": "airlines thai,airlines agents,airlines,Airlines & Travel Agents,airlines & airline agents"
    },
    {
        "id": 57,
        "categoryId": 35,
        "subCategoryName": "Airport Bus & Shuttles",
        "icon": null,
        "clues": "airport bus hire,Airport Bus & Shuttles,airport shuttle bus services,airport bus shuttle,airport bus services,bus & coach charter & tours airport transfers,bus airport,airport shuttle bus,bus & coach scheduled services airport transfers,shuttle bus airport,airport transfer bus,airport bus,airport bus transfers"
    },
    {
        "id": 58,
        "categoryId": 35,
        "subCategoryName": "Airports &/or Airport Services",
        "icon": null,
        "clues": "airports &/or airport services car parking,Airports &/or Airport Services,hotels-accommodation airports,bus & coach scheduled services airports,car parking airports,parking stations airports,airports &/or airport services"
    },
    {
        "id": 133,
        "categoryId": 35,
        "subCategoryName": "Aviation Consultants & Services",
        "icon": null,
        "clues": "aviation insurance,aviation services,aviation consultants & services,aviation consultants,aviation equipment,aviation charter,aviation,aviation training,aviation tyre services,aviation engineers,aviation doctor,aviation school,aviation supplies,aviation maintenance,aviation museum,aviation fuel,aviation theory,safety audits aviation consultants,aviation medical,Aviation Consultants & Services"
    },
    {
        "id": 160,
        "categoryId": 35,
        "subCategoryName": "Barges",
        "icon": null,
        "clues": "barges,Barges"
    },
    {
        "id": 219,
        "categoryId": 35,
        "subCategoryName": "Boat Cruises & Hire",
        "icon": null,
        "clues": "boat cruises,boat hire--drive yourself,fishing boat hire,party boat cruises,boat hire self drive,river boat cruises,Boat Cruises & Hire,boat hire cruises"
    },
    {
        "id": 220,
        "categoryId": 35,
        "subCategoryName": "Boat Hire",
        "icon": null,
        "clues": "boat hire,boat hire--drive yourself,fishing boat hire,boat hire self drive,Boat Hire"
    },
    {
        "id": 222,
        "categoryId": 35,
        "subCategoryName": "Boat Motors & Outboards",
        "icon": null,
        "clues": "spare parts boat motors,Boat Motors & Outboards,boat motors,yamaha boat motors,boat & yacht equipment outboard motors,outboard boat motors"
    },
    {
        "id": 326,
        "categoryId": 35,
        "subCategoryName": "Camper Trailers & Caravan Hire",
        "icon": null,
        "clues": "camper trailer hire,caravan & camper trailer hire,caravan camper hire,hire camper,campervans hire,campervans & motor homes--hire,caravans & camper-trailers--hire,camper hire,Camper Trailers & Caravan Hire,camper trailers for hire,hire camper trailers"
    },
    {
        "id": 327,
        "categoryId": 35,
        "subCategoryName": "Camper Trailers & Caravans",
        "icon": null,
        "clues": "caravan & camper trailers manufacturers,caravans & camper-trailers &/or equipment & supplies used,camper trailers,caravan & camper sales,caravans & camper-trailers &/or equipment & supplies annexes,cub camper trailers,caravans & camper-trailers &/or equipment & supplies,caravans & camper-trailers &/or equipment & supplies box trailers,caravan & camper-trailer sales,caravans & camper-trailers &/or equipment & supplies caravans,off road camper trailers,caravans & camper-trailers &/or equipment & supplies jayco,caravans & camper-trailers &/or equipment & supplies new,caravans & camper-trailers--repairs & servicing,caravan & camper trailers equipment,Camper Trailers & Caravans,caravan & camper trailers"
    },
    {
        "id": 328,
        "categoryId": 35,
        "subCategoryName": "Campervans & Motor Homes",
        "icon": null,
        "clues": "campervans parks,campervans sales,campervans & motor homes--hire,campervans hire,campervans accessories,campervans for sales,campervans & motor homes,campervans equipment,campervans rental,campervans dealers,campervans manufacturers,campervans conversions,campervans repair,campervans insurance,Campervans & Motor Homes,campervans fitters,campervans,campervans canvas repair,campervans for hire"
    },
    {
        "id": 360,
        "categoryId": 35,
        "subCategoryName": "Car Transport",
        "icon": null,
        "clues": "transport - car motorcycles,transport - car interstate,car transport interstate,car transporting rail,Car Transport,interstate car transport,carriers--car transporting transport insurance,car transport carriers,car transport services,car transportation,carriers--car transporting,carriers--car transporting motorcycles,transport - car transport insurance,transport car,car transport,car transportation interstate"
    },
    {
        "id": 471,
        "categoryId": 35,
        "subCategoryName": "Coach & Carriage Builders & Dealers",
        "icon": null,
        "clues": "coach & carriage builders & dealers,bus & coach builders,Coach & Carriage Builders & Dealers,coach builders"
    },
    {
        "id": 832,
        "categoryId": 35,
        "subCategoryName": "Farriers",
        "icon": null,
        "clues": "Farriers,horse farriers,master farriers,farriers tools,Blacksmiths &/or Farriers,farriers supplies,blacksmiths &/or farriers,blacksmiths & farriers,farriers"
    },
    {
        "id": 847,
        "categoryId": 35,
        "subCategoryName": "Ferry",
        "icon": null,
        "clues": "Ferry,ferry,ferry services"
    },
    {
        "id": 933,
        "categoryId": 35,
        "subCategoryName": "Freight & Cargo Services",
        "icon": null,
        "clues": "freight carriers,cargo & freight containers &/or services,cargo & freight containers &/or services container hire,cargo & freight container & services,freight services,cargo & freight,freight couriers services,freight,air freight,freight forwarding,cargo & freight containers &/or services 20' sea,interstate freight,interstate freight services,Freight & Cargo Services,international freight services,air freight services,international freight,road freight services,cargo & freight container,cargo & freight container suppliers,road freight,air cargo services freight"
    },
    {
        "id": 934,
        "categoryId": 35,
        "subCategoryName": "Freight & Transport Companies",
        "icon": null,
        "clues": "sea freight transport,interstate freight transport services,freight transport logistics,freight transport haulage,transport & forwarding agents freight forwarding,transport services freight forwarding,general freight interstate transport,transport sensitive freight,interstate freight transport,freight transport,transport freight haulage,road freight transport,transport general freight,transport road freight,transport freight,general freight transport,transport freight cartage,sensitive freight transport,Freight & Transport Companies"
    },
    {
        "id": 965,
        "categoryId": 35,
        "subCategoryName": "Furniture Removalists & Movers",
        "icon": null,
        "clues": "Furniture Removalists & Movers,office furniture removalists,furniture removalists,furniture removalists western australia,removalists furniture,furniture removalists interstate,furniture movers,taxi truck furniture removalists,local furniture removalists,flood damage furniture removalists,furniture removalists international,water damage furniture removalists,furniture removalists & storage"
    },
    {
        "id": 1071,
        "categoryId": 35,
        "subCategoryName": "Hand Trolleys & Trucks",
        "icon": null,
        "clues": "second hand trucks,hand trolleys,trolleys & trucks--hand,Hand Trolleys & Trucks,hand tracks convertible trolleys"
    },
    {
        "id": 1100,
        "categoryId": 35,
        "subCategoryName": "Heavy Haulage",
        "icon": null,
        "clues": "heavy transport,heavy haulage contractors,heavy haulage towing,carriers--heavy,heavy haulage operator,heavy haulage carriers,Heavy Haulage,heavy carriers,heavy haulage,transport - heavy containers,heavy haulage trucks crane,heavy haulage transport,od heavy haulage"
    },
    {
        "id": 1101,
        "categoryId": 35,
        "subCategoryName": "Helicopters",
        "icon": null,
        "clues": "helicopters mustering,Helicopters,helicopters lessons,helicopters sales,helicopters hire,helicopters surveys,helicopters operator,helicopters spraying,helicopters,helicopters rides,helicopters flights,helicopters tours,helicopters training,helicopters school,helicopters rescue,helicopters charter,helicopters joy flights"
    },
    {
        "id": 1490,
        "categoryId": 35,
        "subCategoryName": "Motorhomes & Campervans Hire",
        "icon": null,
        "clues": "Motorhomes & Campervans Hire,car parking motorhomes"
    },
    {
        "id": 1491,
        "categoryId": 35,
        "subCategoryName": "Motorised Scooters",
        "icon": null,
        "clues": "scooters-motorised,Motorised Scooters,motorised scooters"
    },
    {
        "id": 1834,
        "categoryId": 35,
        "subCategoryName": "Railway & Tramway Rolling Stock",
        "icon": null,
        "clues": "railway & tramway rolling stock,Railway & Tramway Rolling Stock"
    },
    {
        "id": 1836,
        "categoryId": 35,
        "subCategoryName": "Railway Construction, Equipment & Materials",
        "icon": null,
        "clues": "railway equipment,railway construction equipment,railway construction,Railway Construction,Equipment & Materials,railway construction,equipment & materials"
    },
    {
        "id": 1837,
        "categoryId": 35,
        "subCategoryName": "Railway Services",
        "icon": null,
        "clues": "railway services,Railway Services"
    },
    {
        "id": 1863,
        "categoryId": 35,
        "subCategoryName": "Refrigerated Transport Services",
        "icon": null,
        "clues": "refrigerated transportation,refrigerated transport vehicles,transport - heavy refrigerated,refrigerated vans,Refrigerated Transport Services,refrigerated trucks,refrigerated freight,refrigerated couriers,refrigerated transport,refrigerated transport services,refrigerated transport trucks,refrigerated transport trucks & equipment,refrigerated transport hire,transport refrigerated,interstate refrigerated transport,refrigerated transport trucks & equipment food vending bodies,courier services refrigerated transport,refrigerated transport equipment"
    },
    {
        "id": 1864,
        "categoryId": 35,
        "subCategoryName": "Refrigerated Transport Vans & Trucks",
        "icon": null,
        "clues": "refrigerated transportation,refrigerated transport vehicles,transport - heavy refrigerated,refrigerated vans,refrigerated trucks,refrigerated vans hire,refrigerated vans rental,refrigerated transport,refrigerated transport trucks,refrigerated transport trucks & equipment,refrigerated transport hire,transport refrigerated,interstate refrigerated transport,Refrigerated Transport Vans & Trucks,refrigerated transport trucks & equipment food vending bodies,courier services refrigerated transport,refrigerated transport equipment"
    },
    {
        "id": 1902,
        "categoryId": 35,
        "subCategoryName": "Road Transport Vehicles &/or Equipment",
        "icon": null,
        "clues": "road transport vehicles &/or equipment,road transport services,road transport carriers,road transport,road transport trucking companies,transport services road trains,off road vehicles,vehicles--off-road,Road Transport Vehicles &/or Equipment,road transport operator,road transport vehicles,road transport vehicles & equipment"
    },
    {
        "id": 1920,
        "categoryId": 35,
        "subCategoryName": "Rubber &/or Metal Stamp Makers' Equipment & Supplies",
        "icon": null,
        "clues": "rubber manufacturers,Rollers--Wooden,Metal,Rubber Etc.,rubber stamp maker,rubber supplies,rubber flooring,crafts--retail & supplies rubber stamps,rubber matting,rubber stamps,foam rubber,rubber &/or metal stamp makers' equipment & supplies,rubber stamp manufacturers,rubber stamp supplies,Rubber &/or Metal Stamp Makers' Equipment & Supplies,safety equipment & accessories rubber,rubber"
    },
    {
        "id": 1956,
        "categoryId": 35,
        "subCategoryName": "Scheduled Coach & Bus Services",
        "icon": null,
        "clues": "Scheduled Coach & Bus Services,bus & coach scheduled services greyhound,bus & coach scheduled services airport transfers,scheduled services,bus & coach scheduled services airports,bus & coach scheduled services"
    },
    {
        "id": 2031,
        "categoryId": 35,
        "subCategoryName": "Ship Brokers, Charters & Consultants",
        "icon": null,
        "clues": "ship brokers,charters & consultants fishing,ship brokers chartering services,ship brokers,charters & consultants bulk handling equipment,ship brokers,charters & consultants customs clearance,ship brokers,charters & consultants,ship brokers,charters & consultants charters,ship brokers,charters & consultants cargo ships,Ship Brokers,Charters & Consultants,ship brokers,charters & consultants insurance"
    },
    {
        "id": 2035,
        "categoryId": 35,
        "subCategoryName": "Shipping Agents & Freight Companies",
        "icon": null,
        "clues": "shipping container sales,shipping companies & agents international,shipping companies & agents,shipping,shipping container,shipping companies & agents exporting,shipping & removals companies,shipping freight,Shipping Agents & Freight Companies,international shipping agents,freight shipping"
    },
    {
        "id": 2167,
        "categoryId": 35,
        "subCategoryName": "Surveyors--Cargo & Marine",
        "icon": null,
        "clues": "surveyors--cargo & marine,Surveyors--Cargo & Marine"
    },
    {
        "id": 2202,
        "categoryId": 35,
        "subCategoryName": "Taxi",
        "icon": null,
        "clues": "taxi services,taxidermist,taxi,Taxi,taxi truck services,taxi trucks,taxi cabs maxi taxis,maxi taxi,taxi cabs"
    },
    {
        "id": 2203,
        "categoryId": 35,
        "subCategoryName": "Taxi Truck",
        "icon": null,
        "clues": "taxi services,taxi truck hire,taxi truck services,taxi trucks,Taxi Truck,taxi cabs,taxi truck removals,taxi truck furniture removalists,taxidermist,truck taxi,taxi cabs maxi taxis,maxi taxi"
    },
    {
        "id": 2257,
        "categoryId": 35,
        "subCategoryName": "Tipper Truck Contractors",
        "icon": null,
        "clues": "Tipper Truck Contractors,truck tipper hire,tipper truck body builders,truck tipper bodies"
    },
    {
        "id": 2283,
        "categoryId": 35,
        "subCategoryName": "Traffic Control Services & Equipment",
        "icon": null,
        "clues": "traffic control training,traffic control services,traffic control equipment,safety equipment-road or traffic,traffic services,traffic equipment,equipment hire traffic,traffic safety equipment,traffic control,safety audits traffic control services,safety equipment-road or traffic guard rails,traffic control equipment &/or services,traffic equipment hire,Traffic Control Services & Equipment,traffic control equipment & services traffic signals,traffic control equipment & services traffic surveys,traffic control equipment &/or services main road approvals,traffic control equipment & services"
    },
    {
        "id": 2287,
        "categoryId": 35,
        "subCategoryName": "Trailer Hire",
        "icon": null,
        "clues": "car trailer hire,trailer hire furniture trailers,camper trailer hire,trailer hire box trailers,trailer hire horse floats,trailer hire car trailers,trailer hire motorbike trailers,Trailer Hire,trailer hire semi trailers,trailer hire tandem trailers,trailer hire"
    },
    {
        "id": 2288,
        "categoryId": 35,
        "subCategoryName": "Trailers & Trailer Parts",
        "icon": null,
        "clues": "Trailers & Trailer Parts,car trailers,box trailers,camper trailers,trailer renting car trailers,trailers & trailer equipment boat,trailers & trailer equipment truck trailers,trailers & trailer equipment tandem,trailers & equipment,trailers & trailer equipment,caravans & camper-trailers &/or equipment & supplies,trailers,trailers & trailer equipment tradesman,caravans & camper-trailers--hire,boat trailers,trailers & trailer equipment parts,boat & yacht trailers"
    },
    {
        "id": 2291,
        "categoryId": 35,
        "subCategoryName": "Tram Stop",
        "icon": null,
        "clues": "trampoline,tram restaurant,tram depot,tram monte accounting services"
    },
    {
        "id": 2297,
        "categoryId": 35,
        "subCategoryName": "Transport - Light",
        "icon": null,
        "clues": "transport - light courier services,transport - car motorcycles,transport - light forklifts,transport - light storage,Transport - Light,transport - car interstate,transport - light fragile freight,transport & forwarding agents freight forwarding,transport - light packing,transport light,transport - heavy containers,transport - light cold storage,transport - light parcels,transport services,transport & forwarding agents,transport - light interstate,transport & forwarding agents rail,transport - light national,transport - light local,transport - light pallets,transport - light trucks,transport - light state,transport,transport light carriers,transport - light furniture,transport - light distribution,transport - light vans,transport - light mail,transport & forwarding agents logistics management,transport & forwarding agents sea"
    },
    {
        "id": 2298,
        "categoryId": 35,
        "subCategoryName": "Transport Companies & Freight Forwarding Agents",
        "icon": null,
        "clues": "sea freight transport,freight transport logistics,transport services freight forwarding,transport freight haulage,road freight transport,general freight transport,transport & forwarding agents hotshot,freight transport haulage,freight transport,transport road freight,Transport Companies & Freight Forwarding Agents,transport freight,national transport companies,transport & forwarding agents logistics management,transport freight cartage,domestic transport companies,interstate transport logistics companies,road transport trucking companies,transport & forwarding agents freight forwarding,general freight interstate transport,transport freight forwarding,transport general freight,transport & forwarding,transport truck companies,loading agents transport,transport services,transport & forwarding agents,transport & forwarding agents rail,transport loading agents,transport agents,transport sensitive freight,interstate freight transport,transport,transport & forwarding agents interstate,transport & forwarding agents international,sensitive freight transport,transport & forwarding agents air,transport & forwarding agents sea"
    },
    {
        "id": 2299,
        "categoryId": 35,
        "subCategoryName": "Transport Depots",
        "icon": null,
        "clues": "Transport Depots,transport,transport - car depots,transport services"
    },
    {
        "id": 2300,
        "categoryId": 35,
        "subCategoryName": "Transport Escort & Pilot Services",
        "icon": null,
        "clues": "transport services logistics,transport services freight forwarding,transport services crane trucks,transport services containers,transport escorts,Transport Escort & Pilot Services,transport services heavy machinery,transport services cars,transport services,transport services side loaders,transport escort & pilot services,transport services interstate,transport services fragile goods,transport,transport pilots,transport services tilt trays,transport services warehouses"
    },
    {
        "id": 2301,
        "categoryId": 35,
        "subCategoryName": "Transportation Consultants",
        "icon": null,
        "clues": "refrigerated transportation,motorcycle transportation,wedding transportation,animal transportation,container transportation,motorbike transportation,boat transportation,pet transportation,transportation,vehicle transportation,interstate transportation,transportation consultants,airport transportation,car transportation,rail transportation,Transportation Consultants,truck transportation,transportation services,transportation limousines"
    },
    {
        "id": 2313,
        "categoryId": 35,
        "subCategoryName": "Truck Hire & Bus Hire",
        "icon": null,
        "clues": "bus & truck repair,hiab truck hire,truck & bus wreckers,truck & bus parts,hiab crane truck hire,truck & bus hire,truck hire rental,crane truck hire,bus & truck rental,removal truck hire,truck & bus rental refrigerated,truck & bus repair,truck hire,truck & bus,Truck Hire & Bus Hire,water truck hire,refrigerated truck hire,truck & bus repairs,skip & truck hire,tip truck hire,truck & bus rental,truck & bus rental utes,car & truck hire"
    },
    {
        "id": 2323,
        "categoryId": 35,
        "subCategoryName": "Tugs",
        "icon": null,
        "clues": "trailer tugs,Tugs,tugs"
    },
    {
        "id": 2407,
        "categoryId": 35,
        "subCategoryName": "Water Cartage",
        "icon": null,
        "clues": "water cartage tanks,domestic water cartage,Water Cartage,water cartage services,recycled water cartage,water cartage,water cartage water tanks,water cartage contractors"
    },
    {
        "id": 2417,
        "categoryId": 35,
        "subCategoryName": "Water Taxis",
        "icon": null,
        "clues": "water taxis,Water Taxis"
    },
    {
        "id": 2428,
        "categoryId": 35,
        "subCategoryName": "Wedding Cars",
        "icon": null,
        "clues": "limousines wedding cars,wedding cars chauffeur driven,wedding cars chauffeurs,classic wedding cars,wedding cars,Wedding Cars"
    }
    ]
},
{
    "id": 36,
    "icon": null,
    "subCategories": [{
    "id": 264,
    "categoryId": 36,
    "subCategoryName": "Bricks",
    "icon": null,
    "clues": "Bricks,second hand bricks"
},
    {
        "id": 747,
        "categoryId": 36,
        "subCategoryName": "Electricity Companies & Retailers",
        "icon": null,
        "clues": "Electricity Companies & Retailers,electricity retail,electricity retailers"
    },
    {
        "id": 748,
        "categoryId": 36,
        "subCategoryName": "Electricity Distributors",
        "icon": null,
        "clues": "electricity distributors,Electricity Distributors"
    },
    {
        "id": 749,
        "categoryId": 36,
        "subCategoryName": "Electricity Suppliers",
        "icon": null,
        "clues": "electricity supply,electricity suppliers,Electricity Suppliers"
    },
    {
        "id": 774,
        "categoryId": 36,
        "subCategoryName": "Energy Management Consultants & Services",
        "icon": null,
        "clues": "energy management consultants & services agl,energy efficiency consultants,energy rating consultants,energy management consultants,Energy Management Consultants & Services,energy management consultants & services,energy management consultants & services energy australia,energy management consultants & services energy audits,energy saving consultants,energy management consultants & services origin energy"
    },
    {
        "id": 989,
        "categoryId": 36,
        "subCategoryName": "Gas, Oil & Fuel Companies",
        "icon": null,
        "clues": "Gas,Oil & Fuel Companies,oil & gas"
    },
    {
        "id": 1340,
        "categoryId": 36,
        "subCategoryName": "LPG & Gas Conversions",
        "icon": null,
        "clues": "lpg gas conversions,lpg conversions,LPG & Gas Conversions,lpg & natural gas conversions - vehicles,lpg conversions installation,lpg vehicle conversions"
    },
    {
        "id": 1341,
        "categoryId": 36,
        "subCategoryName": "LPG & Natural Gas Equipment & Services",
        "icon": null,
        "clues": "lpg & natural gas equipment & services pressure vessels,lpg & natural gas equipment & services design,lpg conversions,lpg & natural gas equipment & services converters,lpg equipment,lpg & natural gas equipment & services lpg ignition leads,lpg & natural gas equipment & services fabrication,lpg gas conversions,lpg & natural gas equipment & services manchester,lpg installers,lpg & natural gas equipment & services gas dispensers,lpg,lpg gas services,lpg & natural gas equipment & services automotive,lpg gas,lpg & natural gas conversions - vehicles,lpg & natural gas equipment & services filters,LPG & Natural Gas Equipment & Services,lpg car services,lpg & natural gas equipment & services testing,lpg & natural gas equipment & services installations,lpg services,lpg & natural gas equipment & services repairs,lpg & natural gas equipment & services"
    },
    {
        "id": 1342,
        "categoryId": 36,
        "subCategoryName": "LPG Gas Suppliers",
        "icon": null,
        "clues": "gas suppliers lpg,lpg gas suppliers,LPG Gas Suppliers,lpg suppliers"
    },
    {
        "id": 2419,
        "categoryId": 36,
        "subCategoryName": "Water Utilities",
        "icon": null,
        "clues": "water utilities,Water Utilities"
    }
    ]
}
]