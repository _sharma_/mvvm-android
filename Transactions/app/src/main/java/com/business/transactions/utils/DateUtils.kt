package com.business.transactions.utils

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.roundToInt

open class DateUtils {

    fun getDateHeaderForTransactions(date: String): String {
        return try {
            val formatter = SimpleDateFormat("yyyy-MM-dd")
            val transactionDate = formatter.parse(date)
            val transactionDateExcludingYear = transactionDate.toString().substring(0, 10)+" - "
            val difference = ((Date().time - transactionDate.time) / 86400000.toDouble()).roundToInt()

            "$transactionDateExcludingYear$difference days ago"
        } catch (e:Exception) {
            date
        }

    }
}