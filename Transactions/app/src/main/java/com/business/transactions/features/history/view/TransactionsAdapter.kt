package com.business.transactions.features.history.view

import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.business.transactions.R
import com.business.transactions.databinding.HeaderBinding
import com.business.transactions.databinding.ViewTransactionRowBinding
import com.business.transactions.features.history.model.Transactions
import com.business.transactions.utils.AdapterUtils
import com.business.transactions.utils.DateUtils
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

private const val ITEM_VIEW_TYPE_HEADER = 0
private const val ITEM_VIEW_TYPE_TRANSACTION = 1

sealed class DataItem {
    data class TransactionRow(val transaction: Transactions) : DataItem()
    data class HeaderRow(val date: String) : DataItem()
}

class TransactionsAdapter(private val clickListener: ((response: Transactions) -> Unit)?) :
    ListAdapter<DataItem, RecyclerView.ViewHolder>(TransactionsComparator()) {

    private val adapterScope = CoroutineScope(Dispatchers.Default)

    fun addHeaderAndSubmitList(adapterList: List<DataItem>) {
        adapterScope.launch {
            withContext(Dispatchers.Main) {
                submitList(adapterList)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is HeaderViewHolder -> {
                val date = getItem(position) as DataItem.HeaderRow
                date.let { holder.bind(it.date) }
            }
            is SingleTransactionViewHolder -> {
                val transaction = getItem(position) as DataItem.TransactionRow
                transaction.let { holder.bind(it.transaction, clickListener) }
            }
        }
    }

    class HeaderViewHolder private constructor(private val binding: HeaderBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(date: String) {
            binding.date.apply {
                text = DateUtils().getDateHeaderForTransactions(date)
                contentDescription =
                    binding.date.context.getString(R.string.cd_transaction_date) + DateUtils().getDateHeaderForTransactions(
                        date
                    )
            }
        }

        companion object {
            fun from(parent: ViewGroup): HeaderViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = HeaderBinding.inflate(layoutInflater, parent, false)
                return HeaderViewHolder(binding)
            }
        }
    }

    class SingleTransactionViewHolder private constructor(private val binding: ViewTransactionRowBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            transaction: Transactions,
            clickListener: ((Transactions) -> Unit)?
        ) = with(binding) {
            val descriptionText = if (transaction.isPending) {
                val str = SpannableStringBuilder(binding.root.context.getString(R.string.pending_text))
                str.setSpan(StyleSpan(Typeface.BOLD), 0, 7, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                str.append(transaction.description).toString()
            } else {
                transaction.description.toString()
            }

            description.apply {
                text = descriptionText
                contentDescription = context.getString(R.string.cd_transaction_description)+descriptionText
            }

            amount.apply {
                text = binding.root.context.getString(
                    R.string.dollar_value,
                    binding.root.context.getString(R.string.dollar_Symbol), transaction.amount
                )
                contentDescription = context.getString(R.string.cd_amount)+ text
            }

            categoryIcon.setImageDrawable(
                transaction.category?.let { AdapterUtils().getIconForCategory(it) }?.let {
                    ContextCompat.getDrawable(
                        categoryIcon.context,
                        it
                    )
                }
            )
            binding.root.setOnClickListener {
                if (clickListener != null) {
                    clickListener(transaction)
                }
            }
        }

        companion object {
            fun from(parent: ViewGroup): SingleTransactionViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ViewTransactionRowBinding.inflate(layoutInflater, parent, false)
                return SingleTransactionViewHolder(binding)
            }
        }
    }

    class TransactionsComparator : DiffUtil.ItemCallback<DataItem>() {
        override fun areItemsTheSame(
            oldItem: DataItem,
            newItem: DataItem
        ): Boolean {
            return false
        }

        override fun areContentsTheSame(
            oldItem: DataItem,
            newItem: DataItem
        ): Boolean {
            return oldItem == newItem
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is DataItem.HeaderRow -> ITEM_VIEW_TYPE_HEADER
            is DataItem.TransactionRow -> ITEM_VIEW_TYPE_TRANSACTION
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            ITEM_VIEW_TYPE_HEADER -> HeaderViewHolder.from(parent)
            ITEM_VIEW_TYPE_TRANSACTION -> SingleTransactionViewHolder.from(parent)
            else -> throw ClassCastException("Unknown viewType $viewType")
        }
    }
}