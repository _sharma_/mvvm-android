package com.business.transactions

import android.app.Application
import com.business.transactions.di.component.ApplicationComponent
import com.business.transactions.di.component.DaggerApplicationComponent


class TransactionsApplication : Application() {

    val applicationComponent: ApplicationComponent by lazy {
        DaggerApplicationComponent.builder().apply {
            context(this@TransactionsApplication)
        }.build()
    }

}