package com.business.transactions.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.business.transactions.TransactionsApplication
import com.business.transactions.di.component.DaggerInstanceComponent
import com.business.transactions.di.component.InstanceComponent
import com.business.transactions.di.component.ViewModelHost
import com.business.transactions.di.module.ViewModelModule


abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val injector = DaggerInstanceComponent.builder().apply {
            applicationComponent((applicationContext as TransactionsApplication).applicationComponent)
            viewModelModule(ViewModelModule)
            host(ViewModelHost.ActivityHost(this@BaseActivity))
        }.build()
        inject(injector)
    }

    abstract fun inject(injector: InstanceComponent)
}
