package com.business.transactions.utils

import com.business.transactions.R

open class AdapterUtils {
    fun getIconForCategory(category: String): Int {
        val iconResource: Int?
        when (category) {
            "business" -> {
                iconResource = R.drawable.icon_category_business
            }
            "entertainment" -> {
                iconResource = R.drawable.icon_category_entertainment
            }
            "shopping" -> {
                iconResource = R.drawable.icon_category_shopping
            }
            "groceries" -> {
                iconResource = R.drawable.icon_category_groceries
            }
            "eatingOut" -> {
                iconResource = R.drawable.icon_category_eating_out
            }
            "transport"-> {
                iconResource = R.drawable.icon_category_transportation
            }
            "cash"-> {
                iconResource = R.drawable.icon_category_cash
            }
            "uncategorised"-> {
                iconResource = R.drawable.icon_category_uncategorised
            }
            else -> {
                iconResource = R.drawable.icon_category_business
            }
        }

        return iconResource
    }
}