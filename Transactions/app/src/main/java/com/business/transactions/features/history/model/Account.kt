package com.business.transactions.features.history.model

data class Account (

	val bsb : String,
	val accountNumber : String,
	val balance : Double,
	val available : Double,
	val accountName : String
)