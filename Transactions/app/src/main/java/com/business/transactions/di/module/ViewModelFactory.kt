package com.business.transactions.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.business.transactions.di.scope.PerInstance
import com.business.transactions.features.history.data.TransactionsService
import com.business.transactions.features.history.data.TransactionsViewModel
import com.business.transactions.utils.ResourceProvider

import javax.inject.Inject

@PerInstance
class ViewModelFactory @Inject constructor(
    private val transactionsService: TransactionsService,
    private val resourceProvider: ResourceProvider
) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when (modelClass) {

            TransactionsViewModel::class.java -> modelClass.getConstructor(
                TransactionsService::class.java,
                ResourceProvider::class.java
            ).newInstance(transactionsService, resourceProvider)


//            SomeOtherVM::class.java -> modelClass.getConstructor(
//               SomeOtherService::class.java,
//                ResourceProvider::class.java
//            ).newInstance(someService,resourceProvider)

            else -> TODO("Missing viewModel $modelClass")
        } as T
    }
}