package com.business.transactions.api

import com.business.transactions.BuildConfig.BASE_URL
import com.business.transactions.features.history.model.TransactionsResponse
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface TransactionsApi {

    //pagination ...can be set here if required
    @GET("s/inyr8o29shntk9w/exercise.json/")
    suspend fun getTransactions(
        @Query("dl") page: Int
    ): TransactionsResponse

    companion object {
        operator fun invoke(): TransactionsApi = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(OkHttpClient.Builder().also { client ->
                val logging = HttpLoggingInterceptor()
                logging.apply { logging.level = HttpLoggingInterceptor.Level.BODY }
                client.addInterceptor(logging)
            }.build())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(TransactionsApi::class.java)
        //cert pinner can be added to avoid MITM attacks.
        //enable logging only on debug builds
    }
}