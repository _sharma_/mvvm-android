package com.business.transactions

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.business.transactions.api.TransactionsApi
import com.business.transactions.features.history.data.TransactionsRepository
import com.business.transactions.features.history.data.TransactionsService
import com.business.transactions.features.history.data.TransactionsViewModel
import com.business.transactions.features.history.data.TransactionsViewState
import com.business.transactions.features.history.model.TransactionsResponse
import com.business.transactions.utils.ResourceProvider
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.Mockito.*
import org.powermock.modules.junit4.PowerMockRunner

@ExperimentalCoroutinesApi
@RunWith(PowerMockRunner::class)
class ViewModelTest {
    private val transactionsApi: TransactionsApi = mock()
    private lateinit var viewModel: TransactionsViewModel
    private var apiResponse: TransactionsResponse = TransactionsResponse(null, null, null)

    @Rule
    @JvmField
    var instantExecutorRule = InstantTaskExecutorRule()

    @Rule
    @JvmField
    val coRoutineTestRule = CoroutineTestRule()

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private val mockStateObserver = mock<Observer<TransactionsViewState>>()

    private val repository: TransactionsRepository = TransactionsRepository(transactionsApi)
    private val service: TransactionsService = TransactionsService(repository)
    var context: Context = mock(Context::class.java)

    private val rp: ResourceProvider = ResourceProvider(context)

    @Before
    fun before() {
        viewModel = TransactionsViewModel(service, rp).apply {
            viewState.observeForever(mockStateObserver)
        }
    }


    @Test
    fun test_FetchTransactions_FromServer_ShowSuccess() {

        runBlockingTest {
            `when`(transactionsApi.getTransactions(1)).thenReturn(
                apiResponse
            )

            viewModel.getTransactions()

            verify(mockStateObserver, times(2)).onChanged(TransactionsViewState.Loading)
            verify(mockStateObserver, times(3)).onChanged(
                TransactionsViewState.Success(ArgumentMatchers.any())
            )
            verifyNoMoreInteractions(mockStateObserver)
        }
    }

    @Test
    fun testThrowErrorOnListFetchFailed() {

        runBlocking {
            val error = RuntimeException()

            `when`(transactionsApi.getTransactions(12348997)).thenThrow(
                error
            )

            viewModel.getTransactions()

            verify(mockStateObserver, times(2)).onChanged(TransactionsViewState.Loading)
            verify(
                mockStateObserver,
                times(3)
            ).onChanged(TransactionsViewState.Error(ArgumentMatchers.any()))
            verifyNoMoreInteractions(mockStateObserver)
        }
    }

    private inline fun <reified T> mock(): T = mock(T::class.java)

}